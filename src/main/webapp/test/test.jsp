<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sistema edcore - Testing</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
        <meta name="description" content="Sistema Escolar - edcore">
        <meta name="author" content="Instituto Tecnológico Superior de Zapopan">	
        <link rel="stylesheet" type="text/css" href="css/cloud-admin.css" >
        <link rel="stylesheet" type="text/css" href="css/themes/default.css" id="skin-switcher" >
        <link rel="stylesheet" type="text/css" href="css/responsive.css" >
        <link rel="stylesheet" type="text/css" href="js/select2/select2.min.css" />
        <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css" >
        <link rel="stylesheet" type="text/css" href="css/edcore/main.css" >
        <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    </head>
    <body>

        <!-- JAVASCRIPTS -->
        <script src="js/jquery/jquery-2.0.3.min.js"></script>
        <script src="bootstrap-dist/js/bootstrap.min.js"></script>
        <script src="js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
        <script src="js/bootbox/bootbox.min.js"></script>
        <script src="js/spin.js"></script>
        <script src="js/jquery-validate/jquery.validate.js"></script>
        <script src="js/jquery-validate/additional-methods.js"></script>
        <script src="js/edcore/main.js"></script>
        <script src="js/select2/select2.min.js"></script>
        <script src="js/script.js"></script>
        <script>
            jQuery(document).ready(function () {
            
            
		$.getJSON('getMessageList?safe_IdUserAccount=5', function(data) {
                    alert(data.length);
			//alert(data[1]['id']); //uncomment this for debug
			//alert (data.item1+" "+data.item2+" "+data.item3); //further debug
			//$('#showdata').html("item1="+data.item1+" item2="+data.item2+" item3="+data.item3+"");
		});
            

            
            });
        </script>
    </body>
</html>
