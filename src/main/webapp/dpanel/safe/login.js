jQuery(document).ready(function () {

    $.getScript("../../js/validate/form.js", function (data, textStatus, jqxhr) {
        $("#email").rules("add", {required: true, maxlength: 100, minlength: 5});
        $("#passwd").rules("add", {required: true, minlength: 3, maxlength: 15});
    });

    $("#btn_login").click(function (event) {
        if ($("#frm").valid()) {
            spInit();
            var email = $('#email').val();
            var passwd = $('#passwd').val();
            var save = $.ajax({url: "../../safe/login", data: "email=" + email + "&passwd=" + passwd+"&panel=DPANEL", method: "POST"});
            save.done(function (msn) {
                spStop();
                switch (msn) {
                    case "CORE0000":
                        window.location = "../index.jsp";
                        break;
                    case "CORE0001":
                        bootbox.alert("ERROR [CORE0001]: No se pudo iniciar sesion!.", function () {});
                        break;
                    case "CORE0002":
                        bootbox.alert("ERROR [CORE0002]: El password es incorrecto!.", function () {});
                        break;
                    case "CORE0003":
                        bootbox.alert("ERROR [CORE0003]: La cuenta esta inactiva!.", function () {});
                        break;
                    case "CORE0004":
                        bootbox.alert("ERROR [CORE0004]: La cuenta no pertenece a este CU!.", function () {});
                        break;
                    case "CORE0006":
                        bootbox.alert("ERROR [CORE0006]: Verifique que este ingresando al panel correspondiente!.", function () {});
                        break;
                    default:
                        bootbox.alert("ERROR DESCONOCIDO [" + msn + "]", function () {});
                        break;
                };
            });
            save.error(function () {
                spStop();
                bootbox.alert("Al parecer se presento un problema de conexi&oacute;n, intente refrescar la pagina. ", function () {
                });
            });
            event.preventDefault();
        }
    });
});