$.getJSON("/escolar/pub/getMessageList?safe_IdAccount=" + safe_IdAccount+"&status=NO LEIDO", function (data) {
    $("#msn_length").html(data.length);
    $("#msn").append("<li class='dropdown-title'><span><i class='fa fa-envelope-o' ></i> " + data.length + " Mensajes</span></li>");
    $.each(data, function (i, value) {
        $("#msn").append("<li><a href='/escolar/spanel/pub/mensajes.jsp'><img src='/escolar/img/user.jpg?safe_IdUser=" + value.fromUser + "' alt='' /><span class='body'><span class='from'>" + value.fromUserName + " " + value.fromUserFirstLast + "</span><span class='message'>" + value.subject + "</span><span class='time'><i class='fa fa-clock-o'></i><span> " + value.date + "</span></span></span></a></li>");
    });
    $("#msn").append("<li class='footer'><a href='/escolar/spanel/pub/mensajes.jsp'>Ver todos los mensajes<i class='fa fa-arrow-circle-right'></i></a></li>");
});


