<div id="sidebar" class="sidebar">
    <div class="sidebar-menu nav-collapse">
        <div class="divide-20"></div>
        <!-- SIDEBAR MENU -->
        <ul>
            <li class="active">
                <a href="index.jsp">
                    <i class="fa fa-tachometer fa-fw"></i>
                    <span class="menu-text">Panel de Control</span>
                    <span class="selected"></span>
                </a>					
            </li>
            <%if(privileges.contains("INS001")){%>
            <li class="has-sub">
                <a href="javascript:;" class="">
                    <i class="fa fa-bookmark-o fa-fw"></i>
                    <span class="menu-text">Inscripciones</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub">
                    <li><a class="" href="inscripciones/expediente.jsp"><span class="sub-menu-text">Expediente</span></a></li>
                </ul>
            </li>
            <%}%>
        </ul>
        <!-- /SIDEBAR MENU -->
    </div>
</div>