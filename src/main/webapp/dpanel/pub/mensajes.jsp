<%@page import="mx.com.edcore.vo.AccountVO"%>
<%@page import="mx.com.edcore.vo.SessionVO"%>
<%
    HttpSession websession;
    websession = request.getSession();
    SessionVO sesion = (SessionVO) websession.getAttribute("session");
    if (sesion != null) {
        AccountVO account = (AccountVO) websession.getAttribute("account");
%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <title>edcore - Escolar</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
        <meta name="description" content="edcore - Escolar">
        <link rel="icon" type="image/png" sizes="32x32" href="../../img/favicon-32x32.png">
        <meta name="author" content="Instituto Tecnologico Superior de Zapopan">	
        <link rel="stylesheet" type="text/css" href="../../lib/cloudAdmin/css/cloud-admin.css" >
        <link rel="stylesheet" type="text/css" href="../../lib/cloudAdmin/css/default.css" id="skin-switcher" >
        <link rel="stylesheet" type="text/css" href="../../lib/cloudAdmin/css/responsive.css" >
        <link rel="stylesheet" type="text/css" href="../../css/edcore.css" >
        <link rel="stylesheet" type="text/css" href="../../font/awesome/css/font-awesome.min.css">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
        <script> var safe_IdUserAccount = <%= account.getId()%>;</script>
        <script> var safe_IdUser = <%= account.getUser() %>;</script>
    </head>
    <body>
        <!-- HEADER -->
        <%@include file="header.jsp" %>
        <!--/HEADER -->
        <!-- PAGE -->
        <section id="page">
            <!-- SIDEBAR -->
            <%@include file="sidebar.jsp" %>
            <!-- /SIDEBAR -->
            <div id="main-content">
                <div class="container">
                    <div class="row">
                        <div id="content" class="col-lg-12">
                            <!-- PAGE HEADER-->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="page-header">
                                        <!-- BREADCRUMBS -->
                                        <ul class="breadcrumb">
                                            <li><i class="fa fa-home"></i><a href="index.jsp">Inicio</a></li>
                                            <li><i class="fa"></i><a href="#">Publicaciones</a></li>
                                            <li>Mensajer�a</li>
                                        </ul>
                                        <!-- /BREADCRUMBS -->
                                        <div class="clearfix">
                                            <h3 class="content-title pull-left">Sistema de mensajer�a</h3>
                                            <span class="date-range pull-right">
                                                <p class="btn-toolbar">
                                                    <button id="btn_lista" class="btn btn-default"><i class="fa fa-print-square-o"></i> Nuevo </button>
                                                    <button id="btn_print" class="btn btn-default" disabled=""><i class="fa fa-print-square-o"></i> Editar</button>
                                                    <button id="btn_print" class="btn btn-default" disabled=""><i class="fa fa-print-square-o"></i> Imprimir</button>
                                                </p>
                                            </span>
                                        </div>
                                        <div class="description"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- /PAGE HEADER -->
                            <!-- DASHBOARD CONTENT -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box border red">
                                        <div class="box-title">
                                            <h4>B�zon</h4>
                                            <div class="tools hidden-xs"></div>
                                        </div>
                                        <div class="box-body">
                                            <div class="item">
                                                <div class="item-header" style="padding: 5px; height: 300px;">
                                                    <div class="row">
                                                        <div class="tabbable">   
                                                            <ul class="nav nav-tabs">
                                                                <li class="active"><a href="#tab_1_1" data-toggle="tab"><i class="fa fa-envelope"></i> Recibidos</a></li>
                                                                <li><a href="#tab_1_2" data-toggle="tab"><i class="fa fa-envelope"></i> Enviados</a></li>
                                                                <li><a href="#tab_1_3" data-toggle="tab"><i class="fa fa-envelope"></i> Eliminados</a></li>
                                                            </ul>
                                                            <div class="tab-content">
                                                                <div class="tab-pane fade in active" id="tab_1_1">
                                                                    <div class="box-body">
                                                            <table id="datatable" cellpadding="0" cellspacing="0" border="0" class="datatable2 table table-striped table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>id</th>
                                                                        <th>fromUser</th>
                                                                        <th>subject</th>
                                                                        <th>date</th>
                                                                        <th>status</th>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>          
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" style="display: block; margin-left: 1.5%; width: 95%" >
                                    <p style="text-align: center">Copyright &copy; edcore 2015 Instituto T�cnologico Superior de Zapopan<br>Todos los derechos reservados.</p>
                                </div>
                            </div>
                            <!-- /DASHBOARD CONTENT -->
                        </div><!-- /CONTENT-->
                    </div>
                </div>
            </div>
        </section>
        <!--/PAGE -->
        <!-- JAVASCRIPTS -->
        <script src="../../lib/jquery/jquery-2.0.3.min.js"></script>
        <script src="../../lib/bootstrap-dist/js/bootstrap.min.js"></script>
        <script src="../../lib/bootbox/bootbox.min.js"></script>
        <script src="../../js/spin.js"></script>
        <script src="../../lib/jquery-validate/jquery.validate.js"></script>
        <script src="../../lib/jquery-validate/additional-methods.js"></script>
        <script src="../../js/edcore/main.js"></script>
        <script src="../../js/script.js"></script>
        <script type="text/javascript" src="../../js/datatables/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/datatables/media/assets/js/datatables.min.js"></script>
        <script type="text/javascript" src="../../js/datatables/extras/TableTools/media/js/TableTools.min.js"></script>
        <script type="text/javascript" src="../../js/datatables/extras/TableTools/media/js/ZeroClipboard.min.js"></script>
        <script src="messages.js"></script>
        <!-- /JAVASCRIPTS -->
    </body>
</html>
<%
    } else {
        response.sendRedirect("error600.jsp");
    }
%>