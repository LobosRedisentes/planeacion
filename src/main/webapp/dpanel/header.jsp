<header class="navbar clearfix navbar-fixed-top" id="header">
    <div class="container">
        <div class="navbar-brand">
            <!-- COMPANY LOGO -->
            <a href="index.jsp">
                <img src="/escolar/img/logo.png" alt="Edcore Logo" class="img-responsive" height="30" width="120">
            </a>
            <!-- /COMPANY LOGO -->
            <!-- SIDEBAR COLLAPSE -->
            <div id="sidebar-collapse" class="sidebar-collapse btn">
                <i class="fa fa-bars" data-icon1="fa fa-bars" data-icon2="fa fa-bars" ></i>
            </div>
            <!-- /SIDEBAR COLLAPSE -->
        </div>
        <!-- BEGIN TOP NAVIGATION MENU -->					
        <ul class="nav navbar-nav pull-right">
            <li class="dropdown" id="header-message" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-envelope" style="color: white" ></i><span class="badge" id="msn_length"></span>
                </a>
                <ul class="dropdown-menu inbox" id="msn"></ul>
            </li>

            <!-- END NOTIFICATION DROPDOWN -->
            <!-- BEGIN USER LOGIN DROPDOWN -->
            <li class="dropdown user" id="header-user">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img alt="" id="photo" />
                    <span class="username">Usuario</span>
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="/escolar/spanel/conf/configuracion.jsp"><i class="fa fa-cog"></i> Configuración</a></li>
                    <li><a href="/escolar/safe/logout?panel=DPANEL"><i class="fa fa-power-off"></i> Cerrar Sesión</a></li>
                </ul>
            </li>
            <!-- END USER LOGIN DROPDOWN -->
        </ul>
        <!-- END TOP NAVIGATION MENU -->
    </div>
</header>