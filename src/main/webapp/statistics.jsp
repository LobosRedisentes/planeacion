<%
response.setHeader("Cache-Control", 
                   "no-store,max-age=0,must-revalidate");
 
org.hibernate.stat.Statistics stats = mx.com.edcore.util.Configure.statistics;
 
//Primero despliego el numero de conexiones que se han pedido a Hibernate
// (No es el numero actual, sino cuantas se han pedido en total)
out.println("Connection count: " + stats.getConnectCount());
out.println("");
 
//Numero de transacciones completadas (falladas y satisfactorias)
out.println("Trx count: " + stats.getTransactionCount());
out.println("");
 
//Numero de transacciones completadas (solo satisfactorias)
out.println("Succ trx count: " + stats.getSuccessfulTransactionCount());
out.println("");
 
// Numero de sesiones que el codigo ha abierto
out.println("Opened sessions: " + stats.getSessionOpenCount());
out.println("");
 
// Numero de sesiones que el codigo ha cerrado
out.println("Closed sessions: " + stats.getSessionCloseCount());
out.println("");
 
// Numero total de queries ejecutados
out.println("No. queries: " + stats.getQueryExecutionCount());
 
%>
