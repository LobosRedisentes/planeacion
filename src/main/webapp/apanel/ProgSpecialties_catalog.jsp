<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.edcore.model.db.DBConnector"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.edcore.model.safe.User"%>
	<%
		HttpSession sesion;
    sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if(user != null){
			ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
			if(privileges.contains("CAT31")){
	%>
<%
	ResultSet rs;
	DBConnector db = new DBConnector();
	PreparedStatement ps;
	Connection con;
	int addOrEdit=Integer.parseInt(request.getParameter("addOrEdit"));
	con=db.open();

  String clave="";
  String claveOf="";
  int claveOfi=0;
  int credits=0;
  String status="";
  int plan=0;
  String abreviation="";
  String name="";

    int specialId=Integer.parseInt(request.getParameter("idPS"));
      if(specialId>0){
        ps = con.prepareStatement("SELECT E.*, PE.prog_IdPlan FROM  ProgPlanSpecialties AS PE, ProgSpecialties AS E where PE.prog_IdSpecialty = E.prog_IdSpecialty and PE.prog_IdPS=?;");
        ps.setInt(1, specialId);
        rs = ps.executeQuery();
    
    while(rs != null && rs.next()){
        
        clave =rs.getString("E.code");
        claveOf =rs.getString("E.officialCode");
        claveOfi =Integer.parseInt(rs.getString("PE.prog_IdPlan"));
        credits =rs.getInt("E.credits");
        status =rs.getString("E.status");
        abreviation =rs.getString("E.abreviation");
        name =rs.getString("E.name");

            }   
        
    }
	
%>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Especialidades</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
	<link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
	
	<link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- DATE RANGE PICKER -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
	<!-- TYPEAHEAD -->
	<link rel="stylesheet" type="text/css" href="../js/typeahead/typeahead.css" />
	<!-- FILE UPLOAD -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-fileupload/bootstrap-fileupload.min.css" />
	<!-- SELECT2 -->
	<link rel="stylesheet" type="text/css" href="../js/select2/select2.min.css" />
	<!-- UNIFORM -->
	<link rel="stylesheet" type="text/css" href="../js/uniform/css/uniform.default.min.css" />
	<!-- JQUERY UPLOAD -->
	<!-- blueimp Gallery styles -->
	<link rel="stylesheet" href="../js/blueimp/gallery/blueimp-gallery.min.css">
	<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload.css">
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload-ui.css">
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	<style>
	 .error-message, label.error {
	    color: #ff0000;
	    margin:0;
	    display: inline;
	    font-size: 1em !important;
	    font-weight:bold;
	 }
	 </style>
	<!-- LIBRERIA EDCORE -->
  <script src="../dhtmlx/libCompiler/dhtmlxcommon.js"></script>
	<script src="../js/action_dhx.js"></script>
	<script src="../js/spin.js"></script>
</head>
<body>
	<!-- HEADER -->
	<%@include file="header.jsp" %>
	<!--/HEADER -->
	<!-- PAGE -->
	<section id="page">
				<!-- SIDEBAR -->
				<%@include file="sidebar.jsp" %>
				<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<!-- STYLER -->
									
									<!-- /STYLER -->
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="index.jsp">Inicio</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Catalogos</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="ProgSpecialties_admin.jsp">Especialidades</a>
										</li>
										<li>Nuevo/Editar</li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										<h3 class="content-title pull-left">Cat�logo - Modulos</h3>
										<span class="date-range pull-right">
										<p class="btn-toolbar">
											<%if(privileges.contains("CAT66")){%>
											<button id="nuevo" class="btn btn-success" ><i class="fa fa-file-text-o"></i> Nuevo</button>
											<%}%>
											<%if(privileges.contains("CAT67")){%>
											<button id="editar" class="btn btn-success" disabled=""><i class="fa fa-pencil-square-o"></i> Editar</button>
											<%}%>
											<%if(privileges.contains("CAT68")){%>
											<button id="guardar" class="btn btn-success" ><i class="fa fa-save"></i> Guardar</button>
											<%}%>
										</p>
										</span>
									</div>
									<div class="description"></div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						<!-- ADVANCED -->
						<div class="row">
							<div class="col-md-12">
								<!-- BOX -->
								<div class="box border red">
									<div class="box-title">
										<h4><i class="fa fa-bars"></i>Nuevo/Editar Especialid</h4>
									</div>
									<div class="box-body">
										<form class="form-horizontal " action="#" id="frm_especialidades">
										  <div class="form-group">
											 <label class="col-md-2 control-label" for="e1">Plan de Estudios:</label> 
											 <div class="col-md-10">
								<select id="e1" class="col-md-12" name="plan">
                <%
														out.println("<option id='0' selected value=''>Selecciona ..</option>");
                            ps = con.prepareStatement("SELECT officialCode, prog_IdPlan"
                                     + " FROM ProgPlan;");

                            rs = ps.executeQuery();
                            while(rs.next()){
                                if(Integer.parseInt(rs.getString("prog_IdPlan"))== claveOfi){
                                     out.println("<option id='"+rs.getString("prog_IdPlan")+
                                             "' selected>"+rs.getString("officialCode")+"</option>");
                                 }else{
                                     out.println("<option id='"+rs.getString("prog_IdPlan")+"'>"+
                                             rs.getString("officialCode")+"</option>");
                                 }
                             }
                %>
								</select>
											 </div>
										  </div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="clave">Clave:</label> 
											<div class="col-md-10">
                    <% 
                    if(specialId>0){
                    out.println("<input class='form-control' autocomplete='off' type='text' name='clave'  id='clave' value='"+clave+"'>");
                    }else{
                    out.println("<input class='form-control' autocomplete='off' type='text' name='clave'  id='clave' >");    
                    }
                    %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="cvoficial">Clave Oficial:</label> 
											<div class="col-md-10">
                    <% 
                    if(specialId>0){
                    out.println("<input type='text' class='form-control' autocomplete='off' name='cvoficial' id='cvoficial' value='"+claveOf+"'>");
                    }else{
                    out.println("<input type='text' class='form-control' autocomplete='off' name='cvoficial' id='cvoficial' >");    
                    }
                    %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="abr">Abreviatura:</label> 
											<div class="col-md-10">
                    <% 
                    if(specialId>0){
                    out.println("<input class='form-control' autocomplete='off' type='text' name='abr' id='abr' value='"+abreviation+"'>");
                    }else{
                    out.println("<input class='form-control' autocomplete='off' type='text' name='abr' id='abr' >");    
                    }
                    %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="nom">Nombre:</label> 
											<div class="col-md-10">
		                <% 
                    if(specialId>0){
                    out.println("<input class='form-control' autocomplete='off' type='text' name='nom' id='nom' value='"+name+"'>");
                    }else{
                    out.println("<input class='form-control' autocomplete='off' type='text' name='nom' id='nom' >");    
                    }
			             %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="credi">Creditos:</label> 
											<div class="col-md-10">
                    <% 
                    if(specialId>0){
                    out.println("<input class='form-control' autocomplete='off' type='text' name='credi' id='credi' value='"+credits+"'>");
                    }else{
                    out.println("<input class='form-control' autocomplete='off' type='text' name='credi' id='credi' value='0'>");    
                    }
                    %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="credi">Status:</label> 
											<div class="col-md-10">
												<select id="e2" class="col-md-12" name="sta">
                    <%
												String stat[]={"ACTIVO","INACTIVO"};
												out.println("<option id='0' selected value=''>Selecciona ..</option>");
                        for(int i=0;i<2;i++){
                            if(status.equals(stat[i])){
                                out.println("<option id='"+stat[i]+"'selected>"+stat[i]+"</option>");

                            }else{
                                out.println("<option id='"+stat[i]+"'>"+stat[i]+"</option>");
                            }
                        }
                    %>         
                    </select>
											</div>
											</div>
											<input type="hidden" id="hide" value="<% out.println(specialId);%>">
									   </form>
									</div>
								</div>
								<!-- /BOX -->
							</div>
						</div>
						<!-- /ADVANCED -->
						<div class="footer-tools">
							<span class="go-top">
								<i class="fa fa-chevron-up"></i> Top
							</span>
						</div>
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	<script src="../js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="../bootstrap-dist/js/bootstrap.min.js"></script>
	<!-- DATE RANGE PICKER -->
	<script src="../js/bootstrap-daterangepicker/moment.min.js"></script>
	<script src="../js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
	<!-- SLIMSCROLL -->
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
	<!-- BLOCK UI -->
	<script type="text/javascript" src="../js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
	<!-- TYPEHEAD -->
	<script type="text/javascript" src="../js/typeahead/typeahead.min.js"></script>
	<!-- AUTOSIZE -->
	<script type="text/javascript" src="../js/autosize/jquery.autosize.min.js"></script>
	<!-- COUNTABLE -->
	<script type="text/javascript" src="../js/countable/jquery.simplyCountable.min.js"></script>
	<!-- INPUT MASK -->
	<script type="text/javascript" src="../js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
	<!-- FILE UPLOAD -->
	<script type="text/javascript" src="../js/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
	<!-- SELECT2 -->
	<script type="text/javascript" src="../js/select2/select2.min.js"></script>
	<!-- UNIFORM -->
	<script type="text/javascript" src="../js/uniform/jquery.uniform.min.js"></script>
	<!-- JQUERY UPLOAD -->
	<!-- The Templates plugin is included to render the upload/download listings -->
	<script src="../js/blueimp/javascript-template/tmpl.min.js"></script>
	<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="../js/blueimp/javascript-loadimg/load-image.min.js"></script>
	<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script src="../js/blueimp/javascript-canvas-to-blob/canvas-to-blob.min.js"></script>
	<!-- blueimp Gallery script -->
	<script src="../js/blueimp/gallery/jquery.blueimp-gallery.min.js"></script>
	<!-- The basic File Upload plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload.min.js"></script>
	<!-- The File Upload processing plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-process.min.js"></script>
	<!-- The File Upload image preview & resize plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-image.min.js"></script>
	<!-- The File Upload audio preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-audio.min.js"></script>
	<!-- The File Upload video preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-video.min.js"></script>
	<!-- The File Upload validation plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-validate.min.js"></script>
	<!-- The File Upload user interface plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-ui.min.js"></script>
	<!-- The main application script -->
	<script src="../js/jquery-upload/js/main.js"></script>
	<!-- bootbox script -->
	<script src="../js/bootbox/bootbox.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="../js/script.js"></script>
	<script src="../js/jquery-validate/jquery.validate.js"></script>
	<script src="../js/jquery-validate/additional-methods.js"></script>
	<script src="../js/error_message.js"></script>
	<script>
		jQuery(document).ready(function() {		
			App.setPage("forms");  //Set current page
			App.init(); //Initialise plugins and elements
			$("#photo").attr("src","getPhoto?safe_IdUser="+<% if(user != null){out.print(user.getSafe_IdUser());} %>);
			var addOrEdit=<%out.println(request.getParameter("addOrEdit")+";"); %>
			
			$("#frm_especialidades").validate({
				rules:{
					plan:{
						required: true
					},
					clave:{
						required: true, minlength: 2, maxlength: 10
					},
					cvoficial:{
						required: true, minlength: 2, maxlength: 20
					},
					abr:{
						required: true, minlength: 2, maxlength: 30
					},
					nom:{
						required: true, minlength: 2, maxlength: 60
					},
					credi:{
						required: true, digits: true
					},
					sta:{
						required: true
					}
				},
				highlight: function(element){
					$(element).parent().parent().addClass("has-error");
				},
				unhighlight: function(element){
				$(element).parent().parent().removeClass("has-error");
			}});
		
			var clave=$("#clave");
      var cvoficial=$("#cvoficial");
      var credi= $("#credi");
      var sta= $("#e2");
      var plan=$("#e1");
      var abr=$("#abr");
      var nom =$("#nom");
      var hide=$("#hide");
			
			$("#nuevo").click(function () {
				$("#frm_especialidades").valid()
				//window.location = "ProgSpecialties_catalog.jsp?idPS=0&addOrEdit=0";
			});
			$("#editar").click(function () {
         clave.removeAttr("disabled");
         cvoficial.removeAttr("disabled");
         credi.removeAttr("disabled");
         sta.removeAttr("disabled");
         plan.removeAttr("disabled");
         abr.removeAttr("disabled");
         nom.removeAttr("disabled");
         addOrEdit=1;
				$("#editar").attr("disabled","disabled");
			});
			$("#guardar").click(function () {

        var claveVal=clave.val().trim();
        var cvoficialVal=cvoficial.val().trim();
        var crediVal=credi.val().trim();
        var staVal=sta.children(":selected").attr("id");
        var planVal=plan.children(":selected").attr("id");
        var abrVal=abr.val().trim();
        var nomVal=nom.val().trim();

				if($("#frm_especialidades").valid()){
          var values= "values="+parseInt(hide.val())+"#"+parseInt(crediVal)+"#"+claveVal+"#"
              +cvoficialVal+"#"+abrVal+"#"+nomVal+"#"+staVal+"#"+parseInt(planVal)+"&addOrEdit="+addOrEdit;

					spInit();
					dhtmlxAjax.post("ProgSpecialties_save",values,function(loader){
						var resp = loader.xmlDoc.responseText;
						respo = resp.split("#");
						document.getElementById("hide").value=respo[0];
						var resp = parseInt(respo[1]);
						if(resp == 1){
							bootbox.alert("Se guardo con exito!", function() {});
						} else {
							bootbox.alert("Fallo al guardar!", function() {});
						}
						spStop();
					});

        clave.attr("disabled","disabled");
        cvoficial.attr("disabled","disabled");
        credi.attr("disabled","disabled");
        sta.attr("disabled","disabled");
        plan.attr("disabled","disabled");
        abr.attr("disabled","disabled");
        nom.attr("disabled","disabled");
				$("#editar").removeAttr("disabled");
				}
			});
		});
	</script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
			}else{
				response.sendRedirect("error600.jsp");
			}
		}else{
			response.sendRedirect("error600.jsp");
		}
%>