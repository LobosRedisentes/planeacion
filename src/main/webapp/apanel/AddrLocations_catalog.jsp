<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.edcore.model.db.DBConnector"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.edcore.model.safe.User"%>
	<%
		HttpSession sesion;
    sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if(user != null){
			ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
			if(privileges.contains("CAT34")){
	%>
<%
	ResultSet rs;
	DBConnector db = new DBConnector();
	PreparedStatement ps;
	Connection con;
	int state=Integer.parseInt(request.getParameter("idState"));
	int town=Integer.parseInt(request.getParameter("idTown"));
	int addOrEdit=Integer.parseInt(request.getParameter("addOrEdit"));
	int loc=Integer.parseInt(request.getParameter("idLoc"));
	con=db.open();
%>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Colonias</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
	<link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
	
	<link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- DATE RANGE PICKER -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
	<!-- TYPEAHEAD -->
	<link rel="stylesheet" type="text/css" href="../js/typeahead/typeahead.css" />
	<!-- FILE UPLOAD -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-fileupload/bootstrap-fileupload.min.css" />
	<!-- SELECT2 -->
	<link rel="stylesheet" type="text/css" href="../js/select2/select2.min.css" />
	<!-- UNIFORM -->
	<link rel="stylesheet" type="text/css" href="../js/uniform/css/uniform.default.min.css" />
	<!-- JQUERY UPLOAD -->
	<!-- blueimp Gallery styles -->
	<link rel="stylesheet" href="../js/blueimp/gallery/blueimp-gallery.min.css">
	<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload.css">
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload-ui.css">
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	<style>
	 .error-message, label.error {
	    color: #ff0000;
	    margin:0;
	    display: inline;
	    font-size: 1em !important;
	    font-weight:bold;
	 }
	 </style>
	<!-- LIBRERIA EDCORE -->
  <script src="../dhtmlx/libCompiler/dhtmlxcommon.js"></script>
	<script src="../js/action_dhx.js"></script>
</head>
<body>
	<!-- HEADER -->
	<%@include file="header.jsp" %>
	<!--/HEADER -->
	<!-- PAGE -->
	<section id="page">
				<!-- SIDEBAR -->
				<%@include file="sidebar.jsp" %>
				<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<!-- STYLER -->
									
									<!-- /STYLER -->
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="index.jsp">Inicio</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Catalogos</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Localidades</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="AddrLocations_admin.jsp">Colonias</a>
										</li>
										<li>Nuevo/Editar</li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										<h3 class="content-title pull-left">Cat�logo de Colonias</h3>
										<span class="date-range pull-right">
										<p class="btn-toolbar">
											<%if(privileges.contains("CAT75")){%>
											<button id="nuevo" class="btn btn-success" ><i class="fa fa-file-text-o"></i> Nuevo</button>
											<%}%>
											<%if(privileges.contains("CAT76")){%>
											<button id="editar" class="btn btn-success" disabled=""><i class="fa fa-pencil-square-o"></i> Editar</button>
											<%}%>
											<%if(privileges.contains("CAT76")){%>
											<button id="guardar" class="btn btn-success" ><i class="fa fa-save"></i> Guardar</button>
											<%}%>
										</p>
										</span>
									</div>
									<div class="description"></div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						<!-- ADVANCED -->
						<div class="row">
							<div class="col-md-12">
								<!-- BOX -->
								<div class="box border red">
									<div class="box-title">
										<h4><i class="fa fa-bars"></i>Nuevo/Editar Colonia</h4>
									</div>
									<div class="box-body">
										<form class="form-horizontal " action="#" id="frm_localidades">
										  <div class="form-group">
											 <label class="col-md-2 control-label" for="e1">Estado:</label> 
											 <div class="col-md-10">
												 <select id="e1" class="col-md-12" name="state">
                         <%
                                    ps = con.prepareStatement("SELECT AddrStates.name, "
                                            + "AddrStates.addr_idState FROM AddrStates;");
                                    rs = ps.executeQuery();
                                    if(state == 0){
                                       out.println("<option id='0' selected value=''>Selecciona ..</option>");
                                    } else{
                                         out.println("<option id='0' value='' >Selecciona ..</option>");
                                    }
                                      while(rs.next()){
                                            if(rs.getInt("AddrStates.addr_idState")== state){
                                               out.println("<option id='"+
                                                       rs.getInt("AddrStates.addr_idState")+"' selected>"
                                                       +rs.getString("AddrStates.name")+"</option>");
                                          }else{
                                                out.println("<option id='"+
                                                        rs.getInt("AddrStates.addr_IdState")+"'>"+
                                                        rs.getString("AddrStates.name")+"</option>");
                                          }
                                      }																		
                          %>
												 </select>
											 </div>
										  </div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="e2">Municipio</label> 
											<div class="col-md-10">
                                    <%// function for print the combo box of Towns
                                    if (town>0) {
                                        out.println("<select id='e2' class='col-md-12' name='town'>");
                                        if(state>0){
                                         ps = con.prepareStatement("SELECT name, addr_IdState, "
                                                 + "addr_IdTown FROM AddrTown WHERE "
                                                 + "addr_IdState="+state+";");
                                        }
                                         rs = ps.executeQuery();
                                             out.println("<option id='0' value= ''>Selecciona ..</option>");
                                         while(rs.next()){
                                             if(rs.getInt("addr_idTown")== town){
                                                 out.println("<option id='"+rs.getInt("addr_idState")+
                                                         "' selected>"+rs.getString("name")+"</option>");
                                             }else{
                                                 out.println("<option id='"+rs.getInt("addr_IdTown")+"'>"+
                                                         rs.getString("name")+"</option>");
                                             }
                                           }
                                         out.println("</select>");
                                    }else{
                                        out.println("<select id='e2' class='col-md-12' name='town' disabled><option id='0' value='' >Selecciona ..</option></select>");
                                    }
                                 %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="loc">Colonia</label> 
											<div class="col-md-10">
                    <% // function for get de values of location,post code, zone and type
                    String location="";
                    int cp=0;
                    String Zone="";
                    String type="";
                    ps =con.prepareStatement("SELECT * FROM AddrLocations WHERE "
                            + "addr_IdLocation="+loc+"");
                    rs = ps.executeQuery();
                    while(rs.next()){
                        location=rs.getString("name");
                        cp=rs.getInt("postCode");
                        Zone=rs.getString("zone");
                        type=rs.getString("township");
                    }
										
										if(loc>0){
											out.println("<input class='form-control' autocomplete='off' type='text' id='loc' name='loc' placeholder='nombre completo' value='"+location+"'>");
										}else{
											out.println("<input class='form-control' autocomplete='off' type='text' id='loc' name='loc' placeholder='nombre completo'>");
										}
                    %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="cp">CP:</label> 
											<div class="col-md-10">
                                        <%
                                        if(loc>0){
                                            out.println("<input class='form-control' autocomplete='off' type='text' id='cp' name='cp' value='"+cp+"'>");
                                         }else{
                                            out.println("<input class='form-control' autocomplete='off'  type='text' id='cp' name='cp' placeholder='#####' >");
                                        }
                                        %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="e3">Zona:</label> 
											<div class="col-md-10">
												<select id="e3" class="col-md-12" name="zone">
                                        <%// function for print the combobox of Zones
                                            String zones []={"RURAL","SEMIURBANO","URBANO"};
																						if(Zone.equals("")){
																							out.print("<option id='0' value='' selected>Selecciona ..</option>");
																						}
                                            for(int i=0;i<=2;i++){
                                                if(zones[i].equals(Zone)){
                                                    out.println("<option id='"+i+"' selected>"+
                                                            zones[i]+"</option>");
                                                }else{
                                                    out.println("<option id='"+i+"'>"+
                                                            zones[i]+"</option>");
                                                }
                                            }

                                        %>
												</select>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="e4">Tipo:</label> 
											<div class="col-md-10">
                                <%// function for print the combobox of type

                                 ps = con.prepareStatement("SELECT * FROM AddrLocationsShip_view;");
                                 rs = ps.executeQuery();
                                  out.println("<select id='e4' class='col-md-12' name='type'>");

                                  String township;
																	if(type.equals("")){
																		out.print("<option id='0' value='' selected>Selecciona ..</option>");
																	}
                                   while(rs.next()){
                                       township=rs.getString("township");
                                       if(township.equals(type)){
                                            out.println("<option id='"+rs.getString("township")+
                                                    "' selected>"+rs.getString("township")+"</option>");
                                       }
                                       else{
                                             out.println("<option id='"+rs.getString("township")+"'>"
                                                     +rs.getString("township")+"</option>");
                                       }   
                                   }
                                   db.close();
                                 %>
											</div>
											</div>
											<input type="hidden" id="hide" value="<%out.println(loc);%>">
									   </form>
									</div>
								</div>
								<!-- /BOX -->
							</div>
						</div>
						<!-- /ADVANCED -->
						<div class="footer-tools">
							<span class="go-top">
								<i class="fa fa-chevron-up"></i> Top
							</span>
						</div>
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	<script src="../js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="../bootstrap-dist/js/bootstrap.min.js"></script>
	<!-- DATE RANGE PICKER -->
	<script src="../js/bootstrap-daterangepicker/moment.min.js"></script>
	<script src="../js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
	<!-- SLIMSCROLL -->
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
	<!-- BLOCK UI -->
	<script type="text/javascript" src="../js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
	<!-- TYPEHEAD -->
	<script type="text/javascript" src="../js/typeahead/typeahead.min.js"></script>
	<!-- AUTOSIZE -->
	<script type="text/javascript" src="../js/autosize/jquery.autosize.min.js"></script>
	<!-- COUNTABLE -->
	<script type="text/javascript" src="../js/countable/jquery.simplyCountable.min.js"></script>
	<!-- INPUT MASK -->
	<script type="text/javascript" src="../js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
	<!-- FILE UPLOAD -->
	<script type="text/javascript" src="../js/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
	<!-- SELECT2 -->
	<script type="text/javascript" src="../js/select2/select2.min.js"></script>
	<!-- UNIFORM -->
	<script type="text/javascript" src="../js/uniform/jquery.uniform.min.js"></script>
	<!-- JQUERY UPLOAD -->
	<!-- The Templates plugin is included to render the upload/download listings -->
	<script src="../js/blueimp/javascript-template/tmpl.min.js"></script>
	<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="../js/blueimp/javascript-loadimg/load-image.min.js"></script>
	<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script src="../js/blueimp/javascript-canvas-to-blob/canvas-to-blob.min.js"></script>
	<!-- blueimp Gallery script -->
	<script src="../js/blueimp/gallery/jquery.blueimp-gallery.min.js"></script>
	<!-- The basic File Upload plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload.min.js"></script>
	<!-- The File Upload processing plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-process.min.js"></script>
	<!-- The File Upload image preview & resize plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-image.min.js"></script>
	<!-- The File Upload audio preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-audio.min.js"></script>
	<!-- The File Upload video preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-video.min.js"></script>
	<!-- The File Upload validation plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-validate.min.js"></script>
	<!-- The File Upload user interface plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-ui.min.js"></script>
	<!-- The main application script -->
	<script src="../js/jquery-upload/js/main.js"></script>
	<!-- bootbox script -->
	<script src="../js/bootbox/bootbox.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="../js/script.js"></script>
	<script src="../js/jquery-validate/jquery.validate.js"></script>
	<script src="../js/jquery-validate/additional-methods.js"></script>
	<script src="../js/error_message.js"></script>
	<script>
		jQuery(document).ready(function() {		
			App.setPage("forms");  //Set current page
			App.init(); //Initialise plugins and elements
			$("#photo").attr("src","getPhoto?safe_IdUser="+<% if(user != null){out.print(user.getSafe_IdUser());} %>);
			var addOrEdit=<%out.println(request.getParameter("addOrEdit")+";"); %>
			
			$("#frm_localidades").validate({
				rules:{
					state:{
						required: true
					},
					town:{
						required: true
					},
					loc:{
						required: true, minlength: 5, maxlength: 150
					},
					cp:{
						required: true, minlength: 5, maxlength: 5, digits: true
					},
					zone:{
						required: true
					},
					type:{
						required: true
					}
				},
				highlight: function(element){
					$(element).parent().parent().addClass("has-error");
				},
				unhighlight: function(element){
				$(element).parent().parent().removeClass("has-error");
			}});
			
               $("#e1").change(function(){
                var id=$(this).children(":selected").attr("id");
                // remove all options, but not the first
                $('#e2 option:gt(0)').remove(); 
                if(id>0){ 
                    $("#e2").removeAttr("disabled");
                    dhtmlxAjax.post("AddrLocations_auxiliary.jsp",
                    "idState="+id,function (loader){
                    $("#e2").append(loader.xmlDoc.responseText);                    
                });
                }else{
                  $("#e2").attr("disabled","disabled");   
                }
               });
			
			$("#nuevo").click(function () {
				window.location = "AddrLocations_catalog.jsp?idState=0&idTown=0&idLoc=0&addOrEdit=0";
			});
			$("#editar").click(function () {
				$("#e1").removeAttr("disabled");
				$("#e2").removeAttr("disabled");
				$("#e3").removeAttr("disabled");
				$("#e4").removeAttr("disabled");
				$("#loc").removeAttr("disabled");
				$("#cp").removeAttr("disabled");
				addOrEdit=1;
				$("#editar").attr("disabled","disabled");
			});
			$("#guardar").click(function () {
				$("#frm_localidades").valid();
				var state=$("#e1");
				var town=$("#e2");
				var loc=$("#loc");
				var cp=$("#cp");
				var zone=$("#e3");
				var type=$("#e4");
				var hide=$("#hide");
				var stateVal=(state.children(":selected").attr('id').trim());
				var townVal=town.children(":selected").attr('id');
				var locVal=loc.val().trim();
				var cpVal=cp.val().trim();
				var zoneVal=zone.val().trim();
				var typeVal=type.children(":selected").attr('id');
				if(stateVal>0 && townVal>0 && locVal!=="" && cpVal!=="" && zoneVal!=="" && typeVal!=="" ){
					var values= "values="+parseInt(hide.val())+"#"+stateVal+"#"+townVal+"#"+locVal+"#"+cpVal+"#"+typeVal+"#"+zoneVal+"&addOrEdit="+addOrEdit;
					dhtmlxAjax.post("AddrLocations_save",values,function(loader){
						var resp = loader.xmlDoc.responseText;
						respo = resp.split("#");
						document.getElementById("hide").value=respo[0];
						var resp = parseInt(respo[1]);
						if(resp == 1){
							bootbox.alert("Se guardo con exito!", function() {});
						} else {
							bootbox.alert("Fallo al guardar!", function() {});
						}
					});

					state.attr("disabled","disabled");
					town.attr("disabled","disabled");
					loc.attr("disabled","disabled");
					cp.attr("disabled","disabled");
					zone.attr("disabled","disabled");
					type.attr("disabled","disabled");
					$("#editar").removeAttr("disabled");
				}
			});
		});
	</script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
			}else{
				response.sendRedirect("error600.jsp");
			}
		}else{
			response.sendRedirect("error600.jsp");
		}
%>