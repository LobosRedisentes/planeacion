<%-- 
    Document   : message
    Created on : 16-jun-2015, 10:10:07
    Author     : gabo
--%>

<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.edcore.model.db.DBConnector"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mensaje</title>
    </head>
    <body>
        <%
            DBConnector db = new DBConnector();
            ResultSet rs;
            PreparedStatement ps;
            Connection con;
            con=db.open();
            ps = con.prepareStatement("SELECT * FROM VIW013 WHERE pub_IdMessenger = ?");
            ps.setInt(1,Integer.parseInt(request.getParameter("pub_IdMessager")));
            rs = ps.executeQuery();
            if(rs.next()){
                out.print("De: "+request.getParameter(""));
            }
            db.close();
        %>
    </body>
</html>
