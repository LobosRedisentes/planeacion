jQuery(document).ready(function() {
    App.setPage("forms");  //Set current page
    App.init(); //Initialise plugins and elements
    $("#photo").attr("src","getPhoto?safe_IdUser="+id);
    var response = "../getAdminView?table=UserStudents_view&id=user_IdStudent&parameters=matricula,nombre,paterno,materno,plan,curp,status";
    var IDRow;

    grid = new dhtmlXGridObject('grid_container');
    grid.setImagePath("../dhtmlx/dhtmlxGrid/codebase/imgs/");
    grid.setHeader("Matricula,Nombre,Paterno,Materno,Plan,CURP");
    grid.attachHeader("#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter");
    grid.setInitWidths("*,*,*,*,*,200");
    grid.setColAlign("left,left,left,left,left,left");
    grid.setColSorting("str,str,str,str,str,str");
    grid.setEditable(false);
    grid.init();
    grid.setSkin("clear");
    grid.enableSmartRendering(true,50);
    grid.objBox.style.overflowX = "hidden";
    spInit();
    grid.load(response,function(){
        spStop();
        //columnAutoSize();
    });
    grid.attachEvent("onRowSelect",function(id){
        IDRow = id;
        $("#btn_nuevo").removeAttr("disabled");
    });
    
    $("#btn_nuevo").click(function(){
        $('#actuales').removeAttr('checked');
        $('#nocursadas').removeAttr('checked');
        $('#box_nuevo').modal({show:true});
    });
    $("#btn_print").click(function(event){
        var cal_IdCalSchool = $('#cal_IdCalSchool').val();
        if(cal_IdCalSchool !== ''){
            var actuales = 'N';
            var nocursadas = 'N';
            var cursadas = 'N';
            if($('#actuales').prop( "checked" )){
                actuales = 'S';
            }
            if($('#nocursadas').prop( "checked" )){
                nocursadas = 'S';
            }
            if($('#cursadas').prop( "checked" )){
                cursadas = 'S';
            }
            $('#box_nuevo').modal('toggle');
            $('iframe').attr("src","getKardexPdf?user_IdStudent="+IDRow+"&cursadas="+cursadas+"&actuales="+actuales+"&nocursadas="+nocursadas);
            $('#box_print').modal({show:true});
        }else{
            bootbox.alert("Favor de selecciona un calendario! ", function() {});
        }
        event.preventDefault();
    });
    
    $("#actualizar").click(function () {
        grid.clearAndLoad(response);
    });		
});