 /* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var idRow=null;
var ventana;
var layout;
var tool;
var sb;
var grid;
var dhxPopUp;
var values = [];
//inicializa la ventana principal

function add(item){
    values.push(item);
    console.log("Se anadio"+item);
}

function remove(item){
    var index = values.indexOf(item);
    values.splice(index,1);
}

function main(){
    layout = new dhtmlXLayoutObject(document.body,"1C","dhx_skyblue");
    tool = layout.attachToolbar();
    sb = layout.attachStatusBar();      
    grid = dhtmlXGridFromTable("grid");
    dhxPopUp=new dhtmlXPopup();

    layout.cells("a").hideHeader();
    layout.cells("a").attachObject("vp");
//    var addOrEdit=<%//out.println(addOrEdit);%>;


    tool.setIconsPath("../../icon/");
    tool.setSkin("dhx_skyblue");
    tool.loadXML("reinscription_catalog_tool");  
    dhtmlxError.catchError("ALL",function(type,desc,ErrData){
        if (ErrData[0].status>=400 && ErrData[0].status<500){
            dhtmlx.message({
                     type:"alert-error",
                     title:"Advertencia",
                     text:"Imposible conectar con la base de datos" ,
                     callback: function(){
                         action_closeWin(win_id);
                     }
                  });
        }else if (ErrData[0].status>=400 && ErrData[0].status<600){
            dhtmlx.message({
                     type:"alert-error",
                     title:"Advertencia",
                     text:"Oops, no se pudo concretar la operación",
                     callback: function(){
                         action_closeWin(win_id);
                     }
                  });
        }else{

        }
   });
    tool.attachEvent("onClick",
    function (id){
        switch(id){

                case "edit"  :
                    edit();
                    break
                case "save"  :
                    save();
                    break
                case "delete"  :
                    deleteData();
                    break
                case "print"  :
                    break
                case "close"  :    
                     break
        }
    }
);
    grid.setImagePath("../../dhtmlx/dhtmlxGrid/codebase/imgs/");
    grid.setInitWidths("50,*,*,*,*,*,*,*,*,*,*,*,*,*,*");
    grid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left");
    grid.setColSorting("str,str,str,str,str,str,str,str,str,str,str,str,str,str");
    grid.setColTypes("ch,ed,ed,ed,ed,ed,ed,ed,ed,ed,ed,ed,ed,ed");
    grid.setEditable(true);
    grid.setSkin("clear");
    
    grid.attachEvent("onCheck",function(rId,cInd,state){selectedBox(rId,state)});

    grid.attachEvent("onRowSelect",function(id){popupProfessor(id)});

    grid.attachEvent("onXLS",function(){
        $("#control").attr("disabled","disabled");
        $("#search").attr("disabled","disabled");
        tool.disableItem('delete');
        tool.disableItem('print');
        tool.disableItem('save');
        tool.disableItem('edit');
        tool.disableItem('close');
        grid.setEditable(false);
    });

    grid.attachEvent("onXLE",function(){
        $("#control").removeAttr("disabled");
        $("#search").removeAttr("disabled");
        tool.enableItem('delete');
        tool.enableItem('save');
        tool.enableItem('close');
        grid.setEditable(true);
    });

    //funcion que realiza la busqueda del usuario al hacer enter
    $("#control").keyup(function(e){
        if(e.keyCode == 13)
        {   
            searchStud($("#control").val());
        }
    });
                
}
//genera la ventana para realizar la busqueda del estudiante
function selection (){
    var popUpWindow=layout.dhxWins.createWindow("popUp"+ new Date().getTime(),0,0,500,300);
    popUpWindow.setText("Busqueda de Alumnos");
    popUpWindow.button("close").hide();
    popUpWindow.button("minmax1").hide();
    popUpWindow.button("park").hide();
    var popUpLayout=popUpWindow.attachLayout("2E");
    var popUpGrid=popUpLayout.cells('a').attachGrid();
    popUpLayout.cells("a").hideHeader();    
    popUpLayout.cells("b").hideHeader();    

    popUpLayout.cells("b").setHeight('10');

    var response2 = "reinscription_popup_view";
    popUpGrid.setImagePath("../../dhtmlx/dhtmlxGrid/codebase/imgs/");
    popUpGrid.setHeader("No. de Control, Nombre, Estatus");
    popUpGrid.attachHeader("#text_filter,#text_filter,#select_filter_strict");
    popUpGrid.setInitWidths("*,250,*");
    popUpGrid.setColAlign("center,center,center");
    popUpGrid.setColSorting("str,str,str");
    popUpGrid.setEditable(false);
    popUpGrid.init();
    popUpGrid.loadXML(response2);
    popUpGrid.attachEvent("onRowSelect",function(id){
        idRow=id;
        var status=popUpGrid.cellById(id,2).getValue();
        if(status!=='VI'){
            alertStudInactive();
            $("#sel").attr("disabled","disabled");
        }else{
            $("#sel").removeAttr("disabled");
        }
    });
    $('#buttn').removeAttr('style');
    var obj=document.getElementById("buttn");
    popUpLayout.cells("b").attachObject(obj);
    ventana=popUpWindow;
    $("#sel").attr("disabled","disabled");
}
//ventana que busca y asigna los datos correspondientes
function searchStud(code){
    var patt= new RegExp ("[0-9]{8}");
    var res= patt.test(code.trim());
    if(res && (parseInt(code)<= 99999999)){
    var url="reinscription_residence_data";
    var values="code="+code.trim();
    spInit();
    var respo=$.ajax({
        url:url,
        data:values,
        type:"POST",
        dataType:"text"
    }); 
    respo.done(function (msg){
        if(msg !== ""){
            var data=JSON.parse(msg);
            var ahora=new Date();
            var fecha=new Date(data[0].fecha);
            var months =new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
            if (parseInt(data[0].bloq)>0){
                alertBlocked();
								spStop();
                return 0;
            }
            //valida que la fecha de reinscripcion se halla cumplido
            if((data[0].fecha)!=="null"){
                fecha=new Date(data[0].fecha);
                if(ahora<fecha){
                    alertBadDate();
										spStop();
                    return 0;
									}
            }else{
                alertBadDate();
								spStop();
                return 0;
            }
            // valida que el alumno este en estatus de vigente
            if(data[0].status!=='VI'){
                alertStudInactive();
								spStop();
                return 0;
            }
            $("#hiddenControl").val(data[0].IdStudent);
            $("#cMin").val(data[0].cargaMin);
            $("#maximum").val(data[0].cargaMax);
            $("#name").text(data[0].nombre);
            $("#maxLabel").text(data[0].cargaMax);
            $("#minLabel").text(data[0].cargaMin);
            $("#semester").text(data[0].semestre);
            grid.clearAll();
						grid.objBox.style.overflowX = "hidden";
            grid.load("reinscription_residence_view?stud="+data[0].IdStudent+"&plan="+data[0].IdPlan+"&specialty="+data[0].IdSpecialty+"&semester="+semester,function(){
                grid.forEachRow(function(id){
                    var value = id;
                    var cell = parseInt(grid.cellById(id,0).getValue());
                    if(cell === 1){
                        add(value);
                        var sum  = $("#sum");
                        var span = parseInt(sum.text());
                        var cred = parseInt(grid.cellById(id,3).getValue());
                        sum.text(span+cred);
                    }
                });
                spStop();
								$("#guardar").removeAttr("disabled");
								$("#eliminar").removeAttr("disabled");
								$("#Pdf").removeAttr("disabled");
            });
        }else {
						spStop();
            noData();
        }
    });
    }else{
				spStop();
       codeNoValid();
    }
}
//muesta el popup con el nombre del profesor
function popupProfessor(id){
    var position=$(".rowselected").position();
            dhxPopUp.hide();
            var data=grid.getUserData(id,"profesor");
            dhxPopUp.clear();
            dhxPopUp.attachHTML(data);
            var x = position.left+300; // inner dhtmlx method, return left position
            var y = position.top; // inner dhtmlx method, return top position
            var w = 300;//grid.offsetWidth;
            var h = 300;//grid.offsetHeight;
            dhxPopUp.show(x,y,w,h);
}
//raliza la suma al seleccionar en un registro
function selectedBox(rId,state){
    var sum  = $("#sum");
    var span = parseInt(sum.text());
    var cell = parseInt(grid.cellById(rId,3).getValue());
    if (state){
        sum.text(span+cell);
        grid.forEachRow(function(id){
            if(grid.cellById(rId,2).getValue() == grid.cellById(id,2).getValue() && grid.cellById(id,0).getValue() == 1 && id != rId){
								duplicatedSubj();
                if(grid.cellById(rId,12).getValue() === 0){
                    noVacancy();
                    grid.cellById(rId,12).setValue("0");
                    return 0;
                }
                span=parseInt(sum.text());
                sum.text(span-cell);
                grid.cellById(rId,0).setValue(0);
            }
        });
    }else{
        sum.text(span-cell); 
    }
}
//funcion que cierra la ventana de busqueda de estudiantes                
function closePop(){
    $('#buttn').attr('style','visibility: hidden');
    document.body.appendChild(document.getElementById('buttn'));
    ventana.close();
}
//funcion que llama la busqueda al seleccionarse un usuario desde la ventana de busqueda
function selectedStud(code){
         searchStud(code); 
         closePop();
}
//muestra alerta de estudiante inactivo
function alertStudInactive(){
	bootbox.alert("No se debe seleccionar un estudiante inactivo", function() {});
}
//muestra alerta de fecha aun no cumplida para reinscripcion
function alertBadDate(){
		bootbox.alert("Debe esperar hasta su fecha de reinscripcion", function() {});
}
//muestra informacion de alumno bloqueado
function alertBlocked(){
		bootbox.alert("El alumno esta bloqueado, favor de verificar", function() {});
}
//muestra alerta de informacion de alumno no encontrada
function noData(){
	bootbox.alert("No se encontraron datos que coincidan", function() {});
}
//muestra alerta de codigo no valido
function codeNoValid(){
	bootbox.alert("El numero de control no es correcto, Favor de verificar", function() {});
}
//muestra alerta de materias duplicadas
function duplicatedSubj(){
	bootbox.alert("No se debe seleccionar dos grupos de la misma materia", function() {});
}
//muestra alerta de correquisito no seleccionado
function correqNoSelected(){
	bootbox.alert("la materia "+grid.cellById(id,2)+" tiene como correquisito la materia "+grid.cellById(correq,2)+" recuerda que debes tomarla", function() {});
}
//muestra una alerta de grupo lleno
function noVacancy(){
	bootbox.alert("No te puedes inscribir a un grupo lleno", function() {});
}
//muestra alerta de creditos maximos excedidos
function creditsExceeded(){
		bootbox.alert("Excediste el maximo de creditos", function() {});
}
//muestra alerta de creditos minimos no alcanzados
function creditsLess(){
		bootbox.alert("No alcanzaste el minimo de creditos", function() {});
}
//inicializa el formulario
function initForm(){
    $("#hiddenControl").val("0");
    $("#cMin").val("0");
    $("#control").val("");
    $("#name").val("");
    $("#espe").val("");
    $("#semester").val("");
    $("#repe").val("");
    $("#percent").val("");
    $("#espeNum").val("");
    $("#average").val("");
    $("#maximum").val("");
    $("#espeMod").val("");
    $("#date").val("");
    $("#sum").text("0");
    grid.clearAll();
}
//funciona para guardar los datos
function save(){
    var cMin=parseInt($("#cMin").val());
    var cMax=parseInt($("#maximum").val());
    var credits=parseInt($("#sum").text());
    if(credits>cMax){
        //creditsExceeded();   
    }
    if(credits<cMin){
        //creditsLess();
    }
    
}
function saveFunction(){
    var control;
    var data="";
    control = $("#hiddenControl").val();
    grid.forEachRow(function(id){
        if(grid.cellById(id,0).getValue() === 1){
            var correq = parseInt(grid.getUserData(id,"coReq"));
            if(correq !== 0){
                if(parseInt(grid.cellById(correq,0).getValue())===1){
                    data+=id+"#";
                    
                }else{
                   correqNoSelected();
                }
            }else{
                data+=id+"#";
            }
        }
    });
    var ajaxdata="data="+data+"&idStudent="+control;
    alert(ajaxdata);
}
//funcion para eliminar la carga completa
function deleteData(){
		bootbox.confirm("Deseas eliminar la carga academica?",function(result){
			if(result){
                var control=$("#hiddenControl").val();
                var url="Reinscription_Residence_Delete";
                var data="idStudent="+control;
                var respo=$.ajax({
                 url:url,
                 data:data,
                 type:"POST",
                 beforeSend:function(){
                    grid.setEditable(false);
                 }
                });
                respo.done(function(msg){
									bootbox.alert("Borrado exitoso", function() {
												grid.setEditable(true);
                        grid.forEachRow(function(id){
                            var cell=grid.cellById(id,0).getValue();
                            if(cell==1){
                                grid.cellById(id,0).setValue(0);
                            }
                        });
                        $("#sum").text("0");
									});
                });
                respo.fail(function(jqXHR, textStatus){
									bootbox.alert("Error al elilminar los datos", function(){
										grid.setEditable(true);
									});
                });
			}
	});    
}

function engine(){
	
				dhxPopUp=new dhtmlXPopup();
        grid = dhtmlXGridFromTable("grid");
        grid.setColAlign("center,center,left,center,center,center,center,center,center,center,center,center,center,center");
        //grid.setInitWidths(10,10,10,10,10,10,10,10,10,10,10,10);
        grid.setColSorting("str,str,str,str,str,str,str,str,str,str,str,str,str,str");
        grid.enableResizing("false,false,false,false,false,false,false,false,false,false,false,false,false,false")
        grid.setColTypes("ch,ed,ed,ed,ed,ed,ed,ed,ed,ed,ed,ed,ed,ed");
        grid.enableAutoHeight(true);
        grid.enableAutoWidth(false);
        grid.setEditable(true);
        grid.setSkin("clear");
        grid.attachEvent("onCheck",function(rId,cInd,state){
            selectedBox(rId,state);
        });
        grid.attachEvent("onRowSelect",function(id){
            popupProfessor(id);
        });
        grid.attachEvent("onCheck",function(id,e){
            var idItem = id ;
            var x = grid.cells(idItem,0).getValue();
            var status = parseInt(x);
                if (status === 1) {
                    add(idItem);
                } 
                else if (status === 0) {
                    remove(idItem);
                }
                return(0);
         });
}



 
function proto(){
    spInit();
    var max  = parseInt($("#maximum").val());
    var min  = parseInt($("#cMin").val());
    var cred = parseInt($("#sum").text());
    var noControl = parseInt($("#hiddenControl").val());  

		
        var sizeValues  = parseInt(values.length);
        var str = values.toString();
        var data = parseRequest(str,sizeValues);
        if(sizeValues !== 0){
								var ajaxRequest = "data="+data+"&idStudent="+noControl;
								var request = $.ajax({
                               url : "Reinscription_Residence_Save",
                               data: ajaxRequest,
                               type: "POST"
                          });
		            request.done(function(msg){
			           bootbox.alert("Se guardo con exito!", function() {});
				         spStop();
						    });
								request.fail(function(msg){
									bootbox.alert("Ocurrio un error!", function() {});
									spStop();
								});     
        } else {
            var ajaxRequest = "&idStudent="+noControl;
            var request = $.ajax({
                            url : "Reinscription_Residence_Delete",
                            data: ajaxRequest,
                            type: "POST"
                          });
            request.done(function(msg){
               bootbox.alert("Se guardo con exito!", function() {});
               spStop();
            });
            request.fail(function(msg){
               bootbox.alert("Ocurrio un error!", function() {});
               spStop();
            });
        }
}

function parseRequest(str,size){
    var splitStr = str.split(",");
    var data = "";
    for(var i = 0 ; i < size ; i++){
        if(i === 0){
            data = data + splitStr[i];
        } else{
        data = data + "#"+splitStr[i];
        }
    }
    return data;
}