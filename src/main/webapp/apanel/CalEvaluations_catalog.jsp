<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.edcore.model.db.DBConnector"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.edcore.model.safe.User"%>
	<%
		HttpSession sesion;
    sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if(user != null){
			ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
			if(privileges.contains("CONF07")){
	%>
<%
	ResultSet rs;
	DBConnector db = new DBConnector();
	PreparedStatement ps;
	Connection con;

    String clave="";
    int parcial=0;
    String aplicacion="";
    String tipo="";
    String fechaIni="";
    String fechaFin="";
    int name=0;
    String status="";

		con=db.open();
    int calIdEv=Integer.parseInt(request.getParameter("calIdEvaluation"));
    if(calIdEv>0){
        ps = con.prepareStatement("SELECT * FROM CalEvaluations WHERE cal_IdEvaluation=?");
        ps.setInt(1, calIdEv);
        rs = ps.executeQuery();
    
    while(rs != null && rs.next()){
        
        clave =rs.getString("code");
        parcial =rs.getInt("evaluation");
        aplicacion =rs.getString("application");
        tipo =rs.getString("type");
        fechaIni =rs.getString("start");
        fechaFin =rs.getString("end");
        name =rs.getInt("cal_IdCalSchool");
        status =rs.getString("status");
            }   
        
    }
%>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Fechas de Evaluación</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
	<link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
	
	<link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">    
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
	<!-- TYPEAHEAD -->
	<link rel="stylesheet" type="text/css" href="../js/typeahead/typeahead.css" />
	<!-- FILE UPLOAD -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-fileupload/bootstrap-fileupload.min.css" />
	<!-- SELECT2 -->
	<link rel="stylesheet" type="text/css" href="../js/select2/select2.min.css" />
	<!-- UNIFORM -->
	<link rel="stylesheet" type="text/css" href="../js/uniform/css/uniform.default.min.css" />
	<!-- JQUERY UPLOAD -->
	<!-- blueimp Gallery styles -->
	<link rel="stylesheet" href="../js/blueimp/gallery/blueimp-gallery.min.css">
	<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload.css">
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload-ui.css">
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	<style>
	 .error-message, label.error {
	    color: #ff0000;
	    margin:0;
	    display: inline;
	    font-size: 1em !important;
	    font-weight:bold;
	 }
	 </style>
	<!-- LIBRERIA EDCORE -->
  <script src="../dhtmlx/libCompiler/dhtmlxcommon.js"></script>
	<script src="../js/action_dhx.js"></script>
</head>
<body>
	<!-- HEADER -->
	<%@include file="header.jsp" %>
	<!--/HEADER -->
	<!-- PAGE -->
	<section id="page">
				<!-- SIDEBAR -->
				<%@include file="sidebar.jsp" %>
				<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<!-- STYLER -->
									
									<!-- /STYLER -->
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="index.jsp">Inicio</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Configuración</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Fechas</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="CalEvaluations_admin.jsp">Evaluaciones</a>
										</li>
										<li>Nuevo/Editar</li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										<h3 class="content-title pull-left">Catálogo - Fechas de Evaluación</h3>
										<span class="date-range pull-right">
										<p class="btn-toolbar">
											<%if(privileges.contains("CONF12")){%>
											<button id="nuevo" class="btn btn-success" ><i class="fa fa-file-text-o"></i> Nuevo</button>
											<%}%>
											<%if(privileges.contains("CONF13")){%>
											<button id="editar" class="btn btn-success" disabled=""><i class="fa fa-pencil-square-o"></i> Editar</button>
											<%}%>
											<%if(privileges.contains("CONF14")){%>
											<button id="guardar" class="btn btn-success" ><i class="fa fa-save"></i> Guardar</button>
											<%}%>
										</p>
										</span>
									</div>
									<div class="description"></div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						<!-- ADVANCED -->
						<div class="row">
							<div class="col-md-12">
								<!-- BOX -->
								<div class="box border red">
									<div class="box-title">
										<h4><i class="fa fa-bars"></i>Nuevo/Editar Fechas de Evaluación</h4>
									</div>
									<div class="box-body">
										<form class="form-horizontal " action="#" id="frm_evaluaciones">
										  <div class="form-group">
											 <label class="col-md-2 control-label" for="clave">Clave</label> 
											 <div class="col-md-10">
            <% 
            if(clave != null && !clave.equals("")){
							out.println("<input  class='form-control' autocomplete='off' type='text'  id='clave' name='clave' value='"+clave+"'>");
            }else{
							out.println("<input  class='form-control' autocomplete='off' type='text'  id='clave' name='clave' >");    
            }
            %>

											 </div>
										  </div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="e1">Parcial:</label> 
											<div class="col-md-10">
            <select id="e1" class="col-md-12" name="parcial">
            <%
            String par[]={"parcial 1","parcial 2","parcial 3"};
            out.println("<option id='0' value='' selected>Selecciona ..</option>");
						
               for(int i=1;i<4;i++){
                   if(parcial == i){
                   out.println("<option id='"+i+"' selected>"+par[i-1]+"</option>");
               
                   }else{
                   out.println("<option id='"+i+"'>"+par[i-1]+"</option>");
                   }
               } 
                %>
            </select>											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="e2">Aplicación:</label> 
											<div class="col-md-10">
						<select id="e2" class="col-md-12" name="aplic">    
            <%
            String typeArray[]={"ORDINARIO","REGULARIZACION","EXTRAORDINARIO"};
            out.println("<option id='0' value='' selected>Selecciona ..</option>");
                for(int i=0;i<3;i++){
                    if(aplicacion.equals(typeArray[i])){
                        out.println("<option id='"+typeArray[i]+"'selected>"+typeArray[i]+"</option>");
                       // out.println("<script>alert("+aplic[i]+");</script>");
                        
                    }else{
                        out.println("<option id='"+typeArray[i]+"'>"+typeArray[i]+"</option>");
                    }
                }
            %>
            </select>
											</div>
											</div>
											<div class="form-group">
											 <label class="col-md-2 control-label" for="e3">Tipo:</label> 
											 <div class="col-md-10">
					<select id="e3" class="col-md-12" name="tipo">
            <%
            String tip[]={"CAPTURA","APLICACION"};
            out.println("<option id='0' value='' selected>Selecciona ..</option>");
                for(int i=0;i<2;i++){
                    if(tipo.equals(tip[i])){
                        out.println("<option id='"+tip[i]+"'selected>"+tip[i]+"</option>");                        
                    }else{
                        out.println("<option id='"+tip[i]+"'>"+tip[i]+"</option>");
                    }
                }
             %>
            </select>	
											</div>
											</div>
										  <div class="form-group">
											 <label class="col-md-2 control-label" for="FechaInicio">Fecha Inicio:</label> 
											 <div class="col-md-10">
                <%
                if(calIdEv>0) {
                String [] aux = fechaIni.split(" ");
                fechaIni = aux[0]+"T"+aux[1];
                out.println("<input class='form-control' autocomplete='off' type='datetime-local' id='fecha_inicio' value='"+fechaIni+"'>");
                }else{
                out.println("<input class='form-control' autocomplete='off' type='datetime-local' id='fecha_inicio' value=''>");
                }
                %>
											 </div>
											</div>
										  <div class="form-group">
											 <label class="col-md-2 control-label" for="fecha_fin">Fecha Termino:</label> 
											 <div class="col-md-10">
                <%
                if(calIdEv>0) {
                String [] aux2 = fechaFin.split(" ");
                fechaFin = aux2[0]+"T"+aux2[1];
                out.println("<input class='form-control' autocomplete='off' type='datetime-local' id='fecha_fin' value='"+fechaFin+"'>");
                }else{
            
                out.println("<input class='form-control' autocomplete='off' type='datetime-local' id='fecha_fin' value=''>");
                }
                %>
											 </div>
											</div>
										  <div class="form-group">
											 <label class="col-md-2 control-label" for="e4">Calendario:</label> 
											 <div class="col-md-10">
								<select id="e4" class="col-md-12" name="ciclo">    
                <%
                        ps = con.prepareStatement("SELECT name, cal_idCalSchool"
                                 + " FROM CalSchool;");
                        
                        rs = ps.executeQuery();
                        out.println("<option id='0' value='' selected>Selecciona ..</option>");
                        while(rs.next()){
                            if(rs.getInt("cal_idCalSchool")== name){
                                 out.println("<option id='"+rs.getInt("cal_idCalSchool")+
                                         "' selected>"+rs.getString("name")+"</option>");
                             }else{
                                 out.println("<option id='"+rs.getInt("cal_idCalSchool")+"'>"+
                                         rs.getString("name")+"</option>");
                             }
                         }
                         out.println("");
            %>
            </select>											
											 </div>
											</div>
										  <div class="form-group">
											 <label class="col-md-2 control-label" for="e5">Status:</label> 
											 <div class="col-md-10">
								<select id="e5" class="col-md-12" name="status">s
            <%         
                String sta[]={"ACTIVO","INACTIVO","EN REVISION"};
                
                for(int i=0;i<3;i++){
                    if(status.equals(sta[i])){
                        out.println("<option id='"+sta[i]+"' selected>"+sta[i]+"</option>"); 
                    }else{
                        out.println("<option id='"+sta[i]+"'>"+sta[i]+"</option>"); 
                    }
                }
								db.close();
            %>
                </select>											
											 </div>
											</div>
								<input type="hidden" id="hide" value="<%out.println(calIdEv);%>">
									   </form>
									</div>
								</div>
								<!-- /BOX -->
							</div>
						</div>
						<!-- /ADVANCED -->
						<div class="footer-tools">
							<span class="go-top">
								<i class="fa fa-chevron-up"></i> Top
							</span>
						</div>
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	<script src="../js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="../bootstrap-dist/js/bootstrap.min.js"></script>
	<!-- DATE RANGE PICKER -->
	<script src="../js/bootstrap-daterangepicker/moment.min.js"></script>
	<script src="../js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
	<!-- SLIMSCROLL -->
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
	<!-- BLOCK UI -->
	<script type="text/javascript" src="../js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
	<!-- TYPEHEAD -->
	<script type="text/javascript" src="../js/typeahead/typeahead.min.js"></script>
	<!-- AUTOSIZE -->
	<script type="text/javascript" src="../js/autosize/jquery.autosize.min.js"></script>
	<!-- COUNTABLE -->
	<script type="text/javascript" src="../js/countable/jquery.simplyCountable.min.js"></script>
	<!-- INPUT MASK -->
	<script type="text/javascript" src="../js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
	<!-- FILE UPLOAD -->
	<script type="text/javascript" src="../js/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
	<!-- SELECT2 -->
	<script type="text/javascript" src="../js/select2/select2.min.js"></script>
	<!-- UNIFORM -->
	<script type="text/javascript" src="../js/uniform/jquery.uniform.min.js"></script>
	<!-- JQUERY UPLOAD -->
	<!-- The Templates plugin is included to render the upload/download listings -->
	<script src="../js/blueimp/javascript-template/tmpl.min.js"></script>
	<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="../js/blueimp/javascript-loadimg/load-image.min.js"></script>
	<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script src="../js/blueimp/javascript-canvas-to-blob/canvas-to-blob.min.js"></script>
	<!-- blueimp Gallery script -->
	<script src="../js/blueimp/gallery/jquery.blueimp-gallery.min.js"></script>
	<!-- The basic File Upload plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload.min.js"></script>
	<!-- The File Upload processing plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-process.min.js"></script>
	<!-- The File Upload image preview & resize plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-image.min.js"></script>
	<!-- The File Upload audio preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-audio.min.js"></script>
	<!-- The File Upload video preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-video.min.js"></script>
	<!-- The File Upload validation plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-validate.min.js"></script>
	<!-- The File Upload user interface plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-ui.min.js"></script>
	<!-- The main application script -->
	<script src="../js/jquery-upload/js/main.js"></script>
	<!-- bootbox script -->
	<script src="../js/bootbox/bootbox.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="../js/script.js"></script>
	<script src="../js/jquery-validate/jquery.validate.js"></script>
	<script src="../js/jquery-validate/additional-methods.js"></script>
	<script src="../js/error_message.js"></script>
	<script>
		jQuery(document).ready(function() {		
			App.setPage("forms");  //Set current page
			App.init(); //Initialise plugins and elements
			$("#photo").attr("src","getPhoto?safe_IdUser="+<% if(user != null){out.print(user.getSafe_IdUser());} %>);
			var addOrEdit=<%out.println(request.getParameter("addOrEdit"));%>;
			
			
			$("#frm_evaluaciones").validate({
				rules:{
					clave:{
						required: true, minlength: 5, maxlength: 15
					},
					parcial:{
						required: true
					},
					aplic:{
						required: true
					},
					tipo:{
						required: true
					},
					fecha_inicio:{
						required: true
					},
					fecha_fin:{
						required: true
					},
					ciclo:{
						required: true
					},
					status:{
						required: true
					}
				},
				highlight: function(element){
					$(element).parent().parent().addClass("has-error");
				},
				unhighlight: function(element){
				$(element).parent().parent().removeClass("has-error");
			}});
			
	                   var clave=$("#clave");
                     var parcial=$("#e1");
                     var aplic=$("#e2");
                     var tipo=$("#e3");
                     var fecha_inicio=$("#fecha_inicio");
                     var fecha_fin=$("#fecha_fin");
                     var ciclo=$("#e4");
                     var status=$("#e5");
                     var IdEv=$("#hide");

	 
			
			$("#nuevo").click(function () {
				window.location = "CalEvaluations_catalog.jsp?calIdEvaluation=0&addOrEdit=0";
			});
			$("#editar").click(function () {
                            clave.removeAttr("disabled");
                            parcial.removeAttr("disabled");
                            aplic.removeAttr("disabled");
                            tipo.removeAttr("disabled");
                            fecha_inicio.removeAttr("disabled");
                            fecha_fin.removeAttr("disabled");
                            ciclo.removeAttr("disabled");
                            status.removeAttr("disabled");

				addOrEdit=1;
				$("#editar").attr("disabled","disabled");
			});
			$("#guardar").click(function () {
				$("#frm_evaluaciones").valid();

                         function dateValid(date){
                             var aux = date.split("T");
                             var result = aux[0]+" "+aux[1]+":00";
                             return result;
                         }


                        clave.attr("disabled","disabled");
                        parcial.attr("disabled","disabled");
                        aplic.attr("disabled","disabled");
                        tipo.attr("disabled","disabled");
                        fecha_inicio.attr("disabled","disabled");
                        fecha_fin.attr("disabled","disabled");
                        ciclo.attr("disabled","disabled");
                        status.attr("disabled","disabled");
                        
                         var claveval =clave.val();
                         var parcialval=parcial.children(":selected").attr("id");
                         var aplicval=aplic.children(":selected").attr("id");
                         var tipoval=tipo.children(":selected").attr("id");
                         var fecha_inicioval=dateValid(fecha_inicio.val());
                         var fecha_finval=dateValid(fecha_fin.val());
                         var cicloval=ciclo.children(":selected").attr("id");
                         var statusval=status.children(":selected").attr("id");
												 
					var values= "values="+parseInt($("#hide").val())+"#"+parseInt(cicloval)+"#"+claveval+"#"+parseInt(parcialval)+"#"+
                                 aplicval+"#"+fecha_inicioval+"#"+fecha_finval+"#"+tipoval+"#"+statusval+"&addOrEdit="+addOrEdit;


					dhtmlxAjax.post("CalEvaluations_save",values,function(loader){
						var resp = loader.xmlDoc.responseText;
						respo = resp.split("#");
						document.getElementById("hide").value=respo[0];
						var resp = parseInt(respo[1]);
						if(resp == "1"){
							bootbox.alert("Se guardo con exito!", function() {});
						} else {
							bootbox.alert("Fallo al guardar!", function() {});
						}
					});
					$("#editar").removeAttr("disabled");
			});
		});
	</script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
			}else{
				response.sendRedirect("error600.jsp");
			}
		}else{
			response.sendRedirect("error600.jsp");
		}
%>