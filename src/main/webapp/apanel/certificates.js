/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery(document).ready(function() {
    App.setPage("forms");  //Set current page
    App.init(); //Initialise plugins and elements
    $("#photo").attr("src","getPhoto?safe_IdUser="+safe_IdUser);
    var response = "../getAdminView?sql=select * FROM VIW012 &id=dm_IdCertificate&parameters=dm_IdCertificate,code,name,firstLast,secondLast,program,plan,book,sheet,date,type,status";
    var IDRow;            
    grid = new dhtmlXGridObject('grid_container');
    grid.setImagePath("../dhtmlx/dhtmlxGrid/codebase/imgs/");
    grid.setHeader("FOLIO,Matricula,Nombre,Paterno,Materno,Programa,plan,Libro,Hoja,fecha,Tipo,Estatus");
    grid.attachHeader("#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter");
    grid.setInitWidths("*,*,*,*,*,*,*,*,*,*,*,*");
    grid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left");
    grid.setColSorting("str,str,str,str,str,str,str,str,str,str,str,str");
    grid.setEditable(false);
    grid.init();
    grid.setSkin("clear");
    grid.enableSmartRendering(true,50);
    grid.objBox.style.overflowX = "hidden";
    grid.load(response,function(){});
    
    
    grid.attachEvent("onRowSelect",function(id){
        IDRow = id;
        $("#btn_print").removeAttr("disabled");
    });

    $('#btn_nuevo').click(function(){
        var box = bootbox.dialog({
            title: "<h4>Nuevo Certificado</h4>",
            message: '<div class="row">' +
                '<div class="col-md-12"> ' +
                '<form class="form-horizontal" name="frm_nuevo"  id="frm_nuevo"> ' +
                '<div class="form-group"> ' +
                '<label class="col-md-2 control-label" for="user_IdStudent">Alumno</label> ' +
                '<div class="col-md-10"> ' +
                '<select id="user_IdStudent" class="col-md-12"  name="user_IdStudent"></select>' +
                '</div> ' +
                '</div> ' +
                '<div class="form-group"> ' +
                '<label class="col-md-2 control-label" for="book">Libro:</label> ' +
                '<div class="col-md-4"> ' +
                '<input type="text" id="book" name="book" class="form-control" autocomplete="off" />' +
                '</div> ' +
                '<label class="col-md-2 control-label" for="sheet">Hoja:</label> ' +
                '<div class="col-md-4"> ' +
                '<input type="text" id="sheet" name="sheet" class="form-control" autocomplete="off" />' +
                '</div> ' +
                '</div> ' +
                '<div class="form-group"> ' +
                '<label class="col-md-2 control-label" for="date">Fecha</label> ' +
                '<div class="col-md-10"> ' +
                '<input type="text" id="date" name="date" class="form-control" autocomplete="off" data-mask="99/99/9999" /><span class="help-block">dd/mm/yyyy</span>' +
                '</div> ' +
                '</div> ' +
                '<div class="form-group"> ' +
                '<label class="col-md-2 control-label" for="type">Tipo</label> ' +
                '<div class="col-md-10"> ' +
                '<select id="type" class="col-md-12"  name="type"><option>COMPLETO</option><option>PARCIAL</option></select>' +
                '</div> ' +
                '</div> ' +
                '</form> </div></div>',
            buttons: {
                success: {
                    label: "Guardar",
                    className: "btn-danger",
                    callback: function () {
                        $("#frm_nuevo").validate({
                            rules:{
                                user_IdStudent:{required: true},
                                book:{required: true},
                                sheet:{required: true},
                                date:{required: true},
                                type:{required: true}
                            },
                            highlight: function(element){$(element).parent().parent().addClass("has-error");},
                            unhighlight: function(element){$(element).parent().parent().removeClass("has-error");}
                        });
                        if($("#frm_nuevo").valid()){
                            var user_IdStudent = $('#user_IdStudent').children(":selected").attr("value");
                            var book = $('#book').val().trim();
                            var sheet = $('#sheet').val().trim();
                            var date = $('#date').val().trim();
                            var type = $('#type').val();
                            var save = $.ajax({url:"Certificate_save",data:"user_IdStudent="+user_IdStudent+"&book="+book+"&sheet="+sheet+"&date="+date+"&type="+type,method:"POST"});
                            save.done(function(msn){
                                if(msn === '' || msn === '0'){
                                    bootbox.alert("No se pudo guardar! ", function() {});
                                }else{
                                    grid.clearAndLoad(response);
                                }
                            });
                        }else{this.stopPropagation();}
                    }
                },
                'Danger!': {
                    label: "Cancelar",
                    className: "btn-danger",
                    callback: function() {}
                }
            }
        });
        box.on("shown.bs.modal", function() {
            var students = $.ajax({url:"select_students",data:"sql=SELECT US.user_IdStudent, SU.firstLast, SU.secondLast, SU.name   FROM SafeUsers AS SU, UserStudents AS US WHERE SU.safe_IdUser = US.safe_IdUser ORDER BY SU.firstLast, SU.secondLast, SU.name; &id="+(Math.round(Math.random() * (5-1000)) + 5),method:"POST"});
            students.done(function(msn){
                $("#user_IdStudent").html(msn);
            });
            $('#user_IdStudent').parents('.bootbox').removeAttr('tabindex');
            //$('#user_IdStudent').select2();
            $('#type').select2();
        });
    });

    $("#btn_editar").click(function(){
        $('#box_editar').modal({show:true});
    });
    
    $("#btn_print").click(function(){
        $('iframe').attr("src","GetPdfCertificate?dm_IdCertificate="+IDRow);
        $('#box_print').modal({show:true});
    });
});
