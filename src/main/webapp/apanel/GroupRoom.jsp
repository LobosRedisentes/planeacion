<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.edcore.model.db.DBConnector"%>
<%@page import="java.sql.ResultSet"%>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Ocupación de aulas</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
    <meta name="description" content="Sistema educativo">
    <meta name="author" content="Instituto Tecnologico Superior de Zapopan">	
    <link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
    <link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
    <link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
    <!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    <!-- TABLE CLOTH -->
    <link rel="stylesheet" type="text/css" href="../js/tablecloth/css/tablecloth.min.css" />
    <%
        ResultSet rs;
        DBConnector db = new DBConnector();
        PreparedStatement ps;
        Connection con;
        int org_IdRoom = Integer.parseInt(request.getParameter("org_IdRoom"));
        int cal_IdCalSchool = Integer.parseInt(request.getParameter("cal_IdCalSchool"));
        String room = "";
    %>
</head>
<body>
    <div class="row">
        <div class="col-md-12"> 
            <!-- BOX -->      
            <div class="box border inverse">
                <div class="box-title">
                    <h4><i class="fa fa-table"></i>Disponibilidad  <%= request.getParameter("aula")%></h4>
                </div>
                <div class="box-body">
                    <%
                        con=db.open();
                        ps = con.prepareStatement("SELECT * FROM VADM008 WHERE cal_IdCalSchool = ? AND (RIDLU = ? OR RIDMA = ? OR RIDMI = ? OR RIDJU = ? OR RIDVI = ? OR RIDSA = ? ) "
                                + "ORDER BY LU,MA,MI,JU,VI,SA ");
                        ps.setInt(1, cal_IdCalSchool);
                        ps.setInt(2, org_IdRoom);
                        ps.setInt(3, org_IdRoom);
                        ps.setInt(4, org_IdRoom);
                        ps.setInt(5, org_IdRoom);
                        ps.setInt(6, org_IdRoom);
                        ps.setInt(7, org_IdRoom);
                        rs = ps.executeQuery();
                    %>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>GRUPO</th>
                                <th>MATERIA</th>
                                <th>DOCENTE</th>
                                <th>LUN</th>
                                <th>MA</th>
                                <th>MI</th>
                                <th>JU</th>
                                <th>VI</th>
                                <th>SA</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                while(rs != null && rs.next()){
                                    out.print("<tr>");
                                    out.print("<td>"+rs.getString("grupo")+"</td>");
                                    out.print("<td>"+rs.getString("abreviacion")+"</td>");
                                    out.print("<td>"+rs.getString("apellidoPaterno")+" "+rs.getString("apellidoMaterno")+" "+rs.getString("nombre")+"</td>");
                                    
                                    if(rs.getString("LU") != null && rs.getInt("RIDLU") == org_IdRoom){
                                        out.print("<td>"+rs.getString("LU")+"</td>");
                                    }else{
                                        out.print("<td style='background-color: #D0D7E9'></td>");
                                    }
                                    if(rs.getString("MA") != null && rs.getInt("RIDMA") == org_IdRoom){
                                        out.print("<td>"+rs.getString("MA")+"</td>");
                                    }else{
                                        out.print("<td style='background-color: #D0D7E9'></td>");
                                    }
                                    if(rs.getString("MI") != null && rs.getInt("RIDMI") == org_IdRoom){
                                        out.print("<td>"+rs.getString("MI")+"</td>");
                                    }else{
                                        out.print("<td style='background-color: #D0D7E9'></td>");
                                    }
                                    if(rs.getString("JU") != null && rs.getInt("RIDJU") == org_IdRoom){
                                        out.print("<td>"+rs.getString("JU")+"</td>");
                                    }else{
                                        out.print("<td style='background-color: #D0D7E9'></td>");
                                    }
                                    if(rs.getString("VI") != null && rs.getInt("RIDVI") == org_IdRoom){
                                        out.print("<td>"+rs.getString("VI")+"</td>");
                                    }else{
                                        out.print("<td style='background-color: #D0D7E9'></td>");
                                    }
                                    if(rs.getString("SA") != null && rs.getInt("RIDSA") == org_IdRoom){
                                        out.print("<td>"+rs.getString("SA")+"</td>");
                                    }else{
                                        out.print("<td style='background-color: #D0D7E9'></td>");
                                    }
                                    out.print("</tr>");
                                }
                                db.close();
                            %>
                            <tr>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /BOX -->
        </div>
    </div>
    <!-- JAVASCRIPTS -->
    <!-- JQUERY -->
    <script src="../js/jquery/jquery-2.0.3.min.js"></script>
    <!-- JQUERY UI-->
    <script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
    <!-- BOOTSTRAP -->
    <script src="../bootstrap-dist/js/bootstrap.min.js"></script>
    <!-- TABLE CLOTH -->
    <script type="text/javascript" src="js/tablecloth/js/jquery.tablecloth.js"></script>
    <script type="text/javascript" src="js/tablecloth/js/jquery.tablesorter.min.js"></script>
    <!-- CUSTOM SCRIPT -->
    <script src="../js/script.js"></script>
    <script>
        jQuery(document).ready(function() {
            App.setPage("simple_table");  //Set current page
        });
    </script>
    <!-- /JAVASCRIPTS -->
</body>
</html>