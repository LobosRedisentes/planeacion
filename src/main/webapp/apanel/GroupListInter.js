function intercycle_admin(){
    var response = "../getAdminView?table=Intercycles_view&id=group_IdGroupSubject&parameters=grupo,materia,docente,calendario";    
        grid = new dhtmlXGridObject('grid_container');
        grid.setImagePath("../dhtmlx/dhtmlxGrid/codebase/imgs/");
        grid.setHeader("Grupo,Materia,Profesor,Calendario");
        grid.setInitWidths("80,*,200,150,");
        grid.setColAlign("left,left,left,left");
        grid.setColSorting("str,str,str,str");
        grid.setEditable(false);
        grid.init();
        grid.setSkin("clear");
				grid.objBox.style.overflowX = "hidden";
        grid.enableSmartRendering(true,50);
        spInit();
        grid.load(response,function(){
            spStop();
            columnAutoSize();
        });
        var materia;
        var docente;
        var IDRow = 0;
        grid.attachEvent("onRowSelect",function(id){
            IDRow = id;
            materia  = grid.cellById(id,1).getValue();
            docente = grid.cellById(id,2).getValue();
						$("#editar").removeAttr('disabled','disabled');
        });
        $("#editar").click(function(){
           window.location = "GroupListInter_catalog.jsp?group="+IDRow+"&addOrEdit=1&subject="+materia+"&docente="+docente;
        });
        $("#update").click(function(){
            spInit();
            grid.clearAndLoad(response,function(){
                spStop();
            });
        });
}

function intercycle_catalog(groupSub){
    var response = "Intercycles_catalog_Aaux?groupSub="+groupSub+"&evaluation=1";
        grid = new dhtmlXGridObject('grid_container');
        grid.setImagePath("../dhtmlx/dhtmlxGrid/codebase/imgs/");
        grid.setHeader("No,Matricula,Alumno,Calificacion,Opcion");
        grid.setInitWidths("80,100,*,100,150");
        grid.setColAlign("left,left,left,left,left");
        grid.setColSorting("str,str,str,str,str");
        grid.setEditable(true);
        grid.init();
        grid.setSkin("clear");
				grid.objBox.style.overflowX = "hidden";
        spInit();
        grid.load(response,function(){
            spStop();
            columnAutoSize();
        });
}


