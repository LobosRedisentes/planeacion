<%-- 
    Document   : Addr_Locations_Catalog_Aux
    Created on : 25/11/2013, 09:05:08 PM
    Author     : Luis Briseño
--%>
<%@page import="com.edcore.model.db.DBConnector"%>
<%--
    This document get the id of the state from the parameter 'idState'
    and returns the towns of it in options tag format 
--%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    DBConnector db = new DBConnector();
    PreparedStatement ps;
    ResultSet rs;
    //gets the parameter 'idState' which contains the id of the state
    int state=Integer.parseInt(request.getParameter("idState"));
    Connection con=db.open();
    //gets the list of the State's towns from the database
     ps = con.prepareStatement("SELECT name, addr_IdState, addr_IdTown "
             + "FROM AddrTown WHERE addr_IdState="+state+";");
     rs = ps.executeQuery();
     //print the option tags
     while(rs.next()){
             out.println("<option id='"+rs.getInt("addr_IdTown")+"'>"
                     +rs.getString("name")+"</option>");
       }
     db.close();
%>
