<%@page import="mx.com.edcore.escolar.conf.Global"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>edcore - Escolar</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
        <meta name="description" content="edcore - Escolar">
        <link rel="icon" type="image/png" sizes="32x32" href="../../img/favicon-32x32.png">
        <meta name="author" content="Instituto Tecnologico Superior de Zapopan">	
        <link rel="stylesheet" type="text/css" href="../../lib/cloudAdmin/css/cloud-admin.css" >
        <link rel="stylesheet" type="text/css" href="../../lib/cloudAdmin/css/default.css" id="skin-switcher" >
        <link rel="stylesheet" type="text/css" href="../../lib/cloudAdmin/css/responsive.css" >
        <link rel="stylesheet" type="text/css" href="../../css/edcore.css" >
        <link rel="stylesheet" type="text/css" href="../../font/awesome/css/font-awesome.min.css">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    </head>
    <body>

        <div class="col-md-12" style="display: block; margin-left: 5.2%; width: 91%; height: 130px; background-image: url('../../img/header-logo.png'); background-size:contain; background-repeat: no-repeat; background-repeat: round;" >
        </div>
        <div class="col-md-12" style="display: block; margin-left: -1%; width: 95%;" >
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-md-offset-4">
                        <div class="login-box-plain">
                            <p style="text-align: center"><img src="../../img/favicon-32x32.png" /><strong> &nbsp;&nbsp;SISTEMA ESCOLAR EDCORE<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Panel Administrativo</strong></p><br>
                            <form role="form" name="frm" id="frm">
                                <div class="form-group">
                                    <label for="correo">Correo electrónico</label>
                                    <i class="fa fa-envelope"></i>
                                    <input type="email" class="form-control" id="email" name="email" >
                                </div>
                                <div class="form-actions">
                                    <button id="btn_passwd" class="btn btn-default">Recuperar contraseña</button>
                                </div>
                            </form>
                            <div class="login-helpers">
                                Introduce tu email para poder recuperar tu password.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12"  >
            <p  style="text-align: center">Copyright &copy; edcore 2015 Instituto Tecnológico Superior de Zapopan<br>Todos los derechos reservados.</p>
        </div>
        <!-- JAVASCRIPTS -->
        <script src="../../lib/jquery/jquery-2.0.3.min.js"></script>
        <script src="../../lib/bootstrap-dist/js/bootstrap.min.js"></script>
        <script src="../../lib/bootbox/bootbox.min.js"></script>
        <script src="../../lib/jquery-validate/jquery.validate.js"></script>
        <script src="../../lib/jquery-validate/additional-methods.js"></script>
        <script src="../../lib/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
        <script src="../../lib/cloudAdmin/js/script.js"></script>
        <script src="../../js/edcore.js"></script>
        <script src="../../js/spin.js"></script>
        <script src="../../js/exceptions.js"></script>
        <script src="recuperarPassword.js"></script>
    </body>
</html>
