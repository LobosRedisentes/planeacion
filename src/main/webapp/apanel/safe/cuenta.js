jQuery(document).ready(function () {
    App.setPage("form");  //Set current page
    App.init(); //Initialise plugins and element
    $.getScript("../js/messages.js");
    $("#photo").attr("src", "../../users/getPhoto?mode=usr");
    $("#photo2").attr("src", "../../users/getPhoto?mode=usr");
    
    var account = $.ajax({url: "../../safe/getAccountFrm", data: "mode=usr", method: "POST"});
    account.done(function (msn) {
        $("#code").val(msn['code']);
        $("#name").val(msn['name'] + " " + msn['firstLast'] + " " + msn['secondLast']);
        $("#email").val(msn['email']);
        $("#passwd").val("000");
        $("#rol").val(msn['rol']);
    });
    
    $.getScript("../../js/validate/form.js", function (data, textStatus, jqxhr) {
        $("#email").rules("add", {required: true, maxlength: 100, minlength: 5});
        $("#passwd").rules("add", {required: true, minlength: 3, maxlength: 15});
    });
    
    $("#btn_edit").click(function () {
        $("#btn_save").removeAttr("disabled");
        $("#btn_edit").attr( "disabled", "disabled");
        $("#email").removeAttr("disabled");
        $("#passwd").removeAttr("disabled");
    });
    
    $("#btn_save").click(function (event) {
        if ($("#frm").valid()) {
            spInit();
            var email = $('#email').val();
            var passwd = $('#passwd').val();
            var save = $.ajax({url: "../../safe/setAccount", data: "mode=usr&opc=update&sendEmail=true&email=" + email + "&passwd=" + passwd, method: "POST"});
            save.done(function (msn) {
                spStop();
                if(msn === "CORE0000"){
                    $("#btn_save").attr( "disabled", "disabled");
                    $("#email").attr( "disabled", "disabled");
                    $("#passwd").attr( "disabled", "disabled");
                    $("#btn_edit").removeAttr("disabled");
                    bootbox.alert("Transacci&oacute;n exitosa!.", function () {});
                }else{
                    throwException(msn);
                }
            });
            save.error(function () {
                spStop();
                bootbox.alert("Al parecer se presento un problema de conexi&oacute;n, intente refrescar la pagina. ", function () {
                });
            });
            event.preventDefault();
        }
    });
});