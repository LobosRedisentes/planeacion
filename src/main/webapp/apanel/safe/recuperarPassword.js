jQuery(document).ready(function() {

    $.getScript("../../js/validate/form.js", function (data, textStatus, jqxhr) {
        $("#email").rules("add", {required: true, maxlength: 100, minlength: 5});
    });
    $("#btn_passwd").click(function (event) {
        if($("#frm").valid()){
            var email = $('#email').val();
            var save = $.ajax({url:"../../safe/recoverPassword",data:"email="+email,method:"POST"});
            spInit();
            save.done(function(msn){
                spStop();
                if(msn === "CORE0000"){
                    bootbox.alert("Los datos de tu cuenta fueron enviados a tu correo", function() {window.location = "login.jsp";});
                }else{
                    throwException(msn);
                }
            });
            save.error(function(msn){
                spStop();
                bootbox.alert("Al parecer se presento un problema de conexi&oacute;n, intente mas tarde! ", function() {});
            });
            event.preventDefault();
        }
    });
});