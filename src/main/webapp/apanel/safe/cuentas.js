jQuery(document).ready(function () {
    App.setPage("index");  //Set current page
    App.init(); //Initialise plugins and element
    $.getScript("../js/messages.js");
    $("#photo").attr("src", "../../users/getPhoto?mode=usr");
    
    var table = $('#datatable').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sort": "position",
        "sAjaxSource": "../../safe/getAccountsGrid",
        "aoColumns": [
            { "mData": "id", "bSortable": false },
            { "mData": "code" },
            { "mData": "name" },
            { "mData": "rol" },
            { "mData": "type" },
            { "mData": "office" },
            { "mData": "status" }
        ]
    }).columnFilter({ sPlaceHolder: "head:after", aoColumns: [
            null,
            {type: "text"},
            {type: "text"},
            {type: "text"},
            {type: "text"},
            {type: "text"},
            {type: "select", values: [ 'ACTIVO', 'INACTIVO']}
        ]
    });

    $('#datatable tbody').on('click', 'tr', function () {
        id = $('td', this).eq(0).text();
        $("#btn_edit").removeAttr("disabled");
        $("#btn_new").removeAttr("disabled");
    } );
    
    $("#btn_edit").click(function(){
        $('#box_new').modal({show:true});
        var account = $.ajax({url: "../../safe/getAccountFrm", data: "mode=ADM&id="+id, method: "POST"});
        account.done(function (msn) {
            $("#code").val(msn['code']);
            $("#name").val(msn['name'] + " " + msn['firstLast'] + " " + msn['secondLast']);
            $("#email").val(msn['email']);
            $("#passwd").val("000");
            $("#rol").val(msn['rol']);
        });
    });
    $("#btn_new").click(function(){

    });
    
});