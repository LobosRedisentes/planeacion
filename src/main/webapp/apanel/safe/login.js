jQuery(document).ready(function () {

    $.getScript("../../js/validate/form.js", function (data, textStatus, jqxhr) {
        $("#email").rules("add", {required: true, maxlength: 100, minlength: 5});
        $("#passwd").rules("add", {required: true, minlength: 3, maxlength: 15});
    });

    $("#btn_login").click(function (event) {
        if ($("#frm").valid()) {
            spInit();
            var email = $('#email').val();
            var passwd = $('#passwd').val();
            var save = $.ajax({url: "../../safe/login", data: "email=" + email + "&passwd=" + passwd+"&panel=APANEL", method: "POST"});
            save.done(function (msn) {
                spStop();
                if(msn === "CORE0000"){
                    window.location = "../index.jsp";
                }else{
                    throwException(msn);
                }
            });
            save.error(function () {
                spStop();
                bootbox.alert("Al parecer se presento un problema de conexi&oacute;n, intente refrescar la pagina. ", function () {});
            });
            event.preventDefault();
        }
    });
});