<%@page import="com.edcore.model.safe.User"%>
<%@page import="com.edcore.model.db.DBConnector"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.ArrayList"%>
<%
    HttpSession sesion;
    sesion = request.getSession();
    User user = (User) sesion.getAttribute("user");
    if(user != null){
        ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
        if(privileges.contains("CORE")){
%>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Ficha de Examen</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
    <link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
    <link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
    <link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
    <!-- TYPEAHEAD -->
    <link rel="stylesheet" type="text/css" href="../js/typeahead/typeahead.css" />
    <!-- SELECT2 -->
    <link rel="stylesheet" type="text/css" href="../js/select2/select2.min.css" />
    <!-- FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <style>
        .error-message, label.error {
            color: #ff0000;
            margin:0;
            display: inline;
            font-size: 1em !important;
            font-weight:bold;
        }
    </style>
    <!-- LIBRERIA EDCORE -->
    <link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxLayout/codebase/dhtmlxlayout.css">
    <link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxLayout/codebase/skins/dhtmlxlayout_dhx_blue.css">
    <link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxWindows/codebase/dhtmlxwindows.css">
    <link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxWindows/codebase/skins/dhtmlxwindows_dhx_blue.css">
    <link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.css" >
    <script src="../dhtmlx/libCompiler/dhtmlxcommon.js"></script>
    <script type="text/javaScript"  src="../dhtmlx/dhtmlxLayout/codebase/dhtmlxcontainer.js"></script>
    <script type="text/javaScript"  src="../dhtmlx/dhtmlxLayout/codebase/dhtmlxlayout.js"></script>
    <script type="text/javaScript"  src="../dhtmlx/dhtmlxWindows/codebase/dhtmlxwindows.js"></script>
    <script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.js"></script>
    <script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_srnd.js"></script>
    <script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_filter.js"></script>
    <script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgridcell.js"></script>
    <script src="../dhtmlx/dhtmlxDataProcessor/codebase/dhtmlxdataprocessor.js"></script>
    <script src="../dhtmlx/libCompiler/connector.js"></script>
    <script>
        var dhxWins;
        var win;
        function print(id){
            dhxWins= new dhtmlXWindows();
            win = dhxWins.createWindow("printConst", 0,0,900, 470);
            dhxWins.window("printConst").centerOnScreen();
            dhxWins.window("printConst").setText("Imprimir ficha de examen");
            dhxWins.window("printConst").denyResize();
            dhxWins.setSkin("dhx_blue");
            dhxWins.window("printConst").bringToTop();
            dhxWins.window("printConst").stick();
            dhxWins.window("printConst").setModal(true);
            win.attachURL("GetPdfFicha?user_IdApplicant="+id, "AJAX");
        }
</script>
</head>
<body>
<%
    DBConnector db = new DBConnector();
    ResultSet rs;
    PreparedStatement ps;
    Connection con;
    int addOrEdit= Integer.parseInt(request.getParameter("addOrEdit"));
    int user_IdApplicant= Integer.parseInt(request.getParameter("user_IdApplicant"));
    con=db.open();    
    int safeIdUser = 0;
    int prog_IdProgram = 0;
    int cal_IdCalSchool = 0;
    int ins_IdExamRoom =0;
    int sch_IdTurn = 0;
    String type = "";

    if(user_IdApplicant > 0){
        ps = con.prepareStatement("SELECT UA.safe_IdUser, UA.prog_IdProgram, UA.cal_IdCalSchool,UA.type,UA.sch_IdTurn, IA.ins_IdExamRoom FROM UserApplicant AS UA INNER JOIN InsApplications as IA ON UA.user_IdApplicant = IA.user_IdApplicant "
                + " WHERE UA.user_IdApplicant = ?");
        ps.setInt(1, user_IdApplicant);
        rs = ps.executeQuery();
            
        while(rs != null && rs.next()){
            safeIdUser = rs.getInt("safe_IdUser");
            prog_IdProgram = rs.getInt("prog_IdProgram");
            cal_IdCalSchool = rs.getInt("cal_IdCalSchool");
            if(rs.getString("type") != null){
                type = rs.getString("type");
            }
            ins_IdExamRoom = rs.getInt("ins_IdExamRoom");
            sch_IdTurn = rs.getInt("sch_IdTurn");
        }
    }
%>
    <!-- HEADER -->
    <%@include file="header.jsp" %>
    <!--/HEADER -->
    <!-- PAGE -->
    <section id="page">
        <!-- SIDEBAR -->
        <%@include file="sidebar.jsp" %>
        <!-- /SIDEBAR -->
        <div id="main-content">
            <div class="container">
                <div class="row">
                    <div id="content" class="col-lg-12">
                        <!-- PAGE HEADER-->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-header">
                                    <!-- BREADCRUMBS -->
                                    <ul class="breadcrumb">
                                        <li>
                                            <i class="fa fa-home"></i>
                                            <a href="index.jsp">Inicio</a>
                                        </li>
                                        <li>
                                            <i class="fa"></i>
                                            <a href="#">Inscripciones</a>
					</li>
                                        <li>
                                            <i class="fa"></i>
                                            <a href="#">Examen de admisión</a>
					</li>
                                        <li>
                                            <i class="fa"></i>
                                            <a href="InsRequest_admin.jsp">Fichas</a>
					</li>
                                        <li>Nueva</li>
                                    </ul>
                                    <!-- /BREADCRUMBS -->
                                    <div class="clearfix">
                                        <h3 class="content-title pull-left">Nueva ficha de examen</h3>
                                        <span class="date-range pull-right">
                                            <p class="btn-toolbar">
                                                <%if(privileges.contains("CORE")){%>
                                                    <button id="editar" class="btn btn-danger" disabled><i class="fa fa-pencil-square-o"></i> Editar</button>
                                                <%}%>
                                                <%if(privileges.contains("CORE")){%>
                                                    <button id="guardar" class="btn btn-danger" ><i class="fa fa-save"></i> Guardar</button>
                                                <%}%>
                                                <button id="print" class="btn btn-danger"><i class="fa fa-print-square-o"></i> Imprimir</button>
                                            </p>
                                        </span>
                                    </div>
                                    <div class="description"></div>
                                </div>
                            </div>
                        </div>
                        <!-- /PAGE HEADER -->
                        <!-- ADVANCED -->
                        <form role="form" class="form-horizontal" action="" method="post" name="frm_datosGenerales" id="frm_datosGenerales">
                            <div class="box border red col-lg-12">
                                <div class="box-title">
                                    <h4>Nueva ficha</h4>
                                    <div class="tools hidden-xs"></div>
                                </div>
                                <div class="box-body">
                                    <div class="tabbable header-tabs">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a id="tab1" href="#box_tab3" data-toggle="tab"> <span class="hidden-inline-mobile"><i class="fa fa-male"></i></span></a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane fade in active" id="box_tab3">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <center><img height="150px" width="150px" class="img-circle" name="photo2" id="photo2" src="" alt="" /></center>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label" for="e4">Aspirante</label>
                                                            <div class="col-sm-9">
                                                            <%
                                                                ps = con.prepareStatement("SELECT * FROM UserApplicants_view");
                                                                rs = ps.executeQuery();
                                                                if(user_IdApplicant != 0){
                                                                    out.println("<select id='e4' class='col-md-12' name='aspirante' disabled>");
                                                                }else{
                                                                    out.println("<select id='e4' class='col-md-12' name='aspirante'>");
                                                                }
                                                                out.println("<option id='0' value='' selected>Selecciona ..</option>");
                                                                while(rs!= null && rs.next()){
                                                                    if(rs.getInt("user_IdApplicant")== (user_IdApplicant)){
                                                                        out.println("<option id='"+rs.getInt("user_IdApplicant")+"' selected>"+rs.getString("paterno")+" "+rs.getString("materno")+" "+rs.getString("nombre")+"</option>");
                                                                    }
                                                                }
                                                                out.println("</select>");
                                                            %>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label" for="e1">Calendario:</label>
                                                            <div class="col-sm-9">
                                                            <%
                                                                ps = con.prepareStatement("SELECT * FROM CalSchool where  status = 'PREPARACION'");
                                                                rs = ps.executeQuery();
                                                                out.println("<select id='e1' class='col-md-12' name='calendario'>");
                                                                out.println("<option id='0' value='' selected>Selecciona ..</option>");
                                                                while(rs!= null && rs.next()){
                                                                    if(rs.getInt("cal_IdCalSchool")== (cal_IdCalSchool)){
                                                                        out.println("<option id='"+rs.getInt("cal_IdCalSchool")+"' selected>"+rs.getString("name")+"</option>");
                                                                    }else{
                                                                        out.println("<option id='"+rs.getInt("cal_IdCalSchool")+"'>"+rs.getString("name")+"</option>");
                                                                    }
                                                                }
                                                                out.println("</select>");
                                                            %>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label" for="e3">Programa de estudios:</label>
                                                            <div class="col-sm-9">
                                                            <%
                                                                ps = con.prepareStatement("SELECT prog_IdProgram, name FROM Programs where status = 'ACTIVO'");
                                                                rs = ps.executeQuery();
                                                                out.println("<select id='e3' name='programa' class='col-md-12' >");
                                                                out.println("<option id='0' value='' selected>Selecciona ..</option>");
                                                                while(rs.next()){
                                                                    if(rs.getInt("prog_IdProgram")==(prog_IdProgram)){
                                                                        out.println("<option id='"+rs.getInt("prog_IdProgram")+"' selected>"+rs.getString("name")+"</option>");
                                                                    }else{
                                                                        out.println("<option id='"+rs.getInt("prog_IdProgram")+"'>"+rs.getString("name")+"</option>");
                                                                    }
                                                                }
                                                                out.println("</select>");
                                                            %>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label" for="type">Tipo de inscripción</label>
                                                            <div class="col-sm-9">
                                                                <select id="type" name="type" class="form-control">
                                                                <%
                                                                    String tipoArray[]={"NUEVO","CONVALIDACIÓN","EQUIVALENCIA","REVALIDACIÓN"};
                                                                    out.println("<option id='0' selected value=''>Selecciona ..</option>");
                                                                    for(int i=0;i<4;i++){
                                                                        if(type.equals(tipoArray[i])){
                                                                            out.print("<option id='"+tipoArray[i]+"' selected >"+tipoArray[i]+"</option>");
                                                                        }else{
                                                                            out.print("<option id='"+tipoArray[i]+"'>"+tipoArray[i]+"</option>");
                                                                        }
                                                                    }
                                                                %>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label" for="e2">Dia y hora de examen:</label>
                                                            <div class="col-sm-9">
                                                            <%
                                                                ps = con.prepareStatement("SELECT * FROM InsExamRooms");
                                                                rs = ps.executeQuery();
                                                                out.println("<select id='e2' class='col-md-12' name='ins_IdExamRoom' disabled >");
                                                                out.println("<option id='0' value='' selected>Selecciona ..</option>");
                                                                while(rs.next()){
                                                                    if(rs.getInt("ins_IdExamRoom")==(ins_IdExamRoom)){
                                                                        out.println("<option id='"+rs.getInt("ins_IdExamRoom")+"' selected>"+rs.getString("date")+"</option>");
                                                                    }else{
                                                                        out.println("<option id='"+rs.getInt("ins_IdExamRoom")+"'>"+rs.getString("date")+"</option>");
                                                                    }
                                                                }
                                                                out.println("</select>");
                                                            %> 
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label" for="lugares">Lugares para el examen:</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" id="limite" name="limite" class="form-control" autocomplete="off" value="0" disabled />
                                                            </div>
							</div>
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label" for="turn">Turno a inscribir</label>
                                                            <div class="col-sm-9">
                                                                <%
                                                                    ps = con.prepareStatement("SELECT * FROM ScheduleTurns WHERE name = 'MATUTINO' OR name = 'VESPERTINO'");
                                                                    rs = ps.executeQuery();
                                                                    out.println("<select id='sch_IdTurn' class='form-control' name='sch_IdTurn' >");
                                                                    out.println("<option id='0' value='' selected>Selecciona ..</option>");
                                                                    while(rs.next()){
                                                                        if(rs.getInt("sch_IdTurn")==(sch_IdTurn)){
                                                                            out.println("<option id='"+rs.getInt("sch_IdTurn")+"' selected>"+rs.getString("name")+"</option>");
                                                                        }else{
                                                                            out.println("<option id='"+rs.getInt("sch_IdTurn")+"'>"+rs.getString("name")+"</option>");
                                                                        }
                                                                    }
                                                                    out.println("</select>");
                                                                %>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- /ADVANCED -->
                        <div class="footer-tools">
                            <span class="go-top">
                                <i class="fa fa-chevron-up"></i> Top
                            </span>
                        </div>
                    </div><!-- /CONTENT-->
                </div>
            </div>
        </div>
    </section>
    <!--/PAGE -->
    <!-- JAVASCRIPTS -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- JQUERY -->
    <script src="../js/jquery/jquery-2.0.3.min.js"></script>
    <!-- JQUERY UI-->
    <script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
    <!-- BOOTSTRAP -->
    <script src="../bootstrap-dist/js/bootstrap.min.js"></script>
    <!-- TYPEHEAD -->
    <script type="text/javascript" src="../js/typeahead/typeahead.min.js"></script>
    <!-- AUTOSIZE -->
    <script type="text/javascript" src="../js/autosize/jquery.autosize.min.js"></script>
    <!-- INPUT MASK -->
    <script type="text/javascript" src="../js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
    <!-- SELECT2 -->
    <script type="text/javascript" src="../js/select2/select2.min.js"></script>
    <!-- JQUERY UPLOAD -->
    <!-- The basic File Upload plugin -->
    <script src="../js/jquery-upload/js/jquery.fileupload.min.js"></script>
    <!-- bootbox script -->
    <script src="../js/bootbox/bootbox.min.js"></script>
    <!-- COOKIE -->
    <script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
    <!-- CUSTOM SCRIPT -->
    <script src="../js/script.js"></script>
    <script src="../js/jquery-validate/jquery.validate.js"></script>
    <script src="../js/jquery-validate/additional-methods.js"></script>
    <script src="../js/error_message.js"></script>
    <script>
    jQuery(document).ready(function() {		
        App.setPage("forms");  //Set current page
        App.init(); //Initialise plugins and elements
        
        $("#photo").attr("src","getPhoto?safe_IdUser="+<%= user.safe_IdUser %>);
        $("#photo2").attr("src","getPhoto?safe_IdUser="+<% if(safeIdUser == 0){out.print("1");}else{out.print(safeIdUser);}%>);

        $("#type").change(function(){
            var id=$(this).children(":selected").attr("id");
            var calendario = $("#e1").children(":selected").attr("id");
            $('#e2 option:gt(0)').remove();
            if(id == "NUEVO"){ 
                $("#e2").removeAttr("disabled");
                var response= $.ajax({
                    url:"GetAulasExamen",
                    data:"cal_IdCalSchool="+calendario,
                    method:"POST"
                });
                response.done(function(msn){
                    $("#e2").append(msn);
                    $("#limite").val("0");
                });
            }else{
                $("#e2").attr("disabled","disabled");
                $("#limite").val("100");
            }
        });

        $("#e2").change(function(){
            var id=$(this).children(":selected").attr("id");
            var response= $.ajax({
                url:"GetExamAsp",
                data:"ins_IdExamRoom="+id,
                method:"POST"
            });
            response.done(function(msn){
                $("#limite").val(msn);
            });
        });

        $("#frm_datosGenerales").validate({
            ignore: "",
            rules:{
                aspirante:{required: true},
                calendario:{required: true},
                programa:{required: true},
                type:{required: true},
                sch_IdTurn: {required : true}
            },
            highlight: function(element){$(element).parent().parent().addClass("has-error");},
            unhighlight: function(element){$(element).parent().parent().removeClass("has-error");}
        });
        $("#guardar").click(function (event) {
            event.preventDefault();
            if($("#frm_datosGenerales").valid()){
                if($("#type").children(":selected").attr("id") == "NUEVO" && parseInt($("#limite").val()) == 0){
                    bootbox.alert("Lo sentimos pero el dia de aplicación que usted selecciono ya se encuentra lleno!", function() {});
                    return false;
                }else{
                    var values= "values="+$("#e4").children(":selected").attr("id")+"#"+ $("#e1").children(":selected").attr("id")+"#"+ $("#e3").children(":selected").attr("id") +"#"
                            +$("#type").children(":selected").attr("id")+"#"+$("#e2").children(":selected").attr("id")+"#"+$("#limite").val().trim()+"#"+$("#sch_IdTurn").children(":selected").attr("id");
                    alert(values);
                    var response= $.ajax({
                        url:"InsRequest_save",
                        data:values,
                        method:"POST"
                    });
                    response.done(function(msn){
                        alert(msn);
                        if( msn == "1"){
                            bootbox.alert("Se guardo con exito!", function() {});
                            $("#editar").removeAttr("disabled");
                            $("#e1").attr("disabled","disabled");
                            $("#e2").attr("disabled","disabled");
                            $("#e3").attr("disabled","disabled");
                            $("#type").attr("disabled","disabled");
                            $("#sch_IdTurn").attr("disabled","disabled");
                        }else{
                            bootbox.alert("Fallo al guardar!", function() {});
                            return false;
                        }
                    });
                }
            }
        });
        $("#editar").click(function () {
            $("#e1").removeAttr("disabled");
            $("#e3").removeAttr("disabled");
            $("#type").removeAttr("disabled");
            $("#sch_IdTurn").removeAttr("disabled");
        });
	$("#print").click(function () {
            print($("#e4").children(":selected").attr("id"));
        });
    });
    </script>
    <!-- /JAVASCRIPTS -->
</body>
</html>
<%
        con.close();
    }else{
        response.sendRedirect("error600.jsp");
    }
}else{
    response.sendRedirect("error600.jsp");
}
%>