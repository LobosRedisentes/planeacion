<!DOCTYPE html>
<html lang="es">
    <head>
				<meta http-equiv="content-type" content="text/html; charset=UTF-8">
				<meta charset="utf-8">
				<title>Semestres</title>
        <link rel="stylesheet" type="text/css" href="../Horarios/wizard/wizardStyle.css">
        <script type="text/javaScript">
        <!--
            function cancelar()
            {
                parent.dhxWins.window("nuevoGrupo").close();
            }
        -->
        </script>
    </head>
    <body bgcolor="E3EAFA" style=" margin: 0px;">
			  <script type="text/javaScript">
            function enviar()
            {
                parent.dhxWins.window("printConst").setDimension(900,570);
                parent.dhxWins.window("printConst").centerOnScreen();
                form.submit();
            }
        </script>

        <form method="POST" action="GetPdfConsXSem" id="form">
					<input type="hidden" name="user_IdStudent" id="user_IdStudent" value="<%= request.getParameter("user_IdStudent") %>" />
            <table border="0" cellpadding="0px" cellspacing="0px" width="550px" bgcolor="#171F67">
                <tr>
                    <td height="50px">
                        <table width="100%" border="0" cellpadding="0px" cellspacing="0px">
                            <tr>
                                <td width="5%">&nbsp;</td>
                                <td width="80%"><p class="titulo">Selecci�n el rango de Semestres</p></td>
                                <td width="15%" align="center"><img src=".../icons/calendar.png" width="30px" height="30px"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#E3EAFA" height="190px" valign="top">
                        <table border="0" width="100%" cellpadding="10px" cellspacing="10px">
                            <tr>
                                <td>
                                    <p class="descripcion">Porfavor seleccione los semestres a desplegar:</p>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
																	Inicio:
                                    <select name="semesterini" id="semesterini" >
                                        <option selected value="1">1</option>
																				<option  value="2">2</option>
																				<option  value="3">3</option>
																				<option  value="4">4</option>
																				<option  value="5">5</option>
																				<option  value="6">6</option>
																				<option  value="7">7</option>
																				<option  value="8">8</option>
																				<option  value="9">9</option>
                                                                                                                                                                <option  value="10">10</option>
                                                                                                                                                                <option  value="11">11</option>
                                                                                                                                                                <option  value="12">12</option>
                                    </select>
																	&nbsp;&nbsp;&nbsp;&nbsp;Termino:
                                    <select name="semesterfin" id="semesterfin" >                       
																			<option selected value="1">1</option>
																				<option  value="2">2</option>
																				<option  value="3">3</option>
																				<option  value="4">4</option>
																				<option  value="5">5</option>
																				<option  value="6">6</option>
																				<option  value="7">7</option>
																				<option  value="8">8</option>
																				<option  value="9">9</option>
                                                                                                                                                                <option  value="10">10</option>
                                                                                                                                                                <option  value="11">11</option>
                                                                                                                                                                <option  value="12">12</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="30px" bgcolor="#E3EAFA" align="right" valign="top"><input id="siguiente" type="button" value="Siguiente" onclick="enviar()">&nbsp;&nbsp;<input type="button" value="Cancelar" onclick="cancelar()">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>
                    <td height="20px"></td>
                </tr>
            </table>
        </form>
    </body>
</html>
