<%-- 
    Document   : prog_req_subject_catalog_view_aux
    Created on : 9/12/2013, 04:47:12 PM
    Author     : kokuis
--%>

<%@page import="com.edcore.model.db.DBConnector"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>

<%@page contentType="text/xml" pageEncoding="UTF-8"%>
<%
    DBConnector db = new DBConnector();
    PreparedStatement ps;
    ResultSet rs;

    int name=Integer.parseInt(request.getParameter("prog_IdR"));
    int opt =Integer.parseInt(request.getParameter("opt"));
    
    Connection con=db.open();

    if(opt == 0){
        ps = con.prepareStatement("SELECT name, prog_IdSubject FROM ProgSubjects WHERE  prog_IdPlan = "+name+"");
        rs = ps.executeQuery();

        while(rs.next()){
                out.println("<option id='"+rs.getInt("prog_IdSubject")+"'>"
                        +rs.getString("name")+"</option>");
          }
 
    }
    
    if(opt==1){    
        ps = con.prepareStatement("SELECT prog_IdReqSubject FROM ProgReqSubjects WHERE prog_IdBaseSubject ='"+name+"'");
        rs = ps.executeQuery();

        while(rs.next()){
             out.print(rs.getInt("prog_IdReqSubject")+"#");
        }
        out.print("0");
     
    }
    
    db.close();
%>
