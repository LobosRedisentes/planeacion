<%-- 
    Document   : schl_Schools_catalog_Aux
    Created on : 13/12/2013, 01:26:27 PM
    Author     : L01442368
--%>

<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Connection"%>
<%@page import="com.edcore.security.MySqlConexion"%>
<%@page import="java.sql.ResultSet"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    MySqlConexion db = new MySqlConexion();
    PreparedStatement ps;
    ResultSet rs;
    int opt=Integer.parseInt(request.getParameter("opt"));
    //gets the parameter 'idGrade' which contains the id of the state
    if(opt==1){
    int grade=Integer.parseInt(request.getParameter("idGrade"));
    Connection con=db.open();
    //gets the list of the SchoolLevels's towns from the database
     ps = con.prepareStatement("SELECT name, schl_IdLevel "
             + "FROM SchoolLevels WHERE schl_IdGrade="+grade+";");
     rs = ps.executeQuery();
     //print the option tags
     while(rs.next()){
             out.println("<option id='"+rs.getInt("schl_IdLevel")+"'>"
                     +rs.getString("name")+"</option>");
       }
    }else if(opt==2){
     int sLevel=Integer.parseInt(request.getParameter("sLevel"));
    Connection con=db.open();
    //gets the list of the SchoolLevels's towns from the database
     ps = con.prepareStatement("SELECT name, schl_IdSubLevel "
             + "FROM SchoolSubLevels WHERE schl_IdLevel="+sLevel+";");
     rs = ps.executeQuery();
     //print the option tags
     while(rs.next()){
             out.println("<option id='"+rs.getInt("schl_IdSubLevel")+"'>"
                     +rs.getString("name")+"</option>");
       }   
    } else if(opt==3){
        
    Connection con=db.open();
    ps = con.prepareStatement("SELECT name, schl_IdGrade"
                            + " FROM SchoolGrades;");

    rs = ps.executeQuery();
        while(rs.next()){
                    out.println("<option id='"+rs.getString("schl_IdGrade")+"'>"+
                    rs.getString("name")+"</option>");

        }
    }
    
     db.close();
%>