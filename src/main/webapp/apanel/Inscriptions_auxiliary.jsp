<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Connection"%>
<%@page import="com.edcore.model.db.DBConnector"%>
<!DOCTYPE html>
<html lang="es">
    <head>
				<meta http-equiv="content-type" content="text/html; charset=UTF-8">
				<meta charset="utf-8">
				<title>Paquetes</title>
        <link rel="stylesheet" type="text/css" href="../Horarios/wizard/wizardStyle.css">
        <script type="text/javaScript">
        <!--
            function cancelar()
            {
                parent.dhxWins.window("paquetes").close();
            }
        -->
        </script>
    </head>
    <body bgcolor="E3EAFA" style=" margin: 0px;">
			  <script type="text/javaScript">
            function enviar()
            {
                parent.dhxWins.window("paquetes").setDimension(900,570);
                parent.dhxWins.window("paquetes").centerOnScreen();
                form.submit();
            }
        </script>

        <form method="POST" action="Inscription" id="form">
            <table border="0" cellpadding="0px" cellspacing="0px" width="550px" bgcolor="#171F67">
                <tr>
                    <td height="50px">
                        <table width="100%" border="0" cellpadding="0px" cellspacing="0px">
                            <tr>
                                <td width="5%">&nbsp;</td>
                                <td width="80%"><p class="titulo">Selecci�n de paquetes</p></td>
                                <td width="15%" align="center"><img src=".../icons/calendar.png" width="30px" height="30px"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#E3EAFA" height="190px" valign="top">
                        <table border="0" width="100%" cellpadding="10px" cellspacing="10px">
                            <tr>
                                <td>
                                    <p class="descripcion">Porfavor seleccione el paquete a inscribir:</p>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
																	<%
																	DBConnector db = new DBConnector();
																	Connection con;
																	ResultSet rs;
																	PreparedStatement ps;
																	
																	int user_IdApplicant = Integer.parseInt(request.getParameter("user_IdApplicant"));
																	int safe_IdUser = 0;
																	con = db.open();
																	
																	ps = con.prepareStatement("SELECT count(*) AS numReg FROM UserApplicant where user_IdApplicant = ? and status='INSCRITO'");
																	ps.setInt(1, user_IdApplicant);
																	rs = ps.executeQuery();
																	rs.next();
																	if(rs.getInt("numReg") > 0){
																		out.print("<script>alert('EL ALUMNO YA SE ENCUENTRA REGISTRADO');cancelar();</script>");
																	}
																	
																	ps = con.prepareStatement("SELECT concat_ws(' ',U.name, U.firstLast, U.secondLast) AS nombre, L.prog_IdPlan, U.safe_IdUser "
																	+ "FROM UserApplicant AS A, SafeUsers AS U, InsListAccepted AS L "
																	+ "WHERE U.safe_IdUser = A.safe_IdUser AND A.user_IdApplicant = L.user_IdApplicant AND A.user_IdApplicant = ?");
																	ps.setInt(1, user_IdApplicant);
																	rs = ps.executeQuery();
																	if(rs != null && rs.next()){
																		safe_IdUser = rs.getInt("safe_IdUser");
																		out.print("<input type='text' style='width: 400px' value='"+rs.getString("nombre")+"' disabled/><br><br>");
																		ps = con.prepareStatement("SELECT * FROM Package_view WHERE prog_IdPlan = ? ");
																		ps.setInt(1, rs.getInt("prog_IdPlan"));
																		rs = ps.executeQuery();
																		out.print("<select name='group_IdGroup' style='width: 400px'>");
																		while(rs != null && rs.next()){
																			out.print("<option value='"+rs.getString("group_IdGroup") +"'>"+rs.getString("descripcion")+"</option>");
																		}
																		out.print("</select>");
																	}
																	db.close();
																	%>
																	<input type="hidden" name="user_IdApplicant" id="user_IdApplicant" value="<%= user_IdApplicant %>" />
																	<input type="hidden" name="safe_IdUser" id="safe_IdUser" value="<%= safe_IdUser %>" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="30px" bgcolor="#E3EAFA" align="right" valign="top"><input id="siguiente" type="button" value="Siguiente" onclick="enviar()">&nbsp;&nbsp;<input type="button" value="Cancelar" onclick="cancelar()">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>
                    <td height="20px"></td>
                </tr>
            </table>
        </form>
    </body>
</html>
