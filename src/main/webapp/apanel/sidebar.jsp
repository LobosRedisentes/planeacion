<div id="sidebar" class="sidebar">
    <div class="sidebar-menu nav-collapse">
        <div class="divide-20"></div>						
        <!-- SIDEBAR MENU -->
        <ul>
            <li class="active">
                <a href="index.jsp">
                    <i class="fa fa-tachometer fa-fw"></i>
                    <span class="menu-text">Panel de Control</span>
                    <span class="selected"></span>
                </a>					
            </li>
            <!-- MENU INSCRIPCIONES -->
            <%if (privileges.contains("INS001")) {%>
            <li class="has-sub">
                <a href="javascript:;" class="">
                    <i class="fa fa-bookmark-o fa-fw"></i>
                    <span class="menu-text" >Inscripciones</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub">
                    <%if (privileges.contains("INS003")) {%>
                    <li><a class="" href="/edcore/admin/UserNvoApplicants_admin.jsp"><span class="sub-menu-text">Aspirantes</span></a></li>
                        <%}%>                                                    
                    <!--<li><a class="" href="InsRequests_admin.jsp"><span class="sub-menu-text">Solicitudes</span></a></li>-->
                    <%if (privileges.contains("INS005")) {%>
                    <li class="has-sub-sub">
                        <a href="javascript:;" class="">
                            <span class="sub-menu-text">Examen de admisión</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-sub">
                            <%if (privileges.contains("INS007")) {%>
                            <li><a class="" href="/edcore/admin/InsExams_admin.jsp"><span class="sub-sub-menu-text">Tipos</span></a></li>
                                <%}%>
                                <%if (privileges.contains("INS009")) {%>
                            <li><a class="" href="/edcore/admin/InsExamRooms_admin.jsp"><span class="sub-sub-menu-text">Aulas</span></a></li>
                                <%}%>
                                <%if (privileges.contains("INS011")) {%>
                            <li><a class="" href="/edcore/admin/InsRequest_admin.jsp"><span class="sub-sub-menu-text">Fichas</span></a></li>
                                <%}%>
                        </ul>
                    </li>
                    <%}%>
                    <%if (privileges.contains("INS013")) {%>
                    <li><a class="" href="/edcore/admin/InsPackages_admin.jsp"><span class="sub-menu-text">Paquetes nuevo ingreso</span></a></li>
                        <%}%>
                        <%if (privileges.contains("INS015")) {%>
                    <li><a class="" href="/edcore/admin/InsListAccepted_admin.jsp"><span class="sub-menu-text">Aspirantes aceptados</span></a></li>
                        <%}%>
                        <%if (privileges.contains("INS017")) {%>
                    <li><a class="" href="/edcore/admin/Inscriptions_admin.jsp"><span class="sub-menu-text">Inscripciones</span></a></li>
                        <%}%>
                </ul>
            </li>
            <%}%>
            <!-- /MENU INSCRIPCIONES -->
            <!-- MENU REINSCRIPCIONES -->
            <%if (privileges.contains("REIN01")) {%>
            <li class="has-sub">
                <a href="javascript:;" class="">
                    <i class="fa fa-bookmark-o fa-fw"></i>
                    <span class="menu-text">Reinscripciones</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub">
                    <%if (privileges.contains("REIN02")) {%>
                    <li><a class="" href="/edcore/admin/ReinsValidating_admin.jsp"><span class="sub-menu-text">Asesor con validación</span></a></li>
                        <%}%>
                        <%if (privileges.contains("REIN04")) {%>
                    <li><a class="" href="/edcore/admin/ReinsNoValidating_admin.jsp"><span class="sub-menu-text">Asesor sin validación</span></a></li>
                        <%}%>
                        <%if (privileges.contains("REIN04")) {%>
                    <li><a class="" href="/edcore/admin/ReinsResidence_admin.jsp"><span class="sub-menu-text">Residencias</span></a></li>
                        <%}%>
                        <%if (privileges.contains("REIN04")) {%>
                    <li><a class="" href="/edcore/admin/ReinsServicio_admin.jsp"><span class="sub-menu-text">Servicio Social</span></a></li>
                        <%}%>
                        <%if (privileges.contains("REIN04")) {%>
                    <li><a class="" href="/edcore/admin/ReinsGlobal_admin.jsp"><span class="sub-menu-text">Globales/Especiales</span></a></li>
                        <%}%>
                        <%if (privileges.contains("REIN06")) {%>
                    <li><a class="" href="/edcore/admin/ReinsDateTime_admin.jsp"><span class="sub-menu-text">Asignación de fechas</span></a></li>
                        <%}%>
                        <%if (privileges.contains("REIN08")) {%>
                    <li><a class="" href="/edcore/admin/SafeLock_admin.jsp"><span class="sub-menu-text">Bloqueos</span></a></li>
                        <%}%>
                </ul>
            </li>
            <%}%>
            <!-- /MENU REINSCRIPCIONES -->
            <!-- MENU CONVALIDACIONES -->
            <%if (privileges.contains("INS001")) {%>
            <li class="has-sub">
                <a href="javascript:;" class="">
                    <i class="fa fa-bookmark-o fa-fw"></i>
                    <span class="menu-text" >Convalidaciones</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub">
                    <%if (privileges.contains("INS003")) {%>
                    <li><a class="" href="/edcore/admin/ConvCodes.jsp"><span class="sub-menu-text">Matriculas</span></a></li>
                        <%}%>
                        <%if (privileges.contains("INS003")) {%>
                    <li><a class="" href="/edcore/admin/ConvResolutions.jsp"><span class="sub-menu-text">Dictamen</span></a></li>
                        <%}%>
                </ul>
            </li>
            <%}%>
            <!-- /MENU CONVALIDACIONES -->
            <!-- MENU GRUPOS -->
            <%if (privileges.contains("GP01")) {%>
            <li class="has-sub">
                <a href="javascript:;" class="">
                    <i class="fa fa-bookmark-o fa-fw"></i>
                    <span class="menu-text">Grupos</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub">

                    <li class="has-sub-sub">
                        <a href="javascript:;" class="">
                            <span class="sub-menu-text">Aulas</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-sub">

                            <li><a class="" href="/edcore/admin/GroupRoomStatus.jsp"><span class="sub-sub-menu-text">Ocupación</span></a></li>

                        </ul>
                    </li>

                    <%if (privileges.contains("GP02")) {%>
                    <li class="has-sub-sub">
                        <a href="javascript:;" class="">
                            <span class="sub-menu-text">Preparación</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-sub">
                            <%if (privileges.contains("GP03")) {%>
                            <li><a class="" href="/edcore/admin/GroupsPrepared_admin.jsp"><span class="sub-sub-menu-text">Paquetes</span></a></li>
                                <%}%>
                                <%if (privileges.contains("GP05")) {%>
                            <li><a class="" href="/edcore/admin/GroupPSchedule_admin.jsp"><span class="sub-sub-menu-text">Borrador Horario</span></a></li>
                                <%}%>
                        </ul>
                    </li>
                    <%}%>
                    <%if (privileges.contains("GP07")) {%>
                    <li class="has-sub-sub">
                        <a href="javascript:;" class="">
                            <span class="sub-menu-text">Semestre Actual</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-sub">
                            <%if (privileges.contains("GP08")) {%>
                            <li><a class="" href="/edcore/admin/Groups_admin.jsp"><span class="sub-sub-menu-text">Paquetes</span></a></li>
                                <%}%>
                                <%if (privileges.contains("GP10")) {%>
                            <li><a class="" href="/edcore/admin/GroupSubject_admin.jsp"><span class="sub-sub-menu-text">Grupos Reales</span></a></li>
                                <%}%>
                                <%if (privileges.contains("GP12")) {%>
                            <li><a class="" href="/edcore/admin/GroupSchedule_admin.jsp"><span class="sub-sub-menu-text">Horarios</span></a></li>
                                <%}%>
                        </ul>
                    </li>
                    <%}%>
                </ul>
            </li>
            <%}%>
            <!-- /MENU GRUPOS -->
            <!-- MENU CATALOGOS -->
            <%if (privileges.contains("CAT01")) {%>
            <li class="has-sub">
                <a href="javascript:;" class="">
                    <i class="fa fa-bookmark-o fa-fw"></i>
                    <span class="menu-text">Catalogos</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub">
                    <!-- MENU USUARIOS -->
                    <%if (privileges.contains("CAT02")) {%>
                    <li class="has-sub-sub">
                        <a href="javascript:;" class="">
                            <span class="sub-menu-text">Usuarios</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-sub">
                            <%if (privileges.contains("CAT10")) {%>
                            <li><a class="" href="/edcore/admin/UserStudents_admin.jsp"><span class="sub-sub-menu-text">Alumnos</span></a></li>
                                <%}%>
                                <%if (privileges.contains("CAT11")) {%>
                            <li><a class="" href="/edcore/admin/UserProfessors_admin.jsp"><span class="sub-sub-menu-text">Docentes</span></a></li>
                                <%}%>
                                <%if (privileges.contains("CAT11")) {%>
                            <li><a class="" href="/edcore/admin/UserAdministrative_admin.jsp"><span class="sub-sub-menu-text">Administrativos</span></a></li>
                                <%}%>
                        </ul>
                    </li>
                    <%}%>
                    <!-- /MENU USUARIOS -->
                    <!-- MENU CALENDARIO -->
                    <%if (privileges.contains("CAT03")) {%>
                    <li><a class="" href="/edcore/admin/CalSchool_admin.jsp"><span class="sub-menu-text">Calendarios</span></a></li>
                        <%}%>
                    <!-- /MENU CALENDARIO -->
                    <%if (privileges.contains("CAT08")) {%>
                    <li><a class="" href="/edcore/admin/Programs_admin.jsp"><span class="sub-menu-text">Programas de Estudio</span></a></li>
                        <%}%>
                        <%if (privileges.contains("CAT09")) {%>
                    <li><a class="" href="/edcore/admin/ProgPlan_admin.jsp"><span class="sub-menu-text">Planes de Estudio</span></a></li>
                        <%}%>
                    <!-- MENU RETICULA -->
                    <%if (privileges.contains("CAT04")) {%>
                    <li class="has-sub-sub">
                        <a href="javascript:;" class="">
                            <span class="sub-menu-text">Reticula Escolar</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-sub">
                            <%if (privileges.contains("CAT12")) {%>
                            <li><a class="" href="/edcore/admin/ProgSubjects_admin.jsp"><span class="sub-sub-menu-text">Asignaturas</span></a></li>
                                <%}%>
                                <%if (privileges.contains("CAT13")) {%>
                            <li><a class="" href="/edcore/admin/ProgReqSubjects_admin.jsp"><span class="sub-sub-menu-text">Requisitos</span></a></li>
                                <%}%>
                                <%if (privileges.contains("CAT14")) {%>
                            <li><a class="" href="/edcore/admin/ProgCoSubjects_admin.jsp"><span class="sub-sub-menu-text">Correquisitos</span></a></li>
                                <%}%>
                                <%if (privileges.contains("CAT15")) {%>
                            <li><a class="" href="/edcore/admin/ProgCompetences_admin.jsp"><span class="sub-sub-menu-text">Competencias</span></a></li>
                                <%}%>
                                <%if (privileges.contains("CAT16")) {%>
                            <li><a class="" href="/edcore/admin/ProgCompSubject_admin.jsp"><span class="sub-sub-menu-text">Indicadores</span></a></li>
                                <%}%>
                        </ul>
                    </li>
                    <%}%>
                    <!-- /MENU RETICULA -->
                    <!-- MENU ESPECIALIDADES -->
                    <%if (privileges.contains("CAT05")) {%>
                    <li class="has-sub-sub">
                        <a href="javascript:;" class="">
                            <span class="sub-menu-text">Especialidades</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-sub">
                            <%if (privileges.contains("CAT17")) {%>
                            <li><a class="" href="/edcore/admin/ProgSpecialties_admin.jsp"><span class="sub-menu-text">Modulos</span></a></li>
                                <%}%>
                                <%if (privileges.contains("CAT18")) {%>
                            <li><a class="" href="/edcore/admin/ProgSpecialtiesSubjects_admin.jsp"><span class="sub-sub-menu-text">Materias</span></a></li>
                                <%}%>
                        </ul>
                    </li>
                    <%}%>
                    <!-- /MENU ESPECIALIDADES -->
                    <!-- MENU LOCALIDADES -->
                    <%if (privileges.contains("CAT06")) {%>
                    <li class="has-sub-sub">
                        <a href="javascript:;" class="">
                            <span class="sub-menu-text">localidades</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-sub">
                            <%if (privileges.contains("CAT19")) {%>
                            <li><a class="" href="/edcore/admin/AddrTowns_admin.jsp"><span class="sub-sub-menu-text">Municipios</span></a></li>
                                <%}%>
                                <%if (privileges.contains("CAT20")) {%>
                            <li><a class="" href="/edcore/admin/AddrLocations_admin.jsp"><span class="sub-sub-menu-text">Colonias</span></a></li>
                                <%}%>
                                <%if (privileges.contains("CAT21")) {%>
                            <li><a class="" href="/edcore/admin/AddressBook_admin.jsp"><span class="sub-sub-menu-text">Direcciones</span></a></li>
                                <%}%>
                        </ul>
                    </li>
                    <%}%>
                    <%if (privileges.contains("CAT07")) {%>
                    <li><a class="" href="/edcore/admin/Schools_admin.jsp"><span class="sub-menu-text">Escuelas</span></a></li>
                        <%}%>
                    <!-- /MENU LOCALIDADES -->
                </ul>
            </li>
            <%}%>
            <!-- /MENU CATALOGOS -->
            <!-- MENU CONFIGURACION -->
            <%if (privileges.contains("CONF01")) {%>
            <li class="has-sub">
                <a href="javascript:;" class="">
                    <i class="fa fa-bookmark-o fa-fw"></i>
                    <span class="menu-text">Configuración</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub">
                    <!-- MENU FECHAS -->
                    <%if (privileges.contains("CONF02")) {%>
                    <li class="has-sub-sub">
                        <a href="javascript:;" class="">
                            <span class="sub-menu-text">Fechas</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-sub">
                            <%if (privileges.contains("CONF03")) {%>
                            <li><a class="" href="/edcore/admin/CalRegistrations_admin.jsp"><span class="sub-sub-menu-text">Inscripciones</span></a></li>
                                <%}%>
                                <%if (privileges.contains("CONF04")) {%>
                            <li><a class="" href="/edcore/admin/CalEvaluations_admin.jsp"><span class="sub-sub-menu-text">Evaluaciones</span></a></li>
                                <%}%>
                        </ul>
                    </li>
                    <%}%>
                    <!-- /MENU FECHAS -->
                    <%if (privileges.contains("CONF05")) {%>
                    <li><a class="" href="/edcore/admin/Configure_admin.jsp"><span class="sub-menu-text">Parametros</span></a></li>
                        <%}%>
                </ul>
            </li>
            <%}%>
            <!-- MENU /CONFIGURACION -->
            <!-- MENU CALIFICACIONES -->
            <%if (privileges.contains("EVAL01")) {%>
            <li class="has-sub">
                <a href="javascript:;" class="">
                    <i class="fa fa-bookmark-o fa-fw"></i>
                    <span class="menu-text">Calificaciones</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub">
                    <%if (privileges.contains("EVAL02")) {%>
                    <li class="has-sub-sub">
                        <a href="javascript:;" class="">
                            <span class="sub-menu-text">Semestre Actual</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-sub">
                            <%if (privileges.contains("EVAL04")) {%>
                            <li><a class="" href="/edcore/admin/GroupListEval_admin.jsp"><span class="sub-sub-menu-text">Parciales</span></a></li>
                                <%}%>
                                <%if (privileges.contains("EVAL05")) {%>
                            <li><a class="" href="/edcore/admin/GroupList_admin.jsp"><span class="sub-sub-menu-text">Finales</span></a></li>
                                <%}%>
                                <%if (privileges.contains("EVAL06")) {%>
                            <li><a class="" href="/edcore/admin/GroupListInter_admin.jsp"><span class="sub-sub-menu-text">Interciclos</span></a></li>
                                <%}%>
                                <%if (privileges.contains("EVAL07")) {%>
                            <li><a class="" href="/edcore/admin/StudentGrades_admin.jsp"><span class="sub-sub-menu-text">Boletas</span></a></li>
                                <%}%>
                        </ul>
                    </li>
                    <%}%>
                    <%if (privileges.contains("EVAL03")) {%>
                    <li><a class="" href="/edcore/admin/EvalKardex_admin.jsp"><span class="sub-menu-text">Kardex directo</span></a></li>
                        <%}%>
                </ul>
            </li>
            <%}%>
            <!-- MENU /CALIFICACIONES -->
            <!-- MENU TITULACION -->
            <%if (privileges.contains("EVAL01")) {%>
            <li class="has-sub">
                <a href="javascript:;" class="">
                    <i class="fa fa-bookmark-o fa-fw"></i>
                    <span class="menu-text">Titulación</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub">
                    <%if (privileges.contains("EVAL03")) {%>
                    <li><a class="" href="/edcore/admin/titulacion/protocolos.jsp"><span class="sub-menu-text">Protocolos</span></a></li>
                        <%}%>
                </ul>
            </li>
            <%}%>
            <!-- MENU /CALIFICACIONES -->
            <!-- MENU DOCUMENTOS -->
            <%if (privileges.contains("DOC01")) {%>
            <li class="has-sub">
                <a href="javascript:;" class="">
                    <i class="fa fa-bookmark-o fa-fw"></i>
                    <span class="menu-text">DOCUMENTOS</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub">
                    <%if (privileges.contains("DOC02")) {%>
                    <li class="has-sub-sub">
                        <a href="javascript:;" class="">
                            <span class="sub-menu-text">Constancias</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-sub">
                            <%if (privileges.contains("DOC05")) {%>
                            <li><a class="" href="/edcore/admin/ConsXSem_admin.jsp"><span class="sub-sub-menu-text">Calificaciones x semestre</span></a></li>
                                <%}%>
                                <%if (privileges.contains("DOC06")) {%>
                            <li><a class="" href="/edcore/admin/ConsVi_admin.jsp"><span class="sub-sub-menu-text">Vigencia</span></a></li>
                                <%}%>
                                <%if (privileges.contains("DOC07")) {%>
                            <li><a class="" href="/edcore/admin/ConsTerm_admin.jsp"><span class="sub-sub-menu-text">Cons. de terminación</span></a></li>
                                <%}%>
                                <%if (privileges.contains("DOC08")) {%>
                            <li><a class="" href="/edcore/admin/ConsCond_admin.jsp"><span class="sub-sub-menu-text">Cons. de Conducta</span></a></li>
                                <%}%>
                                <%if (privileges.contains("DOC09")) {%>
                            <li><a class="" href="/edcore/admin/ConsServ_admin.jsp"><span class="sub-sub-menu-text">Cons. de Servicio Social</span></a></li>
                                <%}%>
                                <%if (privileges.contains("DOC10")) {%>
                            <li><a class="" href="/edcore/admin/ConsRes_admin.jsp"><span class="sub-sub-menu-text">Cons. de Residencias</span></a></li>
                                <%}%>
                                <%if (privileges.contains("DOC11")) {%>
                            <li><a class="" href="/edcore/admin/ConsCarga_admin.jsp"><span class="sub-sub-menu-text">Cons. Carga academica</span></a></li>
                                <%}%>
                        </ul>
                    </li>
                    <%}%>
                    <%if (privileges.contains("DOC03")) {%>
                    <li><a class="" href="/edcore/admin/kardex.jsp"><span class="sub-menu-text">Kardex</span></a></li>
                        <%}%>
                        <%if (privileges.contains("DOC11")) {%>
                    <li><a class="" href="/edcore/admin/ConsLoad_admin.jsp"><span class="sub-menu-text">Carga academica</span></a></li>
                        <%}%>
                        <%if (privileges.contains("DOC04")) {%>
                    <li><a class="" href="/edcore/admin/certificates.jsp"><span class="sub-menu-text">Certificados</span></a></li>
                        <%}%>
                        <%if (privileges.contains("DOC07")) {%>
                    <li><a class="" href="/edcore/admin/cartapasante.jsp"><span class="sub-menu-text">Carta pasante</span></a></li>
                        <%}%>
                        <%if (privileges.contains("DOC12")) {%>
                    <li><a class="" href="/edcore/admin/examenProfesional.jsp"><span class="sub-menu-text">Examen Profesional</span></a></li>
                        <%}%>
                </ul>
            </li>
            <%}%>
            <!-- MENU /DOCUMENTOS -->
            <!-- MENU SEGURIDAD -->
            <%if (privileges.contains("SAFE01")) {%>
            <li class="has-sub">
                <a href="javascript:;" class="">
                    <i class="fa fa-bookmark-o fa-fw"></i>
                    <span class="menu-text">Seguridad</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub">
                    <%if (privileges.contains("SAFE02")) {%>
                    <li><a class="" href="/edcore/admin/SafeUsers_admin.jsp"><span class="sub-menu-text">Usuarios</span></a></li>
                        <%}%>
                        <%if (privileges.contains("SAFE03")) {%>
                    <li><a class="" href="/escolar/apanel/safe/cuentas.jsp"><span class="sub-menu-text">Cuentas de Usuario</span></a></li>
                        <%}%>
                        <%if (privileges.contains("SAFE04")) {%>
                    <li><a class="" href="/edcore/admin/SafeRoles_admin.jsp"><span class="sub-menu-text">Roles</span></a></li>
                        <%}%>
                        <%if (privileges.contains("CORE")) {%>
                    <li><a class="" href="/edcore/admin/messenger.jsp"><span class="sub-menu-text">Mensajeria</span></a></li>
                        <%}%>
                </ul>
            </li>
            <%}%>
            <!-- MENU /SEGURIDAD -->
            <!-- MENU DE ESTADISTICAS -->
            <%if (privileges.contains("REP01")) {%>
            <li class="has-sub">
                <a href="javascript:;" class="">
                    <i class="fa fa-bookmark-o fa-fw"></i>
                    <span class="menu-text">Estadisticas</span>
                    <span class="arrow"></span>
                </a>
                <%if (privileges.contains("REP02")) {%>
                <ul class="sub">
                    <li class="has-sub-sub">
                        <a href="javascript:;" class="">
                            <span class="sub-menu-text">Encuestas SAT</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-sub">
                            <%if (privileges.contains("REP03")) {%>
                            <li><a class="" href="/edcore/admin/Quiz_report.jsp"><span class="sub-sub-menu-text">Porcentajes</span></a></li>
                                <%}%>
                        </ul>
                    </li>
                </ul>
                <%}%>
            </li>
            <%}%>
            <!-- MENU /SEGURIDAD -->
        </ul>
        <!-- /SIDEBAR MENU -->
    </div>
</div>