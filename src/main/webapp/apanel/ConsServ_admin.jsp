<%@page import="java.util.ArrayList"%>
<%@page import="com.edcore.model.safe.User"%>
<%
    HttpSession sesion;
    sesion = request.getSession();
    User user = (User) sesion.getAttribute("user");
    if(user != null){
        ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
        if(privileges.contains("DOC09")){
%>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Constancia de Servicio Social</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
    <meta name="description" content="Sistema educativo">
    <meta name="author" content="Instituto Tecnologico Superior de Zapopan">	
    <link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
    <link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
    <link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
    <!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    <!-- LIBRERIA DHTMLX -->
    <link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.css" >
    <script src="../dhtmlx/libCompiler/dhtmlxcommon.js"></script>
    <script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.js"></script>
    <script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_srnd.js"></script>
    <script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_filter.js"></script>
    <script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgridcell.js"></script>
    <script src="../dhtmlx/dhtmlxDataProcessor/codebase/dhtmlxdataprocessor.js"></script>
    <script src="../dhtmlx/libCompiler/connector.js"></script>
</head>
<body>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 80%; height: 98%">
            <div class="modal-content" style="width: 100%; height: 90%">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Constancia de Servicio Social</h4>
                </div>
                <div class="modal-body" style="width: 100%; height: 90%">
                    <iframe src="" style="width: 100%; height: 100%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <!-- HEADER -->
    <%@include file="header.jsp" %>
    <!--/HEADER -->
    <!-- PAGE -->
    <section id="page">
        <!-- SIDEBAR -->
        <%@include file="sidebar.jsp" %>
        <!-- /SIDEBAR -->
        <div id="main-content">
            <div class="container">
                <div class="row">
                    <div id="content" class="col-lg-12">
                        <!-- PAGE HEADER-->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-header">
                                    <!-- BREADCRUMBS -->
                                    <ul class="breadcrumb">
                                        <li><i class="fa fa-home"></i><a href="index.jsp">Inicio</a></li>
                                        <li><i class="fa"></i><a href="#">Documentos</a></li>
                                        <li><i class="fa"></i><a href="#">Constancias</a></li>
                                        <li>Serv. Social</li>
                                    </ul>
                                    <!-- /BREADCRUMBS -->
                                    <div class="clearfix">
                                        <h3 class="content-title pull-left">Constancia de Servicio Social</h3>
                                            <span class="date-range pull-right">
                                                <p class="btn-toolbar">
                                                    <button id="print" class="btn btn-primary" disabled=""><i class="fa fa-print-square-o"></i> Imprimir</button>
                                                </p>
                                            </span>
                                    </div>
                                    <div class="description"></div>
                                </div>
                            </div>
                        </div>
                        <!-- /PAGE HEADER -->
                        <!-- DASHBOARD CONTENT -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box border red">
                                    <div class="box-title">
                                        <h4>Alumnos con 70%</h4>
                                        <div class="tools hidden-xs"></div>
                                    </div>
                                    <div class="box-body">
                                        <div id="grid_container" class="box border" style="height:600px; width: 100%"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /DASHBOARD CONTENT -->
                    </div><!-- /CONTENT-->
                </div>
            </div>
        </div>
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- JQUERY -->
	<script src="../js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="../bootstrap-dist/js/bootstrap.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="../js/script.js"></script>
	<script>
            jQuery(document).ready(function() {
                App.setPage("index");  //Set current page
                App.init(); //Initialise plugins and elements
                $("#photo").attr("src","getPhoto?safe_IdUser="+<% if(user != null){out.print(user.getSafe_IdUser());} %>);
                var response = "../getAdminView?table=VADM001&id=user_IdStudent&parameters=matricula,nombre,apellidoPaterno,apellidoMaterno,plan,programa,porcCredAprobados,status";
                var IDRow;
                
                grid = new dhtmlXGridObject('grid_container');
                grid.setImagePath("../dhtmlx/dhtmlxGrid/codebase/imgs/");
                grid.setHeader("Matricula,Nombre,Paterno,Materno,Plan,Programa,Avance,Estado");
                grid.attachHeader("#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter");
                grid.setInitWidths("*,*,*,*,*,*,*,*");
                grid.setColAlign("left,left,left,left,left,left,left,left");
                grid.setColSorting("str,str,str,str,str,str,str,str");
                grid.setEditable(false);
                grid.init();
                grid.setSkin("clear");
                grid.enableSmartRendering(true,50);
                grid.objBox.style.overflowX = "hidden";
                grid.load(response,function(){});
                grid.attachEvent("onRowSelect",function(id){
                    IDRow = id;
                    $("#print").removeAttr("disabled");
		});
                
                $('#print').click(function(){
                    $('iframe').attr("src","GetPdfConsServ?user_IdStudent="+IDRow);
                    $('#myModal').modal({show:true});
                });
            });
	</script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
        }else{
            response.sendRedirect("error600.jsp");
        }
    }else{
        response.sendRedirect("error600.jsp");
    }
%>