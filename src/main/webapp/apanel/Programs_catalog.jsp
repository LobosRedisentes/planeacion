<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.edcore.model.db.DBConnector"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.edcore.model.safe.User"%>
	<%
		HttpSession sesion;
    sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if(user != null){
			ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
			if(privileges.contains("CAT25")){
	%>
<%
	ResultSet rs;
	DBConnector db = new DBConnector();
	PreparedStatement ps;
	Connection con;
	con=db.open();
    //SE DECLARAN LAS VARIABLES PARA EL USO DE LAS LISTAS Y CUADROS DE TEXTO
    ArrayList<String> departments = new ArrayList<String>();
    String code = "";
    String officialCode = "";
    String abbreviation = "";
    String name = "";
    String level = "";
    String mode = "";
    String status = "";
    int org_IdDepartment = 0;
    // SE RECIBE EL IDENTIFICADOR DEL PROGRAMA DE ESTUDIOS
    int prog_IdProgram = Integer.parseInt(request.getParameter("prog_IdProgram"));
    
    if(prog_IdProgram > 0){
        // SE LLENAN LAS LISTAS Y CUADROS DE TEXTO SI EL CATALOGO ESTA EN MODO DE ACTUALIZACION
        ps = con.prepareStatement("SELECT * FROM Programs WHERE prog_IdProgram = ?");
        ps.setInt(1, prog_IdProgram);
        rs = ps.executeQuery();
            
        while(rs != null && rs.next()){
            code = rs.getString("code");
            officialCode = rs.getString("officialCode");
            abbreviation = rs.getString("abbreviation");
            name = rs.getString("name");
            level = rs.getString("level");
            mode = rs.getString("mode");
            status = rs.getString("status");
            org_IdDepartment = rs.getInt("org_IdDepartment");
        }
    }
    // SE EXTREN LOS DATOS DE LOS DEPARTAMENTOS Y DIVISIONES DEL INSTITUTO
    ps = con.prepareStatement("SELECT org_IdDepartment, abbreviation FROM OrgDepartments WHERE type = 'DIVISION' OR type = 'DEPARTAMENTO';");
    rs = ps.executeQuery();
		departments.add("<option value='' selected>Selecciona ..</option>");  
    while(rs != null && rs.next()){
        if(rs.getInt("org_IdDepartment") == org_IdDepartment){
            departments.add("<option value='"+rs.getString("org_IdDepartment")+"' selected>"+rs.getString("abbreviation") +"</option>");
        }else{
            departments.add("<option value='"+rs.getString("org_IdDepartment")+"' >"+rs.getString("abbreviation") +"</option>");
        }
    }
		db.close();
%>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Municipios</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
	<link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
	
	<link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- DATE RANGE PICKER -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
	<!-- TYPEAHEAD -->
	<link rel="stylesheet" type="text/css" href="../js/typeahead/typeahead.css" />
	<!-- FILE UPLOAD -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-fileupload/bootstrap-fileupload.min.css" />
	<!-- SELECT2 -->
	<link rel="stylesheet" type="text/css" href="../js/select2/select2.min.css" />
	<!-- UNIFORM -->
	<link rel="stylesheet" type="text/css" href="../js/uniform/css/uniform.default.min.css" />
	<!-- JQUERY UPLOAD -->
	<!-- blueimp Gallery styles -->
	<link rel="stylesheet" href="../js/blueimp/gallery/blueimp-gallery.min.css">
	<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload.css">
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload-ui.css">
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	<style>
	 .error-message, label.error {
	    color: #ff0000;
	    margin:0;
	    display: inline;
	    font-size: 1em !important;
	    font-weight:bold;
	 }
	 </style>
	<!-- LIBRERIA EDCORE -->
  <script src="../dhtmlx/libCompiler/dhtmlxcommon.js"></script>
  <script src="../js/edcore-sessioncontroller.js"></script>
	<script src="../js/action_dhx.js"></script>
</head>
<body>
	<!-- HEADER -->
	<%@include file="header.jsp" %>
	<!--/HEADER -->
	<!-- PAGE -->
	<section id="page">
				<!-- SIDEBAR -->
				<%@include file="sidebar.jsp" %>
				<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<!-- STYLER -->
									
									<!-- /STYLER -->
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="index.jsp">Inicio</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Catalogos</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="Programs_admin.jsp">Programas de Estudio</a>
										</li>
										<li>Nuevo/Editar</li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										<h3 class="content-title pull-left">Cat�logo - Programas de estudio</h3>
										<span class="date-range pull-right">
										<p class="btn-toolbar">
											<%if(privileges.contains("CAT45")){%>
											<button id="nuevo" class="btn btn-success" ><i class="fa fa-file-text-o"></i> Nuevo</button>
											<%}%>
											<%if(privileges.contains("CAT46")){%>
											<button id="editar" class="btn btn-success" disabled=""><i class="fa fa-pencil-square-o"></i> Editar</button>
											<%}%>
											<%if(privileges.contains("CAT47")){%>
											<button id="guardar" class="btn btn-success" ><i class="fa fa-save"></i> Guardar</button>
											<%}%>
										</p>
										</span>
									</div>
									<div class="description"></div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						<!-- ADVANCED -->
						<div class="row">
							<div class="col-md-12">
								<!-- BOX -->
								<div class="box border red">
									<div class="box-title">
										<h4><i class="fa fa-bars"></i>Nuevo/Editar Programa de Estudios</h4>
									</div>
									<div class="box-body">
										<form class="form-horizontal " action="#" id="frm_programs">
										  <div class="form-group">
											 <label class="col-md-2 control-label" for="code">Clave</label> 
											 <div class="col-md-10">
                            <%
                            //SE DETERMINA EL VALOR DEL CODIGO DEL PROGRAMA CUANDO LA VENTANA ESTA EN MODO DE NUEVO/EDICION
                            if(code != null && !code.equals("")){
                                out.print("<input type='text' id='code' class='form-control' autocomplete='off' name='code'  value='"+code+"' />");
                            }else{
                                out.print("<input type='text' class='form-control' autocomplete='off' id='code' name='code' />");
                            }
                            %>
											 </div>
										  </div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="officialCode">Codigo oficial:</label> 
											<div class="col-md-10">
                            <%
                            //SE DETERMINA EL VALOR DEL CODIGO OFICIAL DEL PROGRAMA CUANDO LA VENTANA ESTA EN MODO DE NUEVO/EDICION
                            if(officialCode != null && !officialCode.equals("")){
                                out.print("<input type='text' id='officialCode' name='officialCode' class='form-control' autocomplete='off' value='"+officialCode+"' />");
                            }else{
                                out.print("<input type='text' id='officialCode' name='officialCode' class='form-control' autocomplete='off' />");
                            }
                            %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="abbreviation">Abreviatura:</label> 
											<div class="col-md-10">
                            <%
                            //SE DETERMINA EL VALOR DE LA ABREVIATURA DEL PROGRAMA CUANDO LA VENTANA ESTA EN MODO DE NUEVO/EDICION
                            if(abbreviation != null && !abbreviation.equals("")){
                                out.print("<input type='text' id='abbreviation' name='abbreviation' class='form-control' autocomplete='off'  value='"+abbreviation+"' />");
                            }else{
                                out.print("<input type='text' id='abbreviation' name='abbreviation' class='form-control' autocomplete='off'  />");
                            }                                
                            %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="name">Nombre:</label> 
											<div class="col-md-10">
                            <%
                                //SE DETERMINA EL VALOR DEL NOMBRE DEL PROGRAMA CUANDO LA VENTANA ESTA EN MODO DE NUEVO/EDICION
                                if(name != null && !name.equals("")){
                                    out.print("<input type='text' id='name' name='name' class='form-control' autocomplete='off' value='"+name+"' />");
                                }else{
                                    out.print("<input type='text' id='name' name='name' class='form-control' autocomplete='off' />");
                                }
                            %>                            
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="e1">Nivel de estudios:</label> 
											<div class="col-md-10">
												<select id="e1" name="level" class="col-md-12"  >
															  <option value='' selected>Selecciona ..</option>
                                <option id="level_lic" value='LICENCIATURA'>LICENCIATURA</option>
                                <option id="level_master" value='MAESTRIA' >MAESTRIA</option>
                                <option id="level_doc" value='DOCTORADO'>DOCTORADO</option>
                            </select>
                            <%
                                //SE DETERMINA EL VALOR DEL NIVEL DEL PROGRAMA CUANDO LA VENTANA ESTA EN MODO DE NUEVO/EDICION
                                if(level!= null && level.equals("LICENCIATURA")){out.print("<script>document.getElementById('level_lic').selected=true;</script>");}
                                if(level!= null && level.equals("MAESTRIA")){out.print("<script>document.getElementById('level_master').selected=true;</script>");}
                                if(level!= null && level.equals("DOCTORADO")){out.print("<script>document.getElementById('level_doc').selected=true;</script>");}
                            %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="e2">Modalidad:</label> 
											<div class="col-md-10">
                            <select id="e2" name="mode" class="col-md-12">;
															<option value='' selected>Selecciona ..</option>
                                <option id="mode_escolarizado" value='ESCOLARIZADO'>ESCOLARIZADO</option>
                                <option id="mode_semiescolarizado" value='SEMIESCOLARIZADO' >SEMIESCOLARIZADO</option>
                                <option id="mode_virtual" value='VIRTUAL' >VIRTUAL</option>
                            </select>
                            <%
                                //SE DETERMINA EL VALOR DEL LA MODALIDAD DEL PROGRAMA CUANDO LA VENTANA ESTA EN MODO DE NUEVO/EDICION
                                if(mode!= null && mode.equals("ESCOLARIZADO")){out.print("<script>document.getElementById('mode_escolarizado').selected=true;</script>");}
                                if(mode!= null && mode.equals("SEMIESCOLARIZADO")){out.print("<script>document.getElementById('mode_semiescolarizado').selected=true;</script>");}
                                if(mode!= null && mode.equals("VIRTUAL")){out.print("<script>document.getElementById('mode_virtual').selected=true;</script>");}
                            %>
											</div>
											</div>											
											<div class="form-group">
											<label class="col-md-2 control-label" for="e3">Status:</label> 
											<div class="col-md-10">
                            <select id="e3" name="status" class="col-md-12" >;
															   <option value='' selected>Selecciona ..</option>
                                <option id="status_activo" value='ACTIVO'>ACTIVO</option>
                                <option id="status_inactivo" value='INACTIVO'>INACTIVO</option>
                            </select>
                            <% 
                                //SE DETERMINA EL VALOR DEL STATUS DEL PROGRAMA CUANDO LA VENTANA ESTA EN MODO DE NUEVO/EDICION
                                if(status!= null && status.equals("ACTIVO")){out.print("<script>document.getElementById('status_activo').selected=true;</script>");}
                                if(status!= null && status.equals("INACTIVO")){out.print("<script>document.getElementById('status_inactivo').selected=true;</script>");}
                            %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="e4">Departamento:</label> 
											<div class="col-md-10">
                            <select id="e4" name="org_IdDepartment" class="col-md-12" >
                            <% 
                                //SE LISTAN LOS DEPARTAMENTOS DEL ITS
                                for(String depto : departments ){ 
                                out.print(depto);} 
                            %>
                            </select>
											</div>
											</div>
											<%= "<input id='prog_IdProgram' name='prog_IdProgram' type='hidden' value='"+prog_IdProgram+"'/>"%>
									   </form>
									</div>
								</div>
								<!-- /BOX -->
							</div>
						</div>
						<!-- /ADVANCED -->
						<div class="footer-tools">
							<span class="go-top">
								<i class="fa fa-chevron-up"></i> Top
							</span>
						</div>
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	<script src="../js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="../bootstrap-dist/js/bootstrap.min.js"></script>
	<!-- DATE RANGE PICKER -->
	<script src="../js/bootstrap-daterangepicker/moment.min.js"></script>
	<script src="../js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
	<!-- SLIMSCROLL -->
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
	<!-- BLOCK UI -->
	<script type="text/javascript" src="../js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
	<!-- TYPEHEAD -->
	<script type="text/javascript" src="../js/typeahead/typeahead.min.js"></script>
	<!-- AUTOSIZE -->
	<script type="text/javascript" src="../js/autosize/jquery.autosize.min.js"></script>
	<!-- COUNTABLE -->
	<script type="text/javascript" src="../js/countable/jquery.simplyCountable.min.js"></script>
	<!-- INPUT MASK -->
	<script type="text/javascript" src="../js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
	<!-- FILE UPLOAD -->
	<script type="text/javascript" src="../js/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
	<!-- SELECT2 -->
	<script type="text/javascript" src="../js/select2/select2.min.js"></script>
	<!-- UNIFORM -->
	<script type="text/javascript" src="../js/uniform/jquery.uniform.min.js"></script>
	<!-- JQUERY UPLOAD -->
	<!-- The Templates plugin is included to render the upload/download listings -->
	<script src="../js/blueimp/javascript-template/tmpl.min.js"></script>
	<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="../js/blueimp/javascript-loadimg/load-image.min.js"></script>
	<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script src="../js/blueimp/javascript-canvas-to-blob/canvas-to-blob.min.js"></script>
	<!-- blueimp Gallery script -->
	<script src="../js/blueimp/gallery/jquery.blueimp-gallery.min.js"></script>
	<!-- The basic File Upload plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload.min.js"></script>
	<!-- The File Upload processing plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-process.min.js"></script>
	<!-- The File Upload image preview & resize plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-image.min.js"></script>
	<!-- The File Upload audio preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-audio.min.js"></script>
	<!-- The File Upload video preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-video.min.js"></script>
	<!-- The File Upload validation plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-validate.min.js"></script>
	<!-- The File Upload user interface plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-ui.min.js"></script>
	<!-- The main application script -->
	<script src="../js/jquery-upload/js/main.js"></script>
	<!-- bootbox script -->
	<script src="../js/bootbox/bootbox.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="../js/script.js"></script>
	<script src="../js/jquery-validate/jquery.validate.js"></script>
	<script src="../js/jquery-validate/additional-methods.js"></script>
	<script src="../js/error_message.js"></script>
	<script>
		jQuery(document).ready(function() {		
			App.setPage("forms");  //Set current page
			App.init(); //Initialise plugins and elements
			$("#photo").attr("src","getPhoto?safe_IdUser="+<% if(user != null){out.print(user.getSafe_IdUser());} %>);
			$("#frm_programs").validate({
				rules:{
					code:{
						required: true, minlength: 1, maxlength: 5
					},
					officialCode:{
						required: true, minlength: 1, maxlength: 20
					},
					abbreviation:{
						required: true, minlength: 1, maxlength: 20
					},
					name:{
						required: true, minlength: 1, maxlength: 80
					},
					level:{
						required: true
					},
					mode:{
						required: true
					},
					status:{
						required: true
					},
					org_IdDepartment:{
						required: true
					}
				},
				highlight: function(element){
					$(element).parent().parent().addClass("has-error");
				},
				unhighlight: function(element){
				$(element).parent().parent().removeClass("has-error");
			}});
			
			$("#nuevo").click(function () {
				window.location = "Programs_catalog.jsp?&prog_IdProgram=0";
			});
			$("#editar").click(function () {
				$("#code").removeAttr("disabled");
				$("#officialCode").removeAttr("disabled");
				$("#abbreviation").removeAttr("disabled");
				$("#name").removeAttr("disabled");
				$("#e1").removeAttr("disabled");
				$("#e2").removeAttr("disabled");
				$("#e3").removeAttr("disabled");
				$("#e4").removeAttr("disabled");
			});
			$("#guardar").click(function () {
				$("#frm_programs").valid();
				var codigo=$("#code");
				var officialCode=$("#officialCode");
				var abbreviation=$("#abbreviation");
				var name=$("#name");
				var level=$("#e1");
				var mode =$("#e2");
				var status=$("#e3");
				var depto = $("#e4");
				var levelVal=level.children(":selected").attr('value');
				var modeVal=mode.children(":selected").attr('value');
				var statusVal=status.children(":selected").attr('value');
				var deptoVal=depto.children(":selected").attr('value');
				var prog_IdProgram = $("#prog_IdProgram");

					var values = "prog_IdProgram="+prog_IdProgram.val()+"&officialCode="+officialCode.val()+"&abbreviation="+abbreviation.val()+"&name="+name.val()+"&level="+levelVal+"&mode="+modeVal+"&status="+statusVal+"&org_IdDepartment="+deptoVal+"&code="+codigo.val();
					alert(values);
					dhtmlxAjax.post("Programs_save",values,function(loader){
						var resp = loader.xmlDoc.responseText;
						respo = resp.split("#");
						prog_IdProgram.value=respo[0];
						var resp = parseInt(respo[1]);
						if(resp == 1){
							bootbox.alert("Se guardo con exito!", function() {});
						} else {
							bootbox.alert("Fallo al guardar!", function() {});
						}
					});

				$("#code").attr("disabled","disabled");
				$("#officialCode").attr("disabled","disabled");
				$("#abbreviation").attr("disabled","disabled");
				$("#name").attr("disabled","disabled");
				$("#e1").attr("disabled","disabled");
				$("#e2").attr("disabled","disabled");
				$("#e3").attr("disabled","disabled");
				$("#e4").attr("disabled","disabled");
				$("#editar").removeAttr("disabled");
			});
		});
	</script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
			}else{
				response.sendRedirect("error600.jsp");
			}
		}else{
			response.sendRedirect("error600.jsp");
		}
%>