<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.edcore.model.db.DBConnector"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.edcore.model.safe.User"%>
	<%
		HttpSession sesion;
    sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if(user != null){
			ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
			if(privileges.contains("REIN05")){
					
	DBConnector db = new DBConnector();
	PreparedStatement ps;
	Connection con;
	ResultSet rs;
	int user_IdStudent=Integer.parseInt(request.getParameter("user_IdStudent"));
	int semester = 1;
	String code;
	con=db.open();
	ps = con.prepareStatement("SELECT code,semester FROM UserStudents where user_IdStudent = ?");
	ps.setInt(1, user_IdStudent);
	rs = ps.executeQuery();
	rs.next();
	code = rs.getString("code");
	semester = rs.getInt("semester");
	con.close();
	%>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Reinscripcion con Globales</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="Sistema educativo">
	<meta name="author" content="Instituto Tecnologico Superior de Zapopan">	
	<link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
	<link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
	<!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
	<!-- LIBRERIA EDCORE -->
	<link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.css" >
	<link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxGrid/codebase/skins/dhtmlxgrid_dhx_skyblue.css" >
  <link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxPopup/codebase/skins/dhtmlxpopup_dhx_skyblue.css" >
	<link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxLayout/codebase/dhtmlxlayout.css">
  <link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxLayout/codebase/skins/dhtmlxlayout_dhx_blue.css">
  <link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxWindows/codebase/dhtmlxwindows.css">
  <link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxWindows/codebase/skins/dhtmlxwindows_dhx_blue.css">
	<link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.css" >
	<script type="text/javaScript"  src="../dhtmlx/dhtmlxLayout/codebase/dhtmlxcontainer.js"></script>
  <script type="text/javaScript"  src="../dhtmlx/dhtmlxLayout/codebase/dhtmlxlayout.js"></script>
  <script type="text/javaScript"  src="../dhtmlx/dhtmlxWindows/codebase/dhtmlxwindows.js"></script>
	<script src="../dhtmlx/libCompiler/dhtmlxcommon.js"></script>
  <script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.js"></script>
  <script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgridcell.js"></script>
  <script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_start.js"></script>
  <script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_srnd.js"></script>
  <script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_filter.js"></script>
  <script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgridcell.js"></script>
  <script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_selection.js"></script>
  <script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_nxml.js"></script>
	<script src="../dhtmlx/dhtmlxPopup/codebase/dhtmlxpopup.js"></script>
	<script src="../js/dhxGrid.js"></script>
	<script src="../js/dhxLoad.js"></script>
	<script src="../dhtmlx/dhtmlxDataProcessor/codebase/dhtmlxdataprocessor.js"></script>
	<script src="../dhtmlx/libCompiler/connector.js"></script>
	<script src="ReinsGlobal.js" type="text/javascript"></script>
	<%="<script>var code = '"+code+"';</script>"%>
	<%="<script>var user_IdStudent = '"+user_IdStudent+"';</script>"%>
	<%="<script>var semester = '"+(semester+2)+"';</script>"%>
	<script>
		  var dhxWins;
      var win;
			function print(id){
				dhxWins= new dhtmlXWindows();
				win = dhxWins.createWindow("printHorario", 0,0, 780, 570);
				dhxWins.window("printHorario").centerOnScreen();
				dhxWins.window("printHorario").setText("Horario");
				dhxWins.window("printHorario").denyResize();
				dhxWins.setSkin("dhx_blue");
				dhxWins.window("printHorario").bringToTop();
				dhxWins.window("printHorario").stick();
				dhxWins.window("printHorario").setModal(true);
				win.attachURL("imprimirCarga?user_IdStudent="+id, "AJAX");
			}
	</script>
</head>
<body>
	<script src="../js/spin.js"></script>
	<input type="hidden" id="hiddenControl" >
  <input type="hidden" id="cMin">
  <input type="hidden" id="maximum">
	<!-- HEADER -->
	<%@include file="header.jsp" %>
	<!--/HEADER -->
	<!-- PAGE -->
	<section id="page">
		<!-- SIDEBAR -->
		<%@include file="sidebar.jsp" %>
		<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="index.jsp">Inicio</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Reinscripciones</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="ReinsGlobal_admin.jsp">Globales/Especiales</a>
										</li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										<h3 class="content-title pull-left">Reinscripci�n con Globales/Especiales</h3>
										<span class="date-range pull-right">
										<p class="btn-toolbar">
											<%if(privileges.contains("REIN15")){%>
											<button id="guardar" class="btn btn-success" onClick="proto();" disabled=""><i class="fa fa-save"></i> Guardar</button>
											<%}%>
											<%if(privileges.contains("REIN16")){%>
											<button id="eliminar" class="btn btn-success" onClick="deleteData();" disabled=""><i class="fa fa-eraser"></i> Eliminar</button>
											<%}%>
											<%if(privileges.contains("REIN17")){%>
											<button id="Pdf" name="Pdf" class="btn btn-success" disabled=""><i class="fa fa-print"></i> Imprimir</button>
											<%}%>
										</p>
										</span>
									</div>
									<div class="description"></div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						<!-- DASHBOARD CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="box border red">
									<div class="box-title">
										<h5><i class="fa fa-bars"></i>
											Nombre : <span id="name"></span> | Semestre : <span id="semester">0</span> | Creditos Asignados : <span id="sum">0</span> | Carga Maxima : <span id="maxLabel">0</span> | Carga Minima : <span id="minLabel">0</span>
										</h5>
									</div>
									<div class="box-body" style="padding-bottom: 10px;">
                                                        <table id="grid" class='dhtmlxGrid' gridWidth="100%" gridHeight="100%" style="width:100% ; height:100%" lightnavigation="true" multiline="true"  imgpath="../dhtmlx/dhtmlxGrid/codebase/imgs/">
                                                            <tr>
                                                                <td width="20px"> </td>
                                                                <td width="40px">Clave</td>
                                                                <td width="120px">Materia</td>
                                                                <td width="30px">CD</td>
                                                                <td width="30px">ED</td>
                                                                <td width="110px">Lunes</td>
                                                                <td width="110px">Martes</td>
                                                                <td width="110px">Miercoles</td>
                                                                <td width="110px">Jueves</td>
                                                                <td width="110px">Viernes</td>
                                                                <td width="110px">S�bado</td>
                                                                <td width="30px">IN</td>
                                                                <td width="30px">LD</td>
                                                                <td width="50px">GPO</td>
                                                                </td>
                                                            </tr>
                                                        </table>
									</div>
								</div>
								</div>
						</div>
					   <!-- /DASHBOARD CONTENT -->
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
			<style>
		.dhtmlxGrid tr { height: 20px; }
	</style>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	<script src="../js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="../bootstrap-dist/js/bootstrap.min.js"></script>
	<!-- DATE RANGE PICKER -->
	<script src="../js/bootstrap-daterangepicker/moment.min.js"></script>	
	<script src="../js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
	<!-- EASY PIE CHART -->
	<script type="text/javascript" src="../js/easypiechart/jquery.easypiechart.min.js"></script>
	<!-- bootbox script -->
	<script src="../js/bootbox/bootbox.min.js"></script>
	<!-- FULL CALENDAR -->
	<script type="text/javascript" src="../js/fullcalendar/fullcalendar.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="../js/script.js"></script>
	<script>
		jQuery(document).ready(function() {
			App.setPage("index");  //Set current page
			App.init(); //Initialise plugins and elements
			$("#photo").attr("src","getPhoto?safe_IdUser="+<% if(user != null){out.print(user.getSafe_IdUser());} %>);
			engine();			
			searchStud(code);
			$("#Pdf").click(function () {
				dhxPopUp.hide();
				print(user_IdStudent);
			});
		});
	</script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
			}else{
				response.sendRedirect("error600.jsp");
			}
		}else{
			response.sendRedirect("error600.jsp");
		}
%>