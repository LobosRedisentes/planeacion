/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery(document).ready(function() {
    App.setPage("forms");  //Set current page
    App.init(); //Initialise plugins and elements
    $("#photo").attr("src","getPhoto?safe_IdUser="+safe_IdUser);
    var response = "../getAdminView?sql=select * FROM VADM003 WHERE tipo <> 'NUEVO' and status <> 'NO INSCRITO' &id=user_IdApplicant&parameters=matricula,nombre,apellidoPaterno,apellidoMaterno,programa,tipo,status,calendario";
                
    grid = new dhtmlXGridObject('grid_container');
    grid.setImagePath("../dhtmlx/dhtmlxGrid/codebase/imgs/");
    grid.setHeader("Matricula,Nombre,Paterno,Materno,Programa,Tipo,Estatus,Calendario");
    grid.attachHeader("#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter");
    grid.setInitWidths("*,*,*,*,*,*,*,*");
    grid.setColAlign("left,left,left,left,left,left,left,left");
    grid.setColSorting("str,str,str,str,str,str,str,str");
    grid.setEditable(false);
    grid.init();
    grid.setSkin("clear");
    grid.enableSmartRendering(true,50);
    grid.objBox.style.overflowX = "hidden";
    grid.load(response,function(){});

    $('#btn_nuevo').click(function(){
        var box = bootbox.dialog({
            title: "<h4>Nueva matricula</h4>",
            message: '<div class="row">' +
                '<div class="col-md-12"> ' +
                '<form class="form-horizontal" name="frm_nuevo"  id="frm_nuevo"> ' +
                '<div class="form-group"> ' +
                '<label class="col-md-2 control-label" for="user_IdApplicant">Aspirante</label> ' +
                '<div class="col-md-10"> ' +
                '<select id="user_IdApplicant" class="col-md-12"  name="user_IdApplicant"></select>' +
                '</div> ' +
                '</div> ' +
                '<div class="form-group"> ' +
                '<label class="col-md-2 control-label" for="cal_IdCalSchool">Calendario</label> ' +
                '<div class="col-md-10"> ' +
                '<select id="cal_IdCalSchool" class="col-md-12"  name="cal_IdCalSchool"></select>' +
                '</div> ' +
                '</div> ' +
                '</form> </div></div>',
            buttons: {
                success: {
                    label: "Guardar",
                    className: "btn-danger",
                    callback: function () {
                        $("#frm_nuevo").validate({
                            rules:{
                                user_IdApplicant:{required: true},
                                cal_IdCalSchool:{required: true}
                            },
                            highlight: function(element){$(element).parent().parent().addClass("has-error");},
                            unhighlight: function(element){$(element).parent().parent().removeClass("has-error");}
                        });
                        if($("#frm_nuevo").valid()){
                            var user_IdApplicant = $('#user_IdApplicant').children(":selected").attr("value");
                            var cal_IdCalSchool = $('#cal_IdCalSchool').children(":selected").attr("value");
                            var save = $.ajax({url:"GenerateCode",data:"user_IdApplicant="+user_IdApplicant+"&cal_IdCalSchool="+cal_IdCalSchool,method:"POST"});
                            save.done(function(msn){
                                if(msn === '' || msn === '0'){
                                    bootbox.alert("No se pudo guardar! ", function() {});
                                }else{
                                    grid.clearAndLoad(response);
                                }
                            });
                        }else{this.stopPropagation();}
                    }
                },
                'Danger!': {
                    label: "Cancelar",
                    className: "btn-danger",
                    callback: function() {}
                }
            }
        });
        box.on("shown.bs.modal", function() {
            var cal = $.ajax({url:"select_calschool",data:"sql=SELECT * FROM CalSchool ORDER BY cal_IdCalSchool DESC&id="+(Math.round(Math.random() * (5-1000)) + 5),method:"POST"});
            cal.done(function(msn){$("#cal_IdCalSchool").html(msn);});
            $('#cal_IdCalSchool').parents('.bootbox').removeAttr('tabindex');
            $('#cal_IdCalSchool').select2();
            var asp = $.ajax({url:"select_applicants",data:"sql=SELECT * FROM VADM003 WHERE tipo <> 'NUEVO' and matricula = '000' ORDER BY apellidoPaterno, apellidoMaterno, nombre&id="+((Math.round(Math.random() * (5-1000)) + 5)),method:"POST"});
            asp.done(function(msn){$("#user_IdApplicant").html(msn);});
            $('#user_IdApplicant').parents('.bootbox').removeAttr('tabindex');
            $('#user_IdApplicant').select2();
        });
    });        
});
