<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.edcore.model.db.DBConnector"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.edcore.model.safe.User"%>
	<%
		HttpSession sesion;
    sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if(user != null){
			ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
			if(privileges.contains("CONF06")){
	%>
<%
	ResultSet rs;
	DBConnector db = new DBConnector();
	PreparedStatement ps;
	Connection con;

    int idReg = Integer.parseInt(request.getParameter("clave"));
    int flag = Integer.parseInt(request.getParameter("addOrEdit"));
    int idCals = 0;
    int idComp = 0;
    
    String clave = "";
    String titulo = "";
    String fechaIni = "";
    String fechaFin = "";
    String status = "";

		con=db.open();
    if(idReg > 0){
        ps = con.prepareStatement("select * from CalRegistrations where cal_IdRegistration = "+idReg+" ");
        rs = ps.executeQuery();

        while(rs != null && rs.next()){
            idReg = Integer.parseInt(rs.getString("cal_IdRegistration"));
            idCals = Integer.parseInt(rs.getString("cal_IdCalSchool"));
            idComp = Integer.parseInt(rs.getString("safe_IdComponent"));
            clave = rs.getString("code");
            titulo = rs.getString("title");
            fechaIni = rs.getString("start");
            fechaFin = rs.getString("end");
            status = rs.getString("status");
        }          
    }
%>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Inscripci�n a Procesos</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
	<link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
	
	<link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">    
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
	<!-- TYPEAHEAD -->
	<link rel="stylesheet" type="text/css" href="../js/typeahead/typeahead.css" />
	<!-- FILE UPLOAD -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-fileupload/bootstrap-fileupload.min.css" />
	<!-- SELECT2 -->
	<link rel="stylesheet" type="text/css" href="../js/select2/select2.min.css" />
	<!-- UNIFORM -->
	<link rel="stylesheet" type="text/css" href="../js/uniform/css/uniform.default.min.css" />
	<!-- JQUERY UPLOAD -->
	<!-- blueimp Gallery styles -->
	<link rel="stylesheet" href="../js/blueimp/gallery/blueimp-gallery.min.css">
	<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload.css">
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload-ui.css">
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	<style>
	 .error-message, label.error {
	    color: #ff0000;
	    margin:0;
	    display: inline;
	    font-size: 1em !important;
	    font-weight:bold;
	 }
	 </style>
	<!-- LIBRERIA EDCORE -->
  <script src="../dhtmlx/libCompiler/dhtmlxcommon.js"></script>
	<script src="../js/action_dhx.js"></script>
</head>
<body>
	<!-- HEADER -->
	<%@include file="header.jsp" %>
	<!--/HEADER -->
	<!-- PAGE -->
	<section id="page">
				<!-- SIDEBAR -->
				<%@include file="sidebar.jsp" %>
				<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<!-- STYLER -->
									
									<!-- /STYLER -->
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="index.jsp">Inicio</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Configuraci�n</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Fechas</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="CalRegistrations_admin.jsp">Inscripciones</a>
										</li>
										<li>Nuevo/Editar</li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										<h3 class="content-title pull-left">Cat�logo - Incripcion de Procesos</h3>
										<span class="date-range pull-right">
										<p class="btn-toolbar">
											<%if(privileges.contains("CONF09")){%>
											<button id="nuevo" class="btn btn-success" ><i class="fa fa-file-text-o"></i> Nuevo</button>
											<%}%>
											<%if(privileges.contains("CONF10")){%>
											<button id="editar" class="btn btn-success" disabled=""><i class="fa fa-pencil-square-o"></i> Editar</button>
											<%}%>
											<%if(privileges.contains("CONF11")){%>
											<button id="guardar" class="btn btn-success" ><i class="fa fa-save"></i> Guardar</button>
											<%}%>
										</p>
										</span>
									</div>
									<div class="description"></div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						<!-- ADVANCED -->
						<div class="row">
							<div class="col-md-12">
								<!-- BOX -->
								<div class="box border red">
									<div class="box-title">
										<h4><i class="fa fa-bars"></i>Nuevo/Editar Fecha Inscripcion</h4>
									</div>
									<div class="box-body">
										<form class="form-horizontal " action="#" id="frm_inscripciones">
										  <div class="form-group">
											 <label class="col-md-2 control-label" for="code">Clave</label> 
											 <div class="col-md-10">
                   <% 
                   ps = con.prepareStatement("select max(cal_IdRegistration) as maximo from CalRegistrations");
                   rs = ps.executeQuery();
                   
                   int maxValue = 0;
                        if(rs != null && rs.next()){
                               maxValue = rs.getInt("maximo");
                        }
                   
                        int result2 = (maxValue + 1); 
                        
                        out.println("<input type ='hidden' id='max' value='"+result2+"'/>");
                        
                   if(idReg == 0){
                        ps = con.prepareStatement("select max(cal_IdRegistration) as maximo from CalRegistrations");
                        rs = ps.executeQuery();
                           if(rs != null && rs.next()){
                               idReg = rs.getInt("maximo");
                           }
                           
                           int result = (idReg + 1);
                           
                           out.println("<input  type ='hidden' id='idReg' value= '"+result+"'/>");
                   } else {
                       
                       out.println("<input  type ='hidden' id='idReg' value= '"+idReg+"'/>");
                       
                   }
                      out.println("<input  type ='hidden' id='flag' value= '"+flag+"'/>");
                      
                      if(clave !=null  && ! clave.equals("")){
                         out.println("<input type='text' class='form-control' autocomplete='off' id='code' name='code' value='"+clave+"'>");
                      }else{
                         out.println("<input type='text' class='form-control' autocomplete='off'  id='code' name='code' >");    
                      }
                   %>
											 </div>
										  </div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="title">T�tulo:</label> 
											<div class="col-md-10">
                   <% 
                      if(titulo !=null  && ! titulo.equals("")){
                         out.println("<input type='text' class='form-control' autocomplete='off' id='title' name='title' value='"+titulo+"'>");
                      }else{
                         out.println("<input type='text' class='form-control' autocomplete='off' id='title' name='title' >");    
                      }
                   %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="e1">Proceso:</label> 
											<div class="col-md-10">
												<%
                        ps = con.prepareStatement("SELECT safe_IdComponent, name FROM SafeComponents WHERE type = 'PROCESO';");
                        rs = ps.executeQuery();
                        
                            out.println("<select id='e1' class='col-md-12' name='process' >");
                            out.println("<option id='0' value='' selected>Selecciona ..</option>");
                        
                        while(rs.next()){                              
                           if(rs.getInt("safe_IdComponent")== idComp){
                                 out.println("<option id='"+rs.getInt("safe_IdComponent")
                                         +"' selected>"+rs.getString("name")+"</option>");   
                           }else{
                              out.println("<option id='"+rs.getInt("safe_IdComponent")
                                      +"'>"+rs.getString("name")+"</option>");
                           }
                       }                       
                       out.println("</select>"); 
											 %>
											</div>
											</div>
											<div class="form-group">
											 <label class="col-md-2 control-label" for="start">Inicio Fecha:</label> 
											 <div class="col-md-10">
                        <%
                      if(fechaIni !=null  && ! fechaIni.equals("")){
                         out.println("<input type='text' class='form-control' id='start' name='start' placeholder='yyy/mm/dd' value='"+fechaIni+"'>");
                      }else{
                         out.println("<input type='text' class='form-control' id='start' name='start' placeholder='yyy/mm/dd'>");    
                      }
                        %>
											 </div>
											</div>
										  <div class="form-group">
											 <label class="col-md-2 control-label" for="FechaInicio">Termino Fecha:</label> 
											 <div class="col-md-10">
                        <%
                      if(fechaFin!=null  && !fechaFin.equals("")){
                         out.println("<input type='text' class='form-control' placeholder='yyy/mm/dd' id='end' name='end' value='"+fechaFin+"'>");
                      }else{
                         out.println("<input type='text' class='form-control' placeholder='yyy/mm/dd' id='end' name='end' >");    
                      }
                        %>
											 </div>
											</div>
										  <div class="form-group">
											 <label class="col-md-2 control-label" for="e2">Calendario:</label> 
											 <div class="col-md-10">
                     <%                    
                        ps = con.prepareStatement("SELECT name, cal_IdCalSchool FROM CalSchool;");
                        rs = ps.executeQuery();
   
                        out.println("<select id='e2' class='col-md-12' name='cicle'>");                        
                        out.println("<option id='0' value='' selected>Selecciona ..</option>");
                        
                        while(rs.next()){                              
                           if(rs.getInt("cal_IdCalSchool")== idCals){
                                 out.println("<option id='"+rs.getInt("cal_IdCalSchool")
                                         +"' selected>"+rs.getString("name")+"</option>");   
                           }else{
                              out.println("<option id='"+rs.getInt("cal_IdCalSchool")
                                      +"'>"+rs.getString("name")+"</option>");
                           }
                       }                       
                       out.println("</select>"); 
                       db.close();  
                     %>
											 </div>
											</div>
										  <div class="form-group">
											 <label class="col-md-2 control-label" for="e3">Status:</label> 
											 <div class="col-md-10">
												 <select class="col-md-12" id='e3' name="status"/>
                        <%
                            String statusArray[]={"ACTIVO","INACTIVO"};
														out.println("<option id='0' value='' selected>Selecciona ..</option>");
                            for(int j=0;j<2;j++){
                                if(status.equals(statusArray[j])){
                                    out.print("<option id='"+statusArray[j]+"' selected>"+statusArray[j]+"</option>");
                                }else{
                                    out.print("<option id='"+statusArray[j]+"'>"+statusArray[j]+"</option>");
                                }
                            }
                        %>
                    </select>
											 </div>
											</div>
									   </form>
									</div>
								</div>
								<!-- /BOX -->
							</div>
						</div>
						<!-- /ADVANCED -->
						<div class="footer-tools">
							<span class="go-top">
								<i class="fa fa-chevron-up"></i> Top
							</span>
						</div>
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	<script src="../js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="../bootstrap-dist/js/bootstrap.min.js"></script>
	<!-- DATE RANGE PICKER -->
	<script src="../js/bootstrap-daterangepicker/moment.min.js"></script>
	<script src="../js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
	<!-- SLIMSCROLL -->
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
	<!-- BLOCK UI -->
	<script type="text/javascript" src="../js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
	<!-- TYPEHEAD -->
	<script type="text/javascript" src="../js/typeahead/typeahead.min.js"></script>
	<!-- AUTOSIZE -->
	<script type="text/javascript" src="../js/autosize/jquery.autosize.min.js"></script>
	<!-- COUNTABLE -->
	<script type="text/javascript" src="../js/countable/jquery.simplyCountable.min.js"></script>
	<!-- INPUT MASK -->
	<script type="text/javascript" src="../js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
	<!-- FILE UPLOAD -->
	<script type="text/javascript" src="../js/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
	<!-- SELECT2 -->
	<script type="text/javascript" src="../js/select2/select2.min.js"></script>
	<!-- UNIFORM -->
	<script type="text/javascript" src="../js/uniform/jquery.uniform.min.js"></script>
	<!-- JQUERY UPLOAD -->
	<!-- The Templates plugin is included to render the upload/download listings -->
	<script src="../js/blueimp/javascript-template/tmpl.min.js"></script>
	<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="../js/blueimp/javascript-loadimg/load-image.min.js"></script>
	<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script src="../js/blueimp/javascript-canvas-to-blob/canvas-to-blob.min.js"></script>
	<!-- blueimp Gallery script -->
	<script src="../js/blueimp/gallery/jquery.blueimp-gallery.min.js"></script>
	<!-- The basic File Upload plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload.min.js"></script>
	<!-- The File Upload processing plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-process.min.js"></script>
	<!-- The File Upload image preview & resize plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-image.min.js"></script>
	<!-- The File Upload audio preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-audio.min.js"></script>
	<!-- The File Upload video preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-video.min.js"></script>
	<!-- The File Upload validation plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-validate.min.js"></script>
	<!-- The File Upload user interface plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-ui.min.js"></script>
	<!-- The main application script -->
	<script src="../js/jquery-upload/js/main.js"></script>
	<!-- bootbox script -->
	<script src="../js/bootbox/bootbox.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="../js/script.js"></script>
	<script src="../js/jquery-validate/jquery.validate.js"></script>
	<script src="../js/jquery-validate/additional-methods.js"></script>
	<script src="../js/error_message.js"></script>
	<script>
		jQuery(document).ready(function() {		
			App.setPage("forms");  //Set current page
			App.init(); //Initialise plugins and elements
			$("#photo").attr("src","getPhoto?safe_IdUser="+<% if(user != null){out.print(user.getSafe_IdUser());} %>);
			var addOrEdit=<%out.println(request.getParameter("addOrEdit")+";"); %>
			
                      $( "#start" ).datepicker({
                        defaultDate: "+1w",
                        changeMonth: true,
                        numberOfMonths: 1,
                        dateFormat:"yy-mm-dd",
                        onClose: function( selectedDate ) {
                          $( "#end" ).datepicker( "option", "minDate", selectedDate );
                        }
                      });
                     
                      $( "#end" ).datepicker({
                        defaultDate: "+1w",
                        changeMonth: true,
                        numberOfMonths: 1,
                        dateFormat:"yy-mm-dd",
                        onClose: function( selectedDate ) {
                          $( "#start" ).datepicker( "option", "maxDate", selectedDate );
                        }
                      });
			
			$("#frm_inscripciones").validate({
				rules:{
					code:{
						required: true, minlength: 5, maxlength: 15
					},
					title:{
						required: true, minlength: 5, maxlength: 60
					},
					process:{
						required: true
					},
					start:{
						required: true
					},
					end:{
						required: true
					},
					cicle:{
						required: true
					},
					status:{
						required: true
					}
				},
				highlight: function(element){
					$(element).parent().parent().addClass("has-error");
				},
				unhighlight: function(element){
				$(element).parent().parent().removeClass("has-error");
			}});
			
	 var code    = $("#code");
   var title   = $("#title");
   var start   = $("#start");
   var end     = $("#end");
   var idCalS   = $("#e2");
	 var idComp  = $("#e1");
   var status  = $("#e3");
   var flag    = $("#flag");
   var idReg   = $("#idReg");
	 
			
			$("#nuevo").click(function () {
				window.location = "CalRegistrations_catalog.jsp?clave=0&addOrEdit=0";
			});
			$("#editar").click(function () {
        code.removeAttr('disabled');
        title.removeAttr('disabled');
        idComp.removeAttr('disabled');
        start.removeAttr('disabled');
        end.removeAttr('disabled');
        idCalS.removeAttr('disabled');
        status.removeAttr('disabled');
        
        flag.val('1');
        var value = idReg.val();
        idReg.val(value);

				addOrEdit=1;
				$("#editar").attr("disabled","disabled");
			});
			$("#guardar").click(function () {
				$("#frm_inscripciones").valid();
        
				var idRegistration = idReg.val();
        var idCalSchool    = idCalS.children(":selected").attr("id");
        var idComponent    = idComp.children(":selected").attr("id");
        var Code    = code.val().trim();
        var Title   = title.val().trim();
        var Start   = start.val().trim();
        var End     = end.val().trim();
        var bandera = flag.val().trim();
        var Status  = status.children(":selected").attr("id");
        var data    = "values="+idRegistration+"#"+idCalSchool+"#"+idComponent+"#"+Code+"#"+Title+"#"+Start+"#"+End+"#"+Status+"&addOrEdit="+addOrEdit;
        var max     = parseInt($("#max").val());

					dhtmlxAjax.post("CalRegistrations_save",data,function(loader){
						var resp = loader.xmlDoc.responseText;
						if(resp == "1"){
							bootbox.alert("Se guardo con exito!", function() {});
						} else {
							bootbox.alert("Fallo al guardar!", function() {});
						}
					});

            code.attr('disabled','disabled');
            title.attr('disabled','disabled');
            start.attr('disabled','disabled');
            end.attr('disabled','disabled');
            status.attr('disabled','disabled');
            idReg.attr('disabled','disabled');
            idCalS.attr('disabled','disabled');
            idComp.attr('disabled','disabled');
            var increment = (max + 1);
            $("#max").val(increment);

					$("#editar").removeAttr("disabled");
			});
		});
	</script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
			}else{
				response.sendRedirect("error600.jsp");
			}
		}else{
			response.sendRedirect("error600.jsp");
		}
%>