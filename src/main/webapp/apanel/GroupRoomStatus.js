/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery(document).ready(function() {
    App.setPage("forms");  //Set current page
    App.init(); //Initialise plugins and elements
    $("#photo").attr("src","getPhoto?safe_IdUser="+safe_IdUser);
    var response = "../getAdminView?sql=select org_IdRoom, code, maxCapacity,type FROM OrgRooms &id=org_IdRoom&parameters=code,maxCapacity,type";
    var IDRow;
    var cal_IdCalSchool;
    var aula;
     
    grid = new dhtmlXGridObject('grid_container');
    grid.setImagePath("../dhtmlx/dhtmlxGrid/codebase/imgs/");
    grid.setHeader("Clave,Capacidad,Tipo");
    grid.attachHeader("#connector_text_filter,#connector_text_filter,#connector_text_filter");
    grid.setInitWidths("*,*,*,*");
    grid.setColAlign("left,left,left");
    grid.setColSorting("str,str,str");
    grid.setEditable(false);
    grid.init();
    grid.setSkin("clear");
    grid.enableSmartRendering(true,50);
    grid.objBox.style.overflowX = "hidden";
    grid.load(response,function(){});
    
    grid.attachEvent("onRowSelect",function(id){
        IDRow = id;
        aula = grid.cellById(id,0).getValue();
	$("#btn_consultar").removeAttr("disabled");
    });

    $('#btn_consultar').click(function(){
        var box = bootbox.dialog({
            title: "<h4>Calendario Escolar</h4>",
            message: '<div class="row">' +
                '<div class="col-md-12"> ' +
                '<form class="form-horizontal" name="frm_calendario"  id="frm_calendario"> ' +
                '<div class="form-group"> ' +
                '<label class="col-md-2 control-label" for="cal_IdCalSchool">Calendario</label> ' +
                '<div class="col-md-10"> ' +
                '<select id="cal_IdCalSchool" class="col-md-12"  name="cal_IdCalSchool"></select>' +
                '</div> ' +
                '</div> ' +
                '</form> </div></div>',
            buttons: {
                success: {
                    label: "Aceptar",
                    className: "btn-danger",
                    callback: function () {
                        $("#frm_calendario").validate({
                            rules:{
                                cal_IdCalSchool:{required: true}
                            },
                            highlight: function(element){$(element).parent().parent().addClass("has-error");},
                            unhighlight: function(element){$(element).parent().parent().removeClass("has-error");}
                        });
                        if($("#frm_calendario").valid()){
                            cal_IdCalSchool = $('#cal_IdCalSchool').children(":selected").attr("value");
                            $('iframe').attr("src","GroupRoom.jsp?org_IdRoom="+IDRow+"&cal_IdCalSchool="+cal_IdCalSchool+"&aula="+aula);
                            $('#myModal').modal({show:true});
                        }else{this.stopPropagation();}
                    }
                },
                'Danger!': {
                    label: "Cancelar",
                    className: "btn-danger",
                    callback: function() {}
                }
            }
        });
        box.on("shown.bs.modal", function() {
            var cal = $.ajax({url:"select_calschool",data:"sql=SELECT * FROM CalSchool ORDER BY cal_IdCalSchool DESC&id="+(Math.round(Math.random() * (5-1000)) + 5),method:"POST"});
            cal.done(function(msn){$("#cal_IdCalSchool").html(msn);});
            $('#cal_IdCalSchool').parents('.bootbox').removeAttr('tabindex');
            $('#cal_IdCalSchool').select2();
        });
    });
    $('#btn_print').click(function(){
        $('iframe').attr("src","imprimirHorarioAula?org_IdRoom="+IDRow+"&cal_IdCalSchool="+cal_IdCalSchool);
    });
});
