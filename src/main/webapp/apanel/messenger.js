jQuery(document).ready(function() {
    App.setPage("forms");  //Set current page
    App.init(); //Initialise plugins and elements
    $("#photo").attr("src","getPhoto?safe_IdUser="+id);
    var response = "../getAdminView?sql=select * from VIW013 where fromAccount = "+account+" or toAccount ="+account+" &id=pub_IdMessenger&parameters=fromEmail,toEmail,subject,date";
    var IDRow;

    grid = new dhtmlXGridObject('grid_container');
    grid.setImagePath("../dhtmlx/dhtmlxGrid/codebase/imgs/");
    grid.setHeader("De,Para,Asunto,Fecha");
    grid.attachHeader("#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter");
    grid.setInitWidths("*,*,*,*");
    grid.setColAlign("left,left,left,left");
    grid.setColSorting("str,str,str,str");
    grid.setEditable(false);
    grid.init();
    grid.setSkin("clear");
    grid.enableSmartRendering(true,50);
    grid.objBox.style.overflowX = "hidden";
    spInit();
    grid.load(response,function(){
        spStop();
        //columnAutoSize();
    });
    grid.attachEvent("onRowSelect",function(id){
        IDRow = id;
        $("#btn_consultar").removeAttr("disabled");
        $("#btn_eliminar").removeAttr("disabled");
        var msn = $.ajax({url:"getMessageFrm",data:"pub_IdMessenger="+id,method:"POST"});
        msn.done(function(msn){
            $('#subject').attr('disabled', true);
            $('#subject').val(msn['subject']);
            $('#body').attr('disabled', true);
            $('#body').val(msn['body']);
        });
    });
    
    $("#btn_nuevo").click(function(){
        $("#para").attr("style", "display: block");
        $("#btn_guardar").attr("style", "display: block");
        $('#safe_IdUserAccount').select2({
            multiple: false,
            maximumSelectionSize: 1,
            tags: [],
            ajax: {
                url: 'getUserAccountOptions',
                dataType: 'json',
                type: "POST",
                quietMillis: 150,
                data: function (term, page) {
                    return {
                        term: term
                    };
                },
                results: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.firstLast +' '+ item.secondLast+' '+ item.name+ ' - '+item.rol,
                                id: item.id
                            };
                        })
                    };
                },
                cache: true,
                placeholder: "Selecciona ..."
            }
        });
        $("#safe_IdUserAccount").removeAttr("disabled");
        $('#safe_IdUserAccount').val('');
        $("#subject").removeAttr("disabled");
        $('#subject').val('');
        $("#body").removeAttr("disabled");
        $('#body').val('');
        $('#box_nuevo').modal({show:true});
    });
    
    $("#btn_consultar").click(function(){
        $("#para").attr("style", "display: none");
        $("#btn_guardar").attr("style", "display: none");
        $('#box_nuevo').modal({show:true});
    });
    $("#btn_eliminar").click(function(){
        bootbox.confirm("Desea eliminar el mensaje seleccionado?", function(result) {
            if(result){
                var msn_delete = $.ajax({url:"messengerDelete",data:"pub_IdMessenger="+IDRow,method:"POST"});
                msn_delete.done(function(msn){
                    grid.clearAndLoad(response);
                    $('#btn_consultar').attr('disabled', true);
                    $('#btn_eliminar').attr('disabled', true);
                });
            }
        });
    });
    
    $("#btn_guardar").click(function(event){
        var safe_IdUserAccount = $('#safe_IdUserAccount').val();
        var subject = $('#subject').val();
        var body = $('#body').val();
        if(safe_IdUserAccount !== '' && subject !== '' && body !== ''){
            var save = $.ajax({url:"messengerSave",data:"userFrom="+id+"&accountTo="+safe_IdUserAccount+"&subject="+subject+"&body="+body,method:"POST"});
            save.done(function(msn){
                if(msn === '' || msn === '0'){
                    bootbox.alert("No se pudo enviar el mensaje! ", function() {});
                    event.preventDefault();
                }else{
                    bootbox.alert("Tu mensaje se envio con exito! ", function() {});
                    $('#subject').attr('disabled', true);
                    $('#body').attr('disabled', true);
                    $('#btn_guardar').attr('disabled', true);
                    event.preventDefault();
                    grid.clearAndLoad(response);
                }
            });
            event.preventDefault();
        }else{
            bootbox.alert("Favor de ingresar todos los datos! ", function() {});
            event.preventDefault();
        }
    });
    
    $("#btn_actualizar").click(function () {
        grid.clearAndLoad(response);
    });		
});