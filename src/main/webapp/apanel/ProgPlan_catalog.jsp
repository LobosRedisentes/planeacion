<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.edcore.model.db.DBConnector"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.edcore.model.safe.User"%>
	<%
		HttpSession sesion;
    sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if(user != null){
			ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
			if(privileges.contains("CAT25")){
	%>

<%
	ResultSet rs;
	DBConnector db = new DBConnector();
	PreparedStatement ps;
	Connection con;
	int progId=Integer.parseInt(request.getParameter("progIdPlan"));
	int addOrEdit=Integer.parseInt(request.getParameter("addOrEdit"));
	con=db.open();
	
    String clave="";
    String claveOf="";
    int creditos=0;
    String tipo="";
    String status="";
    int programa=0;
    int cargaMax=0;
    int cargaMin=0;
    int cargaEsp=0;
    int crucePer=0;
    int credNecServi=0;
    int cargCredMaxServi=0;
    int credNeceResid=0;
    int cargCredMaxResie=0;
		
    if(progId>0){
        ps = con.prepareStatement("SELECT * FROM ProgPlan WHERE prog_idPlan=?");
        ps.setInt(1, progId);
    rs = ps.executeQuery();
    
    while(rs != null && rs.next()){
        
        clave =rs.getString("code");
        claveOf =rs.getString("officialCode");
        creditos =rs.getInt("credits");
        tipo =rs.getString("type");
        status =rs.getString("status");
        programa =rs.getInt("prog_IdProgram");
        cargaMax =rs.getInt("loadMaxCred");
        cargaMin =rs.getInt("loadMinCred");
        cargaEsp =rs.getInt("loadMaxSpecial");
        crucePer =rs.getInt("hrsCrossing");
        credNecServi =rs.getInt("credReqServ");
        cargCredMaxServi =rs.getInt("loadMaxServ");
        credNeceResid =rs.getInt("credReqRes");
        cargCredMaxResie =rs.getInt("loadMaxRes");
            }   
        
    }
%>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Planes de Estudio</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
	<link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
	
	<link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- DATE RANGE PICKER -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
	<!-- TYPEAHEAD -->
	<link rel="stylesheet" type="text/css" href="../js/typeahead/typeahead.css" />
	<!-- FILE UPLOAD -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-fileupload/bootstrap-fileupload.min.css" />
	<!-- SELECT2 -->
	<link rel="stylesheet" type="text/css" href="../js/select2/select2.min.css" />
	<!-- UNIFORM -->
	<link rel="stylesheet" type="text/css" href="../js/uniform/css/uniform.default.min.css" />
	<!-- JQUERY UPLOAD -->
	<!-- blueimp Gallery styles -->
	<link rel="stylesheet" href="../js/blueimp/gallery/blueimp-gallery.min.css">
	<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload.css">
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload-ui.css">
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	<style>
	 .error-message, label.error {
	    color: #ff0000;
	    margin:0;
	    display: inline;
	    font-size: 1em !important;
	    font-weight:bold;
	 }
	 </style>
	<!-- LIBRERIA EDCORE -->
  <script src="../dhtmlx/libCompiler/dhtmlxcommon.js"></script>
	<script src="../js/action_dhx.js"></script>
</head>
<body>
	<!-- HEADER -->
	<%@include file="header.jsp" %>
	<!--/HEADER -->
	<!-- PAGE -->
	<section id="page">
				<!-- SIDEBAR -->
				<%@include file="sidebar.jsp" %>
				<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<!-- STYLER -->
									
									<!-- /STYLER -->
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="index.jsp">Inicio</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Catalogos</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="ProgPlan_admin.jsp">Planes de Estudio</a>
										</li>
										<li>Nuevo/Editar</li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										<h3 class="content-title pull-left">Cat�logo de Planes de Estudio</h3>
										<span class="date-range pull-right">
										<p class="btn-toolbar">
											<%if(privileges.contains("CAT48")){%>
											<button id="nuevo" class="btn btn-success" ><i class="fa fa-file-text-o"></i> Nuevo</button>
											<%}%>
											<%if(privileges.contains("CAT49")){%>
											<button id="editar" class="btn btn-success" disabled=""><i class="fa fa-pencil-square-o"></i> Editar</button>
											<%}%>
											<%if(privileges.contains("CAT50")){%>
											<button id="guardar" class="btn btn-success" ><i class="fa fa-save"></i> Guardar</button>
											<%}%>
										</p>
										</span>
									</div>
									<div class="description"></div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						<!-- ADVANCED -->
						<div class="row">
							<div class="col-md-12">
								<!-- BOX -->
								<div class="box border red">
									<div class="box-title">
										<h4><i class="fa fa-bars"></i>Nuevo/Editar Plan de Estudio</h4>
									</div>
									<div class="box-body">
										<form class="form-horizontal " action="#" id="frm_plan">
										  <div class="form-group">
											 <label class="col-md-2 control-label" for="clave">Clave</label> 
											 <div class="col-md-10">
                <% 
                if(progId>0){
                out.println("<input class='form-control' autocomplete='off' type='text'  id='clave' name='clave' placeholder='Clave' value='"+clave+"'>");
                }else{
                out.println("<input class='form-control' autocomplete='off' type='text'  id='clave' name='clave' placeholder='Clave' >");    
                }
                %>
											 </div>
										  </div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="cvoficial">ClaveOficial:</label> 
											<div class="col-md-10">
                <% 
                if(progId>0){
                out.println("<input class='form-control' autocomplete='off' type='text'  id='cvoficial' name='cvoficial' placeholder='Clave' value='"+claveOf+"'>");
                }else{
                out.println("<input class='form-control' autocomplete='off' type='text'  id='cvoficial' name='cvoficial' placeholder='Clave' >");    
                }
                %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="credi">Creditos:</label> 
											<div class="col-md-10">
                <% 
                if(progId>0){
                out.println("<input class='form-control' autocomplete='off' type='text'  id='credi' name='credi' placeholder='Total de creditos' value='"+creditos+"'>");
                }else{
                out.println("<input class='form-control' autocomplete='off' type='text'  id='credi' name='credi' placeholder='Total de creditos'>");    
                }
                %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="e1">Modalidad:</label> 
											<div class="col-md-10">
								<select id="e1" class="col-md-12" name="tipo">
                <%
                String tip[]={"ESCOLARIZADO","SEMIESCOLARIZADO","VIRTUAL"};
										out.println("<option id='0' selected value=''>Selecciona ..</option>");
                    for(int i=0;i<3;i++){
                        if(tipo.equals(tip[i])){
                            out.println("<option id='"+tip[i]+"'selected>"+tip[i]+"</option>");

                        }else{
                            out.println("<option id='"+tip[i]+"'>"+tip[i]+"</option>");
                        }
                    }
                %>         
							</select>	
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="e2">Status:</label> 
											<div class="col-md-10">
								<select id="e2" class="col-md-12" name="sta">
                <%
                String stat[]={"ACTIVO","INACTIVO"};
										out.println("<option id='0' selected value=''>Selecciona ..</option>");
                    for(int i=0;i<2;i++){
                        if(status.equals(stat[i])){
                            out.println("<option id='"+stat[i]+"'selected>"+stat[i]+"</option>");

                        }else{
                            out.println("<option id='"+stat[i]+"'>"+stat[i]+"</option>");
                        }
                    }
                %>
								</select>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="e3">Programa:</label> 
											<div class="col-md-10">
							<select id="e3" class="col-md-12" name="prog">
                <%
														out.println("<option id='0' selected value=''>Selecciona ..</option>");
                            ps = con.prepareStatement("SELECT name, prog_IdProgram"
                                     + " FROM Programs;");

                            rs = ps.executeQuery();
                            while(rs.next()){
                                if(rs.getInt("prog_IdProgram")== programa){
                                     out.println("<option id='"+rs.getInt("prog_IdProgram")+
                                             "' selected>"+rs.getString("name")+"</option>");
                                 }else{
                                     out.println("<option id='"+rs.getInt("prog_IdProgram")+"'>"+
                                             rs.getString("name")+"</option>");
                                 }
                             }
                %>
            </select>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="cmax">Carga M�xima:</label> 
											<div class="col-md-10">
                <% 
                if(progId>0){
                out.println("<input type='text' class='form-control' autocomplete='off' id='cmax' name='cmax' placeholder='Carga m�xima de cr�ditos' value='"+cargaMax+"'>");
                }else{
                out.println("<input type='text' class='form-control' autocomplete='off' id='cmax' name='cmax' placeholder='Carga m�xima de cr�ditos' >");    
                }
                %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="cmin">Carga M�nima:</label> 
											<div class="col-md-10">
                <% 
                if(progId>0){
                out.println("<input class='form-control' autocomplete='off' type='text'  id='cmin' name='cmin' placeholder='Carga m�nima de cr�ditos' value='"+cargaMin+"'>");
                }else{
                out.println("<input class='form-control' autocomplete='off' type='text'  id='cmin' name='cmin' placeholder='Carga m�nima de cr�ditos' >");    
                }
                %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="cmin">Carga con Especiales:</label> 
											<div class="col-md-10">
                <% 
                if(progId>0){
                out.println("<input class='form-control' autocomplete='off' type='text' placeholder='Carga m�xima de cr�ditos con especiales' id='cesp' name='cesp' value='"+cargaEsp+"'>");
                }else{
                out.println("<input class='form-control' autocomplete='off' type='text' placeholder='Carga m�xima de cr�ditos con especiales' id='cesp' name='cesp'>");    
                }
                %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="cruperm">Cruces Permitidos:</label> 
											<div class="col-md-10">
                <%
                if(progId>0){
                out.println("<input class='form-control' autocomplete='off' type='text'  id='cruperm' name='cruperm' placeholder='Cruces permitidos' value='"+crucePer+"'>");
                }else{
                out.println("<input class='form-control' autocomplete='off' type='text'  id='cruperm' name='cruperm' placeholder='Cruces permitidos' >");    
                }
                %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="c_nec_ser_soc">Cr�ditos para Servicio:</label> 
											<div class="col-md-10">
                <% 
                if(progId>0){
                out.println("<input class='form-control' autocomplete='off' type='text'  id='c_nec_ser_soc' name='c_nec_ser_soc' placeholder='Cr�ditos requeridos para el servicio social' value='"+credNecServi+"'>");
                }else{
                out.println("<input class='form-control' autocomplete='off' type='text'  id='c_nec_ser_soc' name='c_nec_ser_soc' placeholder='Cr�ditos requeridos para el servicio social' >");    
                }
                %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="c_ced_max_c_serv_so">Carga Max Servicio:</label> 
											<div class="col-md-10">
                <% 
                if(progId>0){
                out.println("<input class='form-control' autocomplete='off' type='text'  id='c_ced_max_c_serv_so' name='c_ced_max_c_serv_so' placeholder='Carga m�xima de cr�ditos con servicio social' value='"+cargCredMaxServi+"'>");
                }else{
                out.println("<input class='form-control' autocomplete='off' type='text'  id='c_ced_max_c_serv_so' name='c_ced_max_c_serv_so' placeholder='Carga m�xima de cr�ditos con servicio social'>");    
                }
                %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="c_nec_p_res">Creditos Residencia:</label> 
											<div class="col-md-10">
                <% 
                if(progId>0){
                out.println("<input class='form-control' autocomplete='off' type='text'  id='c_nec_p_res' name='c_nec_p_res' placeholder='Cr�ditos requeridos para el servicio social' value='"+credNeceResid+"'>");
                }else{
                out.println("<input class='form-control' autocomplete='off' type='text'  id='c_nec_p_res' name='c_nec_p_res' placeholder='Cr�ditos requeridos para el servicio social'>");    
                }
                %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="c_cre_max_c_res">Carga M�x Residencia:</label> 
											<div class="col-md-10">
                <% 
                if(progId>0){
									out.println("<input class='form-control' autocomplete='off' type='text' id='c_cre_max_c_res' placeholder='Carga m�xima de cr�ditos con residencias' name='c_cre_max_c_res' value='"+cargCredMaxResie+"'>");
                }else{
									out.println("<input class='form-control' autocomplete='off' type='text' id='c_cre_max_c_res' placeholder='Carga m�xima de cr�ditos con residencias' name='c_cre_max_c_res'>");    
                }
                %>
											</div>
											</div>
											<input type="hidden" id="hide" value="<%out.println(progId);%>">
									   </form>
									</div>
								</div>
								<!-- /BOX -->
							</div>
						</div>
						<!-- /ADVANCED -->
						<div class="footer-tools">
							<span class="go-top">
								<i class="fa fa-chevron-up"></i> Top
							</span>
						</div>
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	<script src="../js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="../bootstrap-dist/js/bootstrap.min.js"></script>
	<!-- DATE RANGE PICKER -->
	<script src="../js/bootstrap-daterangepicker/moment.min.js"></script>
	<script src="../js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
	<!-- SLIMSCROLL -->
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
	<!-- BLOCK UI -->
	<script type="text/javascript" src="../js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
	<!-- TYPEHEAD -->
	<script type="text/javascript" src="../js/typeahead/typeahead.min.js"></script>
	<!-- AUTOSIZE -->
	<script type="text/javascript" src="../js/autosize/jquery.autosize.min.js"></script>
	<!-- COUNTABLE -->
	<script type="text/javascript" src="../js/countable/jquery.simplyCountable.min.js"></script>
	<!-- INPUT MASK -->
	<script type="text/javascript" src="../js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
	<!-- FILE UPLOAD -->
	<script type="text/javascript" src="../js/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
	<!-- SELECT2 -->
	<script type="text/javascript" src="../js/select2/select2.min.js"></script>
	<!-- UNIFORM -->
	<script type="text/javascript" src="../js/uniform/jquery.uniform.min.js"></script>
	<!-- JQUERY UPLOAD -->
	<!-- The Templates plugin is included to render the upload/download listings -->
	<script src="../js/blueimp/javascript-template/tmpl.min.js"></script>
	<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="../js/blueimp/javascript-loadimg/load-image.min.js"></script>
	<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script src="../js/blueimp/javascript-canvas-to-blob/canvas-to-blob.min.js"></script>
	<!-- blueimp Gallery script -->
	<script src="../js/blueimp/gallery/jquery.blueimp-gallery.min.js"></script>
	<!-- The basic File Upload plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload.min.js"></script>
	<!-- The File Upload processing plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-process.min.js"></script>
	<!-- The File Upload image preview & resize plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-image.min.js"></script>
	<!-- The File Upload audio preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-audio.min.js"></script>
	<!-- The File Upload video preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-video.min.js"></script>
	<!-- The File Upload validation plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-validate.min.js"></script>
	<!-- The File Upload user interface plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-ui.min.js"></script>
	<!-- The main application script -->
	<script src="../js/jquery-upload/js/main.js"></script>
	<!-- bootbox script -->
	<script src="../js/bootbox/bootbox.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="../js/script.js"></script>
	<script src="../js/jquery-validate/jquery.validate.js"></script>
	<script src="../js/jquery-validate/additional-methods.js"></script>
	<script src="../js/error_message.js"></script>
	<script>
		jQuery(document).ready(function() {		
			App.setPage("forms");  //Set current page
			App.init(); //Initialise plugins and elements
			$("#photo").attr("src","getPhoto?safe_IdUser="+<% if(user != null){out.print(user.getSafe_IdUser());} %>);
			var addOrEdit=<%out.println(request.getParameter("addOrEdit")+";"); %>
			
			$("#frm_plan").validate({
				rules:{
					clave:{
						required: true, minlength: 3, maxlength: 15
					},
					cvoficial:{
						required: true, minlength: 3, maxlength: 20
					},
					credi:{
						required: true, digits: true
					},
					tipo:{
						required: true
					},
					sta:{
						required: true
					},
					prog:{
						required: true
					},
					cmax:{
						required: true, digits: true
					},
					cmin:{
						required: true, digits: true
					},
					cesp:{
						required: true, digits: true
					},
					cruperm:{
						required: true, digits: true
					},
					c_nec_ser_soc:{
						required: true, digits: true
					},
					c_ced_max_c_serv_so:{
						required: true, digits: true
					},
					c_nec_p_res:{
						required: true, digits: true
					},
					c_cre_max_c_res:{
						required: true, digits: true
					}
				},
				highlight: function(element){
					$(element).parent().parent().addClass("has-error");
				},
				unhighlight: function(element){
				$(element).parent().parent().removeClass("has-error");
			}});

                    var hide=$("#hide");
                    var clave=$("#clave");
                    var cvoficial=$("#cvoficial");
                    var credi=$("#credi");
                    var sta=$("#e2");
                    var prog=$("#e3");
                    var cmax=$("#cmax");
                    var cmin=$("#cmin");
                    var cesp=$("#cesp");
                    var cruperm=$("#cruperm");
                    var c_nec_ser_soc=$("#c_nec_ser_soc");
                    var c_ced_max_c_serv_so=$("#c_ced_max_c_serv_so");
                    var c_nec_p_res=$("#c_nec_p_res");
                    var c_cre_max_c_res=$("#c_cre_max_c_res");
                    var tipo=$("#e1");

			$("#nuevo").click(function () {
				window.location = "ProgPlan_catalog.jsp?progIdPlan=0&addOrEdit=0";
			});
			$("#editar").click(function () {
                           cvoficial.removeAttr("disabled");
                           clave.removeAttr("disabled");
                           credi.removeAttr("disabled");
                           sta.removeAttr("disabled");
                           prog.removeAttr("disabled");
                           cmax.removeAttr("disabled");
                           tipo.removeAttr("disabled");
                           cmin.removeAttr("disabled");
                           cesp.removeAttr("disabled");
                           cruperm.removeAttr("disabled");
                           c_nec_ser_soc.removeAttr("disabled");
                           c_ced_max_c_serv_so.removeAttr("disabled");
                           c_nec_p_res.removeAttr("disabled");
                           c_cre_max_c_res.removeAttr("disabled");
				addOrEdit=1;
				$("#editar").attr("disabled","disabled");
			});
			$("#guardar").click(function () {
				$("#frm_plan").valid();
				
                           var cvoficialVal =cvoficial.val().trim();
                           var claveVal =clave.val().trim();
                           var crediVal =credi.val().trim();
                           var staVal =sta.children(":selected").val().trim();
                           var progVal =prog.children(":selected").attr("id");
                           var cmaxVal =cmax.val().trim();
                           var tipoVal =tipo.children(":selected").attr("id");
                           var cminVal =cmin.val().trim();
                           var cespVal =cesp.val().trim();
                           var crupermVal =cruperm.val().trim();
                           var c_nec_ser_socVal =c_nec_ser_soc.val().trim();
                           var c_ced_max_c_serv_soVal =c_ced_max_c_serv_so.val().trim();
                           var c_nec_p_resVal =c_nec_p_res.val().trim();
                           var c_cre_max_c_resVal =c_cre_max_c_res.val().trim();

                           cvoficial.attr("disabled","disabled");
                           clave.attr("disabled","disabled");
                           credi.attr("disabled","disabled");
                           sta.attr("disabled","disabled");
                           prog.attr("disabled","disabled");
                           cmax.attr("disabled","disabled");
                           tipo.attr("disabled","disabled");
                           cmin.attr("disabled","disabled");
                           cesp.attr("disabled","disabled");
                           cruperm.attr("disabled","disabled");
                           c_nec_ser_soc.attr("disabled","disabled");
                           c_ced_max_c_serv_so.attr("disabled","disabled");
                           c_nec_p_res.attr("disabled","disabled");
                           c_cre_max_c_res.attr("disabled","disabled");
													 
                           var values="values="+parseInt(hide.val())+"#"+progVal+"#"+claveVal+"#"+cvoficialVal+"#"+crediVal+"#"+tipoVal+"#"+"no name"+
                                   "#"+staVal+"#"+cmaxVal+"#"+cminVal+"#"+cespVal+"#"+c_nec_ser_socVal+
                                   "#"+c_ced_max_c_serv_soVal+"#"+c_nec_p_resVal+"#"+c_cre_max_c_resVal+"#"+crupermVal+"&addOrEdit="+addOrEdit;

					dhtmlxAjax.post("ProgPlan_save",values,function(loader){
						var resp = loader.xmlDoc.responseText;
						respo = resp.split("#");
						document.getElementById("hide").value=respo[0];
						var resp = parseInt(respo[1]);
						if(resp == 1){
							bootbox.alert("Se guardo con exito!", function() {});
						} else {
							bootbox.alert("Fallo al guardar!", function() {});
						}
					});

					$("#editar").removeAttr("disabled");
			});
		});
	</script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
			}else{
				response.sendRedirect("error600.jsp");
			}
		}else{
			response.sendRedirect("error600.jsp");
		}
%>