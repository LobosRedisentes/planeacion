//Funciones del administrador de Kardex

function kardex_admin(){
        grid = new dhtmlXGridObject('grid_container');
        var response = "../getAdminView?table=UserStudents_view&id=user_IdStudent&parameters=matricula,nombre,paterno,materno,status";
        var nombreCompleto;
        grid.setImagePath("../dhtmlx/dhtmlxGrid/codebase/imgs/");
        grid.setHeader("Matricula,Nombre,Apellido Paterno,Apellido Materno,Status");
        grid.attachHeader("#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter");
        grid.setInitWidths("80,*,*,*,100");
        grid.setColAlign("left,left,left,left,left");
        grid.setColSorting("str,str,str,str,str");
        grid.setEditable(false);
        var IDRow;
				grid.objBox.style.overflowX = "hidden";
        grid.attachEvent("onRowSelect",function(id){
             $("#editar").removeAttr("disabled","disabled");
             IDRow = id;
             nombreCompleto = grid.cells(id,1).getValue().toUpperCase() +" "+grid.cells(id,2).getValue().toUpperCase()+" "+grid.cells(id,3).getValue().toUpperCase();
        });

        grid.init();
        grid.setSkin("clear");
        grid.enableSmartRendering(true,50);
        spInit();
        grid.load(response,function(){
            spStop();
        });
        
        $("#editar").click(function(){
            window.location = "EvalKardex_catalog.jsp?IdStudent="+IDRow+"&nombreCompleto="+nombreCompleto; 
        });
}


//******************************************************************************


//Funciones del catalogo de Kardex

var idEval1, idEval2,idEval3;
function kardex_catalog(){   
    desbloquear();
    $("#editar").click(function(){
        editKardex(); 
    });
    
    $("#guardar").click(function(){
        saveKardex();
    });
}

function editKardex(){
    $("#e1").removeAttr("disabled");                     
    $("#calendario1").removeAttr("disabled");
    $("#calificacion1").removeAttr("disabled");
    $("#opcion1").removeAttr("disabled");
    $("#calendario2").removeAttr("disabled");
    $("#calificacion2").removeAttr("disabled");
    $("#opcion2").removeAttr("disabled");   
    $("#calendario3").removeAttr("disabled");
    $("#calificacion3").removeAttr("disabled");
    $("#opcion3").removeAttr("disabled");            
}

function saveKardex(){ 
    var  idStudent = parseInt($("#hide").val());
    var  semestre = parseInt($("#hide2").val());
    var materia = $("#e1").children(":selected").attr("id");  
    var calendario1 = $("#calendario1").children(":selected").attr("id");
    var calificacion1 = $("#calificacion1").val().trim();
    var opcion1 = $("#opcion1").children(":selected").val();
    var calendario2 = $("#calendario2").children(":selected").attr("id");
    var calificacion2 = $("#calificacion2").val().trim();
    var opcion2 = $("#opcion2").children(":selected").val();   
    var calendario3 = $("#calendario3").children(":selected").attr("id");
    var calificacion3 = $("#calificacion3").val().trim();
    var opcion3 = $("#opcion3").children(":selected").val();    
    var values1,values2,values3;

    if(materia > 0){               
        if(calendario2 > 0 || calificacion2 !== "" || opcion2 !== "" || calendario3 > 0 || calificacion3 !== "" || opcion3 !== ""){
            if(calendario3 > 0 || calificacion3 !== "" || opcion3 !== ""){
                if(calendario1 > 0 && calificacion1 !== "" && opcion1 !== "" && calendario2 > 0 && calificacion2 !== "" && opcion2 !== "" && calendario3 > 0 && calificacion3 !== "" && opcion3 !== ""){
                    if(idEval1 > 0 && idEval2 > 0 && idEval3 > 0){
                        if(calificacion1 > -1 && calificacion1 <= 100 && calificacion2 > -1 && calificacion2 <= 100 && calificacion3 > -1 && calificacion3 <= 100){
                            values3 = idEval1 +"#"+ idEval2 +"#"+ idEval3 +"#"+ idStudent +"#"+ materia +"#"+ calendario1 +"#"+ calendario2 +"#"+ calendario3
                                    +"#"+ calificacion1 +"#"+ calificacion2 +"#"+ calificacion3 +"#"+ opcion1 +"#"+ opcion2 +"#"+ opcion3 +"#"+6+"#"+semestre;
                            dhtmlxAjax.post("Kardex_catalog_addOrEdit","values3="+values3+"&values2=0#0#0#0#0#0#0#0#0#0#0#0&values1=0#0#0#0#0#0#0#0",function(loader){
                               var resp = loader.xmlDoc.responseText;
                               if(parseInt(resp) === 1){
                                   alert("guardo");
                                   tool.enableItem("_edit");
                                   tool.disableItem("_save");
                                   $("#e1").attr("disabled","disabled");  
                                   $("#calendario1").attr("disabled","disabled");
                                   $("#calificacion1").attr("disabled","disabled");
                                   $("#opcion1").attr("disabled","disabled");
                                   $("#calendario2").attr("disabled","disabled");
                                   $("#calificacion2").attr("disabled","disabled");
                                   $("#opcion2").attr("disabled","disabled");   
                                   $("#calendario3").attr("disabled","disabled");
                                   $("#calificacion3").attr("disabled","disabled");
                                   $("#opcion3").attr("disabled","disabled");
                              } else {
                                   alert("Guardo");                            
                              }       

                           });
                        }else {
                            alert("La calificacion debe ser entre 0 y 100");                    
                        } 
                    }else if(idEval1 > 0 && idEval2 > 0 && idEval3 === 0){
                        if(calificacion1 > -1 && calificacion1 <= 100 && calificacion2 > -1 && calificacion2 <= 100 && calificacion3 > -1 && calificacion3 <= 100){
                            values3 = idEval1 +"#"+ idEval2 +"#"+ idEval3 +"#"+ idStudent +"#"+ materia +"#"+ calendario1 +"#"+ calendario2 +"#"+ calendario3
                                    +"#"+ calificacion1 +"#"+ calificacion2 +"#"+ calificacion3 +"#"+ opcion1 +"#"+ opcion2 +"#"+ opcion3 +"#"+7+"#"+semestre;
                            dhtmlxAjax.post("Kardex_catalog_addOrEdit","values3="+values3+"&values2=0#0#0#0#0#0#0#0#0#0#0#0&values1=0#0#0#0#0#0#0#0",function(loader){
                               var resp = loader.xmlDoc.responseText;
                               if(parseInt(resp) === 1){
                                   alert("Guardo");
                                   $("#e1").attr("disabled","disabled");  
                                   $("#calendario1").attr("disabled","disabled");
                                   $("#calificacion1").attr("disabled","disabled");
                                   $("#opcion1").attr("disabled","disabled");
                                   $("#calendario2").attr("disabled","disabled");
                                   $("#calificacion2").attr("disabled","disabled");
                                   $("#opcion2").attr("disabled","disabled");   
                                   $("#calendario3").attr("disabled","disabled");
                                   $("#calificacion3").attr("disabled","disabled");
                                   $("#opcion3").attr("disabled","disabled");
                              } else {
                                   alert("Guardo");                            
                              }       

                           });
                        }else {
                            alert("La calificacion debe ser entre 0 y 100");                    
                        }
                    }else if(idEval1 > 0 && idEval2 === 0 && idEval3 === 0){
                        if(calificacion1 > -1 && calificacion1 <= 100 && calificacion2 > -1 && calificacion2 <= 100 && calificacion3 > -1 && calificacion3 <= 100){
                            values3 = idEval1 +"#"+ idEval2 +"#"+ idEval3 +"#"+ idStudent +"#"+ materia +"#"+ calendario1 +"#"+ calendario2 +"#"+ calendario3
                                    +"#"+ calificacion1 +"#"+ calificacion2 +"#"+ calificacion3 +"#"+ opcion1 +"#"+ opcion2 +"#"+ opcion3 +"#"+8+"#"+semestre;
                            dhtmlxAjax.post("Kardex_catalog_addOrEdit","values3="+values3+"&values2=0#0#0#0#0#0#0#0#0#0#0&values1=0#0#0#0#0#0#0",function(loader){
                               var resp = loader.xmlDoc.responseText;
                               if(parseInt(resp) === 1){
                                   alert("Guardo");
                                   $("#e1").attr("disabled","disabled");  
                                   $("#calendario1").attr("disabled","disabled");
                                   $("#calificacion1").attr("disabled","disabled");
                                   $("#opcion1").attr("disabled","disabled");
                                   $("#calendario2").attr("disabled","disabled");
                                   $("#calificacion2").attr("disabled","disabled");
                                   $("#opcion2").attr("disabled","disabled");   
                                   $("#calendario3").attr("disabled","disabled");
                                   $("#calificacion3").attr("disabled","disabled");
                                   $("#opcion3").attr("disabled","disabled");
                              } else {
                                   alert("Guardo");                            
                              }       

                           });
                        }else {
                            alert("La calificacion debe ser entre 0 y 100");                    
                        }
                    }else{
                        if(calificacion1 > -1 && calificacion1 <= 100 && calificacion2 > -1 && calificacion2 <= 100 && calificacion3 > -1 && calificacion3 <= 100){
                            values3 = idEval1 +"#"+ idEval2 +"#"+ idEval3 +"#"+ idStudent +"#"+ materia +"#"+ calendario1 +"#"+ calendario2 +"#"+ calendario3
                                    +"#"+ calificacion1 +"#"+ calificacion2 +"#"+ calificacion3 +"#"+ opcion1 +"#"+ opcion2 +"#"+ opcion3 +"#"+9+"#"+semestre;
                            dhtmlxAjax.post("Kardex_catalog_addOrEdit","values3="+values3+"&values2=0#0#0#0#0#0#0#0#0#0#0#0&values1=0#0#0#0#0#0#0#0",function(loader){
                               var resp = loader.xmlDoc.responseText;
                               if(parseInt(resp) === 1){
                                   alert("Guardo");
                                   tool.enableItem("_edit");
                                   tool.disableItem("_save");
                                   $("#e1").attr("disabled","disabled");  
                                   $("#calendario1").attr("disabled","disabled");
                                   $("#calificacion1").attr("disabled","disabled");
                                   $("#opcion1").attr("disabled","disabled");
                                   $("#calendario2").attr("disabled","disabled");
                                   $("#calificacion2").attr("disabled","disabled");
                                   $("#opcion2").attr("disabled","disabled");   
                                   $("#calendario3").attr("disabled","disabled");
                                   $("#calificacion3").attr("disabled","disabled");
                                   $("#opcion3").attr("disabled","disabled");
                              } else {
                                   alert("Guardo");                            
                              }       

                           });
                        }else {
                            alert("La calificacion debe ser entre 0 y 100");                    
                        }
                    }
                }else{
                    
                    alert("Los campos deben ser llenados en su totalidad");
                     
                }
            }else{
                if(calendario1 > 0 && calificacion1 !== "" && opcion1 !== "" && calendario2 > 0 && calificacion2 !== "" && opcion2 !== ""){
                    if(idEval1 > 0 && idEval2 > 0){
                        if(calificacion1 > -1 && calificacion1 <= 100 && calificacion2 > -1 && calificacion2 <= 100){
                            values2 = idEval1 +"#"+ idEval2 +"#"+ idStudent +"#"+ materia +"#"+ calendario1 +"#"+ calendario2 
                                    +"#"+ calificacion1 +"#"+ calificacion2 +"#"+ opcion1 +"#"+ opcion2 +"#"+3+"#"+semestre;
                            dhtmlxAjax.post("Kardex_catalog_addOrEdit","values2="+values2+"&values1=0#0#0#0#0#0#0#0&values3=0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0",function(loader){
                               var resp = loader.xmlDoc.responseText;
                               if(parseInt(resp) === 1){
                                   alert("Guardo");
                                   tool.enableItem("_edit");
                                   tool.disableItem("_save");
                                   $("#e1").attr("disabled","disabled");  
                                   $("#calendario1").attr("disabled","disabled");
                                   $("#calificacion1").attr("disabled","disabled");
                                   $("#opcion1").attr("disabled","disabled");
                                   $("#calendario2").attr("disabled","disabled");
                                   $("#calificacion2").attr("disabled","disabled");
                                   $("#opcion2").attr("disabled","disabled");   
                                   $("#calendario3").attr("disabled","disabled");
                                   $("#calificacion3").attr("disabled","disabled");
                                   $("#opcion3").attr("disabled","disabled");
                              } else {
                                   alert("Guardo");                           
                              }       

                           });
                        }else {
                            alert("La calificacion debe ser entre 0 y 100");                    
                        } 
                    }else if(idEval1 > 0 && idEval2 === 0){
                        if(calificacion1 > -1 && calificacion1 <= 100 && calificacion2 > -1 && calificacion2 <= 100){
                            values2 = idEval1 +"#"+ idEval2 +"#"+ idStudent +"#"+ materia +"#"+ calendario1 +"#"+ calendario2 
                                    +"#"+ calificacion1 +"#"+ calificacion2 +"#"+ opcion1 +"#"+ opcion2 +"#"+5+"#"+semestre;
                            dhtmlxAjax.post("Kardex_catalog_addOrEdit","values2="+values2+"&values1=0#0#0#0#0#0#0#0&values3=0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0",function(loader){
                               var resp = loader.xmlDoc.responseText;
                               if(parseInt(resp) === 1){
                                   alert("Guardo");
                                   tool.enableItem("_edit");
                                   tool.disableItem("_save");
                                   $("#e1").attr("disabled","disabled");  
                                   $("#calendario1").attr("disabled","disabled");
                                   $("#calificacion1").attr("disabled","disabled");
                                   $("#opcion1").attr("disabled","disabled");
                                   $("#calendario2").attr("disabled","disabled");
                                   $("#calificacion2").attr("disabled","disabled");
                                   $("#opcion2").attr("disabled","disabled");   
                                   $("#calendario3").attr("disabled","disabled");
                                   $("#calificacion3").attr("disabled","disabled");
                                   $("#opcion3").attr("disabled","disabled");
                              } else {
                                   alert("Guardo");                           
                              }       

                           });
                        }else {
                            alert("La calificacion debe ser entre 0 y 100");                    
                        }
                    }else{
                        if(calificacion1 > -1 && calificacion1 <= 100 && calificacion2 > -1 && calificacion2 <= 100){
                            if(calificacion1 > -1 && calificacion1 <= 100 && calificacion2 > -1 && calificacion2 <= 100){
                                values2 = idEval1 +"#"+ idEval2 +"#"+ idStudent +"#"+ materia +"#"+ calendario1 +"#"+ calendario2 
                                        +"#"+ calificacion1 +"#"+ calificacion2 +"#"+ opcion1 +"#"+ opcion2 +"#"+4+"#"+semestre;                    
                                dhtmlxAjax.post("Kardex_catalog_addOrEdit","values2="+values2+"&values1=0#0#0#0#0#0#0#0&values3=0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0",function(loader){
                                   var resp = loader.xmlDoc.responseText;
                                   if(parseInt(resp) === 1){
                                       alert("Guardo");
                                       tool.enableItem("_edit");
                                       tool.disableItem("_save");
                                       $("#e1").attr("disabled","disabled");  
                                       $("#calendario1").attr("disabled","disabled");
                                       $("#calificacion1").attr("disabled","disabled");
                                       $("#opcion1").attr("disabled","disabled");
                                       $("#calendario2").attr("disabled","disabled");
                                       $("#calificacion2").attr("disabled","disabled");
                                       $("#opcion2").attr("disabled","disabled");   
                                       $("#calendario3").attr("disabled","disabled");
                                       $("#calificacion3").attr("disabled","disabled");
                                       $("#opcion3").attr("disabled","disabled");
                                  } else {
                                       alert("Guardo");                            
                                  }       

                               });
                            }else {
                                alert("La calificacion debe ser entre 0 y 100");                    
                            }
                        }else {
                            alert("La calificacion debe ser entre 0 y 100");                    
                        } 
                    }
                }else{
                    alert("Los campos deben ser llenados en su totalidad");
                }
            }
        }else{
            if(calendario1 > 0 && calificacion1 !== "" && opcion1 !== ""){
                if(idEval1 > 0){
                    if(calificacion1 > -1 && calificacion1 <= 100){
                        values1 = idEval1 +"#"+ idStudent +"#"+ materia +"#"+ calendario1 +"#"+ calificacion1 +"#"+ opcion1+"#"+1+"#"+semestre;
                        dhtmlxAjax.post("Kardex_catalog_addOrEdit","values1="+values1+"&values2=0#0#0#0#0#0#0#0#0#0#0#0&values3=0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0",function(loader){
                           var resp = loader.xmlDoc.responseText;
                           if(parseInt(resp) === 1){
                               alert("Guardo");
                               $("#e1").attr("disabled","disabled");  
                               $("#calendario1").attr("disabled","disabled");
                               $("#calificacion1").attr("disabled","disabled");
                               $("#opcion1").attr("disabled","disabled");
                               $("#calendario2").attr("disabled","disabled");
                               $("#calificacion2").attr("disabled","disabled");
                               $("#opcion2").attr("disabled","disabled");   
                               $("#calendario3").attr("disabled","disabled");
                               $("#calificacion3").attr("disabled","disabled");
                               $("#opcion3").attr("disabled","disabled");
                          } else {
                               alert("Guardo");                         
                          }       

                       });                    
                    } else {
                        alert("La calificacion debe ser entre 0 y 100");                    
                    }                
                } else {
                    if(calificacion1 > -1 && calificacion1 <= 100){
                        values1 = idEval1 +"#"+ idStudent +"#"+ materia +"#"+ calendario1 +"#"+ calificacion1 +"#"+ opcion1+"#"+2+"#"+semestre;
                        dhtmlxAjax.post("Kardex_catalog_addOrEdit","values1="+values1+"&&values2=0#0#0#0#0#0#0#0#0#0#0#0&values3=0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0",function(loader){
                           var resp = loader.xmlDoc.responseText;
                           if(parseInt(resp) === 1){
                               alert("Guardo");
                               $("#e1").attr("disabled","disabled");  
                               $("#calendario1").attr("disabled","disabled");
                               $("#calificacion1").attr("disabled","disabled");
                               $("#opcion1").attr("disabled","disabled");
                               $("#calendario2").attr("disabled","disabled");
                               $("#calificacion2").attr("disabled","disabled");
                               $("#opcion2").attr("disabled","disabled");   
                               $("#calendario3").attr("disabled","disabled");
                               $("#calificacion3").attr("disabled","disabled");
                               $("#opcion3").attr("disabled","disabled");
                          } else {
                               alert("Guardo");                          
                          }       

                       });              
                    } else {
                        alert("La calificacion debe ser entre 0 y 100");                    
                    }
                }


            }else{
                alert("Los campos deben ser llenados en su totalidad");
            }
        }                                                                
    }else{
        alert("Los campos deben ser llenados en su totalidad");
    }
    
        
}
function desbloquear(){
    var id1,id2;
    
    $("#e1").change(function(){                  
        id1 = $(this).children(":selected").attr("id");
        id2 = parseInt($("#hide").val());

        if(id1 > 0){         
            $('#calendario1 option:gt(0)').remove();
            $('#opcion1').children().remove(); 
            $('#calendario2 option:gt(0)').remove();
            $('#opcion2').children().remove(); 
            $('#calendario3 option:gt(0)').remove();
            $('#opcion3').children().remove();

            dhtmlxAjax.post("Kardex_catalog_aux","idSubject="+id1+"&option=1&idStudent="+id2,function (loader){
                idEval1 = parseInt(loader.xmlDoc.responseText);                                                                                                
            });
            dhtmlxAjax.post("Kardex_catalog_aux","idSubject="+id1+"&option=2&idStudent="+id2,function (loader){
                $("#calendario1").append(loader.xmlDoc.responseText);                                                                                                
            });        
            dhtmlxAjax.post("Kardex_catalog_aux","idSubject="+id1+"&option=3&idStudent="+id2,function (loader){
                $('#calificacion1').val(loader.xmlDoc.responseText);
            });        
            dhtmlxAjax.post("Kardex_catalog_aux","idSubject="+id1+"&option=4&idStudent="+id2,function (loader){
                $("#opcion1").append(loader.xmlDoc.responseText);                                                                
            });
            dhtmlxAjax.post("Kardex_catalog_aux","idSubject="+id1+"&option=5&idStudent="+id2,function (loader){
                idEval2 = parseInt(loader.xmlDoc.responseText);                                                                                                
            });
            dhtmlxAjax.post("Kardex_catalog_aux","idSubject="+id1+"&option=6&idStudent="+id2,function (loader){
                $("#calendario2").append(loader.xmlDoc.responseText);                                                                                                
            });        
            dhtmlxAjax.post("Kardex_catalog_aux","idSubject="+id1+"&option=7&idStudent="+id2,function (loader){
                $('#calificacion2').val(loader.xmlDoc.responseText);
            });        
            dhtmlxAjax.post("Kardex_catalog_aux","idSubject="+id1+"&option=8&idStudent="+id2,function (loader){
                $("#opcion2").append(loader.xmlDoc.responseText);                                                                
            });
            dhtmlxAjax.post("Kardex_catalog_aux","idSubject="+id1+"&option=9&idStudent="+id2,function (loader){
                idEval3 = parseInt(loader.xmlDoc.responseText);                                                                                            
            });
            dhtmlxAjax.post("Kardex_catalog_aux","idSubject="+id1+"&option=10&idStudent="+id2,function (loader){
                $("#calendario3").append(loader.xmlDoc.responseText);                                                                                                
            });        
            dhtmlxAjax.post("Kardex_catalog_aux","idSubject="+id1+"&option=11&idStudent="+id2,function (loader){
                $('#calificacion3').val(loader.xmlDoc.responseText);
            });        
            dhtmlxAjax.post("Kardex_catalog_aux","idSubject="+id1+"&option=12&idStudent="+id2,function (loader){
                $("#opcion3").append(loader.xmlDoc.responseText);                                                                
            });

            $("#calendario1").removeAttr("disabled");
            $("#calificacion1").removeAttr("disabled");
            $("#opcion1").removeAttr("disabled");
            $("#calendario2").removeAttr("disabled");
            $("#calificacion2").removeAttr("disabled");
            $("#opcion2").removeAttr("disabled");
            $("#calendario3").removeAttr("disabled");
            $("#calificacion3").removeAttr("disabled");
            $("#opcion3").removeAttr("disabled");                  
        }else{
            $("#calendario1").attr("disabled","disabled");
            $("#calificacion1").attr("disabled","disabled");
            $("#opcion1").attr("disabled","disabled");
            $("#calendario2").attr("disabled","disabled");
            $("#calificacion2").attr("disabled","disabled");
            $("#opcion2").attr("disabled","disabled");   
            $("#calendario3").attr("disabled","disabled");
            $("#calificacion3").attr("disabled","disabled");
            $("#opcion3").attr("disabled","disabled");
            
            $('#calendario1 option:gt(0)').remove();
            $('#calificacion1').val("");
            $('#opcion1 option:gt(0)').remove(); 
            $('#calendario2 option:gt(0)').remove();
            $('#opcion2 option:gt(0)').remove(); 
            $('#calificacion2').val("");
            $('#calendario3 option:gt(0)').remove();
            $('#opcion3 option:gt(0)').remove();
            $('#calificacion3').val("");
         
        }        
    });
}

function alertError(text){
    dhtmlx.message({
           type : "alert-error",
           title: "Error",
           text : text
    });
}
 