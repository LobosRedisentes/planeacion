<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.edcore.model.db.DBConnector"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.edcore.model.safe.User"%>
	<%
		HttpSession sesion;
    sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if(user != null){
			ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
			if(privileges.contains("CAT26")){
	%>
<%
	ResultSet rs;
	DBConnector db = new DBConnector();
	PreparedStatement ps;
	Connection con;
	int progIdSub = Integer.parseInt(request.getParameter("progIdSubject"));
	int addOrEdit=Integer.parseInt(request.getParameter("addOrEdit"));
	con=db.open();
    
    int progIdPlan=0;
    int orgIdDep=0;
    String clave = "";
    String clave_oficial = "";
    String abreviatura = "";
    String nombre = "";
    String creditos = "";
    String c_requeridos = "";
    String hrs_teoricas = "";
    String hrs_practicas = "";
    String semestre = "";
    String s_requeridos = "";
    String fila = "";
    String columna = "";
    String sylabus = "";
    String es_requerido = "";
    String se_evalua = "";
    String se_promedia = "";
    String tipo = "";
    if(progIdSub > 0){
        ps = con.prepareStatement("SELECT * FROM ProgSubjects WHERE prog_IdSubject = ?");
        ps.setInt(1, progIdSub);
        rs = ps.executeQuery();
            
        while(rs != null && rs.next()){
            progIdSub=Integer.parseInt(rs.getString("prog_IdSubject"));
            progIdPlan = Integer.parseInt(rs.getString("prog_IdPlan"));
            orgIdDep = Integer.parseInt(rs.getString("org_IdDepartment"));
            clave = rs.getString("code");
            clave_oficial = rs.getString("officialCode");
            abreviatura = rs.getString("abreviation");
            nombre = rs.getString("name");
            creditos = rs.getString("credits");
            c_requeridos = rs.getString("reqCredits");
            s_requeridos = rs.getString("reqSemester");
            hrs_practicas = rs.getString("hrsPractices");
            hrs_teoricas = rs.getString("hrsTheorist");
            semestre = rs.getString("semester");
            fila = rs.getString("row");
            columna = rs.getString("column");
            sylabus = rs.getString("sylabus");
            es_requerido = rs.getString("requiered");
            se_evalua = rs.getString("evaluates");
            se_promedia = rs.getString("averaged");
            tipo =  rs.getString("type");
        }
    }
%>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Asignaturas</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
	<link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
	
	<link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- DATE RANGE PICKER -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
	<!-- TYPEAHEAD -->
	<link rel="stylesheet" type="text/css" href="../js/typeahead/typeahead.css" />
	<!-- FILE UPLOAD -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-fileupload/bootstrap-fileupload.min.css" />
	<!-- SELECT2 -->
	<link rel="stylesheet" type="text/css" href="../js/select2/select2.min.css" />
	<!-- UNIFORM -->
	<link rel="stylesheet" type="text/css" href="../js/uniform/css/uniform.default.min.css" />
	<!-- JQUERY UPLOAD -->
	<!-- blueimp Gallery styles -->
	<link rel="stylesheet" href="../js/blueimp/gallery/blueimp-gallery.min.css">
	<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload.css">
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload-ui.css">
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	<style>
	 .error-message, label.error {
	    color: #ff0000;
	    margin:0;
	    display: inline;
	    font-size: 1em !important;
	    font-weight:bold;
	 }
	 </style>
	<!-- LIBRERIA EDCORE -->
  <script src="../dhtmlx/libCompiler/dhtmlxcommon.js"></script>
	<script src="../js/action_dhx.js"></script>
	<script src="../js/spin.js"></script>
</head>
<body>
	<!-- HEADER -->
	<%@include file="header.jsp" %>
	<!--/HEADER -->
	<!-- PAGE -->
	<section id="page">
				<!-- SIDEBAR -->
				<%@include file="sidebar.jsp" %>
				<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<!-- STYLER -->
									
									<!-- /STYLER -->
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="index.jsp">Inicio</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Catalogos</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Reticula Escolar</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="ProgSubjects_admin.jsp">Asignaturas</a>
										</li>
										<li>Nuevo/Editar</li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										<h3 class="content-title pull-left">Cat�logo de Asignaturas</h3>
										<span class="date-range pull-right">
										<p class="btn-toolbar">
											<%if(privileges.contains("CAT51")){%>
											<button id="nuevo" class="btn btn-success" ><i class="fa fa-file-text-o"></i> Nuevo</button>
											<%}%>
											<%if(privileges.contains("CAT52")){%>
											<button id="editar" class="btn btn-success" disabled=""><i class="fa fa-pencil-square-o"></i> Editar</button>
											<%}%>
											<%if(privileges.contains("CAT53")){%>
											<button id="guardar" class="btn btn-success" ><i class="fa fa-save"></i> Guardar</button>
											<%}%>
										</p>
										</span>
									</div>
									<div class="description"></div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						<!-- ADVANCED -->
						<div class="row">
							<div class="col-md-12">
								<!-- BOX -->
								<div class="box border red">
									<div class="box-title">
										<h4><i class="fa fa-bars"></i>Nuevo/Editar Asignatura</h4>
									</div>
									<div class="box-body">
										<form class="form-horizontal " action="#" id="frm_subjects" name="frm_subjects">
										  <div class="form-group">
											 <label class="col-md-2 control-label" for="clave">Clave:</label> 
											 <div class="col-md-10">
                    <%
                        if(clave != null && !clave.equals("")){
                            out.print("<input type='text' id='clave' name='clave' class='form-control' autocomplete='off' value='"+clave+"' />");
                        }else{
                            out.print("<input type='text' id='clave' name='clave' class='form-control' autocomplete='off'/>");
                        }
                    %>
											 </div>
										  </div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="clave_oficial">Clave Oficial:</label> 
											<div class="col-md-10">
                    <%
                        if(clave_oficial != null && !clave_oficial.equals("")){
                            out.print("<input type='text' id='clave_oficial' name='clave_oficial' class='form-control' autocomplete='off' value='"+clave_oficial+"' />");
                        }else{
                            out.print("<input type='text' id='clave_oficial' name='clave_oficial' class='form-control' autocomplete='off' />");
                        }
                    %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="abreviatura">Abreviatura:</label> 
											<div class="col-md-10">
                    <%
                        if(abreviatura != null && !abreviatura.equals("")){
                            out.print("<input type='text' id='abreviatura' name='abreviatura' class='form-control' autocomplete='off' value='"+abreviatura+"' />");
                        }else{
                            out.print("<input type='text' id='abreviatura' name='abreviatura' class='form-control' autocomplete='off' />");
                        }
                    %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="e1">Plan de Estudios:</label> 
											<div class="col-md-10">
                    <%  
                        ps = con.prepareStatement("SELECT officialCode, prog_IdPlan FROM ProgPlan;");
                        rs = ps.executeQuery();
                        out.println("<select class='col-md-12' id='e1' name='plan'>");  
												out.println("<option id='0' selected value=''>Selecciona ..</option>");                   
                        while(rs.next()){
                            if(rs.getInt("ProgPlan.prog_IdPlan")== progIdPlan){
                                 out.println("<option id='"+rs.getInt("ProgPlan.prog_IdPlan")
                                         +"' selected>"+rs.getString("officialCode")+"</option>");
                             }else{
                                 out.println("<option id='"+rs.getInt("ProgPlan.prog_IdPlan")
                                         +"'>"+rs.getString("officialCode")+"</option>");
                             }
                         }                         
                         out.println("</select>");                                                     
                    %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="nombre">Nombre:</label> 
											<div class="col-md-10">
                    <%
                        if(nombre != null && !nombre.equals("")){
                            out.print("<input type='text' id='nombre' name='nombre' class='form-control' autocomplete='off' value='"+nombre+"' />");
                        }else{
                            out.print("<input type='text' id='nombre' name='nombre' class='form-control' autocomplete='off' />");
                        }
                    %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="e2">Departamento:</label> 
											<div class="col-md-10">
                    <%
                        ps = con.prepareStatement("SELECT name, org_IdDepartment FROM OrgDepartments;");
                        rs = ps.executeQuery();
                        out.println("<select class='col-md-12' id='e2' name='dept'>");                         
                        out.println("<option id='0' selected value=''>Selecciona ..</option>");
                        while(rs.next()){
                            if(rs.getInt("org_IdDepartment")== orgIdDep){
                                 out.println("<option id='"+rs.getInt("org_IdDepartment")
                                         +"' selected>"+rs.getString("name")+"</option>");
                             }else{
                                 out.println("<option id='"+rs.getInt("org_IdDepartment")
                                         +"'>"+rs.getString("name")+"</option>");
                             }
                         }                         
                         out.println("</select>");                                
                    %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="e3">Tipo:</label> 
											<div class="col-md-10">
                    <select  class="form-control" id="tipo" name="tipo"/>
                    <%
                        String typeArray[]={"NORMAL","OPTATIVA"};
												out.println("<option selected value=''>Selecciona ..</option>");
                        for(int i=0;i<2;i++){
                            if(tipo.equals(typeArray[i])){
                                out.print("<option id='"+typeArray[i]+"' selected>"+typeArray[i]+"</option>");
                            }else{
                                out.print("<option id='"+typeArray[i]+"'>"+typeArray[i]+"</option>");
                            }
                        }
                    %>
                    </select>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="hrs_teoricas">Horas Teoricas:</label> 
											<div class="col-md-10">
                    <%
                        if(hrs_teoricas != null && !hrs_teoricas.equals("")){
                            out.print("<input type='text' id='hrs_teoricas' name='hrs_teoticas' class='form-control' autocomplete='off' value='"+hrs_teoricas+"' />");
                        }else{
                            out.print("<input type='text' id='hrs_teoricas' name='hrs_teoticas' class='form-control' autocomplete='off' value='0'/>");
                        }
										%>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="c">Horas Practicas:</label> 
											<div class="col-md-10">
                    <%
                        if(hrs_practicas != null && !hrs_practicas.equals("")){
                            out.print("<input type='text' id='hrs_practicas' name='hrs_pacticas' class='form-control' autocomplete='off' value='"+hrs_practicas+"' />");
                        }else{
                            out.print("<input type='text' id='hrs_practicas' name='hrs_pacticas' class='form-control' autocomplete='off' value='0'/>");
                        }
                    %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="creditos">Creditos:</label> 
											<div class="col-md-10">
                    <%
                        if(creditos != null && !creditos.equals("")){
                            out.print("<input type='text' id='creditos' name='creditos' class='form-control' autocomplete='off' value='"+creditos+"' />");
                        }else{
                            out.print("<input type='text' id='creditos' name='creditos' class='form-control' autocomplete='off' value='0'/>");
                        }
                    %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="c_requeridos">Creditos Requeridos:</label> 
											<div class="col-md-10">
                    <%
                        if(c_requeridos != null && !c_requeridos.equals("")){
                            out.print("<input type='text' id='c_requeridos' name='c_requeridos' class='form-control' autocomplete='off' value='"+c_requeridos+"' />");
                        }else{
                            out.print("<input type='text' id='c_requeridos' name='c_requeridos' class='form-control' autocomplete='off' value='0'/>");
                        }
                    %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="semestre">Semestre:</label> 
											<div class="col-md-10">
                    <%
                        if(semestre != null && !semestre.equals("")){
                            out.print("<input type='text' id='semestre' name='semestre' class='form-control' autocomplete='off' value='"+semestre+"' />");
                        }else{
                            out.print("<input type='text' id='semestre' name='semestre' class='form-control' autocomplete='off' value='0'/>");
                        }
                    %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="s_requeridos">Semestre Requerido:</label> 
											<div class="col-md-10">
                    <%
                        if(s_requeridos != null && !s_requeridos.equals("")){
                            out.print("<input type='text' id='s_requeridos' name='s_requeridos' class='form-control' autocomplete='off' value='"+s_requeridos+"' />");
                        }else{
                            out.print("<input type='text' id='s_requeridos' name='s_requeridos' class='form-control' autocomplete='off' value='0'/>");
                        }
                    %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="fila">Orden en Fila:</label> 
											<div class="col-md-10">
                    <%
                        if(fila != null && !fila.equals("")){
                            out.print("<input type='text' id='fila' name='fila' class='form-control' autocomplete='off' value='"+fila+"' />");
                        }else{
                            out.print("<input type='text' id='fila' name='fila' class='form-control' autocomplete='off' value='0'/>");
                        }
                    %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="columnas">Orden en Columna:</label> 
											<div class="col-md-10">
                    <%
                        if(columna != null && !columna.equals("")){
                            out.print("<input type='text' id='columnas' name='columnas' class='form-control' autocomplete='off' value='"+columna+"' />");
                        }else{
                            out.print("<input type='text' id='columnas' name='columnas' class='form-control' autocomplete='off' value='0'/>");
                        }
                    %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="e4">Es requerida:</label> 
											<div class="col-md-10">
                    <select class="form-control" id="es_requerido" name='es_requerido'/>
                    <%
										out.println("<option id='0' selected value=''>Selecciona ..</option>");
                        String es_requeridoArray[]={"S","N"};
                        for(int i=0;i<2;i++){
                            if(es_requerido.equals(es_requeridoArray[i])){
                                out.print("<option id='"+es_requeridoArray[i]+"' selected>"+es_requeridoArray[i]+"</option>");
                            }else{
                                out.print("<option id='"+es_requeridoArray[i]+"'>"+es_requeridoArray[i]+"</option>");
                            }
                        }
                    %>
                    </select>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="e5">Se Evalua:</label> 
											<div class="col-md-10">
                    <select class="form-control" id="se_evalua" name="se_evalua"/>
                    <%
										out.println("<option id='0' selected value=''>Selecciona ..</option>");
                        String se_evaluaArray[]={"S","N"};
                        for(int i=0;i<2;i++){
                            if(se_evalua.equals(se_evaluaArray[i])){
                                out.print("<option id='"+se_evaluaArray[i]+"' selected>"+se_evaluaArray[i]+"</option>");
                            }else{
                                out.print("<option id='"+se_evaluaArray[i]+"'>"+se_evaluaArray[i]+"</option>");
                            }
                        }
                    %>
                    </select>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="e6">Se promedia:</label> 
											<div class="col-md-10">
                    <select class="form-control" id="se_promedia" name="se_promedia"/>
                    <%
										out.println("<option id='0' selected value=''>Selecciona ..</option>");
                        String se_promediaArray[]={"S","N"};
                        for(int i=0;i<2;i++){
                            if(se_promedia.equals(se_promediaArray[i])){
                                out.print("<option id='"+se_promediaArray[i]+"' selected>"+se_promediaArray[i]+"</option>");
                            }else{
                                out.print("<option id='"+se_promediaArray[i]+"'>"+se_promediaArray[i]+"</option>");
                            }
                        }
												db.close();
                    %>
                    </select>
											</div>
											</div>
											<input type="hidden" id="hide" value="<%out.println(request.getParameter("progIdSubject"));%>">
									   </form>
									</div>
								</div>
								<!-- /BOX -->
							</div>
						</div>
						<!-- /ADVANCED -->
						<div class="footer-tools">
							<span class="go-top">
								<i class="fa fa-chevron-up"></i> Top
							</span>
						</div>
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	<script src="../js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="../bootstrap-dist/js/bootstrap.min.js"></script>
	<!-- DATE RANGE PICKER -->
	<script src="../js/bootstrap-daterangepicker/moment.min.js"></script>
	<script src="../js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
	<!-- SLIMSCROLL -->
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
	<!-- BLOCK UI -->
	<script type="text/javascript" src="../js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
	<!-- TYPEHEAD -->
	<script type="text/javascript" src="../js/typeahead/typeahead.min.js"></script>
	<!-- AUTOSIZE -->
	<script type="text/javascript" src="../js/autosize/jquery.autosize.min.js"></script>
	<!-- COUNTABLE -->
	<script type="text/javascript" src="../js/countable/jquery.simplyCountable.min.js"></script>
	<!-- INPUT MASK -->
	<script type="text/javascript" src="../js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
	<!-- FILE UPLOAD -->
	<script type="text/javascript" src="../js/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
	<!-- SELECT2 -->
	<script type="text/javascript" src="../js/select2/select2.min.js"></script>
	<!-- UNIFORM -->
	<script type="text/javascript" src="../js/uniform/jquery.uniform.min.js"></script>
	<!-- JQUERY UPLOAD -->
	<!-- The Templates plugin is included to render the upload/download listings -->
	<script src="../js/blueimp/javascript-template/tmpl.min.js"></script>
	<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="../js/blueimp/javascript-loadimg/load-image.min.js"></script>
	<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script src="../js/blueimp/javascript-canvas-to-blob/canvas-to-blob.min.js"></script>
	<!-- blueimp Gallery script -->
	<script src="../js/blueimp/gallery/jquery.blueimp-gallery.min.js"></script>
	<!-- The basic File Upload plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload.min.js"></script>
	<!-- The File Upload processing plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-process.min.js"></script>
	<!-- The File Upload image preview & resize plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-image.min.js"></script>
	<!-- The File Upload audio preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-audio.min.js"></script>
	<!-- The File Upload video preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-video.min.js"></script>
	<!-- The File Upload validation plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-validate.min.js"></script>
	<!-- The File Upload user interface plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-ui.min.js"></script>
	<!-- The main application script -->
	<script src="../js/jquery-upload/js/main.js"></script>
	<!-- bootbox script -->
	<script src="../js/bootbox/bootbox.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="../js/script.js"></script>
	<script src="../js/jquery-validate/jquery.validate.js"></script>
	<script src="../js/jquery-validate/additional-methods.js"></script>
	<script src="../js/error_message.js"></script>
	<script>
		jQuery(document).ready(function() {		
			App.setPage("forms");  //Set current page
			App.init(); //Initialise plugins and elements
			$("#photo").attr("src","getPhoto?safe_IdUser="+<% if(user != null){out.print(user.getSafe_IdUser());} %>);
			var addOrEdit=<%out.println(request.getParameter("addOrEdit")+";"); %>
			
			$("#frm_subjects").validate({
				rules:{
					clave:{
						required: true,minlength: 1, maxlength: 5
					},
					clave_oficial:{
						required: true,minlength: 1, maxlength: 10
					},
				  abreviatura:{
						required: true,minlength: 3, maxlength: 15
					},
				  plan:{
						required: true
					},
				  nombre:{
						required: true,minlength: 1, maxlength: 55
					},
				  depto:{
						required: true
					},
				  tipo:{
						required: true
					},
				  hrs_teoricas:{
						required: true, digits: true
					},
				  hrs_practicas:{
						required: true, digits: true
					},
				  creditos:{
						required: true, digits: true
					},
				  c_requeridos:{
						required: true, digits: true
					},
				  semestre:{
						required: true, digits: true
					},
				  s_requeridos:{
						required: true, digits: true
					},
				  fila:{
						required: true, digits: true
					},
				  columnas:{
						required: true, digits: true
					},
				  es_requerido:{
						required: true
					},
				  se_evalua:{
						required: true
					},
				  se_promedia:{
						required: true
					}
				},
				highlight: function(element){
					$(element).parent().parent().addClass("has-error");
				},
				unhighlight: function(element){
				$(element).parent().parent().removeClass("has-error");
			}});
			
                    var abreviatura=$("#abreviatura");
                    var clave=$("#clave");
                    var clave_oficial=$("#clave_oficial");
                    var plan=$("#e1");
                    var nombre=$("#nombre");
                    var dept=$("#e2");
                    var tipo=$("#tipo");
                    var creditos=$("#creditos");
                    var c_requeridos=$("#c_requeridos");
                    var hrs_teoricas=$("#hrs_teoricas");
                    var hrs_practicas=$("#hrs_practicas");
                    var semestre=$("#semestre");
                    var s_requeridos=$("#s_requeridos");
                    var fila=$("#fila");
                    var columnas=$("#columnas");
                    var es_requerido=$("#es_requerido");
                    var se_evalua=$("#se_evalua");
                    var se_promedia=$("#se_promedia");
                    var idSubj=$("#hide");

			$("#nuevo").click(function () {
				window.location = "ProgSubjects_catalog.jsp?progIdSubject=0&addOrEdit=0";
			});
			$("#editar").click(function () {
                           abreviatura.removeAttr("disabled");
                           clave.removeAttr("disabled");
                           clave_oficial.removeAttr("disabled");
                           plan.removeAttr("disabled");
                           nombre.removeAttr("disabled");
                           dept.removeAttr("disabled");
                           tipo.removeAttr("disabled");
                           creditos.removeAttr("disabled");
                           c_requeridos.removeAttr("disabled");
                           hrs_teoricas.removeAttr("disabled");
                           hrs_practicas.removeAttr("disabled");
                           semestre.removeAttr("disabled");
                           s_requeridos.removeAttr("disabled");
                           fila.removeAttr("disabled");
                           columnas.removeAttr("disabled");
                           es_requerido.removeAttr("disabled");
                           se_evalua.removeAttr("disabled");
                           se_promedia.removeAttr("disabled");

				addOrEdit=1;
				$("#editar").attr("disabled","disabled");
			});
			$("#guardar").click(function () {
				if($("#frm_subjects").valid()){
				
                           var abVal =abreviatura.val().trim();
                           var claveVal =clave.val().trim();
                           var clave_oficialVal =clave_oficial.val().trim();
                           var planVal =plan.children(":selected").attr("id");
                           var nombreVal =nombre.val().trim();
                           var deptVal =dept.children(":selected").attr("id");
                           var tipoVal =tipo.children(":selected").attr("id");
                           var creditosVal =parseInt(creditos.val().trim());
                           var c_requeridosVal =parseInt(c_requeridos.val().trim());
                           var hrs_teoricasVal =parseInt(hrs_teoricas.val().trim());
                           var hrs_practicasVal =parseInt(hrs_practicas.val().trim());
                           var semestreVal =parseInt(semestre.val().trim());
                           var s_requeridosVal =parseInt(s_requeridos.val().trim());
                           var filaVal =parseInt(fila.val().trim());
                           var columnasVal =parseInt(columnas.val().trim());
                           var es_requeridoVal =es_requerido.children(":selected").attr("id");
                           var se_evaluaVal =se_evalua.children(":selected").attr("id");
                           var se_promediaVal =se_promedia.children(":selected").attr("id");

                           var values="values="+parseInt(idSubj.val())+"#"+planVal+"#"+deptVal+"#"+claveVal+"#"+clave_oficialVal+"#"+abVal+"#"+nombreVal+
                               "#"+creditosVal+"#"+c_requeridosVal+"#"+s_requeridosVal+"#"+hrs_practicasVal+"#"+hrs_teoricasVal+
                                   "#"+semestreVal+"#"+filaVal+"#"+columnasVal+"#"+es_requeridoVal+
                                   "#"+se_evaluaVal+"#"+se_promediaVal+"#"+tipoVal+"&addOrEdit="+addOrEdit;

					spInit();
					dhtmlxAjax.post("ProgSubjects_save",values,function(loader){
						var resp = loader.xmlDoc.responseText;
						respo = resp.split("#");
						document.getElementById("hide").value=respo[0];
						var resp = parseInt(respo[1]);
						if(resp == 1){
							bootbox.alert("Se guardo con exito!", function() {});
						} else {
							bootbox.alert("Fallo al guardar!", function() {});
						}
						spStop();
					});
                           abreviatura.attr("disabled","disabled");
                           clave.attr("disabled","disabled");
                           clave_oficial.attr("disabled","disabled");
                           plan.attr("disabled","disabled");
                           nombre.attr("disabled","disabled");
                           dept.attr("disabled","disabled");
                           tipo.attr("disabled","disabled");
                           creditos.attr("disabled","disabled");
                           c_requeridos.attr("disabled","disabled");
                           hrs_teoricas.attr("disabled","disabled");
                           hrs_practicas.attr("disabled","disabled");
                           semestre.attr("disabled","disabled");
                           s_requeridos.attr("disabled","disabled");
                           fila.attr("disabled","disabled");
                           columnas.attr("disabled","disabled");
                           es_requerido.attr("disabled","disabled");
                           se_evalua.attr("disabled","disabled");
                           se_promedia.attr("disabled","disabled");
					$("#editar").removeAttr("disabled");
				}});
		});
	</script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
			}else{
				response.sendRedirect("error600.jsp");
			}
		}else{
			response.sendRedirect("error600.jsp");
		}
%>