<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.edcore.model.db.DBConnector"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.edcore.model.safe.User"%>
	<%
		HttpSession sesion;
    sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if(user != null){
			ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
			if(privileges.contains("CAT35")){
	%>
<%
	ResultSet rs;
	DBConnector db = new DBConnector();
	PreparedStatement ps;
	Connection con;
	int addOrEdit=Integer.parseInt(request.getParameter("addOrEdit"));
    String clave="";
    String nombre="";
    String tipo="";
    int IdSch=0;
    int IdSublev=0;
    int IdLev=0;
    int IdGrad=0;
    int IdState=0;
	  con=db.open();
	
    int IdScho=Integer.parseInt(request.getParameter("idSch"));
      if(IdScho>0){
        ps = con.prepareStatement("SELECT Schools.*, SchoolSubLevels.schl_IdSubLevel, "
 + "SchoolLevels.schl_IdLevel, SchoolGrades.schl_IdGrade, AddrStates.name, AddrStates.addr_IdState "
 + "FROM (((Schools JOIN SchoolSubLevels ON Schools.schl_IdSubLevel=SchoolSubLevels.schl_IdSubLevel) "
 + "JOIN SchoolLevels ON SchoolSubLevels.schl_IdLevel=SchoolLevels.schl_IdLevel) "
 + "JOIN SchoolGrades ON SchoolLevels.schl_IdGrade=SchoolGrades.schl_IdGrade) "
 + "JOIN AddrStates ON Schools.addr_IdState=AddrStates.addr_IdState WHERE Schools.schl_IdSchool=?;");
        ps.setInt(1, IdScho);
    rs = ps.executeQuery();
    
    while(rs != null && rs.next()){
        
        clave =rs.getString("Schools.code");
        nombre =rs.getString("Schools.name");
        tipo =rs.getString("Schools.type");
        IdSch =Integer.parseInt(rs.getString("Schools.schl_IdSchool"));
        IdSublev =rs.getInt("SchoolSubLevels.schl_IdSubLevel");
        IdLev =rs.getInt("SchoolLevels.schl_IdLevel");
        IdGrad =rs.getInt("SchoolGrades.schl_IdGrade");
        IdState =rs.getInt("AddrStates.addr_IdState");
            }
    }
%>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Centros de estudio</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
	<link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
	
	<link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- DATE RANGE PICKER -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
	<!-- TYPEAHEAD -->
	<link rel="stylesheet" type="text/css" href="../js/typeahead/typeahead.css" />
	<!-- FILE UPLOAD -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-fileupload/bootstrap-fileupload.min.css" />
	<!-- SELECT2 -->
	<link rel="stylesheet" type="text/css" href="../js/select2/select2.min.css" />
	<!-- UNIFORM -->
	<link rel="stylesheet" type="text/css" href="../js/uniform/css/uniform.default.min.css" />
	<!-- JQUERY UPLOAD -->
	<!-- blueimp Gallery styles -->
	<link rel="stylesheet" href="../js/blueimp/gallery/blueimp-gallery.min.css">
	<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload.css">
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload-ui.css">
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	<style>
	 .error-message, label.error {
	    color: #ff0000;
	    margin:0;
	    display: inline;
	    font-size: 1em !important;
	    font-weight:bold;
	 }
	 </style>
	<!-- LIBRERIA EDCORE -->
  <script src="../dhtmlx/libCompiler/dhtmlxcommon.js"></script>
	<script src="../js/action_dhx.js"></script>
</head>
<body>
	<!-- HEADER -->
	<%@include file="header.jsp" %>
	<!--/HEADER -->
	<!-- PAGE -->
	<section id="page">
				<!-- SIDEBAR -->
				<%@include file="sidebar.jsp" %>
				<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<!-- STYLER -->
									
									<!-- /STYLER -->
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="index.jsp">Inicio</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Catalogos</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="Schools_admin.jsp">Centros de Estudio</a>
										</li>
										<li>Nuevo/Editar</li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										<h3 class="content-title pull-left">Cat�logo - Centro de Estudios</h3>
										<span class="date-range pull-right">
										<p class="btn-toolbar">
											<%if(privileges.contains("CAT78")){%>
											<button id="nuevo" class="btn btn-success" ><i class="fa fa-file-text-o"></i> Nuevo</button>
											<%}%>
											<%if(privileges.contains("CAT79")){%>
											<button id="editar" class="btn btn-success" disabled=""><i class="fa fa-pencil-square-o"></i> Editar</button>
											<%}%>
											<%if(privileges.contains("CAT80")){%>
											<button id="guardar" class="btn btn-success" ><i class="fa fa-save"></i> Guardar</button>
											<%}%>
										</p>
										</span>
									</div>
									<div class="description"></div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						<!-- ADVANCED -->
						<div class="row">
							<div class="col-md-12">
								<!-- BOX -->
								<div class="box border red">
									<div class="box-title">
										<h4><i class="fa fa-bars"></i>Nuevo/Editar Centro de Estudio</h4>
									</div>
									<div class="box-body">
										<form class="form-horizontal " action="#" id="frm_school">
										  <div class="form-group">
											 <label class="col-md-2 control-label" for="clave">Clave</label> 
											 <div class="col-md-10">
                            <% 
                            if(IdScho>0){
                            out.println("<input class='form-control' autocomplete='off' type='text' name='clave' id='clave' value='"+clave+"'>");
                            }else{
                            out.println("<input class='form-control' autocomplete='off'  type='text' name='clave'  id='clave' >");    
                            }
                            %>
											 </div>
										  </div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="nom">Nombre:</label> 
											<div class="col-md-10">
												<% 
                            if(IdScho>0){
                            out.println("<input class='form-control' autocomplete='off' type='text' name='nom' id='nom' value='"+nombre+"'>");
                            }else{
                            out.println("<input class='form-control' autocomplete='off'  type='text' name='nom' id='nom' >");    
                            }
                        %>

											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="e1">Tipo:</label> 
											<div class="col-md-10">
											<select id="e1" class="col-md-12" name="tip">
                            <%
                            ps = con.prepareStatement("SELECT type"
                                     + " FROM Schools GROUP BY type DESC;");

                            rs = ps.executeQuery();
                            out.println("<option id='0' value=''>Seleccione ..</option>");
                            while(rs.next()){
                                if(rs.getString("type").equals(tipo)){
                                     out.println("<option id='"+rs.getString("type")+
                                             "' selected>"+rs.getString("type")+"</option>");
                                 }else{
                                     out.println("<option id='"+rs.getString("type")+"'>"+
                                             rs.getString("type")+"</option>");
                                 }
                             }
                             %>
                        </select>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="e2">Estado:</label> 
											<div class="col-md-10">
											<select id="e2" class="col-md-12" name="sta">
                            <%
                            ps = con.prepareStatement("SELECT name, addr_IdState"
                                     + " FROM AddrStates;");

                            rs = ps.executeQuery();
                            out.println("<option id='0' value=''>Seleccione ..</option>");
                            while(rs.next()){
                                if(Integer.parseInt(rs.getString("addr_IdState"))== IdState){
                                     out.println("<option id='"+rs.getString("addr_IdState")+
                                             "' selected>"+rs.getString("name")+"</option>");
                                 }else{
                                     out.println("<option id='"+rs.getString("addr_IdState")+"'>"+
                                             rs.getString("name")+"</option>");
                                 }
                             }
                             %>
                        </select>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="e3">Grado:</label> 
											<div class="col-md-10">
												<select id="e3" class="col-md-12"  name="grade">
                            <%
                            ps = con.prepareStatement("SELECT name, schl_IdGrade"
                                     + " FROM SchoolGrades;");

                            rs = ps.executeQuery();
                            out.println("<option id='0' value=''>Seleccione ..</option>");
                            while(rs.next()){
                                if(Integer.parseInt(rs.getString("schl_IdGrade"))== IdGrad){
                                     out.println("<option id='"+rs.getString("schl_IdGrade")+
                                             "' selected>"+rs.getString("name")+"</option>");
                                 }else{
                                     out.println("<option id='"+rs.getString("schl_IdGrade")+"'>"+
                                             rs.getString("name")+"</option>");
                                 }
                             }
                             %>
												</select>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="e4">Nivel:</label> 
											<div class="col-md-10">
                            <%
                            if(addOrEdit>0){
                            out.println("<select class='col-md-12' id='e4' name='level'>");
                            ps = con.prepareStatement("SELECT name, schl_IdLevel"
                                     + " FROM SchoolLevels;");

                            rs = ps.executeQuery();
                            out.println("<option id='0' value=''>Seleccione ..</option>");
                            while(rs.next()){
                                if(Integer.parseInt(rs.getString("schl_IdLevel"))== IdLev){
                                     out.println("<option id='"+rs.getString("schl_IdLevel")+
                                             "' selected>"+rs.getString("name")+"</option>");
                                 }else{
                                     out.println("<option id='"+rs.getString("schl_IdLevel")+"'>"+
                                             rs.getString("name")+"</option>");
                                 }
                             }
                            out.println("</select>");
                            
                            }else{
                                 out.println("<select class='col-md-12' id='e4' name='level' disabled>");
                                 out.println("<option id='0' value=''>Seleccione ..</option>");
                                 out.println("</select>");
                            }
                             %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="e5">Subnivel:</label> 
											<div class="col-md-10">
                            <%
                            if(addOrEdit>0){
																out.println("<select class='col-md-12' id='e5' name='subLevel'>");
                                ps = con.prepareStatement("SELECT name, schl_IdSubLevel"
                                         + " FROM SchoolSubLevels;");

                                rs = ps.executeQuery();
                                out.println("<option id='0' value=''>Seleccione ..</option>");
                                while(rs.next()){
                                    if(Integer.parseInt(rs.getString("schl_IdSubLevel"))== IdSublev){
                                         out.println("<option id='"+rs.getString("schl_IdSubLevel")+
                                                 "' selected>"+rs.getString("name")+"</option>");
                                     }else{
                                         out.println("<option id='"+rs.getString("schl_IdSubLevel")+"'>"+
                                                 rs.getString("name")+"</option>");
                                     } 
                                 }
                                out.println("</select>");
                            }else{
																out.println("<select class='col-md-12' id='e5' name='subLevel' disabled>");
                                 out.println("<option id='0' value=''>Seleccione ..</option>");
                                 out.println("</select>");
                            }
														db.close();
                             %>
												
											</div>
											</div>
											<input type="hidden" id="hide" value="<%=IdScho%>">
									   </form>
									</div>
								</div>
								<!-- /BOX -->
							</div>
						</div>
						<!-- /ADVANCED -->
						<div class="footer-tools">
							<span class="go-top">
								<i class="fa fa-chevron-up"></i> Top
							</span>
						</div>
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	<script src="../js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="../bootstrap-dist/js/bootstrap.min.js"></script>
	<!-- DATE RANGE PICKER -->
	<script src="../js/bootstrap-daterangepicker/moment.min.js"></script>
	<script src="../js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
	<!-- SLIMSCROLL -->
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
	<!-- BLOCK UI -->
	<script type="text/javascript" src="../js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
	<!-- TYPEHEAD -->
	<script type="text/javascript" src="../js/typeahead/typeahead.min.js"></script>
	<!-- AUTOSIZE -->
	<script type="text/javascript" src="../js/autosize/jquery.autosize.min.js"></script>
	<!-- COUNTABLE -->
	<script type="text/javascript" src="../js/countable/jquery.simplyCountable.min.js"></script>
	<!-- INPUT MASK -->
	<script type="text/javascript" src="../js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
	<!-- FILE UPLOAD -->
	<script type="text/javascript" src="../js/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
	<!-- SELECT2 -->
	<script type="text/javascript" src="../js/select2/select2.min.js"></script>
	<!-- UNIFORM -->
	<script type="text/javascript" src="../js/uniform/jquery.uniform.min.js"></script>
	<!-- JQUERY UPLOAD -->
	<!-- The Templates plugin is included to render the upload/download listings -->
	<script src="../js/blueimp/javascript-template/tmpl.min.js"></script>
	<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="../js/blueimp/javascript-loadimg/load-image.min.js"></script>
	<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script src="../js/blueimp/javascript-canvas-to-blob/canvas-to-blob.min.js"></script>
	<!-- blueimp Gallery script -->
	<script src="../js/blueimp/gallery/jquery.blueimp-gallery.min.js"></script>
	<!-- The basic File Upload plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload.min.js"></script>
	<!-- The File Upload processing plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-process.min.js"></script>
	<!-- The File Upload image preview & resize plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-image.min.js"></script>
	<!-- The File Upload audio preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-audio.min.js"></script>
	<!-- The File Upload video preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-video.min.js"></script>
	<!-- The File Upload validation plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-validate.min.js"></script>
	<!-- The File Upload user interface plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-ui.min.js"></script>
	<!-- The main application script -->
	<script src="../js/jquery-upload/js/main.js"></script>
	<!-- bootbox script -->
	<script src="../js/bootbox/bootbox.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="../js/script.js"></script>
	<script src="../js/jquery-validate/jquery.validate.js"></script>
	<script src="../js/jquery-validate/additional-methods.js"></script>
	<script src="../js/error_message.js"></script>
	<script>
		jQuery(document).ready(function() {		
			App.setPage("forms");  //Set current page
			App.init(); //Initialise plugins and elements
			$("#photo").attr("src","getPhoto?safe_IdUser="+<% if(user != null){out.print(user.getSafe_IdUser());} %>);
			var addOrEdit=<%out.println(request.getParameter("addOrEdit")+";"); %>
			
			$("#frm_school").validate({
				rules:{
					clave:{
						required: true, minlength: 1, maxlength: 10
					},
					nom:{
						required: true, minlength: 1, maxlength: 120
					},
					tip:{
						required: true
					},
					sta:{
						required: true
					},
					grade:{
						required: true
					},
					level:{
						required: true
					},
					subLevel:{
						required: true
					}
				},
				highlight: function(element){
					$(element).parent().parent().addClass("has-error");
				},
				unhighlight: function(element){
				$(element).parent().parent().removeClass("has-error");
			}});
		
            var grade=$("#e3");
            var level=$("#e4");
            var subLevel=$("#e5");
						
            var gradeVal =grade.children(":selected").attr("id");
            var levelVal =level.children(":selected").attr("id");
            var subLevelVal =subLevel.children(":selected").attr("id");
						
                    var clave=$("#clave");
                    var nom=$("#nom");
                    var tip=$("#e1");
                    var sta=$("#e2");
                    var idSchool=$("#hide");

			
			$("#nuevo").click(function () {
				window.location = "Schools_catalog.jsp?idSch=0&addOrEdit=0";
			});
			$("#editar").click(function () {
                           clave.removeAttr("disabled");
                           nom.removeAttr("disabled");
                           tip.removeAttr("disabled");
                           sta.removeAttr("disabled");
                           grade.removeAttr("disabled");
                           level.removeAttr("disabled");
                           subLevel.removeAttr("disabled");
				addOrEdit=1;
				$("#editar").attr("disabled","disabled");
			});
			
             $("#e3").change(function(){
                var id=$(this).children(":selected").attr("id");
                // remove all options, but not the first
                $('#e4 option:gt(0)').remove();
                $('#e5 option:gt(0)').remove();
                if(id>0){ 
                    $("#e4").removeAttr("disabled");
                    
                    dhtmlxAjax.post("Schools_auxiliary.jsp",
                    "idGrade="+id+"&opt=1",function (loader){
                    $("#e4").append(loader.xmlDoc.responseText);                    
                });
                $("#e5").attr("disabled","disabled");
                
                }else{
                  $("#e4").attr("disabled","disabled");
                  $("#e5").attr("disabled","disabled");
                }
               });
               
               $("#e4").change(function(){
                var id=$(this).children(":selected").attr("id");
                // remove all options, but not the first
                $('#e5 option:gt(0)').remove(); 
                if(id>0){ 
                    $("#e5").removeAttr("disabled");
                    dhtmlxAjax.post("Schools_auxiliary.jsp",
                    "sLevel="+id+"&opt=2",function (loader){
                    $("#e5").append(loader.xmlDoc.responseText);                    
                });
                }else{
                  $("#e5").attr("disabled","disabled");                  
                }
               });

			$("#guardar").click(function () {
				$("#frm_school").valid();

                         var claveVal =clave.val().trim();
                           var nomVal =nom.val().trim();
                           var tipVal =tip.val().trim();
                           var staVal =sta.children(":selected").attr("id");
                          
                           subLevelVal =subLevel.children(":selected").attr("id");
                           
                           clave.attr("disabled","disabled");
                           nom.attr("disabled","disabled");
                           tip.attr("disabled","disabled");
                           sta.attr("disabled","disabled");
                           grade.attr("disabled","disabled");
                           level.attr("disabled","disabled");
                           subLevel.attr("disabled","disabled");
  
					var values= "values="+parseInt(idSchool.val())+"#"+parseInt(subLevelVal)+"#"+parseInt(staVal)+"#"+claveVal+"#"+nomVal+"#"+tipVal+"&addOrEdit="+addOrEdit;
					alert(values);
					dhtmlxAjax.post("Schools_save",values,function(loader){
						var resp = loader.xmlDoc.responseText;
						respo = resp.split("#");
						document.getElementById("hide").value=respo[0];
						var resp = parseInt(respo[1]);
						if(resp == 1){
							bootbox.alert("Se guardo con exito!", function() {});
						} else {
							bootbox.alert("Fallo al guardar!", function() {});
						}
					});

					$("#editar").removeAttr("disabled");
			});
		});
	</script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
			}else{
				response.sendRedirect("error600.jsp");
			}
		}else{
			response.sendRedirect("error600.jsp");
		}
%>