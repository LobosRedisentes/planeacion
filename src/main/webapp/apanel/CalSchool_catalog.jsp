<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.edcore.model.db.DBConnector"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.edcore.model.safe.User"%>
	<%
		HttpSession sesion;
    sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if(user != null){
			ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
			if(privileges.contains("CAT24")){
	%>

<%
	ResultSet rs;
	DBConnector db = new DBConnector();
	PreparedStatement ps;
	Connection con;
    int cal_IdCalSchool=Integer.parseInt(request.getParameter("cal_IdCalSchool"));
    String clave = "";
    String Abreviation = "";
    String Name = "";
    String Start = "";
    String End = "";
    String StartVacation = "";
    String EndVacation = "";
    String Status = "";
		   
	int addOrEdit=Integer.parseInt(request.getParameter("addOrEdit"));
	con=db.open();
	
    if(cal_IdCalSchool > 0){
        ps = con.prepareStatement("SELECT * FROM CalSchool WHERE cal_IdCalSchool = ?");
        ps.setInt(1, cal_IdCalSchool);
        rs = ps.executeQuery();
            
        while(rs != null && rs.next()){
            clave = rs.getString("code");
            Abreviation = rs.getString("abreviation");
            Name = rs.getString("name");
            Start = rs.getString("start");
            End = rs.getString("end");
            StartVacation = rs.getString("startVacation");
            EndVacation = rs.getString("endVacation");
            Status = rs.getString("status");
        }
    }

%>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Calendarios</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
	<link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
	
	<link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- DATE RANGE PICKER -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
	<!-- TYPEAHEAD -->
	<link rel="stylesheet" type="text/css" href="../js/typeahead/typeahead.css" />
	<!-- FILE UPLOAD -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-fileupload/bootstrap-fileupload.min.css" />
	<!-- SELECT2 -->
	<link rel="stylesheet" type="text/css" href="../js/select2/select2.min.css" />
	<!-- UNIFORM -->
	<link rel="stylesheet" type="text/css" href="../js/uniform/css/uniform.default.min.css" />
	<!-- JQUERY UPLOAD -->
	<!-- blueimp Gallery styles -->
	<link rel="stylesheet" href="../js/blueimp/gallery/blueimp-gallery.min.css">
	<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload.css">
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload-ui.css">
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	<style>
	 .error-message, label.error {
	    color: #ff0000;
	    margin:0;
	    display: inline;
	    font-size: 1em !important;
	    font-weight:bold;
	 }
	 </style>
	<!-- LIBRERIA EDCORE -->
  <script src="../dhtmlx/libCompiler/dhtmlxcommon.js"></script>
	<script src="../js/action_dhx.js"></script>
	<%="<script>var cal_IdCalSchool = "+cal_IdCalSchool +";</script>"%>
</head>
<body>
	<!-- HEADER -->
	<%@include file="header.jsp" %>
	<!--/HEADER -->
	<!-- PAGE -->
	<section id="page">
				<!-- SIDEBAR -->
				<%@include file="sidebar.jsp" %>
				<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<!-- STYLER -->
									
									<!-- /STYLER -->
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="index.jsp">Inicio</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Catalogos</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="CalSchool_admin.jsp">Calendarios</a>
										</li>
										<li>Nuevo/Editar</li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										<h3 class="content-title pull-left">Cat�logo de Calendarios</h3>
										<span class="date-range pull-right">
										<p class="btn-toolbar">
											<%if(privileges.contains("CAT42")){%>
											<button id="nuevo" class="btn btn-success" ><i class="fa fa-file-text-o"></i> Nuevo</button>
											<%}%>
											<%if(privileges.contains("CAT43")){%>
											<button id="editar" class="btn btn-success" disabled=""><i class="fa fa-pencil-square-o"></i> Editar</button>
											<%}%>
											<%if(privileges.contains("CAT44")){%>
											<button id="guardar" class="btn btn-success" ><i class="fa fa-save"></i> Guardar</button>
											<%}%>
										</p>
										</span>
									</div>
									<div class="description"></div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						<!-- ADVANCED -->
						<div class="row">
							<div class="col-md-12">
								<!-- BOX -->
								<div class="box border red">
									<div class="box-title">
										<h4><i class="fa fa-bars"></i>Nuevo/Editar Calendario</h4>
									</div>
									<div class="box-body">
										<form class="form-horizontal " action="#" id="frm_calendario">
											<input type="hidden" name="IdCalSchool" id="IdCalSchool" value="0">
										  <div class="form-group">
											 <label class="col-md-2 control-label" for="Abreviatura">Abreviatura:</label> 
											 <div class="col-md-10">
                                 <%
                                    if(Abreviation != null && !Abreviation.equals("")){
                                        out.print("<input class='form-control' autocomplete='off' type='text' id='Abreviation' name='Abreviation' value='"+Abreviation+"'/>");
                                    }else{
                                        out.print("<input class='form-control' autocomplete='off' type='text' id='Abreviation' name='Abreviation'/>");
                                    }
                                %>
											 </div>
											</div>
											 <div class="form-group">
											 <label class="col-md-2 control-label" for="e1">Clave:</label>
											 <div class="col-md-10">
                                 <%
                                    String cla[]={"A","B","I"};
                                    out.print("<select class='col-md-12' type='select' id='e1' name='Clave'/>");
																		if(clave.equals("")){
																			out.println("<option id='-1' selected value='' >Selecciona ..</option>");
																		}else{
																			out.println("<option id='-1' value=''>Selecciona ..</option>");
																		}
                                    for(int i=0;i<3;i++){
                                        if(clave.equals(cla[i])){
                                            out.print("<option id='"+cla[i]+"' selected>"+cla[i]+"</option>");

                                        }else{
                                            out.print("<option id='"+cla[i]+"' >"+cla[i]+"</option>");
                                        }
                                    }
                                    out.print("</select>");
                                %>
											 </div>
										  </div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="Name">Nombre:</label> 
											<div class="col-md-10">
                                 <%
                                    out.print("<input type='text' class='form-control' id='Name' name='Name'  value='"+Name+"'/>"); 
                                %>
											</div>
											</div>
										  <div class="form-group">
											 <label class="col-md-2 control-label" for="FechaInicio">Inicio del Periodo:</label> 
											 <div class="col-md-10">
                        <%
                        if(Start != null && !Start.equals("")){
                            out.print("<input type='date' class='form-control' id='FechaInicio' name='FechaInicio'  value='"+Start+"' />");
                        }else{
                            out.print("<input type='date' class='form-control' id='FechaInicio' name='FechaInicio'/>");
                        }
                        %>
											 </div>
											</div>
											<div class="form-group">
											 <label class="col-md-2 control-label" for="FechaTermino">Termino del periodo:</label>
											 <div class="col-md-10">
                        <%
                        if(End != null && !End.equals("")){
                            out.print("<input type='date' class='form-control' id='FechaTermino' name='FechaTermino'  value='"+End+"'/>");
                        }else{
                            out.print("<input type='date' class='form-control' id='FechaTermino' name='FechaTermino'/>");
                        }
                        %> 
											 </div>
										  </div>
										  <div class="form-group">
											 <label class="col-md-2 control-label" for="FechaInicioVacaciones">Inicio de vacaciones:</label> 
											 <div class="col-md-10">
                        <%
                        if(StartVacation != null && !StartVacation.equals("")){
                            out.print("<input type='date' class='form-control' id='FechaInicioVacaciones' name='FechaInicioVacaciones'  value='"+Start+"'/>");
                        }else{
                            out.print("<input type='date' class='form-control' id='FechaInicioVacaciones' name='FechaInicioVacaciones'/>");
                        }
                        %>
											 </div>
											</div>
											<div class="form-group">
											 <label class="col-md-2 control-label" for="FechaTerminoVacaciones">Termino de vacaciones:</label>
											 <div class="col-md-10">
                        <%
                        if(EndVacation != null && !EndVacation.equals("")){
                            out.print("<input type='date' class='form-control' id='FechaTerminoVacaciones' name='FechaTerminoVacaciones'   value='"+End+"'/>");
                        }else{
                            out.print("<input type='date' class='form-control' id='FechaTerminoVacaciones' name='FechaTerminoVacaciones' />");
                        }
                        %> 
											 </div>
										  </div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="e2">Status:</label> 
											<div class="col-md-10">
                        <%
                        String sta[]={"ACTIVO","INACTIVO","PREPARACION"};
                        out.print("<select class='col-md-12' type='select' id='e2' name='Status'/>");
												if(Status.equals("")){
													out.println("<option id='-1' selected value='' >Selecciona ..</option>");
												}else{
													out.println("<option id='-1' value=''>Selecciona ..</option>");
												}
                        for(int i=0;i<3;i++){
                            if(Status.equals(sta[i])){
                                out.print("<option id='"+sta[i]+"' selected>"+sta[i]+"</option>");

                            }else{
                                out.print("<option id='"+sta[i]+"' >"+sta[i]+"</option>");
                            }
                        }
                        out.print("</select>");
												db.close();
                        %>
											</div>
											</div>
											<input type="hidden" id="hide" value="<% out.print(cal_IdCalSchool);%>">
									   </form>
									</div>
								</div>
								<!-- /BOX -->
							</div>
						</div>
						<!-- /ADVANCED -->
						<div class="footer-tools">
							<span class="go-top">
								<i class="fa fa-chevron-up"></i> Top
							</span>
						</div>
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	<script src="../js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="../bootstrap-dist/js/bootstrap.min.js"></script>
	<!-- DATE RANGE PICKER -->
	<script src="../js/bootstrap-daterangepicker/moment.min.js"></script>
	<script src="../js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
	<!-- SLIMSCROLL -->
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
	<!-- BLOCK UI -->
	<script type="text/javascript" src="../js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
	<!-- TYPEHEAD -->
	<script type="text/javascript" src="../js/typeahead/typeahead.min.js"></script>
	<!-- AUTOSIZE -->
	<script type="text/javascript" src="../js/autosize/jquery.autosize.min.js"></script>
	<!-- COUNTABLE -->
	<script type="text/javascript" src="../js/countable/jquery.simplyCountable.min.js"></script>
	<!-- INPUT MASK -->
	<script type="text/javascript" src="../js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
	<!-- FILE UPLOAD -->
	<script type="text/javascript" src="../js/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
	<!-- SELECT2 -->
	<script type="text/javascript" src="../js/select2/select2.min.js"></script>
	<!-- UNIFORM -->
	<script type="text/javascript" src="../js/uniform/jquery.uniform.min.js"></script>
	<!-- JQUERY UPLOAD -->
	<!-- The Templates plugin is included to render the upload/download listings -->
	<script src="../js/blueimp/javascript-template/tmpl.min.js"></script>
	<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="../js/blueimp/javascript-loadimg/load-image.min.js"></script>
	<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script src="../js/blueimp/javascript-canvas-to-blob/canvas-to-blob.min.js"></script>
	<!-- blueimp Gallery script -->
	<script src="../js/blueimp/gallery/jquery.blueimp-gallery.min.js"></script>
	<!-- The basic File Upload plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload.min.js"></script>
	<!-- The File Upload processing plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-process.min.js"></script>
	<!-- The File Upload image preview & resize plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-image.min.js"></script>
	<!-- The File Upload audio preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-audio.min.js"></script>
	<!-- The File Upload video preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-video.min.js"></script>
	<!-- The File Upload validation plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-validate.min.js"></script>
	<!-- The File Upload user interface plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-ui.min.js"></script>
	<!-- The main application script -->
	<script src="../js/jquery-upload/js/main.js"></script>
	<!-- bootbox script -->
	<script src="../js/bootbox/bootbox.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="../js/script.js"></script>
	<script src="../js/jquery-validate/jquery.validate.js"></script>
	<script src="../js/jquery-validate/additional-methods.js"></script>
	<script src="../js/error_message.js"></script>
	<script>
		jQuery(document).ready(function() {		
			App.setPage("forms");  //Set current page
			App.init(); //Initialise plugins and elements
			$("#photo").attr("src","getPhoto?safe_IdUser="+<% if(user != null){out.print(user.getSafe_IdUser());} %>);
			var idResp=0;
			var valiAbv=true;
			var valiName=true;

			var addOrEdit=<%out.println(request.getParameter("addOrEdit")+";"); %>
			
			$("#frm_calendario").validate({
				rules:{
					Abreviation:{
						required: true, minlength: 5, maxlength: 20
					},
					Clave:{
						required: true
					},
					Name:{
						required: true, minlength: 10, maxlength: 50
					},
					FechaInicio:{
						required: true
					},
					FechaTermino:{
						required: true
					},
					FechaInicioVacaciones:{
						required: true
					},
					FechaTerminoVacaciones:{
						required: true
					},
					Status:{
						required: true
					}
				},
				highlight: function(element){
					$(element).parent().parent().addClass("has-error");
				},
				unhighlight: function(element){
				$(element).parent().parent().removeClass("has-error");
			}});
			
			$("#nuevo").click(function () {
				window.location = "CalSchool_catalog.jsp?cal_IdCalSchool=0&addOrEdit=0";
			});
			$("#editar").click(function () {
				$("#Abreviatura").removeAttr("disabled");
				$("#e1").removeAttr("disabled");
				$("#Clave").removeAttr("disabled");
				$("#Name").removeAttr("disabled");
				$("#FechaInicio").removeAttr("disabled");
				$("#FechaTermino").removeAttr("disabled");
				$("#e2").removeAttr("disabled");
				$("#FechaInicioVacaciones").removeAttr("disabled");
				$("#FechaTerminoVacaciones").removeAttr("disabled");
				addOrEdit=1;
				$("#editar").attr("disabled","disabled");
			});
			$("#guardar").click(function () {
				$("#frm_calendario").valid();

			var Abreviation=$("#Abreviation");
			var clave=$("#e1");
			var Name= $("#Name");
			var FechaInicio= $("#FechaInicio");
			var FechaTermino=$("#FechaTermino");
			var Status=$("#e2");
			var FechaInicioVacaciones =$("#FechaInicioVacaciones");
			var FechaTerminoVacaciones=$("#FechaTerminoVacaciones");
			var hide=$("#hide");
			var StatusVal=Status.children(":selected").attr("id");
			var claveVal=clave.children(":selected").attr("id");
			var NameVal=Name.val().trim();
			var FiVal=FechaInicio.val().trim();
			var FtVal=FechaTermino.val().trim();
			var FiVacVal=FechaInicioVacaciones.val().trim();
			var FtVacVal=FechaTerminoVacaciones.val().trim();
			var AbrVal=Abreviation.val().trim();

				if(StatusVal!=="" && claveVal!=="" && NameVal!=="" && FiVal!=="" && FtVal!=="" && FiVacVal!=="" && FtVacVal!==""){
					var values= "values="+hide.val()+"#"+claveVal+"#"+AbrVal+"#"+NameVal+"#"+FiVal+"#"+FtVal+"#"+FiVacVal+"#"+FtVacVal+"#"+StatusVal+"&addOrEdit="+addOrEdit;
					
					dhtmlxAjax.post("CalSchool_save",values,function(loader){
						var resp = loader.xmlDoc.responseText;
						respo = resp.split("#");
						document.getElementById("hide").value=respo[0];
						var resp = parseInt(respo[1]);
						if(resp == 1){
							bootbox.alert("Se guardo con exito!", function() {});
						} else {
							bootbox.alert("Fallo al guardar!", function() {});
						}
					});

					Abreviation.attr("disabled","disabled");
					clave.attr("disabled","disabled");
					Name.attr("disabled","disabled");
					FechaInicio.attr("disabled","disabled");
					FechaTermino.attr("disabled","disabled");
					Status.attr("disabled","disabled");
					FechaInicioVacaciones.attr("disabled","disabled");
					FechaTerminoVacaciones.attr("disabled","disabled");

					$("#editar").removeAttr("disabled");
				}
			});
		});
	</script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
			}else{
				response.sendRedirect("error600.jsp");
			}
		}else{
			response.sendRedirect("error600.jsp");
		}
%>