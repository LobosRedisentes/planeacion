<%@page import="java.util.List"%>
<%@page import="mx.com.edcore.vo.AccountVO"%>
<%@page import="mx.com.edcore.vo.SessionVO"%>
<%
    HttpSession websession;
    websession = request.getSession();
    SessionVO sesion = (SessionVO) websession.getAttribute("session");
    if (sesion != null) {
        List<String> privileges = (List<String>) websession.getAttribute("privileges");
%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <title>edcore - Escolar</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
        <meta name="description" content="edcore - Escolar">
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        <meta name="author" content="Instituto Tecnologico Superior de Zapopan">	
        <link rel="stylesheet" type="text/css" href="../lib/cloudAdmin/css/cloud-admin.css" >
        <link rel="stylesheet" type="text/css" href="../lib/cloudAdmin/css/default.css" id="skin-switcher" >
        <link rel="stylesheet" type="text/css" href="../lib/cloudAdmin/css/responsive.css" >
        <link rel="stylesheet" type="text/css" href="../css/edcore.css" >
        <link rel="stylesheet" type="text/css" href="../font/awesome/css/font-awesome.min.css">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <!-- HEADER -->
        <%@include file="header.jsp" %>
        <!--/HEADER -->
        <!-- PAGE -->
        <section id="page">
            <!-- SIDEBAR -->
            <%@include file="sidebar.jsp" %>
            <!-- /SIDEBAR -->
            <div id="main-content">
                <div class="container">
                    <div class="row">
                        <div id="content" class="col-lg-12">
                            <!-- PAGE HEADER-->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="page-header">
                                        <!-- BREADCRUMBS -->
                                        <ul class="breadcrumb">
                                            <li><i class="fa fa-home"></i><a href="index.html">Inicio</a></li>
                                            <li>Panel de Control</li>
                                        </ul>
                                        <!-- /BREADCRUMBS -->
                                        <div class="clearfix">
                                            <h3 class="content-title pull-left">Panel de Control</h3>
                                        </div>
                                        <div class="description"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- /PAGE HEADER -->
                            <!-- DASHBOARD CONTENT -->
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- BOX -->
                                    <div class="box border">
                                        <img src="../img/main.png" style="width: 100%">
                                    </div>
                                </div>
                            </div>
                            <!-- /DASHBOARD CONTENT -->
                        </div><!-- /CONTENT-->
                    </div>
                </div>
            </div>
        </section>
        <!--/PAGE -->
        <!-- JAVASCRIPTS -->
        <script src="../lib/jquery/jquery-2.0.3.min.js"></script>
        <script src="../lib/bootstrap-dist/js/bootstrap.min.js"></script>
        <script src="../lib/jQuery-Cookie/jquery.cookie.min.js"></script>
        <script src="../lib/cloudAdmin/js/script.js"></script>
        <script src="index.js"></script>
        <!-- /JAVASCRIPTS -->
    </body>
</html>
<%
    } else {
        response.sendRedirect("error600.jsp");
    }
%>