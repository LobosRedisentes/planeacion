//Funciones para guardar datos de semestrales parciales e interciclos

function action_saveGrade(){     
        var values;
        var array = [];
        var IDList;
        var grade;
        var option;
        var status;
        grid.forEachRow(function(id,e){
        IDList = id;
        grade  = grid.cells(id,3).getValue().toUpperCase();
        option  = grid.cells(id,4).getValue().toUpperCase();
        var params;


        if(grade === 'NC' || grade === 'NP'){
            status  = grade ;
            grade = 0;
            parseInt(grade);
            if(option === 'ORD' || option === 'REG' || option === 'REPO'){
                params = IDList+"#"+grade+"#"+option+"#"+status;

                if(grade > -1 && grade <= 100){
                    array.push(params);
                } else {
                    alertError("La calificacion debe ser entre 0 y 100");
                    e.preventDefault();
                }
            }else {
                    alertError("Solo se permiten los valores ORD, REG o REPO");
                    e.preventDefault();
            }
        } else if(!isNaN(grade)){
            status = 'CP';        
            parseInt(grade);
            if(option === 'ORD' || option === 'REG' || option === 'REPO'){
                params = IDList+"#"+grade+"#"+option+"#"+status;
                if(grade > -1 && grade <= 100){                
                    array.push(params);
                } else {
                    alertError("La calificacion debe ser entre 0 y 100");
                    e.preventDefault();
                }
            }else {
                    alertError("Solo se permiten los valores ORD, REG o REPO");
                    e.preventDefault();
            }
        }else{
            alertError("La calificacion debe ser entre 0 y 100 o NC O NP");
            e.preventDefault();
        }


        });
        var values = "values="+array;
        spInit();
        dhtmlxAjax.post("Grades_AEdit",values,function(loader){
            var resp = loader.xmlDoc.responseText;
            if(parseInt(resp) === 1){
            alertSucces();
            spStop();
           } else {
                alertError("Ha ocurrido un error!");
                spStop();
           }       
        });
    }


function action_saveFinal(){
    var values;
        var array = [];
        var IDList;
        var grade;
        var option;
        var status;
        grid.forEachRow(function(id,e){
        IDList = id;
        grade  = grid.cells(id,3).getValue().toUpperCase();
        option  = grid.cells(id,4).getValue().toUpperCase();
        var params;


        if(grade === 'NC' || grade === 'NP'){
            status  = grade ;
            grade = 0;
            parseInt(grade);
            if(option === 'ORD' || option === 'REG' || option === 'REPO'){
                params = IDList+"#"+grade+"#"+option+"#"+status;

                if(grade > -1 && grade <= 100){
                    array.push(params);
                } else {
                    alertError("La calificacion debe ser entre 0 y 100");
                    e.preventDefault();
                }
            }else {
                    alertError("Solo se permiten los valores ORD, REG o REPO");
                    e.preventDefault();
            }
        } else if(!isNaN(grade)){
            status = 'CP';        
            parseInt(grade);
            if(option === 'ORD' || option === 'REG' || option === 'REPO'){
                params = IDList+"#"+grade+"#"+option+"#"+status;
                if(grade > -1 && grade <= 100){                
                    array.push(params);
                } else {
                    alertError("La calificacion debe ser entre 0 y 100");
                    e.preventDefault();
                }
            }else {
                    alertError("Solo se permiten los valores ORD, REG o REPO");
                    e.preventDefault();
            }
        }else{
            alertError("La calificacion debe ser entre 0 y 100 o NC O NP");
            e.preventDefault();
        }


        });
        var values = "values="+array;
        spInit();
        dhtmlxAjax.post("GroupList_Asave",values,function(loader){
            var resp = loader.xmlDoc.responseText;
            if(parseInt(resp) === 1){
                alertSucces();
                spStop();
           } else {
                alertError("Ha ocurrido un error!");
                spStop();
           }       
        });
    
}

function alertError(text){
    bootbox.alert(text, function() {});
}

function alertSucces(){
    bootbox.alert("Se ha guardado exitosamente!", function() {});
}