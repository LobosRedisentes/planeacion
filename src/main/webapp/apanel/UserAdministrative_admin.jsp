<%@page import="java.util.ArrayList"%>
<%@page import="com.edcore.model.safe.User"%>
	<%
		HttpSession sesion;
    sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if(user != null){
			ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
			if(privileges.contains("CAT11")){
	%>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Administrativos</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="Sistema educativo">
	<meta name="author" content="Instituto Tecnologico Superior de Zapopan">	
    <!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
    <link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
    <link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
    <link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
    <!-- TYPEAHEAD -->
    <link rel="stylesheet" type="text/css" href="../js/typeahead/typeahead.css" />
    <!-- SELECT2 -->
    <link rel="stylesheet" type="text/css" href="../js/select2/select2.min.css" />
    <!-- FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <style>
        .error-message, label.error {
            color: #ff0000;
            margin:0;
            display: inline;
            font-size: 1em !important;
            font-weight:bold;
        }
    </style>
	
	<!-- LIBRERIA EDCORE -->
	<link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxLayout/codebase/dhtmlxlayout.css">
  <link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxLayout/codebase/skins/dhtmlxlayout_dhx_blue.css">
  <link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxWindows/codebase/dhtmlxwindows.css">
  <link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxWindows/codebase/skins/dhtmlxwindows_dhx_blue.css">
	<link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.css" >
	<link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxGrid/codebase/skins/dhtmlxgrid_dhx_skyblue.css" >
  <script src="../dhtmlx/libCompiler/dhtmlxcommon.js"></script>
	<script type="text/javaScript"  src="../dhtmlx/dhtmlxLayout/codebase/dhtmlxcontainer.js"></script>
  <script type="text/javaScript"  src="../dhtmlx/dhtmlxLayout/codebase/dhtmlxlayout.js"></script>
  <script type="text/javaScript"  src="../dhtmlx/dhtmlxWindows/codebase/dhtmlxwindows.js"></script>
	<script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.js"></script>
	<script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_srnd.js"></script>
	<script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_filter.js"></script>
	<script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgridcell.js"></script>
	<script type="text/javaScript"  src="Groups_admin.js"></script>
	<script src="../js/dhxGrid.js"></script>
	<script src="../js/dhxLoad.js"></script>
	<script src="../dhtmlx/dhtmlxDataProcessor/codebase/dhtmlxdataprocessor.js"></script>
	<script src="../dhtmlx/libCompiler/connector.js"></script>
	<script src="../js/spin.js"></script>
	<script>
		  var dhxWins;
      var win;
			function print(id){
				dhxWins= new dhtmlXWindows();
				win = dhxWins.createWindow("printCard", 0,0,310, 440);
				dhxWins.window("printCard").centerOnScreen();
				dhxWins.window("printCard").setText("Imprimir credencial");
				dhxWins.window("printCard").denyResize();
				dhxWins.setSkin("dhx_blue");
				dhxWins.window("printCard").bringToTop();
				dhxWins.window("printCard").stick();
				dhxWins.window("printCard").setModal(true);
				win.attachURL("getAdministrativeCard?id="+id, "AJAX");
			}
	</script>
</head>
<body>
	<!-- HEADER -->
	<%@include file="header.jsp" %>
	<!--/HEADER -->
	<!-- PAGE -->
	<section id="page">
		<!-- SIDEBAR -->
		<%@include file="sidebar.jsp" %>
		<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="index.jsp">Inicio</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Catalogos</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Usuarios</a>
										</li>
										<li>Administrativos</li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										<h3 class="content-title pull-left">Administración de administrativos</h3>
										<span class="date-range pull-right">
										<p class="btn-toolbar">
											<%if(privileges.contains("CAT39")){%>
											<button id="nuevo" class="btn btn-success" ><i class="fa fa-file-text-o"></i> Nuevo</button>
											<%}%>
											<%if(privileges.contains("CAT40")){%>
											<button id="editar" class="btn btn-success" disabled=""><i class="fa fa-pencil-square-o"></i> Editar</button>
											<%}%>
                                                                                        <button id="print" class="btn btn-success" disabled=""><i class="fa fa-print-square-o"></i>Credencial</button>
											<button id="actualizar" class="btn btn-success" ><i class="fa fa-rotate-right"></i> Actualizar</button>
										</p>
										</span>
									</div>
									<div class="description"></div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						<!-- DASHBOARD CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<!-- BOX -->
								<div id="grid_container" class="box border" style="height:600px; width: 100%">
								</div>
							</div>
						</div>
					   <!-- /DASHBOARD CONTENT -->
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
    <!--/PAGE -->
    <!-- JAVASCRIPTS -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- JQUERY -->
    <script src="../js/jquery/jquery-2.0.3.min.js"></script>
    <!-- JQUERY UI-->
    <script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
    <!-- BOOTSTRAP -->
    <script src="../bootstrap-dist/js/bootstrap.min.js"></script>
    <!-- TYPEHEAD -->
    <script type="text/javascript" src="../js/typeahead/typeahead.min.js"></script>
    <!-- AUTOSIZE -->
    <script type="text/javascript" src="../js/autosize/jquery.autosize.min.js"></script>
    <!-- INPUT MASK -->
    <script type="text/javascript" src="../js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
    <!-- SELECT2 -->
    <script type="text/javascript" src="../js/select2/select2.min.js"></script>
    <!-- JQUERY UPLOAD -->
    <!-- The basic File Upload plugin -->
    <script src="../js/jquery-upload/js/jquery.fileupload.min.js"></script>
    <!-- bootbox script -->
    <script src="../js/bootbox/bootbox.min.js"></script>
    <!-- COOKIE -->
    <script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
    <!-- CUSTOM SCRIPT -->
    <script src="../js/script.js"></script>
    <script src="../js/jquery-validate/jquery.validate.js"></script>
    <script src="../js/jquery-validate/additional-methods.js"></script>
    <script src="../js/error_message.js"></script>
	<script>
		jQuery(document).ready(function() {
			App.setPage("index");  //Set current page
			App.init(); //Initialise plugins and elements
			$("#photo").attr("src","getPhoto?safe_IdUser="+<% if(user != null){out.print(user.getSafe_IdUser());} %>);
			var response = "../getAdminView?table=VIW024&id=user_IdAdministrative&parameters=code,name,firstLast,secondLast,department,workstation,status";
			var IDRow;
			var name;
			var values;
			
			grid = new dhtmlXGridObject('grid_container');
			grid.setImagePath("../dhtmlx/dhtmlxGrid/codebase/imgs/");
      grid.setHeader("Clave,Nombre,Paterno,Materno,Departamento,Puesto,Estado");
      grid.attachHeader("#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter");
      grid.setInitWidths("80,100,100,100,*,*,*");
      grid.setColAlign("left,left,left,left,left");
      grid.setColSorting("str,str,str,str,str");
			grid.setEditable(false);
			grid.init();
			grid.setSkin("clear");
			grid.enableSmartRendering(true,50);
			grid.load(response,function(){
				columnAutoSize(); 
			});
			grid.attachEvent("onRowSelect",function(id){
				//split para editar
				IDRow = id;
				$("#editar").removeAttr("disabled");
                                $("#print").removeAttr("disabled");
		});

			grid.attachEvent("onRowDblClicked",function(id){
				IDRow = id;
				window.location = "UserAdministrative_catalog.jsp?id="+IDRow+"&addOrEdit=1";
		});

			$("#nuevo").click(function () {
				window.location = "UserAdministrative_catalog.jsp?id=0&addOrEdit=0";
			});
			$("#editar").click(function () {
				window.location = "UserAdministrative_catalog.jsp?id="+IDRow+"&addOrEdit=1";
			});
			$("#print").click(function () {
				print(IDRow);
			});
			$("#actualizar").click(function () {
				grid.clearAndLoad(response);
			});		
		
		});
	</script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
			}else{
				response.sendRedirect("error600.jsp");
			}
		}else{
			response.sendRedirect("error600.jsp");
		}
%>