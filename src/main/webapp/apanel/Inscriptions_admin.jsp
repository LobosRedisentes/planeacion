<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.edcore.model.db.DBConnector"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.edcore.model.safe.User"%>
	<%
		HttpSession sesion;
    sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if(user != null){
			ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
			if(privileges.contains("INS017")){
	%>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Inscripciones</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="Sistema educativo">
	<meta name="author" content="Instituto Tecnologico Superior de Zapopan">	
	<link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
	<link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
	<!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
	<!-- LIBRERIA EDCORE -->
	<link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.css" >
	<link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxGrid/codebase/skins/dhtmlxgrid_dhx_skyblue.css" >
  <link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxPopup/codebase/skins/dhtmlxpopup_dhx_skyblue.css" >
	<link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxLayout/codebase/dhtmlxlayout.css">
  <link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxLayout/codebase/skins/dhtmlxlayout_dhx_blue.css">
  <link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxWindows/codebase/dhtmlxwindows.css">
  <link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxWindows/codebase/skins/dhtmlxwindows_dhx_blue.css">
	<link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.css" >
	<script type="text/javaScript"  src="../dhtmlx/dhtmlxLayout/codebase/dhtmlxcontainer.js"></script>
  <script type="text/javaScript"  src="../dhtmlx/dhtmlxLayout/codebase/dhtmlxlayout.js"></script>
  <script type="text/javaScript"  src="../dhtmlx/dhtmlxWindows/codebase/dhtmlxwindows.js"></script>
	<script src="../dhtmlx/libCompiler/dhtmlxcommon.js"></script>
  <script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.js"></script>
  <script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgridcell.js"></script>
  <script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_start.js"></script>
  <script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_srnd.js"></script>
  <script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_filter.js"></script>
  <script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgridcell.js"></script>
  <script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_selection.js"></script>
  <script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_nxml.js"></script>
	<script src="../dhtmlx/dhtmlxPopup/codebase/dhtmlxpopup.js"></script>
	<script src="../js/dhxGrid.js"></script>
	<script src="../js/dhxLoad.js"></script>
	<script src="../dhtmlx/dhtmlxDataProcessor/codebase/dhtmlxdataprocessor.js"></script>
	<script src="../dhtmlx/libCompiler/connector.js"></script>
	<script>
		  var dhxWins;
      var win;
			function save(id){
				dhxWins= new dhtmlXWindows();
				win = dhxWins.createWindow("paquetes", 0,0, 555, 320);
				dhxWins.window("paquetes").centerOnScreen();
				dhxWins.window("paquetes").setText("Paqutes de nuevo ingreso");
				dhxWins.window("paquetes").denyResize();
				dhxWins.setSkin("dhx_blue");
				dhxWins.window("paquetes").bringToTop();
				dhxWins.window("paquetes").stick();
				dhxWins.window("paquetes").setModal(true);
				win.attachURL("Inscriptions_auxiliary.jsp?user_IdApplicant="+id, "AJAX");
			}
	</script>
</head>
<body>
	<script src="../js/spin.js"></script>
	<!-- HEADER -->
	<%@include file="header.jsp" %>
	<!--/HEADER -->
	<!-- PAGE -->
	<section id="page">
		<!-- SIDEBAR -->
		<%@include file="sidebar.jsp" %>
		<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="index.jsp">Inicio</a>
										</li>
										<li>Inscripciones</li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										<h3 class="content-title pull-left">Asignaci�n de la carga acad�mica</h3>
										<span class="date-range pull-right">
										<p class="btn-toolbar">
											<%if(privileges.contains("INS041")){%>
											<button id="guardar" class="btn btn-danger"  disabled=""><i class="fa fa-save"></i> Inscribir</button>
											<%}%>
										</p>
										</span>
									</div>
									<div class="description"></div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						<!-- DASHBOARD CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="box border red">
									<div class="box-title">
                                                                            <h4>Inscripci�n</h4>
                                                                            <div class="tools hidden-xs">
                                                                            </div>
									</div>
									<div class="box-body" style="padding-bottom: 10px;">
										<div id="grid_container" class="box border" style="height:600px; width: 100%"></div>
									</div>
								</div>
								</div>
						</div>
					   <!-- /DASHBOARD CONTENT -->
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	<script src="../js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="../bootstrap-dist/js/bootstrap.min.js"></script>
	<!-- bootbox script -->
	<script src="../js/bootbox/bootbox.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="../js/script.js"></script>
	<script>
		jQuery(document).ready(function() {
			App.setPage("index");  //Set current page
			App.init(); //Initialise plugins and elements
			$("#photo").attr("src","getPhoto?safe_IdUser="+<% if(user != null){out.print(user.getSafe_IdUser());} %>);


			var response = "../getAdminView?table=InsListAccepted_view&id=user_IdApplicant&parameters=clave,aspirante,programa,plan,status";
			var IDRow;
			var name;
			var values;
			
			grid = new dhtmlXGridObject('grid_container');
			grid.setImagePath("../dhtmlx/dhtmlxGrid/codebase/imgs/");
      grid.setHeader("Clave, Aspirante, Programa, Plan, status");
      grid.attachHeader("#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter");
      grid.setInitWidths("*,*,*,*,*");
      grid.setColAlign("left,left,left,left,left");
      grid.setColSorting("str,str,str,str,str");
			grid.setEditable(false);
			grid.init();
			grid.setSkin("clear");
			grid.enableSmartRendering(true,50);
			grid.objBox.style.overflowX = "hidden";
			
			grid.attachEvent("onRowSelect",function(id){
				IDRow = id;
				$("#guardar").removeAttr("disabled");
			});

			grid.attachEvent("onRowDblClicked",function(id){
				IDRow = id;
				guardar(IDRow);
		});


			$("#guardar").click(function () {
				save(IDRow);
			});
			$("#actualizar").click(function () {
				grid.clearAndLoad(response);
			});		

		});
	</script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
			}else{
				response.sendRedirect("error600.jsp");
			}
		}else{
			response.sendRedirect("error600.jsp");
		}
%>