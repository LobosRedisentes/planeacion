<%@page import="com.edcore.model.safe.User"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.edcore.model.db.DBConnector"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.edcore.model.safe.User"%>
	<%
		HttpSession sesion;
    sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if(user != null){
			ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
			if(privileges.contains("SAFE07")){
	%>
<%
	ResultSet rs;
	DBConnector db = new DBConnector();
	PreparedStatement ps;
	Connection con;
	
int codigo = 0;
String nomRol = "";
String descripcion = "";

int IdSafeR = Integer.parseInt(request.getParameter("idRole"));
int addOrEdit=Integer.parseInt(request.getParameter("addOrEdit"));

	con=db.open();
      if(IdSafeR > 0){
          
        ps = con.prepareStatement("SELECT SafeRoles.*, SafeComponents.code, "
        + "SafeComponents.name, SafeComponents.version, SafeComponents.type "
        + "FROM  SafeComponents INNER JOIN SafeRoles "
        + "ON SafeComponents.safe_IdComponent = SafeRoles.safe_IdComponent "
        + "where SafeRoles.safe_IdRol=?");
        ps.setInt(1, IdSafeR);
    	rs = ps.executeQuery();
    
	    while(rs != null && rs.next()){
	        codigo = rs.getInt("SafeRoles.safe_IdComponent");
                descripcion = rs.getString("SafeRoles.description");
                nomRol = rs.getString("SafeRoles.name");
	     }
    }
%>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Roles</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
	<link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
	
	<link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- FILE UPLOAD -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-fileupload/bootstrap-fileupload.min.css" />
	<!-- SELECT2 -->
	<link rel="stylesheet" type="text/css" href="../js/select2/select2.min.css" />
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	<style>
	 .error-message, label.error {
	    color: #ff0000;
	    margin:0;
	    display: inline;
	    font-size: 1em !important;
	    font-weight:bold;
	 }
	 </style>
	<!-- LIBRERIA EDCORE -->
	<link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.css" >
  <script src="../dhtmlx/libCompiler/dhtmlxcommon.js"></script>
	<script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.js"></script>
	<script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_srnd.js"></script>
	<script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_filter.js"></script>
	<script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgridcell.js"></script>
	<script src="../js/dhxGrid.js"></script>
	<script src="../js/dhxLoad.js"></script>
	<script src="../dhtmlx/dhtmlxDataProcessor/codebase/dhtmlxdataprocessor.js"></script>
	<script src="../dhtmlx/libCompiler/connector.js"></script>
	<script src="../js/spin.js"></script>
</head>
<body>
	<!-- HEADER -->
	<%@include file="header.jsp" %>
	<!--/HEADER -->
	<!-- PAGE -->
	<section id="page">
				<!-- SIDEBAR -->
				<%@include file="sidebar.jsp" %>
				<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<!-- STYLER -->
									
									<!-- /STYLER -->
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="index.jsp">Inicio</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Seguridad</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="SafeRoles_admin.jsp">Roles</a>
										</li>
										<li>Nuevo/Editar</li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										<h3 class="content-title pull-left">Cat�logo - Roles</h3>
										<span class="date-range pull-right">
										<p class="btn-toolbar">
											<%if(privileges.contains("SAFE15")){%>
											<button id="nuevo" class="btn btn-success" ><i class="fa fa-file-text-o"></i> Nuevo</button>
											<%}%>
											<%if(privileges.contains("SAFE16")){%>
											<button id="editar" class="btn btn-success" disabled=""><i class="fa fa-pencil-square-o"></i> Editar</button>
											<%}%>
											<%if(privileges.contains("SAFE17")){%>
											<button id="guardar" class="btn btn-success" ><i class="fa fa-save"></i> Guardar</button>
											<%}%>
										</p>
										</span>
									</div>
									<div class="description"></div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						<!-- ADVANCED -->
						<div class="row">
							<div class="col-md-12">
								<!-- BOX -->
								<div class="box border red">
									<div class="box-title">
										<h4><i class="fa fa-bars"></i>Nuevo/Editar Rol</h4>
									</div>
									<div class="box-body">
										<form class="form-horizontal " action="#" id="frm_roles">
										  <div class="form-group">
											 <label class="col-md-2 control-label" for="e1">Modulo:</label> 
											 <div class="col-md-10">
												 <select id="e1" class="col-md-12" name="offi">
                     <%
                        ps = con.prepareStatement("SELECT safe_IdComponent, name FROM edcore.SafeComponents WHERE type='SUBSISTEMA';");
                        rs = ps.executeQuery();
                        out.println("<option id='0' value=''>Selecciona ..</option>");
                        while(rs.next()){
                           if(Integer.parseInt(rs.getString("safe_IdComponent"))== codigo){
                              out.println("<option id='"+rs.getString("safe_IdComponent")+
                              "' selected>"+rs.getString("name")+"</option>");
                           }else{
                              out.println("<option id='"+rs.getString("safe_IdComponent")+"'>"+
                              rs.getString("name")+"</option>");
                           }
                       }
                    %>
											</select>
											 </div>
										  </div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="clave">Nombre:</label> 
											<div class="col-md-10">
                   <% 
                      if(nomRol !=null  && !nomRol.equals("")){
                         out.println("<input type='text' class='form-control' autocomplete='off' name='clave' id='clave' value='"+nomRol+"'>");
                      }else{
                         out.println("<input type='text' class='form-control' autocomplete='off' name='clave'  id='clave' >");    
                      }
                   %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="desc">Descripci�n:</label> 
											<div class="col-md-10">
                <% 
                   if(descripcion !=null){
                      out.println("<textarea id='desc' name='desc' class='form-control' rows='5' cols='80'>"+descripcion+"</textarea>");
                   }else{
                      out.println("<textarea id='desc' name='desc' class='form-control' rows='5' cols='80'></textarea>");
                   }
                %>
											</div>
											</div>
											<div class="form-group">
											<div class="col-md-10" id="grid_container" style="width:100%;height:300px;">
											</div>
											</div>
											<input type="hidden" id="hide" value="<%if(IdSafeR>0){out.println(IdSafeR);}else{out.println("0");}%>">
									   </form>
									</div>
								</div>
								<!-- /BOX -->
							</div>
						</div>
						<!-- /ADVANCED -->
						<div class="footer-tools">
							<span class="go-top">
								<i class="fa fa-chevron-up"></i> Top
							</span>
						</div>
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	<script src="../js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="../bootstrap-dist/js/bootstrap.min.js"></script>
	<!-- TYPEHEAD -->
	<script type="text/javascript" src="../js/typeahead/typeahead.min.js"></script>
	<!-- AUTOSIZE -->
	<script type="text/javascript" src="../js/autosize/jquery.autosize.min.js"></script>
	<!-- COUNTABLE -->
	<script type="text/javascript" src="../js/countable/jquery.simplyCountable.min.js"></script>
	<!-- INPUT MASK -->
	<script type="text/javascript" src="../js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
	<!-- SELECT2 -->
	<script type="text/javascript" src="../js/select2/select2.min.js"></script>
	<!-- UNIFORM -->
	<script type="text/javascript" src="../js/uniform/jquery.uniform.min.js"></script>
	<script src="../js/jquery-upload/js/jquery.fileupload.min.js"></script>
	<!-- bootbox script -->
	<script src="../js/bootbox/bootbox.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="../js/script.js"></script>
	<script src="../js/jquery-validate/jquery.validate.js"></script>
	<script src="../js/jquery-validate/additional-methods.js"></script>
	<script src="../js/error_message.js"></script>
	<script>
		jQuery(document).ready(function() {		
			App.setPage("forms");  //Set current page
			App.init(); //Initialise plugins and elements
			$("#photo").attr("src","getPhoto?safe_IdUser="+<% if(user != null){out.print(user.getSafe_IdUser());} %>);
			var idRole=<%out.println(IdSafeR);%>;
			var addOrEdit=<%out.println(addOrEdit);%>;
			var code=<%out.println(codigo);%>;
			
			var grid = new dhtmlXGridObject('grid_container');
			grid.setImagePath("../dhtmlx/dhtmlxGrid/codebase/imgs/");
			grid.setHeader(" ,CODIGO,NOMBRE,VERSION,TIPO");
      grid.attachHeader(",#text_filter,#text_filter,#select_filter,#select_filter");
      grid.setInitWidths("50,*,*,100,*");
      grid.setColAlign("left,left,left,left,left");
      grid.setColSorting("str,str,str,str,str");
      grid.setColTypes("ch,txt,txt,txt,txt");
      //grid.setEditable(true);
      grid.init();
      grid.setSkin("clear");
      spInit();
      grid.load("SafeRoles_grid?idR="+$("#hide").val()+"&bComp="+code+"&addOrEdit="+addOrEdit,function(){
				spStop();
      });
			grid.objBox.style.overflowX = "hidden";
      grid.attachEvent("onXLS",function(){
				spInit();
      });
      grid.attachEvent("onXLE",function(){
				spStop();
      });
      $("#e1").change(function(){
        var id=$(this).children(":selected").attr("id");
        if(id>0){
          grid.clearAndLoad("SafeRoles_grid?idR="+$("#hide").val()+"&bComp="+id+"&addOrEdit=1");
        }else{
          grid.clearAll();
        }
      });
			
			$("#frm_roles").validate({
				rules:{
					offi:{
						required: true
					},
					clave:{
						required: true, minlength: 5, maxlength: 50
					}
				},
				highlight: function(element){
					$(element).parent().parent().addClass("has-error");
				},
				unhighlight: function(element){
				$(element).parent().parent().removeClass("has-error");
			}});
		
			var sist=$("#e1");
			var n_rol=$("#clave");
			var descr=$("#desc");
			
			$("#nuevo").click(function () {
				window.location = "SafeRoles_catalog.jsp?idRole=0&addOrEdit=0";
			});
			$("#editar").click(function () {
				sist.removeAttr("disabled");
				n_rol.removeAttr("disabled");
				descr.removeAttr("disabled");                       
				grid.setEditable(true);
				addOrEdit=1;
				$("#editar").attr("disabled","disabled");
			});
			$("#guardar").click(function () {
				if($("#frm_roles").valid()){

					var sistVal =sist.children(":selected").attr("id");
					var n_rolVal =n_rol.val().trim();
					var descrVal =descr.val().trim();
					var itemsVal ="";
					var values= "values="+parseInt($("#hide").val())+"#"+sistVal+"#"+n_rolVal+"#"+descrVal;

					grid.forEachRow(function(id){
             if(parseInt(grid.cells(id,0).getValue())==1){
               itemsVal+="#"+id;
             }
          });
					values+=itemsVal;
					values+="&addOrEdit="+addOrEdit;

					dhtmlxAjax.post("SafeRoles_save",values,function(loader){
						var resp = loader.xmlDoc.responseText;
						respo = resp.split("#");
						document.getElementById("hide").value=respo[0];
						var resp = parseInt(respo[1]);
						if(resp == 1){
							bootbox.alert("Se guardo con exito!", function() {});
						} else {
							bootbox.alert("Fallo al guardar!", function() {});
						}
					});

          sist.attr("disabled","disabled");
          n_rol.attr("disabled","disabled");
          descr.attr("disabled","disabled");
					grid.setEditable(false);
					$("#editar").removeAttr("disabled");
				}
			});
		});
	</script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
			}else{
				response.sendRedirect("error600.jsp");
			}
		}else{
			response.sendRedirect("error600.jsp");
		}
%>