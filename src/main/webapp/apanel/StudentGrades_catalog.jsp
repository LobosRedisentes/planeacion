<%@page import="java.util.TreeMap"%>
<%@page import="com.edcore.web.grades.StudentGrades"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.edcore.model.safe.User"%>
	<%
		HttpSession sesion;
    sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if(user != null){
			ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
			if(privileges.contains("EVAL11")){				
	%>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Calificaciones</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="Sistema educativo">
	<meta name="author" content="Instituto Tecnologico Superior de Zapopan">	
	<link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
	<link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
	<!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- ANIMATE -->
	<link rel="stylesheet" type="text/css" href="../css/animatecss/animate.min.css" />
	<!-- GRITTER -->
	<link rel="stylesheet" type="text/css" href="../js/gritter/css/jquery.gritter.css" />
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	<!-- TABLE CLOTH -->
	<link rel="stylesheet" type="text/css" href="../js/tablecloth/css/tablecloth.min.css" />
</head>
<body>
	  <%
		StudentGrades grades = new StudentGrades(Integer.parseInt(request.getParameter("safe_IdUser")));
    grades.imprimir();
    %>

	<!-- HEADER -->
	<%@include file="header.jsp" %>
	<!--/HEADER -->
	<!-- PAGE -->
	<section id="page">
		<!-- SIDEBAR -->
		<%@include file="sidebar.jsp" %>
		<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="index.jsp">Inicio</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="index.jsp">Calificaciones</a>
										</li>
										<li>Semestre actual</li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										<h3 class="content-title pull-left">Semestre actual</h3>
									</div>
									<div class="description"></div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						<!-- DASHBOARD CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<!-- BOX -->
								<div class="col-md-12">
								<div class="box border red">
									<div class="box-title">
										<h4><i class="fa fa-dot-circle-o"></i>Calificaciones</h4>
									</div>
									<div class="box-body">
										<table class="table table-bordered">
											<thead>
											  <tr>
												<th>Grupo</th>
												<th>Materia</th>
												<th>Parcial</th>
												<th>Calificación parcial</th>
                                                                                                <th>Acreditación final</th>
                                                                                                <th>Calificación final</th>
											  </tr>
											</thead>
											<tbody>
                                                                                            <%
                                                                                                for(int x=0; x<grades.datos.size(); x++){
                                                                                                    TreeMap info = (TreeMap)grades.datos.get(grades.datos.keySet().toArray()[x]);
                                                                                                    String p1[] = (String[])info.get("1");
                                                                                                    String p2[] = (String[])info.get("2");
                                                                                                    String p3[] = (String[])info.get("3");
                                                                                                    String f ="0";
                                                                                                    if(((Integer.parseInt(p1[0])+Integer.parseInt(p2[0])+Integer.parseInt(p3[0]))/3)>=70){
                                                                                                        f = Integer.toString((Integer.parseInt(p1[0])+Integer.parseInt(p2[0])+Integer.parseInt(p3[0]))/3);
                                                                                                    }else{
                                                                                                        f="N/A";
                                                                                                    }
                                                                                                    out.print("<tr><td rowspan='3' style='vertical-align:middle;'>"+grades.datos.keySet().toArray()[x]+"</td><td rowspan='3' style='vertical-align:middle;'>"+info.get("nombre")+"</td>");
                                                                                                    out.print("<td>1 </td><td>"+p1[0]+"<sup>"+p1[1]+"</sup></td><td >"+p1[2]+"</td><td rowspan='3' style='vertical-align:middle;'>"+f+"</td></tr>");
                                                                                                    out.print("<tr><td>2 </td><td>"+p2[0]+"<sup>"+p2[1]+"</sup></td><td>"+p2[2]+"</td></tr>");
                                                                                                    out.print("<tr><td>3 </td><td>"+p3[0]+"<sup>"+p3[1]+"</sup></td><td>"+p3[2]+"</td></tr>");

                                                                                                }
                                                                                                
                                                                                            %>
											  
                                                                                          
											  
											</tbody>
										  </table>
									</div>
								</div>
							</div>
								<!-- /BOX -->
							</div>
						</div>
					   <!-- /DASHBOARD CONTENT -->
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	<script src="../js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="../bootstrap-dist/js/bootstrap.min.js"></script>
	<!-- DATE RANGE PICKER -->
	<script src="../js/bootstrap-daterangepicker/moment.min.js"></script>	
	<script src="../js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
	<!-- SLIMSCROLL -->
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
	<!-- SLIMSCROLL -->
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script><script type="text/javascript" src="js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
	<!-- BLOCK UI -->
	<script type="text/javascript" src="../js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
	<!-- SPARKLINES -->
	<script type="text/javascript" src="../js/sparklines/jquery.sparkline.min.js"></script>
	<!-- EASY PIE CHART -->
	<script src="../js/jquery-easing/jquery.easing.min.js"></script>
	<script type="text/javascript" src="../js/easypiechart/jquery.easypiechart.min.js"></script>
	<!-- FLOT CHARTS -->
	<script src="../js/flot/jquery.flot.min.js"></script>
	<script src="../js/flot/jquery.flot.time.min.js"></script>
	<script src="../js/flot/jquery.flot.selection.min.js"></script>
	<script src="../js/flot/jquery.flot.resize.min.js"></script>
	<script src="../js/flot/jquery.flot.pie.min.js"></script>
	<script src="../js/flot/jquery.flot.stack.min.js"></script>
	<script src="../js/flot/jquery.flot.crosshair.min.js"></script>
	<!-- TODO -->
	<script type="text/javascript" src="../js/jquery-todo/js/paddystodolist.js"></script>
	<!-- TIMEAGO -->
	<script type="text/javascript" src="../js/timeago/jquery.timeago.min.js"></script>
	<!-- FULL CALENDAR -->
	<script type="text/javascript" src="../js/fullcalendar/fullcalendar.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- KNOB -->
	<script type="text/javascript" src="../js/jQuery-Knob/js/jquery.knob.min.js"></script>
	<!-- GRITTER -->
	<script type="text/javascript" src="../js/gritter/js/jquery.gritter.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="../js/script.js"></script>
	<script>
		jQuery(document).ready(function() {
			App.setPage("sliders_progress");  //Set current page
			App.init(); //Initialise plugins and elements
			$("#photo").attr("src","getPhoto?safe_IdUser="+<% if(user != null){out.print(user.getSafe_IdUser());} %>);
		});
	</script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
			}else{
				response.sendRedirect("error600.jsp");
			}
		}else{
			response.sendRedirect("error600.jsp");
		}
%>