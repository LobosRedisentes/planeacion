/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery(document).ready(function () {
    App.setPage("forms");  //Set current page
    App.init(); //Initialise plugins and elements
    $("#photo").attr("src", "/edcore/admin/getPhoto?safe_IdUser=" + safe_IdUser);
    var response = "../getAdminView?sql=select * FROM VIW047 &id=doc_IdExamProfessional&parameters=book,sheet,code,studentName,studentFirstLast,studentSecondLast,option,expYear,status";
    var IDRow;
    grid = new dhtmlXGridObject('grid_container');
    grid.setImagePath("../dhtmlx/dhtmlxGrid/codebase/imgs/");
    grid.setHeader("Libro,Foja,Matricula,Nombre,Paterno,Materno,Opcion,A&ntilde;o,status");
    grid.attachHeader("#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter");
    grid.setInitWidths("*,*,*,*,*,*,*,*,*");
    grid.setColAlign("left,left,left,left,left,left,left,left,left");
    grid.setColSorting("str,str,str,str,str,str,str,str,str");
    grid.setEditable(false);
    grid.init();
    grid.setSkin("clear");
    grid.enableSmartRendering(true, 50);
    grid.objBox.style.overflowX = "hidden";
    //grid.load(response, function () {});


    $("#frm_nuevo").validate({
        rules: {
            book: {required: true, maxlength: 5, minlength: 1},
            sheet: {required: true, maxlength: 5, minlength: 1},
            user_IdStudent: {required: true},
            meetingDay: {required: true, digits: true},
            meetMonth: {required: true},
            meetingYear: {required: true, digits: true},
            meetingHour: {required: true,digits: true},
            user_IdProfessor1: {required: true},
            user_IdProfessor2: {required: true},
            user_IdProfessor3: {required: true},
            org_IdRoom: {required: true},
            option: {required: true},
            subject: {required: true, maxlength: 55, minlength: 1},
            status: {required: true},
            expDay: {required: true, digits: true},
            expMonth: {required: true},
            expYear: {required: true, digits: true},
            expHour: {required: true, digits: true}
        },
        highlight: function (element) {
            $(element).parent().parent().addClass("has-error");
        },
        unhighlight: function (element) {
            $(element).parent().parent().removeClass("has-error");
        }
    });

    //$('#month').select2();
    grid.attachEvent("onRowSelect", function (id) {
        IDRow = id;
        $("#btn_print").removeAttr("disabled");
    });

    $("#btn_nuevo").click(function () {
        $('#box_nuevo').modal({show: true});

        $('#user_IdStudent').select2({
            multiple: false,
            maximumSelectionSize: 1,
            tags: [],
            ajax: {
                url: "getStudentsOptions?status=EG",
                dataType: 'json',
                type: "POST",
                quietMillis: 150,
                data: function (term, page) {
                    return {
                        term: term
                    };
                },
                results: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        })
                    };
                },
                cache: true,
                placeholder: "Selecciona ..."
            }
        });
        $('#user_IdProfessor1').select2({
            multiple: false,
            maximumSelectionSize: 1,
            tags: [],
            ajax: {
                url: "../getProfessorsOptions?status=VI",
                dataType: 'json',
                type: "POST",
                quietMillis: 150,
                data: function (term, page) {
                    return {
                        term: term
                    };
                },
                results: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        })
                    };
                },
                cache: true,
                placeholder: "Selecciona ..."
            }
        });
        $('#user_IdProfessor2').select2({
            multiple: false,
            maximumSelectionSize: 1,
            tags: [],
            ajax: {
                url: "../getProfessorsOptions?status=VI",
                dataType: 'json',
                type: "POST",
                quietMillis: 150,
                data: function (term, page) {
                    return {
                        term: term
                    };
                },
                results: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        })
                    };
                },
                cache: true,
                placeholder: "Selecciona ..."
            }
        });
        $('#user_IdProfessor3').select2({
            multiple: false,
            maximumSelectionSize: 1,
            tags: [],
            ajax: {
                url: "../getProfessorsOptions?status=VI",
                dataType: 'json',
                type: "POST",
                quietMillis: 150,
                data: function (term, page) {
                    return {
                        term: term
                    };
                },
                results: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        })
                    };
                },
                cache: true,
                placeholder: "Selecciona ..."
            }
        });
        $('#org_IdRoom').select2({
            multiple: false,
            maximumSelectionSize: 1,
            tags: [],
            ajax: {
                url: "../getRoomOptions",
                dataType: 'json',
                type: "POST",
                quietMillis: 150,
                data: function (term, page) {
                    return {
                        term: term
                    };
                },
                results: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        })
                    };
                },
                cache: true,
                placeholder: "Selecciona ..."
            }
        });
    });

    $("#btn_print").click(function () {
        $('iframe').attr("src", "getPdfExamProfesional?doc_IdExamProfessional=" + IDRow);
        $('#print_box').modal({show: true});
    });
    $("#btn_guardar").click(function () {
        if ($("#frm_nuevo").valid()) {
            var book = $('#book').val().trim();
            var sheet = $('#sheet').val().trim();
            var user_IdStudent = $('#user_IdStudent').val();
            var meetingDay = $('#meetingDay').val().trim();
            var meetingMonth = $('#meetingMonth').children(":selected").attr("value");
            var meetingYear = $('#meetingYear').val().trim();
            var meetingHour = $('#meetingHour').val();
            var user_IdProfessor1 = $('#user_IdProfessor1').val();
            var user_IdProfessor2 = $('#user_IdProfessor2').val();
            var user_IdProfessor3 = $('#user_IdProfessor3').val();
            var org_IdRoom = $('#org_IdRoom').val();
            var option = $('#option').val();
            var subject = $('#subject').val();
            var status = $('#status').val();
            var expDay =  $('#expDay').val();
            var expMonth =  $('#expMonth').children(":selected").attr("value");
            var expYear =  $('#expYear').val();
            var expHour =  $('#expHour').val();
                             
            var save = $.ajax({url: "examenProfesional_save", data: "book=" + book + "&sheet=" + sheet + "&user_IdStudent=" + user_IdStudent + "&meetingDay=" + meetingDay + "&meetingMonth=" + meetingMonth+ "&meetingYear=" + meetingYear +
                         "&meetingHour=" + meetingHour+ "&user_IdProfessor1=" + user_IdProfessor1+ "&user_IdProfessor2=" + user_IdProfessor2+ "&user_IdProfessor3=" + user_IdProfessor3+
                         "&option=" + option+ "&status=" + status+ "&expDay=" + expDay+ "&expMonth=" + expMonth+ "&expYear=" + expYear+ "&expHour=" + expHour+ "&org_IdRoom=" + org_IdRoom+ "&subject=" + subject, method: "POST"});
            save.done(function (msn) {
                if (msn === '' || msn === '0') {
                    bootbox.alert("No se pudo guardar! ", function () {
                    });
                } else {
                    grid.clearAndLoad(response);
                }
            });
            event.preventDefault();
        }
    });
});
