<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.edcore.model.db.DBConnector"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.edcore.model.safe.User"%>
	<%
		HttpSession sesion;
    sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if(user != null){
			ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
			if(privileges.contains("INS016")){
	%>
<%
            DBConnector db = new DBConnector();
            Connection con = db.open();
            PreparedStatement ps = null;
            ResultSet rs = null; 
						
            int idList = Integer.parseInt(request.getParameter("idList"));
            int safe_IdUser = 0;
            
            int ProgS = 0;
            String ProgS_name="";
            int ProgI = 0;
            int Plan = 0;
            int Group = 0;
            
            if(idList > 0){
                ps = con.prepareStatement("SELECT SafeUsers.safe_IdUser "
                        + "FROM SafeUsers,InsListAccepted,UserApplicant "
                        + "WHERE InsListAccepted.user_IdApplicant=UserApplicant.user_IdApplicant "
                        + "AND UserApplicant.safe_IdUser=SafeUsers.safe_IdUser "
                        + "AND InsListAccepted.ins_IdListAccepted='"+idList+"'");
                rs = ps.executeQuery();
                while(rs != null && rs.next()){
                    safe_IdUser =rs.getInt("SafeUsers.safe_IdUser");
                }
                
                ps = con.prepareStatement("SELECT A.prog_IdProgram, P.name "
                        + "FROM UserApplicant AS A, Programs AS P "
                        + "WHERE A.prog_IdProgram = P.prog_IdProgram "
                        + "AND A.safe_IdUser ="+safe_IdUser+"");
                rs = ps.executeQuery();
                while(rs != null && rs.next()){
                    ProgS =rs.getInt("prog_IdProgram");
                    ProgS_name = rs.getString("name");
                }
                
                ps = con.prepareStatement("SELECT ProgPlan.prog_IdProgram,ProgPlan.prog_IdPlan "
                        + "FROM ProgPlan,InsListAccepted "
                        + "WHERE InsListAccepted.prog_IdPlan=ProgPlan.prog_IdPlan "
                        + "AND InsListAccepted.ins_IdListAccepted='"+idList+"'");
                rs = ps.executeQuery();
                while(rs != null && rs.next()){
                    ProgI =rs.getInt("ProgPlan.prog_IdProgram");
                    Plan =rs.getInt("ProgPlan.prog_IdPlan");
                }
                
                ps = con.prepareStatement("SELECT group_IdGroupAssign "
                        + "FROM GroupAssignLists "
                        + "WHERE user_IdApplicant = ");
                rs = ps.executeQuery();
                while(rs != null && rs.next()){
                    Group =rs.getInt("group_IdGroupAssign");
                }

            }
%>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Aspirantes aceptados</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
	<link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
	
	<link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- DATE RANGE PICKER -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
	<!-- TYPEAHEAD -->
	<link rel="stylesheet" type="text/css" href="../js/typeahead/typeahead.css" />
	<!-- FILE UPLOAD -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-fileupload/bootstrap-fileupload.min.css" />
	<!-- SELECT2 -->
	<link rel="stylesheet" type="text/css" href="../js/select2/select2.min.css" />
	<!-- UNIFORM -->
	<link rel="stylesheet" type="text/css" href="../js/uniform/css/uniform.default.min.css" />
	<!-- JQUERY UPLOAD -->
	<!-- blueimp Gallery styles -->
	<link rel="stylesheet" href="../js/blueimp/gallery/blueimp-gallery.min.css">
	<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload.css">
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload-ui.css">
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	<style>
	 .error-message, label.error {
	    color: #ff0000;
	    margin:0;
	    display: inline;
	    font-size: 1em !important;
	    font-weight:bold;
	 }
	 </style>
	<!-- LIBRERIA EDCORE -->
  <script src="../dhtmlx/libCompiler/dhtmlxcommon.js"></script>
	<script src="../js/action_dhx.js"></script>	
</head>
<body>
	<!-- HEADER -->
	<%@include file="header.jsp" %>
	<!--/HEADER -->
	<!-- PAGE -->
	<section id="page">
				<!-- SIDEBAR -->
				<%@include file="sidebar.jsp" %>
				<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<!-- STYLER -->
									
									<!-- /STYLER -->
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="index.jsp">Inicio</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Inscripciones</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="InsListAccepted_admin.jsp">Aspirantes aceptados</a>
										</li>
										<li>Nuevo/Editar</li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										<h3 class="content-title pull-left">Aspirantes aceptados</h3>
										<span class="date-range pull-right">
										<p class="btn-toolbar">
											<%if(privileges.contains("INS036")){%>
											<button id="nuevo" class="btn btn-danger" ><i class="fa fa-file-text-o"></i> Nuevo</button>
											<%}%>
											<%if(privileges.contains("INS037")){%>
											<button id="editar" class="btn btn-danger" disabled=""><i class="fa fa-pencil-square-o"></i> Editar</button>
											<%}%>
											<%if(privileges.contains("INS039")){%>
											<button id="guardar" class="btn btn-danger" ><i class="fa fa-save"></i> Guardar</button>
											<%}%>
										</p>
										</span>
									</div>
									<div class="description"></div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						<!-- ADVANCED -->
						<div class="row">
							<div class="col-md-12">
								<!-- BOX -->
								<div class="box border red">
									<div class="box-title">
										<h4><i class="fa fa-bars"></i>Nuevo/Editar Aceptado</h4>
									</div>
									<div class="box-body">
										<form class="form-horizontal " action="#" id="frm_list">
											<div class="form-group">
											<label class="col-md-3 control-label" for="paquete">Aspirante:</label> 
											<div class="col-md-9">
                       <%
														if(safe_IdUser == 0){
															out.print("<select  id='e1' name='e1' class='col-md-12' > ");
														}else{
															out.print("<select  id='e1' name='e1' class='col-md-12' disabled> ");
														}
														out.print("<option value='0' selected>Seleccione ...</option>");
                            ps = con.prepareStatement("SELECT UserApplicant.user_IdApplicant,SafeUsers.name,SafeUsers.firstLast,SafeUsers.secondLast "
                                        + "FROM SafeUsers,InsApplications,UserApplicant,InsExamRooms,CalSchool "
                                        + "WHERE InsApplications.user_IdApplicant=UserApplicant.user_IdApplicant "
                                        + "AND InsApplications.ins_IdExamRoom = InsExamRooms.ins_IdExamRoom "
                                        + "AND InsExamRooms.cal_IdCalSchool=CalSchool.cal_IdCalSchool "
                                        + "AND UserApplicant.safe_IdUser=SafeUsers.safe_IdUser "
                                        + "AND CalSchool.status = 'ACTIVO' "
                                        + "AND UserApplicant.status = 'NO INSCRITO' ");
                            rs = ps.executeQuery();
                            while(rs.next()){
                                if(Integer.parseInt(rs.getString("user_IdApplicant"))== safe_IdUser){
                                    out.println("<option id='"+rs.getString("safe_IdUser")+"' selected>"+
                                        rs.getString("name")+" "+rs.getString("firstLast")+" "+rs.getString("secondLast")+"</option>");
                                }else{
                                    out.println("<option id='"+rs.getString("user_IdApplicant")+"'>"+
                                        rs.getString("name")+" "+rs.getString("firstLast")+" "+rs.getString("secondLast")+"</option>");
                                }
                            }
														out.print("</select>");
                       %>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-3 control-label" for="carrera">Programa seleccionado:</label> 
											<div class="col-md-9">
												<select id="ProgS" name="ProgS" class="form-control" disabled=""> 
												<%
													if(safe_IdUser == 0){
														out.print("<option value='0'>Seleccione ...</option>");
													}else{
														out.print("<option value='"+ProgS+"'>"+ProgS_name+"</option>");
													}
												%>
											</select>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-3 control-label" for="carrera">Programa Inscrito:</label> 
											<div class="col-md-9">
												<select id="ProgI" name="ProgI" class="form-control" > 
													<option value="0" selected>Seleccione ...</option>
                        <%
                            ps = con.prepareStatement("SELECT Programs.prog_IdProgram,Programs.name "
                                        + "FROM Programs");
                            rs = ps.executeQuery();
                            while(rs.next()){
                                if(Integer.parseInt(rs.getString("prog_IdProgram"))== ProgI){
                                
                                    out.println("<option id='"+rs.getString("prog_IdProgram")+"' selected>"+
                                        rs.getString("name")+"</option>");
                                }else{
                                    out.println("<option id='"+rs.getString("prog_IdProgram")+"'>"+
                                        rs.getString("name")+"</option>");
                                }
                            }
                       %>
											</select>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-3 control-label" for="carrera">Plan a inscribir:</label> 
											<div class="col-md-9">
												<select  id="Plan" name="Plan" class="form-control"> 
													<option value="0" selected>Seleccione ...</option>
                        <%
                            ps = con.prepareStatement("SELECT prog_IdPlan, officialCode "
                                        + "FROM ProgPlan where prog_IdProgram = ?");
                            ps.setInt(1, ProgI);
                            rs = ps.executeQuery();
                            while(rs.next()){
                                if(Integer.parseInt(rs.getString("prog_IdPlan"))== Plan){
                                
                                    out.println("<option value='"+rs.getString("prog_IdPlan")+"' selected>"+
                                        rs.getString("officialCode")+"</option>");
                                }else{
                                    out.println("<option value='"+rs.getString("prog_IdPlan")+"'>"+
                                        rs.getString("officialCode")+"</option>");
                                }
                            }
                       %>
												</select>
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-3 control-label" for="carrera">Paquete a entregar:</label> 
											<div class="col-md-9">
												<select  id="package" name="package" class="form-control"> 
													<option value="0" selected>Seleccione ...</option>
                        <%
                            ps = con.prepareStatement("SELECT * "
                                        + "FROM GroupIncoming_view where group_IdGroupAssign = ?");
                            ps.setInt(1, Group);
                            rs = ps.executeQuery();
                            if(rs!= null && rs.next()){
                                    out.println("<option value='"+rs.getString("group_IdGroupAssign")+"' selected>"+
                                    rs.getString("descripcion")+"</option>");
                            }
                       %>
												</select>
											</div>
											</div>
        <%
            if(idList != 0){
                out.print("<input type='hidden' id='idList' name='idList' value='"+idList+"' />");
            }else{
                out.print("<input type='hidden' id='idList' name='idList' value='0' />");
            }
        %>
									   </form>
									</div>
								</div>
								<!-- /BOX -->
							</div>
						</div>
						<!-- /ADVANCED -->
						<div class="footer-tools">
							<span class="go-top">
								<i class="fa fa-chevron-up"></i> Top
							</span>
						</div>
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	<script src="../js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="../bootstrap-dist/js/bootstrap.min.js"></script>
	<!-- DATE RANGE PICKER -->
	<script src="../js/bootstrap-daterangepicker/moment.min.js"></script>
	<script src="../js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
	<!-- SLIMSCROLL -->
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
	<!-- BLOCK UI -->
	<script type="text/javascript" src="../js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
	<!-- TYPEHEAD -->
	<script type="text/javascript" src="../js/typeahead/typeahead.min.js"></script>
	<!-- AUTOSIZE -->
	<script type="text/javascript" src="../js/autosize/jquery.autosize.min.js"></script>
	<!-- COUNTABLE -->
	<script type="text/javascript" src="../js/countable/jquery.simplyCountable.min.js"></script>
	<!-- INPUT MASK -->
	<script type="text/javascript" src="../js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
	<!-- FILE UPLOAD -->
	<script type="text/javascript" src="../js/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
	<!-- SELECT2 -->
	<script type="text/javascript" src="../js/select2/select2.min.js"></script>
	<!-- UNIFORM -->
	<script type="text/javascript" src="../js/uniform/jquery.uniform.min.js"></script>
	<!-- JQUERY UPLOAD -->
	<!-- The Templates plugin is included to render the upload/download listings -->
	<script src="../js/blueimp/javascript-template/tmpl.min.js"></script>
	<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="../js/blueimp/javascript-loadimg/load-image.min.js"></script>
	<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script src="../js/blueimp/javascript-canvas-to-blob/canvas-to-blob.min.js"></script>
	<!-- blueimp Gallery script -->
	<script src="../js/blueimp/gallery/jquery.blueimp-gallery.min.js"></script>
	<!-- The basic File Upload plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload.min.js"></script>
	<!-- The File Upload processing plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-process.min.js"></script>
	<!-- The File Upload image preview & resize plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-image.min.js"></script>
	<!-- The File Upload audio preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-audio.min.js"></script>
	<!-- The File Upload video preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-video.min.js"></script>
	<!-- The File Upload validation plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-validate.min.js"></script>
	<!-- The File Upload user interface plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-ui.min.js"></script>
	<!-- The main application script -->
	<script src="../js/jquery-upload/js/main.js"></script>
	<!-- bootbox script -->
	<script src="../js/bootbox/bootbox.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="../js/script.js"></script>
	<script src="../js/jquery-validate/jquery.validate.js"></script>
	<script src="../js/jquery-validate/additional-methods.js"></script>
	<script src="../js/error_message.js"></script>
	<script>
		jQuery(document).ready(function() {		
			App.setPage("forms");  //Set current page
			App.init(); //Initialise plugins and elements
			$("#photo").attr("src","getPhoto?safe_IdUser="+<% if(user != null){out.print(user.getSafe_IdUser());} %>);
			var addOrEdit=<%out.println(request.getParameter("addOrEdit")+";");%>
			
               $("#ProgI").change(function(){
                var id=$(this).children(":selected").attr("id");
                   dhtmlxAjax.post("SelectPlan","prog_IdProgram="+id,function(loader){
                    $("#Plan").html(loader.xmlDoc.responseText);
                    });
               });
							 
               $("#e1").change(function(){
                var id=$(this).children(":selected").attr("id");
                   dhtmlxAjax.post("getProgram","user_IdApplicant="+id,function(loader){
												$("#ProgS").html(loader.xmlDoc.responseText);
                    });
               });
               
               $("#Plan").change(function(){
                var id=$(this).children(":selected").attr("value");
                   dhtmlxAjax.post("selectPackage","prog_IdPlan="+id,function(loader){
                    $("#package").html(loader.xmlDoc.responseText);
                    });
               });

			$("#nuevo").click(function () {
				window.location = "InsListAccepted_catalog.jsp?idList=0&addOrEdit=0";
			});
			$("#editar").click(function () {
				$("#ProgI").removeAttr("disabled");
				$("#Plan").removeAttr("disabled");
        addOrEdit=1;
			});
			$("#guardar").click(function () {
                            var idListVal = $("#idList").val().trim();
                            var aspirante = parseInt($("#e1").children(":selected").attr('id'));
                            var plan = parseInt($("#Plan option:selected").val());
                            var package = parseInt($("#package option:selected").val());
                             if(aspirante != 0 && plan != 0){
                                var values = "values="+idListVal+"_"+aspirante+"_"+plan+"_"+package+"&addOrEdit="+addOrEdit;
					alert(values);
					dhtmlxAjax.post("ListAccepted_catalog_AddOrEdit",values,function(loader){
						var resp = loader.xmlDoc.responseText;
						respo = resp.split("_");
						//document.getElementById("hide").value=respo[0];
						var resp = parseInt(respo[1]);
						if(resp == 1){
							$("#idList").val(respo[0]);
							bootbox.alert("Se guardo con exito!", function() {});
							$("#e1").attr("disabled","disabled");
							$("#ProgI").attr("disabled","disabled");
							$("#Plan").attr("disabled","disabled");
							$("#editar").removeAttr("disabled");
						} else {
							bootbox.alert("Fallo al guardar!", function() {});
						}
					});
                             }else{
															 bootbox.alert("Los campos deben ser llenados en su totalidad");
                             }
			});
		});
	</script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
			}else{
				response.sendRedirect("error600.jsp");
			}
		}else{
			response.sendRedirect("error600.jsp");
		}
%>