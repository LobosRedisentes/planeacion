<%@page import="java.util.ArrayList"%>
<%@page import="com.edcore.model.safe.User"%>
<%
    HttpSession sesion;
    sesion = request.getSession();
    User user = (User) sesion.getAttribute("user");
    if(user != null){
        ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
        if(privileges.contains("DOC06")){
%>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Administración de notificaciones</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
    <meta name="description" content="Sistema educativo">
    <meta name="author" content="Instituto Tecnologico Superior de Zapopan">	
    <link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
    <link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
    <link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
    <!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    <!-- JQUERY ERROR MESSAGE-->
    <link href="../css/jquery_errormsn.css" rel="stylesheet">
    <!-- SELECT2 -->
    <link rel="stylesheet" type="text/css" href="../js/select2/select2.min.css" />
    <!-- LIBRERIA DHTMLX -->
    <link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.css" >
    <script src="../dhtmlx/libCompiler/dhtmlxcommon.js"></script>
    <script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.js"></script>
    <script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_srnd.js"></script>
    <script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_filter.js"></script>
    <script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgridcell.js"></script>
    <script src="../dhtmlx/dhtmlxDataProcessor/codebase/dhtmlxdataprocessor.js"></script>
    <script src="../dhtmlx/libCompiler/connector.js"></script>
</head>
<body>
    <!-- HEADER -->
    <%@include file="header.jsp" %>
    <!--/HEADER -->
    <!-- PAGE -->
    <section id="page">
        <!-- SIDEBAR -->
        <%@include file="sidebar.jsp" %>
        <!-- /SIDEBAR -->
        <div id="main-content">
            <div class="container">
                <div class="row">
                    <div id="content" class="col-lg-12">
                        <!-- PAGE HEADER-->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-header">
                                    <!-- BREADCRUMBS -->
                                    <ul class="breadcrumb">
                                        <li><i class="fa fa-home"></i><a href="index.jsp">Inicio</a></li>
                                        <li><i class="fa"></i><a href="#">Seguridad</a></li>
                                        <li>Notificaciones</li>
                                    </ul>
                                    <!-- /BREADCRUMBS -->
                                    <div class="clearfix">
                                        <h3 class="content-title pull-left">Administración de notificaciones</h3>
                                            <span class="date-range pull-right">
                                                <p class="btn-toolbar">
                                                    <button id="btn_nuevo" class="btn btn-danger"><i class="fa fa-print-square-o"></i> Nuevo </button>
                                                </p>
                                            </span>
                                    </div>
                                    <div class="description"></div>
                                </div>
                            </div>
                        </div>
                        <!-- /PAGE HEADER -->
                        <!-- DASHBOARD CONTENT -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box border red">
                                    <div class="box-title">
                                        <h4> Mensajes</h4>
                                        <div class="tools hidden-xs"></div>
                                    </div>
                                    <div class="box-body">
                                        <div id="grid_container" class="box border" style="height:600px; width: 100%"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /DASHBOARD CONTENT -->
                    </div><!-- /CONTENT-->
                </div>
            </div>
        </div>
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- JQUERY -->
	<script src="../js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="../bootstrap-dist/js/bootstrap.min.js"></script>
        <!-- SELECT2 -->
        <script type="text/javascript" src="../js/select2/select2.min.js"></script>
        <!-- bootbox script -->
        <script src="../js/bootbox/bootbox.min.js"></script>
  	<!-- COOKIE -->
	<script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="../js/script.js"></script>
	<script src="../js/jquery-validate/jquery.validate.js"></script>
	<script src="../js/jquery-validate/additional-methods.js"></script>
        <script src="../js/jquery_errormsn.js"></script>
        <script>
            var safe_IdUser = <% if(user != null){out.print(user.getSafe_IdUser());} %>;
        </script>
        <script src="Notificaciones.js"></script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
        }else{
            response.sendRedirect("error600.jsp");
        }
    }else{
        response.sendRedirect("error600.jsp");
    }
%>