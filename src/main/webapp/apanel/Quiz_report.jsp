
<%@page import="com.edcore.model.safe.User"%>
<%@page import="com.edcore.model.quiz.Quiz_report"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html>
<html lang="es">
<head>
	<%
		HttpSession sesion;
    sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if(user != null){
			ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
			if(privileges.contains("REP03")){
	%>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Encuestas SAT</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
	<link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
	
	<!-- FONTS -->
	<link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
</head>
<body>
	<!-- HEADER -->
	<%@include file="header.jsp" %>
	<!--/HEADER -->
	<!-- PAGE -->
	<section id="page">
				<!-- SIDEBAR -->
				<%@include file="sidebar.jsp" %>
				<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<!-- STYLER -->
									
									<!-- /STYLER -->
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="index.jsp">Inicio</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Estadisticas</a>
										</li>
										<li>Encuestas SAT</li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										<h3 class="content-title pull-left">Grupo de encuestas SAT</h3>
									</div>
									<div class="description"></div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						<!-- ADVANCED -->
						<div class="row">
							<div class="col-md-12">
								<!-- BOX -->
								<div class="box border red">
									<div class="box-title">
										<h4><i class="fa fa-bars"></i>Encuestas SAT</h4>
									</div>
									<div class="box-body">
										<!-- OVERVIEW -->
											   <div class="tab-pane fade in active" id="pro_overview">
												  <div class="row">
													<!-- PROFILE PIC -->
													<div class="col-md-3">
														<div class="list-group">
														  <li class="list-group-item zero-padding">
																<img alt="" class="img-responsive" src="../img/encuesta.jpg">
														  </li>
														  <div class="list-group-item profile-details">
																<h2>Importante!</h2>
																<p>Las graficas que acontinuación se exponen, son generadas en linea.</p>
														 </div>
														</div>
													</div>
													<!-- /PROFILE PIC -->
													<div class="col-md-9">
													<% 
																ArrayList ret = Quiz_report.ListQuiz();
																for(int x=0; x<ret.size(); x++){
																	String s[] = (String [])ret.get(x);
                                  out.println("<a class='btn btn-danger btn-icon input-block-level' href='Quiz_statistics.jsp?quiz="+s[0]+"'>");
																	out.print("<div><div style='float: left'><i class='fa fa-bar-chart-o fa-2x'></i></div>");
                                  out.println(s[1]);
																	out.print("</div></a>");
																}
													%>
													</div>
											   </div>
												 </div>
											   <!-- /OVERVIEW -->
									</div>
								</div>
								<!-- /BOX -->
							</div>
						</div>
						<!-- /ADVANCED -->
						<div class="footer-tools">
							<span class="go-top">
								<i class="fa fa-chevron-up"></i> Top
							</span>
						</div>
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	<script src="../js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="../bootstrap-dist/js/bootstrap.min.js"></script>
	<!-- AUTOSIZE -->
	<script type="text/javascript" src="../js/autosize/jquery.autosize.min.js"></script>
	<!-- The basic File Upload plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="../js/script.js"></script>
	<script>
		jQuery(document).ready(function() {		
			App.setPage("flot_charts");  //Set current page
			App.init(); //Initialise plugins and elements
			$("#photo").attr("src","getPhoto?safe_IdUser="+<% if(user != null){out.print(user.getSafe_IdUser());} %>);
		});
	</script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
			}else{
				response.sendRedirect("error600.jsp");
			}
		}else{
			response.sendRedirect("error600.jsp");
		}
%>
