<%@page import="java.util.ArrayList"%>
<%@page import="com.edcore.model.safe.User"%>
	<%
		HttpSession sesion;
    sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if(user != null){
			ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
			if(privileges.contains("SAFE18")){
	%>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Mensajerķa</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
        <meta name="description" content="Sistema educativo">
        <meta name="author" content="Instituto Tecnologico Superior de Zapopan">	
        <link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
        <link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
        <link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
        <!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
        <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
        <!-- JQUERY ERROR MESSAGE-->
        <link href="../css/jquery_errormsn.css" rel="stylesheet">
        <!-- SELECT2 -->
        <link rel="stylesheet" type="text/css" href="../js/select2/select2.min.css" />
        <!-- LIBRERIA DHTMLX -->
        <link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.css" >
        <script src="../dhtmlx/libCompiler/dhtmlxcommon.js"></script>
        <script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.js"></script>
        <script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_srnd.js"></script>
        <script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_filter.js"></script>
        <script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgridcell.js"></script>
        <script src="../dhtmlx/dhtmlxDataProcessor/codebase/dhtmlxdataprocessor.js"></script>
        <script src="../dhtmlx/libCompiler/connector.js"></script>
        <script>
            var id =<% if(user != null){out.print(user.getSafe_IdUser());} %>;
        </script>
</head>
<body>
    <!-- Modal -->
    <div class="modal fade" id="box_nuevo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 500px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header" style="background-color: rgb(207, 0, 0); color: #FFFFFF; height: 50px">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Nuevo mensaje</h4>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="form-horizontal" name="frm_nuevo"  id="frm_nuevo">
                            <div class="form-group" id="para" name="para">
                                <label class="col-md-2 control-label" for="safe_IdUserAccount">Para:</label>
                                <div class="col-md-10">
                                    <input type="hidden" id="safe_IdUserAccount" class="col-md-12"  name="safe_IdUserAccount" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="subject">Asunto:</label>
                                <div class="col-md-10">
                                    <input type="text" id="subject" class="col-md-12"  name="subject" maxlength="25" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="body">Contenido:</label>
                                <div class="col-md-10">
                                    <textarea name="body" id="body"  class="form-control" rows="3"></textarea>
                                </div>
                            </div>
                            <span class="date-range pull-right">
                                <button id="btn_guardar" class="btn btn-danger"><i class="fa fa-print-square-o"></i> Guardar </button>
                            </span>
                            </form>
                        </div>
                     </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="box_consultar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 500px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header" style="background-color: rgb(207, 0, 0); color: #FFFFFF; height: 50px">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Nuevo mensaje</h4>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <div class="row">
                        <div class="col-md-12">
                            <iframe src="" style="width: 100%; height: 100%" frameborder="0"></iframe>
                        </div>
                     </div>
                </div>
            </div>
        </div>
    </div>
	<!-- HEADER -->
	<%@include file="header.jsp" %>
        <script>
            var account =<%= account %>;
        </script>
	<!--/HEADER -->
	<!-- PAGE -->
	<section id="page">
		<!-- SIDEBAR -->
		<%@include file="sidebar.jsp" %>
		<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="index.jsp">Inicio</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Seguridad</a>
										</li>
										<li>Mensajerķa</li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										<h3 class="content-title pull-left"></h3>
										<span class="date-range pull-right">
										<p class="btn-toolbar">
                                                                                    <button id="btn_nuevo" class="btn btn-danger"><i class="fa fa-print-square-o"></i> Nuevo</button>
                                                                                    <button id="btn_consultar" class="btn btn-danger" disabled="true"><i class="fa fa-print-square-o"></i> Consultar</button>
                                                                                    <button id="btn_eliminar" class="btn btn-danger" disabled="true"><i class="fa fa-print-square-o"></i> Eliminar</button>
                                                                                    <button id="btn_actualizar" class="btn btn-danger"><i class="fa fa-print-square-o"></i> Actualizar</button>
										</p>
										</span>
									</div>
									<div class="description"></div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						<!-- DASHBOARD CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<!-- BOX -->
								<div id="grid_container" class="box border" style="height:600px; width: 100%">
								</div>
							</div>
						</div>
					   <!-- /DASHBOARD CONTENT -->
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- JQUERY -->
	<script src="../js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="../bootstrap-dist/js/bootstrap.min.js"></script>
        <!-- SELECT2 -->
        <script type="text/javascript" src="../js/select2/select2.min.js"></script>
        <!-- DATA MASK -->
        <script type="text/javascript" src="../js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
        <!-- bootbox script -->
        <script src="../js/bootbox/bootbox.min.js"></script>
  	<!-- COOKIE -->
	<script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="../js/script.js"></script>
	<script src="../js/jquery-validate/jquery.validate.js"></script>
	<script src="../js/jquery-validate/additional-methods.js"></script>
        <script src="../js/jquery_errormsn.js"></script>
        <script src="../js/spin.js" ></script>
	<script src="messenger.js"></script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
			}else{
				response.sendRedirect("error600.jsp");
			}
		}else{
			response.sendRedirect("error600.jsp");
		}
%>