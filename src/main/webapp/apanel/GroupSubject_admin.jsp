<%@page import="java.util.ArrayList"%>
<%@page import="com.edcore.model.safe.User"%>
	<%
		HttpSession sesion;
    sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if(user != null){
			ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
			if(privileges.contains("GP10")){
	%>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Grupos</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="Sistema educativo">
	<meta name="author" content="Instituto Tecnologico Superior de Zapopan">	
	<link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
	<link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
	<!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	
	<!-- LIBRERIA EDCORE -->
	<link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxLayout/codebase/dhtmlxlayout.css">
  <link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxLayout/codebase/skins/dhtmlxlayout_dhx_blue.css">
  <link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxWindows/codebase/dhtmlxwindows.css">
  <link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxWindows/codebase/skins/dhtmlxwindows_dhx_blue.css">
	<link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.css" >
  <script src="../dhtmlx/libCompiler/dhtmlxcommon.js"></script>
	<script type="text/javaScript"  src="../dhtmlx/dhtmlxLayout/codebase/dhtmlxcontainer.js"></script>
  <script type="text/javaScript"  src="../dhtmlx/dhtmlxLayout/codebase/dhtmlxlayout.js"></script>
  <script type="text/javaScript"  src="../dhtmlx/dhtmlxWindows/codebase/dhtmlxwindows.js"></script>
	<script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.js"></script>
	<script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_srnd.js"></script>
	<script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_filter.js"></script>
	<script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgridcell.js"></script>
	<script type="text/javaScript"  src="Groups_admin.js"></script>
	<script src="../js/dhxGrid.js"></script>
	<script src="../js/dhxLoad.js"></script>
	<script src="../dhtmlx/dhtmlxDataProcessor/codebase/dhtmlxdataprocessor.js"></script>
	<script src="../dhtmlx/libCompiler/connector.js"></script>
	<script src="../js/spin.js"></script>
	<script>
		  var dhxWins;
      var win;
			function editar(id){
				dhxWins= new dhtmlXWindows();
				win = dhxWins.createWindow("editarGrupo", 0,0, 1000, 570);
				dhxWins.window("editarGrupo").centerOnScreen();
				dhxWins.window("editarGrupo").setText("Editar grupo");
				dhxWins.window("editarGrupo").denyResize();
				dhxWins.setSkin("dhx_blue");
				dhxWins.window("editarGrupo").bringToTop();
				dhxWins.window("editarGrupo").stick();
				dhxWins.window("editarGrupo").setModal(true);
				win.attachURL("../Horarios/editarGrupo.jsp?group_IdGroupSubject="+id, "AJAX");
			}
	</script>
</head>
<body>
	<!-- HEADER -->
	<%@include file="header.jsp" %>
	<!--/HEADER -->
	<!-- PAGE -->
	<section id="page">
		<!-- SIDEBAR -->
		<%@include file="sidebar.jsp" %>
		<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="index.jsp">Inicio</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Grupos</a>
										</li>
										<li>Grupos Reales</li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										<h3 class="content-title pull-left">Administración de Grupos</h3>
										<span class="date-range pull-right">
										<p class="btn-toolbar">
											<%if(privileges.contains("GP18")){%>
											<button id="editar" class="btn btn-success" disabled=""><i class="fa fa-pencil-square-o"></i> Editar</button>
											<%}%>
											<button id="actualizar" class="btn btn-success" ><i class="fa fa-rotate-right"></i> Actualizar</button>
										</p>
										</span>
									</div>
									<div class="description"></div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						<!-- DASHBOARD CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<!-- BOX -->
								<div id="grid_container" class="box border" style="height:600px; width: 100%">
								</div>
							</div>
						</div>
					   <!-- /DASHBOARD CONTENT -->
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	<script src="../js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="../bootstrap-dist/js/bootstrap.min.js"></script>
	<!-- DATE RANGE PICKER -->
	<script src="../js/bootstrap-daterangepicker/moment.min.js"></script>	
	<script src="../js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
	<!-- EASY PIE CHART -->
	<script type="text/javascript" src="../js/easypiechart/jquery.easypiechart.min.js"></script>
	<!-- TODO -->
	<script type="text/javascript" src="../js/jquery-todo/js/paddystodolist.js"></script>
	<!-- FULL CALENDAR -->
	<script type="text/javascript" src="../js/fullcalendar/fullcalendar.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="../js/script.js"></script>
	<script>
		jQuery(document).ready(function() {
			App.setPage("index");  //Set current page
			App.init(); //Initialise plugins and elements
			$("#photo").attr("src","getPhoto?safe_IdUser="+<% if(user != null){out.print(user.getSafe_IdUser());} %>);
			var response = "../getAdminView?table=GroupSubject_view&id=group_IdGroupSubject&parameters=package,gpo,subject,professor,program,plan,calendar";
			var IDRow;
			var name;
			var values;
			
			grid = new dhtmlXGridObject('grid_container');
			grid.setImagePath("../dhtmlx/dhtmlxGrid/codebase/imgs/");
      grid.setHeader("Clave,Materia,Requisito");
      grid.setHeader("PAQUETE,GRUPO,MATERIA,DOCENTE,PROGRAMA,PLAN,CALENDARIO");
      grid.attachHeader("#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter");
      grid.setInitWidths("70,70,150,150,120,90,180");
      grid.setColAlign("left,left,left,left,left,left,left");
      grid.setColSorting("connector,connector,connector,connector,connector,connector,connector");
			grid.setEditable(false);
			grid.init();
			grid.setSkin("clear");
			grid.enableSmartRendering(true,50);
			grid.objBox.style.overflowX = "hidden";
			spInit();
			grid.load(response,function(){
				spStop();
				columnAutoSize(); 
			});
			grid.attachEvent("onRowSelect",function(id){
				IDRow = id;
				$("#editar").removeAttr("disabled");
			});
			grid.attachEvent("onRowDblClicked",function(id){
				IDRow = id;
				editar(IDRow);
			});
			$("#editar").click(function () {
				editar(IDRow);
			});
			$("#actualizar").click(function () {
				grid.clearAndLoad(response);
			});

		});
	</script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
			}else{
				response.sendRedirect("error600.jsp");
			}
		}else{
			response.sendRedirect("error600.jsp");
		}
%>