<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.edcore.model.db.DBConnector"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.edcore.model.safe.User"%>
	<%
		HttpSession sesion;
    sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if(user != null){
			ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
			if(privileges.contains("CAT23")){
	%>
<%
	DBConnector db = new DBConnector();
    ResultSet rs;
		PreparedStatement ps;
	Connection con;
	con=db.open();    

	            String name="";
             String firstName="";
             String lastName="";
             int schlLevel=0;
						 int safe_IdUser = 0;
						 String code="";
             String identification="";
             int user_IdProfessor=0;
             int certificate=0;
             int department=0;

       
    int idProf =Integer.parseInt(request.getParameter("idProf"));
    int addOrEdit=Integer.parseInt(request.getParameter("addOrEdit"));
    
      if(addOrEdit>0){
				
                ps = con.prepareStatement("SELECT  U.safe_IdUser, U.name,P.user_IdProfessor, "
                                          + "U.firstLast, U.secondLast, U.safe_IdUser, "
                                          + "P.code, P.professionalCell, P.conf_IdCertificate, P.org_IdDepartment  "
                                          + "FROM SafeUsers AS U inner join UserProfessors as P  on U.safe_IdUser = P.safe_IdUser "
                                          + "WHERE U.safe_IdUser=?");
                ps.setInt(1,idProf);
                rs = ps.executeQuery();
          
                while( rs!= null  && rs.next()){
                     
                         name= rs.getString("U.name");
                         firstName= rs.getString("U.firstLast");
                         lastName= rs.getString("U.secondLast");
			safe_IdUser = rs.getInt("U.safe_IdUser");
			code = rs.getString("P.code");
			user_IdProfessor = rs.getInt("P.user_IdProfessor");
                         identification=rs.getString("P.professionalCell");
			certificate=rs.getInt("P.conf_IdCertificate");
			department=rs.getInt("P.org_IdDepartment");
              }
             }
%>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Docente</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
	<link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
	
	<link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- DATE RANGE PICKER -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
	<!-- TYPEAHEAD -->
	<link rel="stylesheet" type="text/css" href="../js/typeahead/typeahead.css" />
	<!-- FILE UPLOAD -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-fileupload/bootstrap-fileupload.min.css" />
	<!-- SELECT2 -->
	<link rel="stylesheet" type="text/css" href="../js/select2/select2.min.css" />
	<!-- UNIFORM -->
	<link rel="stylesheet" type="text/css" href="../js/uniform/css/uniform.default.min.css" />
	<!-- JQUERY UPLOAD -->
	<!-- blueimp Gallery styles -->
	<link rel="stylesheet" href="../js/blueimp/gallery/blueimp-gallery.min.css">
	<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload.css">
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload-ui.css">
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	<style>
	 .error-message, label.error {
	    color: #ff0000;
	    margin:0;
	    display: inline;
	    font-size: 1em !important;
	    font-weight:bold;
	 }
	 </style>
	<!-- LIBRERIA EDCORE -->

  <script src="../dhtmlx/libCompiler/dhtmlxcommon.js"></script>
	<script src="../js/action_dhx.js"></script>
</head>
<body>
	<!-- HEADER -->
	<%@include file="header.jsp" %>
	<!--/HEADER -->
	<!-- PAGE -->
	<section id="page">
				<!-- SIDEBAR -->
				<%@include file="sidebar.jsp" %>
				<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<!-- STYLER -->
									
									<!-- /STYLER -->
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="index.jsp">Inicio</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Catalogos</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Usuarios</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="UserProfessors_admin.jsp">Docentes</a>
										</li>
										<li>Nuevo/Editar</li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										<h3 class="content-title pull-left">Cat�logo - Docentes</h3>
										<span class="date-range pull-right">
										<p class="btn-toolbar">
											<%if(privileges.contains("CAT39")){%>
											<button id="nuevo" class="btn btn-success" ><i class="fa fa-file-text-o"></i> Nuevo</button>
											<%}%>
											<%if(privileges.contains("CAT40")){%>
											<button id="editar" class="btn btn-success" disabled=""><i class="fa fa-pencil-square-o"></i> Editar</button>
											<%}%>
											<%if(privileges.contains("CAT41")){%>
											<button id="guardar" class="btn btn-success" ><i class="fa fa-save"></i> Guardar</button>
											<%}%>
										</p>
										</span>
									</div>
									<div class="description"></div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						<!-- ADVANCED -->
								  	<form role="form" class="form-horizontal" action="" method="post" name="frm_cuentas" id="frm_docentes">
										<div class="box border red col-lg-12">
											<div class="box-title">
												<h4>Docentes</h4>
												<div class="tools hidden-xs">
												</div>
											</div>
											<div class="box-body">
												<div class="tabbable header-tabs">
												  <ul class="nav nav-tabs">
													 <li class="active"><a id="tab1" href="#box_tab3" data-toggle="tab"> <span class="hidden-inline-mobile"><i class="fa fa-male"></i></span></a></li>
												  </ul>
													  <div class="tab-content">
														 <div class="tab-pane fade in active" id="box_tab3">
															 <div class="row">
															 	<div class="col-md-2">
																	<center>
																		<img height="150px" width="150px" class="img-circle" name="photo2" id="photo2" src="" alt="" />
																	</center>
																</div>
																<div class="col-md-10">
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="user">Usuario:</label>
																		<div class="col-sm-9">
                    <% 
												if(addOrEdit>0){
													out.print("<select id='user' name='user' class='form-control' disabled>");
												}else{
													out.print("<select id='user' name='user' class='form-control'>");
													out.print("<option id='0' value='' selected>Selecciona ..</option>");
												}
                        ps = con.prepareStatement("SELECT safe_IdUser, concat_ws(' ',firstLast,secondLast,name) as name FROM SafeUsers order by name");
                        rs = ps.executeQuery();                         
                                              
                        while(rs.next()){
                            if(rs.getInt("safe_IdUser")== safe_IdUser){
                                 out.println("<option id='"+rs.getInt("safe_IdUser")
                                         +"' selected>"+rs.getString("name")+"</option>");
                             }else{
                                 out.println("<option id='"+rs.getInt("safe_IdUser")
                                         +"'>"+rs.getString("name")+"</option>");
                             }
                         }
												out.print("</select>");
                    %>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="Name">Nombre:</label>
																		<div class="col-sm-9">
                    <%
                        if(addOrEdit>0){
                            out.print("<input type='text' id='Name' name='Name'  value='"+name+"' class='form-control' autocomplete='off' disabled/>");
                        }else{
                            out.print("<input type='text' id='Name' name='Name' class='form-control' autocomplete='off' disabled/>");
                        }
                    %>															
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="flName">Apellido Paterno:</label>
																		<div class="col-sm-9">
                    <%
                        if(firstName != null && !firstName.equals("")){
                            out.print("<input type='text' name='flName' id='flName' class='form-control' value='"+firstName+"'  disabled/>");
                        }else{
                            out.print("<input type='text' name='flName' id='flName' class='form-control' disabled/>");
                        }
                    %>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="slName">Apellido Materno:</label>
																		<div class="col-sm-9">
                    <%
                        if(lastName != null && !lastName.equals("")){
                            out.print("<input type='text' name='slName' id='slName' class='form-control'  value='"+lastName+"'  disabled/>");
                        }else{
                            out.print("<input type='text' name='slName' id='slName' class='form-control'  disabled/>");
                        }
                    %>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="nomina">No nomina:</label>
																		<div class="col-sm-9">
                            <%
                            if(addOrEdit>0){
                            out.println("<input type='text' class='form-control' autocomplete='off' id='code' name='code' value='"+code+"'>");
                            }else{
                            out.println("<input  type='text' class='form-control' autocomplete='off' id='code' name='code'>");    
                            }
                            %>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="e2">Departamento:</label>
																		<div class="col-sm-9">
              <% 
                        ps = con.prepareStatement("SELECT  OrgDepartments.name,OrgDepartments.org_IdDepartment"
                                                  + "  FROM OrgDepartments;"); 
                        out.println("<select id='e2'  name='dept' class='col-md-12'>");
												out.print("<option id='0' value='' selected>Selecciona ..</option>");
                        rs = ps.executeQuery();
                             
                        while(rs != null && rs.next()){
                              if(rs.getInt("OrgDepartments.org_IdDepartment")== department){
                                 out.println("<option id='"+rs.getInt("OrgDepartments.org_IdDepartment")
                                             +"' selected>"+rs.getString("OrgDepartments.name")+"</option>");
                                 }else{
                                       out.println("<option id='"+rs.getInt("OrgDepartments.org_IdDepartment")
                                                   +"'>"+rs.getString("OrgDepartments.name")+"</option>");
                             }
                        }  
                         out.println("</select>");                   
                    %>                
																		</div>
																	</div>
																		<div class="form-group">
																		<label class="col-sm-3 control-label" for="identification">Cedula profesional:</label>
																		<div class="col-sm-9">
                                   <%
                                     out.println("<input  type ='text' name='identification' id='identification' value= '"+identification+"' class='form-control' autocomplete='off' />");
                                   %> 
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="e4">Titulo:</label>
																		<div class="col-sm-9">
                   <%  
                        ps = con.prepareStatement("SELECT  ConfCertificates.name, ConfCertificates.conf_IdCertificate  FROM ConfCertificates;");
                        rs = ps.executeQuery();  
                        
                        out.println("<select id='e4' name='cert' class='col-md-12'>"); 
												out.print("<option id='0' value='' selected>Selecciona ..</option>");
                        
                           while(rs.next()){
                                 if(rs.getInt("ConfCertificates.conf_IdCertificate")== certificate){
                                    out.println("<option id='"+rs.getInt("ConfCertificates.conf_IdCertificate")
                                                +"' selected>"+rs.getString("ConfCertificates.name")+"</option>");
                                    }else{
                                          out.println("<option id='"+rs.getInt("ConfCertificates.conf_IdCertificate")
                                                      +"'>"+rs.getString("ConfCertificates.name")+"</option>");
                                         }
                           }
                         out.println("</select>");
												 db.close();          
                    %>         
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="bank">Referencia Bancaria:</label>
																		<div class="col-sm-9">
                            <% 
                            out.println("<input type='text' class='form-control' autocomplete='off' id='bank' name='bank'  disabled>");
                            %>
																		</div>
																	</div>
																		
																</div>
															 </div>
														 </div>
													  </div>
            <input type="hidden" id="hideProf" value="<%= idProf%>">
            <input type="hidden" id="hide" value="<%= user_IdProfessor%>">
												   </div>
												</div>
											</div>
						</form>

						<!-- /ADVANCED -->
						<div class="footer-tools">
							<span class="go-top">
								<i class="fa fa-chevron-up"></i> Top
							</span>
						</div>
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	<script src="../js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="../bootstrap-dist/js/bootstrap.min.js"></script>
	<!-- DATE RANGE PICKER -->
	<script src="../js/bootstrap-daterangepicker/moment.min.js"></script>
	<script src="../js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
	<!-- SLIMSCROLL -->
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
	<!-- BLOCK UI -->
	<script type="text/javascript" src="../js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
	<!-- TYPEHEAD -->
	<script type="text/javascript" src="../js/typeahead/typeahead.min.js"></script>
	<!-- AUTOSIZE -->
	<script type="text/javascript" src="../js/autosize/jquery.autosize.min.js"></script>
	<!-- COUNTABLE -->
	<script type="text/javascript" src="../js/countable/jquery.simplyCountable.min.js"></script>
	<!-- INPUT MASK -->
	<script type="text/javascript" src="../js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
	<!-- FILE UPLOAD -->
	<script type="text/javascript" src="../js/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
	<!-- SELECT2 -->
	<script type="text/javascript" src="../js/select2/select2.min.js"></script>
	<!-- UNIFORM -->
	<script type="text/javascript" src="../js/uniform/jquery.uniform.min.js"></script>
	<!-- JQUERY UPLOAD -->
	<!-- The Templates plugin is included to render the upload/download listings -->
	<script src="../js/blueimp/javascript-template/tmpl.min.js"></script>
	<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="../js/blueimp/javascript-loadimg/load-image.min.js"></script>
	<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script src="../js/blueimp/javascript-canvas-to-blob/canvas-to-blob.min.js"></script>
	<!-- blueimp Gallery script -->
	<script src="../js/blueimp/gallery/jquery.blueimp-gallery.min.js"></script>
	<!-- The basic File Upload plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload.min.js"></script>
	<!-- The File Upload processing plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-process.min.js"></script>
	<!-- The File Upload image preview & resize plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-image.min.js"></script>
	<!-- The File Upload audio preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-audio.min.js"></script>
	<!-- The File Upload video preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-video.min.js"></script>
	<!-- The File Upload validation plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-validate.min.js"></script>
	<!-- The File Upload user interface plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-ui.min.js"></script>
	<!-- The main application script -->
	<script src="../js/jquery-upload/js/main.js"></script>
	<!-- bootbox script -->
	<script src="../js/bootbox/bootbox.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="../js/script.js"></script>
	<script src="../js/jquery-validate/jquery.validate.js"></script>
	<script src="../js/jquery-validate/additional-methods.js"></script>
	<script src="../js/error_message.js"></script>
	<script>
		jQuery(document).ready(function() {		
			App.setPage("forms");  //Set current page
			App.init(); //Initialise plugins and elements
		  var id=$("#hideProf").val();
		  $("#photo2").attr("src","getPhoto?safe_IdUser="+<% if(idProf == 0){out.print(1);}else{out.print(idProf);} %>);
			$("#photo").attr("src","getPhoto?safe_IdUser="+<% if(user != null){out.print(user.getSafe_IdUser());} %>);
			var addOrEdit=<%out.println(request.getParameter("addOrEdit")+";"); %>      
			$("#frm_docentes").validate({
				ignore: "",
				rules:{
					user:{
						required: true
					},
					code:{
						required: true, minlength: 1, maxlength: 3
					},
					dept:{
						required: true
					},
					identification:{
						required: true, digits: true
					},
					cert:{
						required: true
					}
				},
				highlight: function(element){
					$(element).parent().parent().addClass("has-error");
				},
				unhighlight: function(element){
				$(element).parent().parent().removeClass("has-error");
			}});
            var addOrEdit=<%out.println(addOrEdit);%>;
            var usr=$("#user");
            var name=$("#Name");
            var fistName=$("#flName");
            var lastName=$("#slName");
            var identification=$("#identification");
            var dept=$("#e2");
            var code=$("#code");
            var cert=$("#e4");
            
            var hide=$("#hide");
            var hideProf=$("#hideProf");
						

               $("#user").change(function(){
                var id=$(this).children(":selected").attr("id");
                   dhtmlxAjax.post("SafeUserAccounts_auxiliary.jsp","idUs="+id,function(loader){
                        data=(loader.xmlDoc.responseText).split("#");
                        name.val(data[0]);
                        fistName.val(data[1]);
                        lastName.val(data[2]);
												$("#hideProf").val(id);
												$("#photo").attr("src","getPhoto?safe_IdUser="+id);                        
                    });
               });

			
			$("#nuevo").click(function () {
					window.location = "UserProfessors_catalog.jsp?idProf=0&addOrEdit=0";
			});
			$("#editar").click(function () {
                        identification.removeAttr("disabled");
                        dept.removeAttr("disabled");
                        cert.removeAttr("disabled");
											  code.removeAttr("disabled");
                        addOrEdit=1;
				$("#editar").attr("disabled","disabled");
			});
			$("#guardar").click(function () {
				
                         var codeVal=code.val().trim();
                         var nameVal=name.val().trim();
                         var fistNameVal=fistName.val().trim();
                         var lastNameVal=lastName.val().trim();
                         var identificationVal=identification.val().trim();
                         var deptVal=parseInt(dept.children(":selected").attr('id'));
                         var nomiVal=code.val().trim();
                         var certVal=parseInt(cert.children(":selected").attr('id'));
												 
				if($("#frm_docentes").valid()){
															 													 
                             var values="values="+parseInt(hide.val())+"#"+parseInt(hideProf.val())+"#0#"+deptVal+"#"+ 
                             certVal+"#"+identificationVal+"#"+codeVal+"&addOrEdit="+addOrEdit;
													 
													 
					dhtmlxAjax.post("UserProfessors_save",values,function(loader){
						var resp = loader.xmlDoc.responseText;
						respo = resp.split("#");
						document.getElementById("hide").value=respo[0];
						var resp = parseInt(respo[1]);
						if(resp == 1){
							bootbox.alert("Se guardo con exito!", function() {});
						} else {
							bootbox.alert("Fallo al guardar!", function() {});
						}
					});

                            code.attr("disabled","disabled");
                            name.attr("disabled","disabled");
                            fistName.attr("disabled","disabled");
                            lastName.attr("disabled","disabled");
                            identification.attr("disabled","disabled");
                            dept.attr("disabled","disabled");
                            cert.attr("disabled","disabled");

													$("#editar").removeAttr("disabled");
				}else{
					bootbox.alert("Datos incorrectos!", function() {});
				}
			});
		});
	</script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
			}else{
				response.sendRedirect("error600.jsp");
			}
		}else{
			response.sendRedirect("error600.jsp");
		}
%>