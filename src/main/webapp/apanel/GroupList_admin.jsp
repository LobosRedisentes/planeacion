<%@page import="java.util.ArrayList"%>
<%@page import="com.edcore.model.safe.User"%>
	<%
		HttpSession sesion;
    sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if(user != null){
			ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
			if(privileges.contains("EVAL05")){
	%>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Calificaciones Finales</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="Sistema educativo">
	<meta name="author" content="Instituto Tecnologico Superior de Zapopan">	
        <link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
        <link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
        <link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
        <!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
        <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    <!-- JQUERY ERROR MESSAGE-->
    <link href="../css/jquery_errormsn.css" rel="stylesheet">
    <!-- SELECT2 -->
    <link rel="stylesheet" type="text/css" href="../js/select2/select2.min.css" />
	
	<!-- LIBRERIA EDCORE -->
	<link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.css" >
	<link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxGrid/codebase/skins/dhtmlxgrid_dhx_skyblue.css" >
  <script src="../dhtmlx/libCompiler/dhtmlxcommon.js"></script>
	<script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.js"></script>
	<script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_srnd.js"></script>
	<script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_filter.js"></script>
	<script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgridcell.js"></script>
	<script src="../js/dhxGrid.js"></script>
	<script src="../js/dhxLoad.js"></script>
	<script src="../dhtmlx/dhtmlxDataProcessor/codebase/dhtmlxdataprocessor.js"></script>
	<script src="../dhtmlx/libCompiler/connector.js"></script>
	<script src="GroupList.js"></script>
	<script src="../js/spin.js"></script>
</head>
<body>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 80%; height: 98%">
            <div class="modal-content" style="width: 100%; height: 90%">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                </div>
                <div class="modal-body" style="width: 100%; height: 90%">
                    <div style="float: right">
                        <button id="btn_print" class="btn btn-primary" ><i class="fa fa-print-square-o"></i> Imprimir</button>
                    </div>
                    <br><br><br>
                    <iframe src="" style="width: 100%; height: 100%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
	<!-- HEADER -->
	<%@include file="header.jsp" %>
	<!--/HEADER -->
	<!-- PAGE -->
	<section id="page">
		<!-- SIDEBAR -->
		<%@include file="sidebar.jsp" %>
		<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="index.jsp">Inicio</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Calificaciones</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Semestre actual</a>
										</li>
										<li>Finales</li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										<h3 class="content-title pull-left">Administración de Calificaciones Finales</h3>
										<span class="date-range pull-right">
										<p class="btn-toolbar">
											<%if(privileges.contains("EVAL15")){%>
                                                                                        <button id="btn_consultar" class="btn btn-danger"><i class="fa fa-print-square-o"></i> Generar Actas </button>
											<button id="editar" class="btn btn-success" disabled><i class="fa fa-pencil-square-o"></i>Editar</button>
											<%}%>
										</p>
										</span>
									</div>
									<div class="description"></div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						<!-- DASHBOARD CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<!-- BOX -->
								<div id="grid_container" class="box border" style="height:600px; width: 100%">
								</div>
							</div>
						</div>
					   <!-- /DASHBOARD CONTENT -->
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- JQUERY -->
	<script src="../js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="../bootstrap-dist/js/bootstrap.min.js"></script>
        <!-- SELECT2 -->
        <script type="text/javascript" src="../js/select2/select2.min.js"></script>
        <!-- bootbox script -->
        <script src="../js/bootbox/bootbox.min.js"></script>
  	<!-- COOKIE -->
	<script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="../js/script.js"></script>
	<script src="../js/jquery-validate/jquery.validate.js"></script>
	<script src="../js/jquery-validate/additional-methods.js"></script>
        <script src="../js/jquery_errormsn.js"></script>
	<script>
		jQuery(document).ready(function() {
			App.setPage("forms");  //Set current page
			App.init(); //Initialise plugins and elements
			$("#photo").attr("src","getPhoto?safe_IdUser="+<% if(user != null){out.print(user.getSafe_IdUser());} %>);
			final_admin();
                        
                        
    $('#btn_consultar').click(function(){
        var box = bootbox.dialog({
            title: "<h4>Generar Listas</h4>",
            message: '<div class="row">' +
                '<div class="col-md-12"> ' +
                '<form class="form-horizontal" name="frm_generar"  id="frm_generar"> ' +
                '<div class="form-group"> ' +
                '<label class="col-md-2 control-label" for="cal_IdCalSchool">Calendario</label> ' +
                '<div class="col-md-10"> ' +
                '<select id="cal_IdCalSchool" class="col-md-10"  name="cal_IdCalSchool"></select>' +
                '</div></div>' +
                 '<div class="form-group"> ' +
                '<label class="col-md-2 control-label" for="cal_IdCalSchool">Departamento</label> ' +
                '<div class="col-md-10"> ' +
                '<select id="org_IdDepartment" class="col-md-10"  name="org_IdDepartment"></select>' +
                '</div> ' +
                '</div> ' +
                '</form> </div></div>',
            buttons: {
                success: {
                    label: "Aceptar",
                    className: "btn-danger",
                    callback: function () {
                        $("#frm_generar").validate({
                            rules:{
                                cal_IdCalSchool:{required: true},
                                org_IdDepartment:{required: true}
                            },
                            highlight: function(element){$(element).parent().parent().addClass("has-error");},
                            unhighlight: function(element){$(element).parent().parent().removeClass("has-error");}
                        });
                        if($("#frm_generar").valid()){
                            cal_IdCalSchool = $('#cal_IdCalSchool').children(":selected").attr("value");
                            org_IdDepartment = $('#org_IdDepartment').children(":selected").attr("value");
                            $('iframe').attr("src","GetPdfActas?org_IdDepartment="+org_IdDepartment+"&cal_IdCalSchool="+cal_IdCalSchool);
                            $('#myModal').modal({show:true});
                        }else{this.stopPropagation();}
                    }
                },
                'Danger!': {
                    label: "Cancelar",
                    className: "btn-danger",
                    callback: function() {}
                }
            }
        });
        box.on("shown.bs.modal", function() {
            var cal = $.ajax({url:"select_calschool",data:"sql=SELECT * FROM CalSchool ORDER BY cal_IdCalSchool DESC&id="+(Math.round(Math.random() * (5-1000)) + 5),method:"POST"});
            cal.done(function(msn){$("#cal_IdCalSchool").html(msn);});
            $('#cal_IdCalSchool').parents('.bootbox').removeAttr('tabindex');
            $('#cal_IdCalSchool').select2();
            var dep = $.ajax({url:"select_departments",data:"sql=SELECT * FROM OrgDepartments ORDER BY org_IdDepartment &id="+(Math.round(Math.random() * (5-1000)) + 5),method:"POST"});
            dep.done(function(msn){$("#org_IdDepartment").html(msn);});
            $('#org_IdDepartment').parents('.bootbox').removeAttr('tabindex');
            $('#org_IdDepartment').select2();
        });
    });
		});
	</script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
			}else{
				response.sendRedirect("error600.jsp");
			}
		}else{
			response.sendRedirect("error600.jsp");
		}
%>