<%-- 
    Document   : ProgCompSubject_admin_aux
    Created on : 19/12/2013, 09:36:24 PM
    Author     : Rafael
--%>

<%@page import="com.edcore.model.db.DBConnector"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    DBConnector db = new DBConnector();
    PreparedStatement ps;
    ResultSet rs;
   
    int opt=Integer.parseInt(request.getParameter("opt"));
    int val=Integer.parseInt(request.getParameter("prog_IdPlan"));
    Connection con=db.open();
    
     if(opt==0){
        ps = con.prepareStatement("SELECT ProgSubjects.name, ProgSubjects.prog_IdSubject FROM ProgSubjects "
                                 + "WHERE ProgSubjects.prog_IdPlan ='"+val+"'");
        rs = ps.executeQuery();
  
        while(rs.next()){
            out.println("<option id='"+rs.getInt("ProgSubjects.prog_IdSubject")+"'>" 
                                      +rs.getString("ProgSubjects.name")+"</option>");
        }
    }
     
    if(opt==1){
        
        ps = con.prepareStatement("SELECT prog_IdCompetence,percent FROM ProgCompSubject WHERE prog_IdSubject="+val);
        rs = ps.executeQuery();
        
        while(rs.next()){
                    out.print(rs.getInt("prog_IdCompetence")+"-");
                    out.print(rs.getFloat("percent")+"#");
            }
            out.print("0");
        
        
    }
    db.close();   
    
%>