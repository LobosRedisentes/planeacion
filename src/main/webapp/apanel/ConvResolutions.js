/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery(document).ready(function() {
    App.setPage("forms");  //Set current page
    App.init(); //Initialise plugins and elements
    $("#photo").attr("src","getPhoto?safe_IdUser="+safe_IdUser);
    var response = "../getAdminView?sql=select * FROM VADM004&id=conv_IdResolution&parameters=matricula,nombre,apellidoPaterno,apellidoMaterno,programa,plan,materias,tipo,status,calendario";
    var IDRow;
                
    grid = new dhtmlXGridObject('grid_container');
    grid.setImagePath("../dhtmlx/dhtmlxGrid/codebase/imgs/");
    grid.setHeader("Matricula,Nombre,Paterno,Materno,Programa,Plan,Materias,Tipo,Estatus,Calendario");
    grid.attachHeader("#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter");
    grid.setInitWidths("*,*,*,*,*,*,*,*,*,*");
    grid.setColAlign("left,left,left,left,left,left,left,left,left,left");
    grid.setColSorting("str,str,str,str,str,str,str,str,str,str");
    grid.setEditable(false);
    grid.init();
    grid.setSkin("clear");
    grid.enableSmartRendering(true,50);
    grid.objBox.style.overflowX = "hidden";
    grid.load(response,function(){});
                
    grid.attachEvent("onRowSelect",function(id){
        IDRow = id;
        $("#btn_edit").removeAttr("disabled");
    });

    $('#btn_edit').click(function(){
        window.location = "ConvSubjects.jsp?conv_IdResolution="+IDRow;
    });
                
    $('#btn_box').click(function(){
        var box = bootbox.dialog({
            title: "<h4>Nuevo dictamen</h4>",
            message: '<div class="row">' +
                '<div class="col-md-12"> ' +
                '<form class="form-horizontal" name="frm_nuevo"  id="frm_nuevo"> ' +
                '<div class="form-group"> ' +
                '<label class="col-md-2 control-label" for="user_IdApplicant">Aspirante</label> ' +
                '<div class="col-md-10"> ' +
                '<select id="user_IdApplicant" class="col-md-12"  name="user_IdApplicant"></select>' +
                '</div> ' +
                '</div> ' +
                '<div class="form-group"> ' +
                '<label class="col-md-2 control-label" for="prog_IdPlan">Plan</label> ' +
                '<div class="col-md-10"> ' +
                '<select id="prog_IdPlan" class="col-md-12"  name="prog_IdPlan"></select>' +
                '</div> ' +
                '</div> ' +
                '</form> </div></div>',
            buttons: {
                success: {
                    label: "Guardar",
                    className: "btn-danger",
                    callback: function () {
                        $("#frm_nuevo").validate({
                            rules:{
                                user_IdApplicant:{required: true},
                                prog_IdPlan:{required: true}
                            },
                            highlight: function(element){$(element).parent().parent().addClass("has-error");},
                            unhighlight: function(element){$(element).parent().parent().removeClass("has-error");}
                        });
                        if($("#frm_nuevo").valid()){
                            var user_IdApplicant = $('#user_IdApplicant').children(":selected").attr("value");
                            var prog_IdPlan = $('#prog_IdPlan').children(":selected").attr("value");
                            var save = $.ajax({url:"ConvResolution_save",data:"user_IdApplicant="+user_IdApplicant+"&prog_IdPlan="+prog_IdPlan,method:"POST"});
                            save.done(function(msn){
                                if(msn === '' || msn === '0'){
                                    bootbox.alert("No se pudo guardar! ", function() {});
                                }else{
                                    window.location = "ConvSubjects.jsp?conv_IdResolution="+msn;
                                }
                            });
                        }else{
                            this.stopPropagation();
                        }
                    }
                },
                'Danger!': {
                    label: "Cancelar",
                    className: "btn-danger",
                    callback: function() {}
                }
            }
        });
        box.on("shown.bs.modal", function() {
            var asp = $.ajax({url:"select_applicants",data:"sql=SELECT * FROM VADM003 WHERE tipo <> 'NUEVO' and matricula <> '000' ORDER BY apellidoPaterno, apellidoMaterno, nombre&id="+((Math.round(Math.random() * (5-1000)) + 5)),method:"POST"});
            asp.done(function(msn){$("#user_IdApplicant").html(msn);});
            $('#user_IdApplicant').parents('.bootbox').removeAttr('tabindex');
            $('#user_IdApplicant').select2();
            $('#prog_IdPlan').parents('.bootbox').removeAttr('tabindex');
            $('#prog_IdPlan').select2();
            $("#user_IdApplicant").change(function(){
                var user_IdApplicant=$(this).children(":selected").attr("value");
                var plan = $.ajax({url:"select_plan",
                    data:"sql=SELECT PP.prog_IdPlan AS prog_IdPlan, PP.officialCode AS officialCode FROM ProgPlan AS PP, Programs AS P, UserApplicant as U WHERE PP.prog_IdProgram = P.prog_IdProgram AND P.prog_IdProgram = U.prog_IdProgram AND PP.status <> 'INACTIVO' AND U.user_IdApplicant="+user_IdApplicant+" ORDER BY PP.prog_IdPlan DESC",
                    method:"POST"
                });
                plan.done(function(msn){$("#prog_IdPlan").html(msn);});
            });
        });
    });
});