/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


jQuery(document).ready(function() {
    App.setPage("forms");  //Set current page
    App.init(); //Initialise plugins and elements
    $("#photo").attr("src","getPhoto?safe_IdUser="+safe_IdUser);
    var response = "../getAdminView?sql=select * FROM VADM005 WHERE conv_IdResolution="+conv_IdResolution+"&id=conv_IdSubject&parameters=clave,materia,calificacion,creditos,horasPracticas,horasTeoricas,plan,semestre";
    var IDRow;

    grid = new dhtmlXGridObject('grid_container');
    grid.setImagePath("../dhtmlx/dhtmlxGrid/codebase/imgs/");
    grid.setHeader("Clave,Materia,Calificacion,Creditos,Horas practicas,Horas teoricas,Plan,Semestre");
    grid.attachHeader("#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter");
    grid.setInitWidths("*,*,*,*,*,*,*,*,*,*");
    grid.setColAlign("left,left,left,left,left,left,left,left,left,left");
    grid.setColSorting("str,str,str,str,str,str,str,str,str,str");
    grid.setEditable(false);
    grid.init();
    grid.setSkin("clear");
    grid.enableSmartRendering(true,50);
    grid.objBox.style.overflowX = "hidden";
    grid.load(response,function(){});

    grid.attachEvent("onRowSelect",function(id){
        IDRow = id;
        $("#btn_editar").removeAttr("disabled");
        $("#btn_eliminar").removeAttr("disabled");
    });

    $('#btn_nuevo').click(function(){
        var box = bootbox.dialog({
            title: "<h4>Agregar materia</h4>",
            message: '<div class="row">' +
                '<div class="col-md-12"> ' +
                '<form class="form-horizontal" name="frm_nuevo"  id="frm_nuevo"> ' +
                '<div class="form-group"> ' +
                '<label class="col-md-2 control-label" for="prog_IdSubject">Materia</label> ' +
                '<div class="col-md-10"> ' +
                '<select id="prog_IdSubject" class="col-md-12"  name="prog_IdSubject"></select>' +
                '</div> ' +
                '</div> ' +
                '<div class="form-group"> ' +
                '<label class="col-md-2 control-label" for="grade">Calificacion</label> ' +
                '<div class="col-md-10"> ' +
                '<input id="grade" name="grade" type="text" class="form-control" value="0"> ' +
                '</div> ' +
                '</div> ' +
                '</form> </div></div>',
            buttons: {
                success: {
                    label: "Guardar",
                    className: "btn-danger",
                    callback: function () {
                        $("#frm_nuevo").validate({
                            rules:{
                                prog_IdSubject:{required: true},
                                grade:{required: true, digits: true}
                            },
                            highlight: function(element){$(element).parent().parent().addClass("has-error");},
                            unhighlight: function(element){$(element).parent().parent().removeClass("has-error");}
                        });
                        if($("#frm_nuevo").valid()){
                            var grade = $('#grade').val().trim();
                            var prog_IdSubject = $('#prog_IdSubject').children(":selected").attr("value");
                            var save = $.ajax({url:"ConvSubjects_save",data:"prog_IdSubject="+prog_IdSubject+"&grade="+grade+"&conv_IdResolution="+conv_IdResolution+"&operation=NUEVO",method:"POST"});
                            save.done(function(msn){
                                if(msn === '' || msn === '0'){
                                    bootbox.alert("No se pudo guardar! ", function() {});
                                }else if(msn === '-100'){
                                    bootbox.alert("La materia ya se encuentra registrada!", function() {});
                                }else{
                                    grid.clearAndLoad(response);
                                }
                            });
                        }else{
                            this.stopPropagation();
                        }
                    }
                },
                'Danger!': {
                    label: "Cancelar",
                    className: "btn-danger",
                    callback: function() {}
                }
            }
        });
        box.on("shown.bs.modal", function() {
            var subjects = $.ajax({url:"select_subjects",data:"sql=SELECT * FROM ProgSubjects WHERE prog_IdPlan="+prog_IdPlan+" ORDER BY name",method:"POST"});
            subjects.done(function(msn){$("#prog_IdSubject").html(msn);});
            $('#prog_IdSubject').parents('.bootbox').removeAttr('tabindex');
            $('#prog_IdSubject').select2();
        });
    });        

    $('#btn_editar').click(function(){
        bootbox.dialog({
            title: "<h4>Editar materia</h4>",
            message: '<div class="row">' +
                '<div class="col-md-12"> ' +
                '<form class="form-horizontal" name="frm_editar"  id="frm_editar"> ' +
                '<div class="form-group"> ' +
                '<label class="col-md-2 control-label" for="prog_IdSubject">Materia</label> ' +
                '<div class="col-md-10"> ' +
                '<input id="prog_IdSubject" name="prog_IdSubject" type="text" class="form-control" disabled="" value="'+grid.cellById(IDRow,1).getValue()+'"> ' +
                '</div> ' +
                '</div> ' +
                '<div class="form-group"> ' +
                '<label class="col-md-2 control-label" for="grade">Calificacion</label> ' +
                '<div class="col-md-10"> ' +
                '<input id="grade" name="grade" type="text" class="form-control" value="'+grid.cellById(IDRow,2).getValue()+'"> ' +
                '</div> ' +
                '</div> ' +
                '</form> </div></div>',
            buttons: {
                success: {
                    label: "Guardar",
                    className: "btn-danger",
                    callback: function () {
                        $("#frm_editar").validate({
                            rules:{
                                grade:{required: true, digits: true}
                            },
                            highlight: function(element){$(element).parent().parent().addClass("has-error");},
                            unhighlight: function(element){$(element).parent().parent().removeClass("has-error");}
                        });
                        if($("#frm_editar").valid()){
                            var grade = $('#grade').val().trim();
                            var save = $.ajax({url:"ConvSubjects_save",data:"grade="+grade+"&conv_IdSubject="+IDRow+"&operation=ACTUALIZAR",method:"POST"});
                            save.done(function(msn){
                                if(msn === '' || msn === '0'){
                                    bootbox.alert("No se pudo guardar! ", function() {});
                                }else{
                                    grid.clearAndLoad(response);
                                }
                            });
                        }else{
                            this.stopPropagation();
                        }
                    }
                },
                'Danger!': {
                    label: "Cancelar",
                    className: "btn-danger",
                    callback: function() {}
                }
            }
        });
    });
    $('#btn_eliminar').click(function(){
        bootbox.dialog({
            title: "<h>Eliminar materia</h4>",
            message: '<div class="row">' +
                '<div class="col-md-12"> ' +
                '<div class="form-group"> ' +
                '<label class="col-md-12 control-label" for="name">Desea realmente eliminar la materia: '+grid.cellById(IDRow,1).getValue()+'?</label> ' +                           
                '</div> ' +
                '</div></div>',
            buttons: {
                success: {
                    label: "Eliminar",
                    className: "btn-danger",
                    callback: function () {
                        var save = $.ajax({url:"ConvSubjects_save",data:"conv_IdSubject="+IDRow+"&operation=ELIMINAR",method:"POST"});
                        save.done(function(msn){
                            if(msn === '' || msn === '0'){
                                bootbox.alert("No se pudo eliminar! ", function() {});
                            }else{
                                grid.clearAndLoad(response);    
                            }
                        });
                    }
                },
                'Danger!': {
                    label: "Cancelar",
                    className: "btn-danger",
                    callback: function() {}
                }
            }
        });
    });
});