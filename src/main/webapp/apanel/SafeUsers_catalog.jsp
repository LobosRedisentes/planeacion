<%@page import="java.util.ArrayList"%>
<%@page import="com.edcore.model.safe.User"%>
<%@page import="java.sql.Blob"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.edcore.model.db.DBConnector"%>
<%@page import="java.sql.ResultSet"%>

<%
		HttpSession sesion;
    sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if(user != null){
			ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
			if(privileges.contains("SAFE05")){
%>

<%
	DBConnector db = new DBConnector();
    ResultSet rs;
		PreparedStatement ps;
    PreparedStatement ps2;
    PreparedStatement ps3;
    PreparedStatement ps4;
	Connection con;
	int addOrEdit=Integer.parseInt(request.getParameter("addOrEdit"));
	con=db.open();    int safeIdUser =Integer.parseInt(request.getParameter("IdUser"));
    String nombre = "";
    String paterno = "";
    String materno = "";
    String fNac = "";
    int stateN  = 0;
    int townN  = 0;
    String NSS = "";
    String fSang = "";
    String genero = "";
    String RFC = "";
    String CURP = "";
    String ocupa = "";
    String stCiv = "";
    //////////////////
    String Tel = "";
    String Cel = "";
    String CorA = "";
    
    int IdAdBo = 0;
    int Edo = 0;
    int Mpo = 0;
    int Col = 0;
    String calle = "";
    String NumEx = "";
    String NumIn = "";
    
    if(safeIdUser > 0){
        ps = con.prepareStatement("SELECT SafeUsers.* FROM SafeUsers WHERE safe_IdUser = ?");
        ps.setInt(1, safeIdUser);        
        rs = ps.executeQuery();
            
        while(rs != null && rs.next()){
            nombre = rs.getString("name");
            paterno = rs.getString("firstLast");
            materno = rs.getString("secondLast");
            fNac = rs.getString("birthDate");
            stateN = rs.getInt("birthState");
            townN = rs.getInt("birthTown");
            NSS = rs.getString("socialSecurity");
            fSang = rs.getString("blobFactor");
            genero = rs.getString("sex");
            RFC = rs.getString("rfc");
            CURP = rs.getString("curp");
            ocupa = rs.getString("occupation");
            IdAdBo = rs.getInt("addr_IdAddrBook");
            stCiv = rs.getString("status");
        }
        
        ps2 = con.prepareStatement("SELECT AddrBook.* FROM AddrBook WHERE addr_IdAddrBook = "+IdAdBo+"");        
        rs = ps2.executeQuery();
            
        while(rs != null && rs.next()){
            Col = rs.getInt("addr_IdLocation");
            calle = rs.getString("street");
            NumEx = rs.getString("numExternal");
            NumIn = rs.getString("numInternal");
            Tel = rs.getString("phone");
            Cel = rs.getString("cellPhone");
            CorA = rs.getString("alternativeEmail");
        }
        
        ps3 = con.prepareStatement("SELECT addr_IdState, addr_IdTown "
                + "FROM AddrLocations WHERE addr_IdLocation = "+Col+"");        
        rs = ps3.executeQuery();
            
        while(rs != null && rs.next()){
            Edo = rs.getInt("addr_IdState");
            Mpo = rs.getInt("addr_IdTown");
        }
    }
%>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Usuarios</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
	<link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
	
	<link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- DATE RANGE PICKER -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
	<!-- TYPEAHEAD -->
	<link rel="stylesheet" type="text/css" href="../js/typeahead/typeahead.css" />
	<!-- FILE UPLOAD -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-fileupload/bootstrap-fileupload.min.css" />
	<!-- SELECT2 -->
	<link rel="stylesheet" type="text/css" href="../js/select2/select2.min.css" />
	<!-- UNIFORM -->
	<link rel="stylesheet" type="text/css" href="../js/uniform/css/uniform.default.min.css" />
	<!-- JQUERY UPLOAD -->
	<!-- blueimp Gallery styles -->
	<link rel="stylesheet" href="../js/blueimp/gallery/blueimp-gallery.min.css">
	<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload.css">
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload-ui.css">
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	<style>
	 .error-message, label.error {
	    color: #ff0000;
	    margin:0;
	    display: inline;
	    font-size: 1em !important;
	    font-weight:bold;
	 }
	 </style>
	<!-- LIBRERIA EDCORE -->

  <script src="../dhtmlx/libCompiler/dhtmlxcommon.js"></script>
	<script src="../js/action_dhx.js"></script>
</head>
<body>
	<!-- HEADER -->
	<%@include file="header.jsp" %>
	<!--/HEADER -->
	<!-- PAGE -->
	<section id="page">
				<!-- SIDEBAR -->
				<%@include file="sidebar.jsp" %>
				<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<!-- STYLER -->
									
									<!-- /STYLER -->
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="index.jsp">Inicio</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Seguridad</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="SafeUsers_admin.jsp">Usuarios</a>
										</li>
										<li>Nuevo/Editar</li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										<h3 class="content-title pull-left">Cat�logo de Usuarios</h3>
										<span class="date-range pull-right">
										<p class="btn-toolbar">
											<%if(privileges.contains("SAFE08")){%>
											<button id="nuevo" class="btn btn-success" ><i class="fa fa-file-text-o"></i> Nuevo</button>
											<%}%>
											<%if(privileges.contains("SAFE09")){%>
											<button id="editar" class="btn btn-success" disabled=""><i class="fa fa-pencil-square-o"></i> Editar</button>
											<%}%>
											<%if(privileges.contains("SAFE10")){%>
											<button id="guardar" class="btn btn-success" ><i class="fa fa-save"></i> Guardar</button>
											<%}%>
										</p>
											<form role="form" class="form-horizontal" id="form_photo" name="form_photo"  enctype="multipart/form-data" method="post">
												<%if(privileges.contains("SAFE11")){%>
																	<span class="btn btn-success fileinput-button">
																		<span>Cambiar foto</span>
																		<input type="file" id="file" name="file" />
																	</span>
											<%}%>
											</form>
										</span>
									</div>
									<div class="description"></div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						<!-- ADVANCED -->
								  	<form role="form" class="form-horizontal"  name="frm_usuarios" id="frm_usuarios">
										<div class="box border red col-lg-12">
											<div class="box-title">
												<h4>Usuarios</h4>
												<div class="tools hidden-xs">
												</div>
											</div>
											<div class="box-body">
												<div class="tabbable header-tabs">
												  <ul class="nav nav-tabs">
													 <li ><a href="#box_tab1" id="tab3" data-toggle="tab"> <span class="hidden-inline-mobile"> <i class="fa fa-home"></i> Domicilio</span></a></li>
													  <li><a href="#box_tab2" id="tab2" data-toggle="tab"> <span class="hidden-inline-mobile"><i class="fa fa-calendar"></i> Datos de nacimiento</span></a></li>
													 <li class="active"><a id="tab1" href="#box_tab3" data-toggle="tab"> <span class="hidden-inline-mobile"><i class="fa fa-male"></i> Datos generales</span></a></li>
												  </ul>
                <%
                        if(safeIdUser != 0){
                            
                            out.print("<input type='hidden' id='safeIdUser' name='safeIdUser' value='"+safeIdUser+"' />");
                        }else{
                            out.print("<input type='hidden' id='safeIdUser' name='safeIdUser' />");
                        }
                  
                        if(IdAdBo !=0){
                            out.print("<input type='hidden' id='IdAdBo' name='IdAdBo'  value='"+IdAdBo+"' />");
                        }else{
                            out.print("<input type='hidden' id='IdAdBo' name='IdAdBo' />");
                        }
               %>
															
													  <div class="tab-content">
														 <div class="tab-pane fade in active" id="box_tab3">
															 <div class="row">
															 	<div class="col-md-2">
																	<center><img height="150px" width="150px" class="img-circle" name="photo2" id="photo2" src="" alt="" /></center>
																</div>
																<div class="col-md-10">
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="nombre">Nombre</label>
																		<div class="col-sm-9">
                    <%
                        if(nombre != null && !nombre.equals("")){
                            out.print("<input type='text' id='nombre' name='nombre' class='form-control' autocomplete='off' value='"+nombre+"' />");
                        }else{
                            out.print("<input type='text' id='nombre' name='nombre' class='form-control' autocomplete='off' />");
                        }
                    %>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="paterno">Apellido paterno</label>
																		<div class="col-sm-9">
                    <%
                        if(paterno != null && !paterno.equals("")){
                            out.print("<input type='text' id='paterno' name='paterno' class='form-control' autocomplete='off' value='"+paterno+"' />");
                        }else{
                            out.print("<input type='text' id='paterno' name='paterno' class='form-control' autocomplete='off' />");
                        }
                    %>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="materno">Apellido Materno</label>
																		<div class="col-sm-9">
                    <%
                        if(materno != null && !materno.equals("")){
                            out.print("<input type='text' id='materno' name='materno' class='form-control' autocomplete='off' value='"+materno+"' />");
                        }else{
                            out.print("<input type='text' id='materno' name='materno' class='form-control' autocomplete='off' />");
                        }
                    %>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="stCiv">Estado civil</label>
																		<div class="col-sm-9">
                    <select  id="stCiv" name="stCiv" class="form-control" >
                        <%
                            String stCivArray[]={"SOLTERO","CASADO","DIVORCIADO","VIUDO","UNION LIBRE","FINADO"};
														out.println("<option id='0' selected value=''>Selecciona ..</option>");
                            for(int i=0;i<5;i++){
                                if(stCiv.equals(stCivArray[i])){
                                    out.print("<option id='"+stCivArray[i]+"' selected>"+stCivArray[i]+"</option>");
                                }else{
                                    out.print("<option id='"+stCivArray[i]+"'>"+stCivArray[i]+"</option>");
                                }
                            }
                        %>
                    </select>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="genero">Genero</label>
																		<div class="col-sm-9">
                    <select id="genero" name="genero" class="form-control" >
                        <%
                            String generoArray[]={"M","F"};
														out.println("<option id='0' selected value=''>Selecciona ..</option>");
                            for(int i=0;i<2;i++){
                                if(genero.equals(generoArray[i])){
                                    out.print("<option id='"+generoArray[i]+"' selected>"+generoArray[i]+"</option>");
                                }else{
                                    out.print("<option id='"+generoArray[i]+"'>"+generoArray[i]+"</option>");
                                }
                            }
                        %>
                    </select>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="fsang">Tipo de Sangre:</label>
																		<div class="col-sm-9">
                    <select id="fsang" name="fsang" class="form-control">
                        <%
                            String fsangArray[]={"INDEFINIDO","A+","A-","B+","B-","O+","O-","AB+","AB-"};
														out.println("<option id='0' selected value=''>Selecciona ..</option>");
                            for(int j=0;j<8;j++){
                                if(fSang.equals(fsangArray[j])){
                                    out.print("<option id='"+fsangArray[j]+"' selected>"+fsangArray[j]+"</option>");
                                }else{
                                    out.print("<option id='"+fsangArray[j]+"'>"+fsangArray[j]+"</option>");
                                }
                            }
                        %>
                    </select>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="ocupa">Ocupacion:</label>
																		<div class="col-sm-9">
                    <select id="ocupa" name="ocupa" class="form-control" >
                        <%
                            String ocupaArray[]={"ESTUDIANTE","TRABAJADOR","AMA DE CASA","OTRO"};
														out.println("<option id='0' selected value=''>Selecciona ..</option>");
                            for(int j=0;j<4;j++){
                                if(ocupa.equals(ocupaArray[j])){
                                    out.print("<option id='"+ocupaArray[j]+"' selected>"+ocupaArray[j]+"</option>");
                                }else{
                                    out.print("<option id='"+ocupaArray[j]+"'>"+ocupaArray[j]+"</option>");
                                }
                            }
                        %>
                    </select>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="NSS">Num Seguro</label>
																		<div class="col-sm-9">
                    <%
                        if(NSS != null && !NSS.equals("")){
                            out.print("<input type='text' class='form-control' autocomplete='off' id='NSS' name='NSS' value='"+NSS+"' />");
                        }else{
                            out.print("<input type='text' class='form-control' autocomplete='off' id='NSS' name='NSS' value='0' />");
                        }
                    %>
																		</div>
																	</div>			
																</div>
															 </div>
														 </div>
														 <div class="tab-pane fade" id="box_tab2">
															<div class="row">
																<div class="col-md-12">
																	
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="e1">Estado de Nacimiento</label>
																		<div class="col-sm-9">
                    <%
                        ps = con.prepareStatement("SELECT AddrStates.name, AddrStates.addr_IdState FROM AddrStates");
                        rs = ps.executeQuery();

                        out.println("<select id='e1' class='col-md-12' name='stateN'>");
                         out.println("<option id='0' value='' selected>Selecciona ..</option>");
                        while(rs.next()){
                            if(rs.getInt("AddrStates.addr_IdState")== (stateN)){
                                 out.println("<option id='"+rs.getInt("AddrStates.addr_IdState")+"' selected>"+rs.getString("AddrStates.name")+"</option>");
                            }
                            else{
                                out.println("<option id='"+rs.getInt("AddrStates.addr_IdState")+"'>"+rs.getString("AddrStates.name")+"</option>");
                            }
                        }
                        out.println("</select>");
                    %>

																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="e2">Municipio de Nacimiento</label>
																		<div class="col-sm-9">
                    <%
                        ps = con.prepareStatement("SELECT AddrTown.name, AddrTown.addr_IdTown FROM AddrTown where AddrTown.addr_IdState="+stateN+" ");
                        rs = ps.executeQuery();

                        out.println("<select id='e2' class='col-md-12' name='townN'>");
                        out.println("<option id='0' value='' selected>Selecciona ..</option>");
                        while(rs.next()){
                            if(rs.getInt("AddrTown.addr_IdTown")==(townN)){
                                 out.println("<option id='"+rs.getInt("AddrTown.addr_IdTown")+"' selected>"+rs.getString("AddrTown.name")+"</option>");
                            }
                            else{
                                out.println("<option id='"+rs.getInt("AddrTown.addr_IdTown")+"'>"+rs.getString("AddrTown.name")+"</option>");
                            }
                        }
                        out.println("</select>");
                    %> 
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="fNac">Fecha de nacimiento:</label>
																		<div class="col-sm-9">
                    <%
                        if(fNac != null && !fNac.equals("")){
                            out.print("<input type='date' id='fNac' name='fNac' class='form-control' autocomplete='off' value='"+fNac+"' />");
                        }else{
                            out.print("<input type='date' id='fNac' name='fNac' class='form-control' autocomplete='off' />");
                        }
                    %>
																		</div>
																		</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="RFC">RFC:</label>
																		<div class="col-sm-9">
                    <%
                        if(RFC != null && !RFC.equals("")){
                            out.print("<input type='text' id='RFC' name='RFC' class='form-control' autocomplete='off' value='"+RFC+"' />");
                        }else{
                            out.print("<input type='text' id='RFC' name='RFC' class='form-control' autocomplete='off' value='INDEFINIDO' />");
                        }
                    %>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label">CURP:</label>
																		<div class="col-sm-9">
                    <%
                        if(CURP != null && !CURP.equals("")){
                            out.print("<input type='text' id='CURP' name='CURP' class='form-control' autocomplete='off' value='"+CURP+"' />");
                        }else{
                            out.print("<input type='text' id='CURP' name='CURP' class='form-control' autocomplete='off' value = 'INDEFINIDO' />");
                        }
                    %>
																		</div>
																	</div>
																	
																</div>
															</div> 
														 </div>
														 <div class="tab-pane fade" id="box_tab1">
															<div class="row">
																<div class="col-md-12">
																	
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="e3">Estado</label>
																		<div class="col-sm-9">
                    <%
                        ps = con.prepareStatement("SELECT AddrStates.name, AddrStates.addr_IdState FROM AddrStates");
                        rs = ps.executeQuery();

                        out.println("<select id='e3' name='Edo' class='col-md-12'>");
                         out.println("<option id='0' value='' selected>Selecciona ..</option>");
                        while(rs.next()){
                            if(rs.getInt("AddrStates.addr_IdState")== (Edo)){
                                 out.println("<option id='"+rs.getInt("AddrStates.addr_IdState")+"' selected>"+rs.getString("AddrStates.name")+"</option>");
                            }
                            else{
                                out.println("<option id='"+rs.getInt("AddrStates.addr_IdState")+"'>"+rs.getString("AddrStates.name")+"</option>");
                            }
                        }
                        out.println("</select>");
                    %>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="e4">Municipio</label>
																		<div class="col-sm-9">
                    <%
                        ps = con.prepareStatement("SELECT AddrTown.name, AddrTown.addr_IdTown FROM AddrTown where AddrTown.addr_IdState="+Edo+"");
                        rs = ps.executeQuery();

                        out.println("<select id='e4' name='Mpo' class='col-md-12'>");
                        out.println("<option id='0' value='' selected>Selecciona ..</option>");
                        while(rs.next()){
                            if(rs.getInt("AddrTown.addr_IdTown")==(Mpo)){
                                 out.println("<option id='"+rs.getInt("AddrTown.addr_IdTown")+"' selected>"+rs.getString("AddrTown.name")+"</option>");
                            }
                            else{
                                out.println("<option id='"+rs.getInt("AddrTown.addr_IdTown")+"'>"+rs.getString("AddrTown.name")+"</option>");
                            }
                        }
                        out.println("</select>");
                    %> 																			   
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="e5">Colonia:</label>
                                                                        <div class="col-sm-9">
                    <%
                        ps = con.prepareStatement("SELECT AddrLocations.name, AddrLocations.addr_IdLocation FROM AddrLocations where AddrLocations.addr_IdTown="+Mpo+"");
                        rs = ps.executeQuery();

                        out.println("<select id='e5' name='Col' class='col-md-12'>");
                        out.println("<option id='0' value='' selected>Selecciona ..</option>");
                        while(rs.next()){
                            if(rs.getInt("AddrLocations.addr_IdLocation")== (Col)){
                                 out.println("<option id='"+rs.getInt("AddrLocations.addr_IdLocation")+"' selected>"+rs.getString("AddrLocations.name")+"</option>");
                            }
                            else{
                                out.println("<option id='"+rs.getInt("AddrLocations.addr_IdLocation")+"'>"+rs.getString("AddrLocations.name")+"</option>");
                            }
                        }
                        out.println("</select>");
                    %>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
																																			<label class="col-sm-3 control-label" for="calle">Calle:</label>
                                                                        <div class="col-sm-9">
                    <%
                        if(calle != null && !calle.equals("")){
                            out.print("<input type='text' id='calle' name='calle' class='form-control' autocomplete='off' value='"+calle+"' />");
                        }else{
                            out.print("<input type='text' id='calle' name='calle' class='form-control' autocomplete='off' />");
                        }
                    %>
                                                                        </div>
                                                                    </div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="NumEx">Num Ext:</label>
																		<div class="col-sm-3">
                    <%
                        if(NumEx != null && !NumEx.equals("")){
                            out.print("<input type='text' id='NumEx' name='NumEx' class='form-control' autocomplete='off' value='"+NumEx+"' />");
                        }else{
                            out.print("<input type='text' id='NumEx' name='NumEx' class='form-control' autocomplete='off' value='0'/>");
                        }
                    %>
																		</div>
																		<label class="col-sm-2 control-label" for="NumIn">Num Int:</label>
																		<div class="col-sm-4">
                    <%
                        if(NumIn != null && !NumIn.equals("")){
                            out.print("<input type='text' id='NumIn' name='NumIn' class='form-control' autocomplete='off' value='"+NumIn+"' />");
                        }else{
                            out.print("<input type='text' id='NumIn' name='NumIn' class='form-control' autocomplete='off' value='0' />");
                        }
                    %>

																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="Tel">Telefono:</label>
																		<div class="col-sm-9">
                    <%
                        if(Tel != null && !Tel.equals("")){
                            out.print("<input type='text' id='Tel' name='Tel' class='form-control' autocomplete='off' value='"+Tel+"' />");
                        }else{
                            out.print("<input type='text' id='Tel' name='Tel' class='form-control' autocomplete='off' />");
                        }
                    %>
																		</div>
                                                                       
																	</div>
                                                                    <div class="form-group">
																																			<label class="col-sm-3 control-label" for="Cel">Celular:</label>
																		<div class="col-sm-9">
                    <%
                        if(Cel != null && !Cel.equals("")){
                            out.print("<input type='text' id='Cel' name='Cel' class='form-control' autocomplete='off' value='"+Cel+"' />");
                        }else{
                            out.print("<input type='text' id='Cel' name='Cel' class='form-control' autocomplete='off' />");
                        }
                    %>
																		</div>
																	</div>
                                                                    <div class="form-group">
																																			<label class="col-sm-3 control-label" for="Cor">Correo:</label>
																		<div class="col-sm-9">
                    <%
                        if(CorA != null && !CorA.equals("")){
                            out.print("<input type='text' id='Cor' name='Cor' class='form-control' autocomplete='off' value='"+CorA+"' />");
                        }else{
                            out.print("<input type='text' id='Cor' name='Cor' class='form-control' autocomplete='off' />");
                        }
                    %>
																		</div>
																	</div>
																</div>
															</div> 
														 </div>
													  </div>
													<input type="hidden" id="hide" value="<% if(Integer.parseInt(request.getParameter("IdUser"))>0){out.println(request.getParameter("IdUser"));}else{out.println("0");} %>" >
												   </div>
												</div>
											</div>
						</form>

						<!-- /ADVANCED -->
						<div class="footer-tools">
							<span class="go-top">
								<i class="fa fa-chevron-up"></i> Top
							</span>
						</div>
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	<script src="../js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="../bootstrap-dist/js/bootstrap.min.js"></script>
	<!-- DATE RANGE PICKER -->
	<script src="../js/bootstrap-daterangepicker/moment.min.js"></script>
	<script src="../js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
	<!-- SLIMSCROLL -->
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
	<!-- BLOCK UI -->
	<script type="text/javascript" src="../js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
	<!-- TYPEHEAD -->
	<script type="text/javascript" src="../js/typeahead/typeahead.min.js"></script>
	<!-- AUTOSIZE -->
	<script type="text/javascript" src="../js/autosize/jquery.autosize.min.js"></script>
	<!-- COUNTABLE -->
	<script type="text/javascript" src="../js/countable/jquery.simplyCountable.min.js"></script>
	<!-- INPUT MASK -->
	<script type="text/javascript" src="../js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
	<!-- FILE UPLOAD -->
	<script type="text/javascript" src="../js/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
	<!-- SELECT2 -->
	<script type="text/javascript" src="../js/select2/select2.min.js"></script>
	<!-- UNIFORM -->
	<script type="text/javascript" src="../js/uniform/jquery.uniform.min.js"></script>
	<!-- JQUERY UPLOAD -->
	<!-- The Templates plugin is included to render the upload/download listings -->
	<script src="../js/blueimp/javascript-template/tmpl.min.js"></script>
	<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="../js/blueimp/javascript-loadimg/load-image.min.js"></script>
	<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script src="../js/blueimp/javascript-canvas-to-blob/canvas-to-blob.min.js"></script>
	<!-- blueimp Gallery script -->
	<script src="../js/blueimp/gallery/jquery.blueimp-gallery.min.js"></script>
	<!-- The basic File Upload plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload.min.js"></script>
	<!-- The File Upload processing plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-process.min.js"></script>
	<!-- The File Upload image preview & resize plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-image.min.js"></script>
	<!-- The File Upload audio preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-audio.min.js"></script>
	<!-- The File Upload video preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-video.min.js"></script>
	<!-- The File Upload validation plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-validate.min.js"></script>
	<!-- The File Upload user interface plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-ui.min.js"></script>
	<!-- The main application script -->
	<script src="../js/jquery-upload/js/main.js"></script>
	<!-- bootbox script -->
	<script src="../js/bootbox/bootbox.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="../js/script.js"></script>
	<script src="../js/jquery-validate/jquery.validate.js"></script>
	<script src="../js/jquery-validate/additional-methods.js"></script>
	<script src="../js/error_message.js"></script>
	<script>
		jQuery(document).ready(function() {		
			App.setPage("forms");  //Set current page
			App.init(); //Initialise plugins and elements
			
		  var id=$("#hide").val();
		  $("#photo2").attr("src","getPhoto?safe_IdUser="+<% if(safeIdUser != 0){out.print(safeIdUser);}else{out.print(1);} %>);
			$("#photo").attr("src","getPhoto?safe_IdUser="+<% if(user != null){out.print(user.getSafe_IdUser());} %>);
               $("#e1").change(function(){
                var id=$(this).children(":selected").attr("id");
                // remove all options, but not the first
                $('#e2 option:gt(0)').remove(); 
                if(id>0){ 
                    $("#e2").removeAttr("disabled");
                    dhtmlxAjax.post("SafeUsers_auxiliary.jsp",
                    "addr_IdState="+id,function (loader){
                    $("#e2").append(loader.xmlDoc.responseText);
         
                });
                }else{
                  $("#e2").attr("disabled","disabled");   
                }
               });
							 
               $("#e3").change(function(){
                var id=$(this).children(":selected").attr("id");
                // remove all options, but not the first
                $('#e4 option:gt(0)').remove(); 
                if(id>0){ 
                    $("#e4").removeAttr("disabled");
                    dhtmlxAjax.post("SafeUsers_auxiliary.jsp",
                    "addr_IdState="+id,function (loader){
                    $("#e4").append(loader.xmlDoc.responseText);
         
                });
                }else{
                  $("#e4").attr("disabled","disabled");   
                }
               });

			          $("#e4").change(function(){
                var id=$(this).children(":selected").attr("id");
                $('#e5 option:gt(0)').remove(); 
                if(id>0){ 
                    $("#e5").removeAttr("disabled");
                    dhtmlxAjax.post("SafeUsers_auxiliary2.jsp",
                    "addr_IdTown="+id,function (loader){
                    $("#e5").append(loader.xmlDoc.responseText);
                });
                }else{
                  $("#e5").attr("disabled","disabled");
                }
               });

			var addOrEdit=<%out.println(request.getParameter("addOrEdit")+";"); %>			
			$("#frm_usuarios").validate({
				ignore: "",
				rules:{
					nombre:{
						required: true, minlength: 3, maxlength: 100
					},
					paterno:{
						required: true, minlength: 3, maxlength: 25
					},
					materno:{
						required: true, minlength: 1, maxlength: 100
					},
					stCiv:{
						required: true
					},
					genero:{
						required: true
					},
					fsang:{
						required: true
					},
					ocupa:{
						required: true
					},
					NSS:{
						minlength: 1, maxlength: 45, digits: true
					},
					stateN:{
						required: true
					},
					townN:{
						required: true
					},
					fNac:{
						required: true
					},
					RFC:{
						maxlength: 13,required: true
					},
					CURP:{
						maxlength: 18, required: true
					},
					Edo:{
						required: true
					},
					Mpo:{
						required: true
					},
					Col:{
						required: true
					},
					calle:{
						required: true, minlength: 3, maxlength: 80
					},
					NumEx:{
						required: true, digits: true
					},
					NumIn:{
						minlength: 1, maxlength: 100
					},
					Tel:{
						minlength: 8, maxlength: 20
					},
					Cel:{
						minlength: 10, maxlength: 20
					},
					Cor:{
						minlength: 3, maxlength: 60
					}
				},
				highlight: function(element){
					$(element).parent().parent().addClass("has-error");
				},
				unhighlight: function(element){
				$(element).parent().parent().removeClass("has-error");
			}
		});

                    var  nombre=$("#nombre");
                    var  paterno=$("#paterno");
                    var  materno=$("#materno");
                    var  stCiv=$("#stCiv");
                    var  genero=$("#genero");
                    var  fsang=$("#fsang");
                    var  ocupa=$("#ocupa");
                    var  stateN=$("#e1");
                    var  townN=$("#e2");
                    var  fNac=$("#fNac");
                    var  RFC=$("#RFC");
                    var  CURP=$("#CURP");
                    var  NSS=$("#NSS");
                    var  Tel=$("#Tel");
                    var  Cel=$("#Cel");
                    var  Cor=$("#Cor");
                    var  Edo=$("#e3");
                    var  Mpo=$("#e4");
                    var  Col=$("#e5");
                    var  calle=$("#calle");
                    var  NumEx=$("#NumEx");
                    var  NumIn=$("#NumIn");
                    var safeIdUser=$("#safeIdUser"); 

			
			$("#nuevo").click(function () {
					window.location = "SafeUsers_catalog.jsp?IdUser=0&addOrEdit=0";
			});
			$("#editar").click(function () {
                            nombre.removeAttr("disabled");
                            paterno.removeAttr("disabled");
                            materno.removeAttr("disabled");
                            stCiv.removeAttr("disabled");
                            genero.removeAttr("disabled");
                            fsang.removeAttr("disabled");
                            ocupa.removeAttr("disabled");
                            stateN.removeAttr("disabled");
                            townN.removeAttr("disabled");
                            fNac.removeAttr("disabled");
                            RFC.removeAttr("disabled");
                            CURP.removeAttr("disabled");
                            NSS.removeAttr("disabled");
                            Tel.removeAttr("disabled");
                            Cel.removeAttr("disabled");
                            Cor.removeAttr("disabled");
                            Edo.removeAttr("disabled");
                            Mpo.removeAttr("disabled");
                            Col.removeAttr("disabled");
                            calle.removeAttr("disabled");
                            NumEx.removeAttr("disabled");
                            NumIn.removeAttr("disabled");
				addOrEdit=1;
				$("#editar").attr("disabled","disabled");
			});
			$("#guardar").click(function () {
				
                            var nombreval =nombre.val().trim();
                            var paternoval =paterno.val().trim();
                            var maternoval =materno.val().trim();
                            var stCivval =stCiv.children(":selected").attr("id");
                            var generoval =genero.children(":selected").attr("id");
                            var fsangval =fsang.children(":selected").attr("id");
                            var ocupaval=ocupa.children(":selected").attr("id");
                            var stateNval=stateN.children(":selected").attr("id");
                            var townNval=townN.children(":selected").attr("id");
                            var fNacval =fNac.val().trim();
                            var RFCval =RFC.val().trim();
                            var CURPval =CURP.val().trim();
                            var NSSval =NSS.val().trim();
                            var Telval =Tel.val().trim();
                            var Celval =Cel.val().trim();
                            var Corval =Cor.val().trim();
                            var Edoval=Edo.children(":selected").attr("id");
                            var Mpoval=Mpo.children(":selected").attr("id");
                            var Colval=Col.children(":selected").attr("id");
                            var calleval =calle.val().trim();
                            var NumExval =NumEx.val().trim();
                            var NumInval =NumIn.val().trim();

				if($("#frm_usuarios").valid()){
							
                             var values= "values="+parseInt($("#hide").val()) +"#"+ parseInt(Colval) +"#"+ calleval +"#"+ 
                                 parseInt(NumExval) +"#"+ NumInval +"#"+ Telval +"#" + Celval  +"#"+ Corval +"#"+
                                 "0" +"#"+ nombreval +"#"+ paternoval +"#"+ maternoval +"#"+ fNacval +"#"+ 
                                 parseInt(stateNval) +"#"+ parseInt(townNval) +"#"+ NSSval +"#"+encodeURIComponent(fsangval)+"#"+ 
                                 generoval +"#"+ RFCval  +"#"+ CURPval +"#"+ ocupaval +"#"+
                                 stCivval+"&addOrEdit="+addOrEdit;
															 
					dhtmlxAjax.post("SafeUsers_save",values,function(loader){
						var resp = loader.xmlDoc.responseText;
						respo = resp.split("#");
						document.getElementById("hide").value=respo[0];
						var resp = parseInt(respo[1]);
						if(resp == 1){
						var id=$("#hide").val();
						$("#form_photo").attr("action","SafeUsers_saveImage?safeId="+id);
						alert("SafeUsers_saveImage?safeId="+id);
						$("#form_photo").submit();
							bootbox.alert("Se guardo con exito!", function() {});
						} else {
							bootbox.alert("Fallo al guardar!", function() {});
						}
					});

                            nombre.attr("disabled","disabled");
                            paterno.attr("disabled","disabled");
                            materno.attr("disabled","disabled");
                            stCiv.attr("disabled","disabled");
                            genero.attr("disabled","disabled");
                            fsang.attr("disabled","disabled");
                            ocupa.attr("disabled","disabled");
                            stateN.attr("disabled","disabled");
                            townN.attr("disabled","disabled");
                            fNac.attr("disabled","disabled");
                            RFC.attr("disabled","disabled");
                            CURP.attr("disabled","disabled");
                            NSS.attr("disabled","disabled");
                            Tel.attr("disabled","disabled");
                            Cel.attr("disabled","disabled");
                            Cor.attr("disabled","disabled");
                            Edo.attr("disabled","disabled");
                            Mpo.attr("disabled","disabled");
                            Col.attr("disabled","disabled");
                            calle.attr("disabled","disabled");
                            NumEx.attr("disabled","disabled");
                            NumIn.attr("disabled","disabled");
					$("#editar").removeAttr("disabled");
				}else{
					bootbox.alert("Datos incorrectos!", function() {});
				}
			});
		});
	</script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
			}else{
				response.sendRedirect("error600.jsp");
			}
		}else{
			response.sendRedirect("error600.jsp");
		}
%>