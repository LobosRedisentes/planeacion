<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.edcore.model.db.DBConnector"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.edcore.model.safe.User"%>
	<%
		HttpSession sesion;
    sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if(user != null){
			ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
			if(privileges.contains("CAT30")){
	%>

<%
	ResultSet rs;
	DBConnector db = new DBConnector();
	PreparedStatement ps;
	Connection con;
	int addOrEdit=Integer.parseInt(request.getParameter("addOrEdit"));
	con=db.open();

                        int progIdSub = Integer.parseInt(request.getParameter("progIdSubject"));
                        int progIdPlan = 0;
                        int progIdSubject = 0;
                        
                        ps = con.prepareStatement("SELECT ProgCompSubject.prog_IdSubject,ProgPlan.prog_IdPlan FROM ProgCompSubject"
                                                + " JOIN ProgSubjects"
                                                + " ON (ProgSubjects.prog_IdSubject= ProgCompSubject.prog_IdSubject)"
                                                + " JOIN ProgPlan"
                                                + " ON (ProgPlan.prog_IdPlan = ProgSubjects.prog_IdPlan)"
                                                + " WHERE ProgCompSubject.prog_IdSubject =" + progIdSub);
                        rs = ps.executeQuery();
                        
                        if(rs != null && rs.next()){
                            progIdPlan = rs.getInt("ProgPlan.prog_IdPlan");
                            progIdSubject = rs.getInt("ProgCompSubject.prog_IdSubject");    
                        }

%>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Indicadores</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
	<link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
	
	<link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- DATE RANGE PICKER -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
	<!-- TYPEAHEAD -->
	<link rel="stylesheet" type="text/css" href="../js/typeahead/typeahead.css" />
	<!-- FILE UPLOAD -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-fileupload/bootstrap-fileupload.min.css" />
	<!-- SELECT2 -->
	<link rel="stylesheet" type="text/css" href="../js/select2/select2.min.css" />
	<!-- UNIFORM -->
	<link rel="stylesheet" type="text/css" href="../js/uniform/css/uniform.default.min.css" />
	<!-- JQUERY UPLOAD -->
	<!-- blueimp Gallery styles -->
	<link rel="stylesheet" href="../js/blueimp/gallery/blueimp-gallery.min.css">
	<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload.css">
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload-ui.css">
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	<style>
	 .error-message, label.error {
	    color: #ff0000;
	    margin:0;
	    display: inline;
	    font-size: 1em !important;
	    font-weight:bold;
	 }
	 </style>
	<!-- LIBRERIA EDCORE -->
	<link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.css" >
	<link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxGrid/codebase/skins/dhtmlxgrid_dhx_skyblue.css" >
  <script src="../dhtmlx/libCompiler/dhtmlxcommon.js"></script>
  <script src="../dhtmlx/dhtmlxWindows/codebase/dhtmlxwindows.js"></script>
  <script src="../dhtmlx/libCompiler/dhtmlxcontainer.js"></script>
  <script src="../dhtmlx/dhtmlxLayout/codebase/dhtmlxlayout.js"></script>
	<script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.js"></script>
	<script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_srnd.js"></script>
	<script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_filter.js"></script>
	<script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgridcell.js"></script>
	<script src="../js/dhxGrid.js"></script>
	<script src="../js/dhxLoad.js"></script>
	<script src="../dhtmlx/dhtmlxDataProcessor/codebase/dhtmlxdataprocessor.js"></script>
	<script src="../dhtmlx/libCompiler/connector.js"></script>
	<script src="../js/spin.js"></script>
	<script src="ProgCompSubject_catalog.js"></script>
</head>
<body>
	<!-- HEADER -->
	<%@include file="header.jsp" %>
	<!--/HEADER -->
	<!-- PAGE -->
	<section id="page">
				<!-- SIDEBAR -->
				<%@include file="sidebar.jsp" %>
				<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<!-- STYLER -->
									
									<!-- /STYLER -->
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="index.jsp">Inicio</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Catalogos</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Reticula Escolar</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="ProgCompSubject_admin.jsp">Indicadores</a>
										</li>
										<li>Nuevo/Editar</li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										<h3 class="content-title pull-left">Cat�logo de Indicadores</h3>
										<span class="date-range pull-right">
										<p class="btn-toolbar">
											<button id="nuevo" class="btn btn-success" ><i class="fa fa-pencil-square-o"></i> Nuevo</button>
											<button id="editar" class="btn btn-success" disabled=""><i class="fa fa-pencil-square-o"></i> Editar</button>
											<button id="guardar" class="btn btn-success" ><i class="fa fa-save"></i> Guardar</button>
										</p>
										</span>
									</div>
									<div class="description"></div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						<!-- ADVANCED -->
						<div class="row">
							<div class="col-md-12">
								<!-- BOX -->
								<div class="box border red">
									<div class="box-title">
										<h4><i class="fa fa-bars"></i>Nuevo/Editar Indicador</h4>
									</div>
									<div class="box-body">
										<form class="form-horizontal " action="#" id="frm_indicadores">
										  <div class="form-group">
											 <label class="col-md-2 control-label" for="e1">Plan:</label> 
											 <div class="col-md-10">
                    <% 
                        ps = con.prepareStatement("SELECT prog_IdPlan , name FROM ProgPlan");
                        rs = ps.executeQuery();

                        out.println("<select id='e1' name='plan' class='col-md-12'>");
                        out.println("<option id='-1' value='' selected>Selecciona ..</option>");
                        while(rs.next()){
                            if(rs.getInt("prog_IdPlan")== (progIdPlan)){
                                 out.println("<option id='"+rs.getInt("prog_IdPlan")+"' selected>"+rs.getString("name")+"</option>");
                            }
                            else{
                                out.println("<option id='"+rs.getInt("prog_IdPlan")+"'>"+rs.getString("name")+"</option>");
                            }
                        }
                        out.println("</select>");
                    %>
											 </div>
										  </div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="e2">Materia:</label> 
											<div class="col-md-10">
                    <%
                        if(progIdSub > 0){
                        ps = con.prepareStatement("SELECT ProgSubjects.name, ProgSubjects.prog_IdSubject FROM ProgSubjects WHERE prog_IdPlan ="+progIdPlan);
                        rs = ps.executeQuery();

                        out.println("<select id='e2' name='mat' class='col-md-12'>");
                        out.println("<option id='-1' value='' selected>Selecciona ..</option>");    
                        while(rs.next()){
                            if(rs.getInt("ProgSubjects.prog_IdSubject")==progIdSubject){
                                 out.println("<option id='"+rs.getInt("ProgSubjects.prog_IdSubject")+"' selected>"+rs.getString("ProgSubjects.name")+"</option>");
                            }
                            else{
                                out.println("<option id='"+rs.getInt("ProgSubjects.prog_IdSubject")+"'>"+rs.getString("ProgSubjects.name")+"</option>");
                            }
                        }
                        out.println("</select>");
                        }else{
                            out.println("<select id='e2' name='mat' class='col-md-12' disabled>"); 
                            out.println("<option id='-1'> </option>");
                            out.println("</select>");
                        }
                    %> 
											</div>
											</div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="nombre">Nombre:</label> 
											<div class="col-md-10">
												<div class="col-md-10" id="grid_container" style="width:100%;height:300px;">
											</div>
											</div>
											<input type="hidden" id="hide" value="<%out.println(progIdPlan);%>">
									   </form>
									</div>
								</div>
								<!-- /BOX -->
							</div>
						</div>
						<!-- /ADVANCED -->
						<div class="footer-tools">
							<span class="go-top">
								<i class="fa fa-chevron-up"></i> Top
							</span>
						</div>
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	<script src="../js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="../bootstrap-dist/js/bootstrap.min.js"></script>
	<!-- DATE RANGE PICKER -->
	<script src="../js/bootstrap-daterangepicker/moment.min.js"></script>
	<script src="../js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
	<!-- SLIMSCROLL -->
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
	<!-- BLOCK UI -->
	<script type="text/javascript" src="../js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
	<!-- TYPEHEAD -->
	<script type="text/javascript" src="../js/typeahead/typeahead.min.js"></script>
	<!-- AUTOSIZE -->
	<script type="text/javascript" src="../js/autosize/jquery.autosize.min.js"></script>
	<!-- COUNTABLE -->
	<script type="text/javascript" src="../js/countable/jquery.simplyCountable.min.js"></script>
	<!-- INPUT MASK -->
	<script type="text/javascript" src="../js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
	<!-- FILE UPLOAD -->
	<script type="text/javascript" src="../js/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
	<!-- SELECT2 -->
	<script type="text/javascript" src="../js/select2/select2.min.js"></script>
	<!-- UNIFORM -->
	<script type="text/javascript" src="../js/uniform/jquery.uniform.min.js"></script>
	<!-- JQUERY UPLOAD -->
	<!-- The Templates plugin is included to render the upload/download listings -->
	<script src="../js/blueimp/javascript-template/tmpl.min.js"></script>
	<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="../js/blueimp/javascript-loadimg/load-image.min.js"></script>
	<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script src="../js/blueimp/javascript-canvas-to-blob/canvas-to-blob.min.js"></script>
	<!-- blueimp Gallery script -->
	<script src="../js/blueimp/gallery/jquery.blueimp-gallery.min.js"></script>
	<!-- The basic File Upload plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload.min.js"></script>
	<!-- The File Upload processing plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-process.min.js"></script>
	<!-- The File Upload image preview & resize plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-image.min.js"></script>
	<!-- The File Upload audio preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-audio.min.js"></script>
	<!-- The File Upload video preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-video.min.js"></script>
	<!-- The File Upload validation plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-validate.min.js"></script>
	<!-- The File Upload user interface plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-ui.min.js"></script>
	<!-- The main application script -->
	<script src="../js/jquery-upload/js/main.js"></script>
	<!-- bootbox script -->
	<script src="../js/bootbox/bootbox.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="../js/script.js"></script>
	<script src="../js/jquery-validate/jquery.validate.js"></script>
	<script src="../js/jquery-validate/additional-methods.js"></script>
	<script src="../js/error_message.js"></script>
	<script>
		jQuery(document).ready(function() {		
			App.setPage("forms");  //Set current page
			App.init(); //Initialise plugins and elements
			$("#photo").attr("src","getPhoto?safe_IdUser="+<% if(user != null){out.print(user.getSafe_IdUser());} %>);
			var addOrEdit=<%out.println(request.getParameter("addOrEdit")+";"); %>
			var grid = new dhtmlXGridObject('grid_container');
			grid.setImagePath("../dhtmlx/dhtmlxGrid/codebase/imgs/");
      grid.setHeader(" ,Competencia,Tipo,Porcentaje (%)");
      grid.setInitWidths("50,300,*,*");
      grid.setColAlign("left,left,left,left");
      grid.setColSorting("center,str,left,left");
      grid.setColTypes("ch,txt,txt,ed");
      grid.setEditable(true);
      grid.init();
			grid.setSkin("clear");
						
			var planVal  = $("#e1").children(":selected").attr("id");
      var values= [];

				if(planVal === "-1"){
               $("#e1").change(function(){
                    var id=$(this).children(":selected").attr("id");
                    $('#e2 option:gt(0)').remove(); 
                
                    if(id>0){ 
                        grid.clearAll();
                        $("#e2").removeAttr("disabled");
                        dhtmlxAjax.post("ProgCompSubject_auxiliary.jsp","prog_IdPlan="+id+"&opt=0",function (loader){
                        $("#e2").append(loader.xmlDoc.responseText);
                        grid.loadXML("ProgCompSubject_catalog_view?idSubject=0");
                    });
                    }else{
                        $("#e2").attr("disabled","disabled");
                    }
               });
               $("#e2").change(function(){
                   grid.clearAll();
                   var req = $(this).children(":selected").attr("id");
                   grid.load("ProgCompSubject_catalog_view?idSubject="+req , function (){
                        if( req > 0 ){                       
                            dhtmlxAjax.post("ProgCompSubject_auxiliary.jsp","prog_IdPlan="+req+"&opt=1",function (loader){
                                 var values = loader.xmlDoc.responseText;
                                 var result = values.split("#");
                                 alert(values);
                                 var size = result.filter(function (value){
                                    return value 
                                 }).length;

                                 var resultSize = (size - 1);
                                 
                                 for(var i = 0 ; i < resultSize ; i++){
                                       var data = result[i];
                                       add(data);
                                       alert(data);
                                       var splited = data.split("-");
                                       alert(splited);
                                       var idRow = parseInt(splited[0]);
                                       var percent = splited[1];
                                       grid.cellById(idRow,0).setValue("1");
                                       grid.cellById(idRow,3).setValue(percent);
                                 }
                            });
                         }
                    });
                            grid.attachEvent("onCheck",function(id,e){
                                 var idItem = id ;//ID del Registro
                                 var x = grid.cells(idItem,0).getValue();//status
                                 var status = parseInt(x);
                                 var y = grid.cells(idItem,3).getValue();//Porcentaje
                                 var z = $("#e2").children(":selected").attr("id");
                                 var data = String(idItem+"-"+y);
                                      if (status === 1) {
                                          console.log("Valor " + data + " checked");
                                          //grid.CellById(idItem,3).setEditable(true);
                                          add(data);
                                      } 

                                      else if (status === 0) {
                                          console.log("Valor " + data + " unchecked");
                                          remove(data);
                                          //grid.cells(idItem,3).setEditable(false);
                                      }
                                 showValues();
                                 e.stopPropagation();
                           });
                });
				}else{
                   $(document).ready(function (){
                        $("#e1").attr("disabled","disabled");
                        var req = $("#e2").children(":selected").attr("id");
                        grid.load("ProgCompSubject_catalog_view",function (){
                        dhtmlxAjax.post("ProgCompSubject_auxiliary.jsp","prog_IdPlan="+req+"&opt=1",function (loader){
                                 var response = loader.xmlDoc.responseText;
                                 console.log("Respuesta del Servidor :"+response);
                                 var result = response.split("#");
                                 console.log("Respuesta del Split    :"+result);
                                 
                                 var size = parseInt(result.filter(function (value){
                                    return value 
                                 }).length);
                                 console.log("Tama�o de valores del resultado :" + size);

                                 var resultSize = parseInt((size - 1));
                                 console.log("Elementos validos a recorrer : " +resultSize);

                                 if(resultSize > 0){
                                      for(var i = 0 ; i < resultSize ; i++){
                                           var data = result[i];
                                           add(data);
                                           var splited = data.split("-");
                                           var idRow = parseInt(splited[0]);
                                           var percent = splited[1];
                                           grid.cellById(idRow,0).setValue("1");
                                           grid.cellById(idRow,3).setValue(percent);
                                      }
                                 }
                           });
                       });
                       grid.attachEvent("onCheck",function(id,e){
                                 var idItem = id ;
                                 var x = grid.cells(idItem,0).getValue();
                                 var status = parseInt(x);
                                 var y = grid.cells(idItem,3).getValue();
                                 var data = String(idItem+"-"+y);

                                      if (status === 1) {
                                          console.log("Valor " + data + " checked");
                                          add(data);
                                      } 

                                      else if (status === 0) {
                                          console.log("Valor " + data + " unchecked");
                                          remove(data);
                                      }
                                 showValues();
                                 e.stopPropagation();
                        });


                           $("#e1").change(function(){
                              var id=$(this).children(":selected").attr("id");
                              // remove all options, but not the first
                              $('#e2 option:gt(0)').remove(); 

                                if(id > 0){
                                    grid.clearAll();
                                    dhtmlxAjax.post("ProgCompSubject_auxiliary.jsp.jsp","prog_IdPlan="+id+"&opt=0",function (loader){
                                    $("#e2").append(loader.xmlDoc.responseText);
                                    grid.loadXML("ProgCompSubject_catalog_view");
                                    $("#e2").removeAttr("disabled");
                                });
                                }else{
                                  $("#e2").attr("disabled","disabled");   
                                }

                            });
                            
                        $("#e2").change(function(){
                            emptyArray();
                            grid.clearAll();
                            var req  = $(this).children(":selected").attr("id");
                            grid.load("ProgCompSubject_catalog_view" ,function () {
                                if( req > 0 ){
                                    dhtmlxAjax.post("ProgCompSubject_auxiliary.jsp","prog_IdPlan="+req+"&opt=1",function (loader){
                                        var response = loader.xmlDoc.responseText;
                                        var result = response.split("#");
                                        var size = result.filter(function (value){
                                           return value 
                                        }).length;

                                        var resultSize = (size - 1);

                                        if(resultSize > 0){
                                           for(var i = 0 ; i < resultSize ; i++){
                                                var data = result[i];
                                                add(data);
                                                var splited = data.split("-");
                                                var idRow = parseInt(splited[0]);
                                                var percent = splited[1];
                                                grid.cellById(idRow,0).setValue("1");
                                                grid.cellById(idRow,3).setValue(percent);
                                           }
                                        }
                                    });
                                }
                            });

                            grid.attachEvent("onCheck",function(id,e){
                                 var idItem = id ;
                                 var x = grid.cells(idItem,0).getValue();
                                 var status = parseInt(x);
                                 var y = grid.cells(idItem,3).getValue();
                                 var data = String(idItem+"-"+y);
                                      if (status === 1) {
                                          console.log("Valor " + data + " checked");
                                          add(data);
                                      } 

                                      else if (status === 0) {
                                          console.log("Valor " + data + " unchecked");
                                          remove(data);
                                      }
                                 e.stopPropagation();
                           });
                         });
                    });
				}
			
			$("#frm_indicadores").validate({
				rules:{
					plan:{
						required: true
					},
					mat:{
						required: true
					}
				},
				highlight: function(element){
					$(element).parent().parent().addClass("has-error");
				},
				unhighlight: function(element){
				$(element).parent().parent().removeClass("has-error");
			}});

			$("#nuevo").click(function () {
				$("#frm_indicadores").valid();
				//window.location = "ProgCompSubject_catalog.jsp?progIdSubject=0&addOrEdit=0";
			});
			$("#editar").click(function () {
				action_catalog_editComp();
				addOrEdit=1;
				$("#editar").attr("disabled","disabled");
			});
			
			$("#guardar").click(function () {
				
				if($("#frm_indicadores").valid()){
					
     var plan = $('#e1');
     var planName = $('#e2');
        
        plan.attr('disabled','disabled');
        planName.attr('disabled','disabled');
        
    values="";
    var x=0;
    var idReq =  planName.children(":selected").attr("id");//cacha el valor de la materia base
    grid.forEachRow(function(id){
        if(parseInt(grid.cells(id,0).getValue())==1){
            values+=id+"-"+grid.cells(id,3).getValue()+"#";
            x+=parseInt(grid.cells(id,3).getValue());
        }
    });
    if(x==100){
    var data = "idSubject="+idReq+"&values="+values;
    var url = "ProgCompSubject_save";
    var request = url+"?"+data+"&addOrEdit="+addOrEdit;
		alert(request);
    dhtmlxAjax.get(request,function(loader){
       var resp = loader.xmlDoc.responseText;
       if(resp === "1"){
           bootbox.alert("Se guardo con exito!", function() {});
       }else{
						bootbox.alert("Fallo al guardar!", function() {});
			 }
    });
    grid.setEditable(false);
    }else{
        dhtmlx.message({
                    type:"alert",
                    title:"Status",
                    text:"El porcentaje debe ser igual a 100."
        }); 
    }
					
					
					
					var values= "values="+parseInt(hide.val())+"#"+claveVal+"#"+abreviacionVal+"#"+nombreVal+
               "#"+tipoVal+"#"+descripcionVal+"&addOrEdit="+addOrEdit;
					spInit();
					dhtmlxAjax.post("ProgCompetences_save",values,function(loader){
						var resp = loader.xmlDoc.responseText;
						respo = resp.split("#");
						document.getElementById("hide").value=respo[0];
						var resp = parseInt(respo[1]);
						if(resp == 1){
							bootbox.alert("Se guardo con exito!", function() {});
						} else {
							bootbox.alert("Fallo al guardar!", function() {});
						}
						spStop();
					});

					clave.attr("disabled","disabled");
          abreviacion.attr("disabled","disabled");
          nombre.attr("disabled","disabled");
          tipo.attr("disabled","disabled");
          descripcion.attr("disabled","disabled");

					$("#editar").removeAttr("disabled");
				}
			});
		});
	</script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
			}else{
				response.sendRedirect("error600.jsp");
			}
		}else{
			response.sendRedirect("error600.jsp");
		}
%>