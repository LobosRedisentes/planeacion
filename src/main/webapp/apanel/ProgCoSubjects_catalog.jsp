<%@page import="com.edcore.model.safe.User"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.edcore.model.db.DBConnector"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.edcore.model.safe.User"%>
	<%
		HttpSession sesion;
    sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if(user != null){
			ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
			if(privileges.contains("CAT28")){
	%>
<%
	ResultSet rs;
	DBConnector db = new DBConnector();
	PreparedStatement ps;
	Connection con;
	
int addOrEdit=Integer.parseInt(request.getParameter("addOrEdit"));

	con=db.open();
                        int progIdCS = Integer.parseInt(request.getParameter("progIdCS"));
                        int progIdPlan = 0;
                        int progIdBase = 0;
                        ps = con.prepareStatement("SELECT ProgPlan.prog_IdPlan,ProgSubjects.prog_IdSubject FROM ProgCoSubjects"
                                +" JOIN ProgSubjects ON (ProgCoSubjects.prog_IdBaseSubject = ProgSubjects.prog_IdSubject)"
                                +" JOIN ProgPlan ON (ProgSubjects.prog_IdPlan = ProgPlan.prog_IdPlan)"
                                +" WHERE ProgCoSubjects.prog_IdCS =" + progIdCS );
                        rs = ps.executeQuery();
                        
                        if(rs != null && rs.next()){
                            progIdPlan = rs.getInt("ProgPlan.prog_IdPlan");
                            progIdBase = rs.getInt("ProgSubjects.prog_IdSubject");
                        }
%>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Corequisitos</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
	<link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
	
	<link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- FILE UPLOAD -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-fileupload/bootstrap-fileupload.min.css" />
	<!-- SELECT2 -->
	<link rel="stylesheet" type="text/css" href="../js/select2/select2.min.css" />
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	<style>
	 .error-message, label.error {
	    color: #ff0000;
	    margin:0;
	    display: inline;
	    font-size: 1em !important;
	    font-weight:bold;
	 }
	 </style>
	<!-- LIBRERIA EDCORE -->
	<link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.css" >
  <script src="../dhtmlx/libCompiler/dhtmlxcommon.js"></script>
	<script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.js"></script>
	<script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_srnd.js"></script>
	<script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_filter.js"></script>
	<script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgridcell.js"></script>
	<script src="../js/dhxGrid.js"></script>
	<script src="../js/dhxLoad.js"></script>
	<script src="../dhtmlx/dhtmlxDataProcessor/codebase/dhtmlxdataprocessor.js"></script>
	<script src="../dhtmlx/libCompiler/connector.js"></script>
	<script>
		var values = [];
	</script>
	<script src="ProgReqSubjects.js"></script>
</head>
<body>
	<!-- HEADER -->
	<%@include file="header.jsp" %>
	<!--/HEADER -->
	<!-- PAGE -->
	<section id="page">
				<!-- SIDEBAR -->
				<%@include file="sidebar.jsp" %>
				<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<!-- STYLER -->
									
									<!-- /STYLER -->
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="index.jsp">Inicio</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Catalogos</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Reticula Escolar</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="ProgCoSubjects_admin.jsp">Corequisitos</a>
										</li>
										<li>Nuevo/Editar</li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										<h3 class="content-title pull-left">Cat�logo - Corequisitos</h3>
										<span class="date-range pull-right">
										<p class="btn-toolbar">
											<button id="nuevo" class="btn btn-success" ><i class="fa fa-pencil-square-o"></i> Nuevo</button>
											<button id="guardar" class="btn btn-success" ><i class="fa fa-save"></i> Guardar</button>
										</p>
										</span>
									</div>
									<div class="description"></div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						<!-- ADVANCED -->
						<div class="row">
							<div class="col-md-12">
								<!-- BOX -->
								<div class="box border red">
									<div class="box-title">
										<h4><i class="fa fa-bars"></i>Nuevo/Editar Corequisito</h4>
									</div>
									<div class="box-body">
										<form class="form-horizontal " action="#" id="frm_roles">
										  <div class="form-group">
											 <label class="col-md-2 control-label" for="e1">Plan:</label> 
											 <div class="col-md-10">
                     <%
                        ps = con.prepareStatement("SELECT  prog_IdPlan, officialCode FROM ProgPlan");
                        rs = ps.executeQuery();
                        out.println("<select id='e1' name='plan' class='col-md-12'>");                         
                        out.println("<option id='-1' value='' selected>Selecciona ..</option>");                          
                        while(rs.next()){
                            if(rs.getInt("prog_IdPlan") == progIdPlan){
                                 out.println("<option id='"+rs.getInt("prog_IdPlan")
                                         +"' selected>"+rs.getString("officialCode")+"</option>");
                             }else{
                                 out.println("<option id='"+rs.getInt("prog_IdPlan")
                                         +"'>"+rs.getString("officialCode")+"</option>");
                             }
                         }                         
                         out.println("</select>");                                
                    %>
											 </div>
										  </div>
											<div class="form-group">
											<label class="col-md-2 control-label" for="e2">Materia Base:</label> 
											<div class="col-md-10">
                   <% 
                        if(progIdPlan>0){
                        con=db.open();                                           
                        ps = con.prepareStatement("SELECT ProgSubjects.name,ProgSubjects.prog_IdSubject FROM ProgSubjects;");
                        rs = ps.executeQuery();
                        out.println("<select id='e2' name='planName'  class='col-md-12' disabled>");                         
                        out.println("<option id='-1' value='' selected>Seleccione ..</option>");                           
                        while(rs.next()){
                            if(rs.getInt("ProgSubjects.prog_IdSubject") == progIdBase){
                                 out.println("<option id='"+rs.getInt("ProgSubjects.prog_IdSubject")
                                         +"' selected>"+rs.getString("ProgSubjects.name")+"</option>");
                             }else{
                                 out.println("<option id='"+rs.getInt("ProgSubjects.prog_IdSubject")
                                         +"'>"+rs.getString("ProgSubjects.name")+"</option>");
                             }
                         }                         
                         out.println("</select>");
                        }else{
                            out.println("<select id='e2' name='planName'  class='col-md-12' disabled>"); 
                            out.println("<option id='-1' value='' selected>Seleccione ..</option>");
                            out.println("</select>");
                        }
												 db.close();
                   %>
											</div>
											</div>
											<div class="form-group">
											<div class="col-md-10" id="grid_container" style="width:100%;height:300px;">
											</div>
											</div>
											<input type="hidden" id="text" value="<%out.println(progIdPlan);%>">
									   </form>
									</div>
								</div>
								<!-- /BOX -->
							</div>
						</div>
						<!-- /ADVANCED -->
						<div class="footer-tools">
							<span class="go-top">
								<i class="fa fa-chevron-up"></i> Top
							</span>
						</div>
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	<script src="../js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="../bootstrap-dist/js/bootstrap.min.js"></script>
	<!-- TYPEHEAD -->
	<script type="text/javascript" src="../js/typeahead/typeahead.min.js"></script>
	<!-- AUTOSIZE -->
	<script type="text/javascript" src="../js/autosize/jquery.autosize.min.js"></script>
	<!-- COUNTABLE -->
	<script type="text/javascript" src="../js/countable/jquery.simplyCountable.min.js"></script>
	<!-- INPUT MASK -->
	<script type="text/javascript" src="../js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
	<!-- SELECT2 -->
	<script type="text/javascript" src="../js/select2/select2.min.js"></script>
	<!-- UNIFORM -->
	<script type="text/javascript" src="../js/uniform/jquery.uniform.min.js"></script>
	<script src="../js/jquery-upload/js/jquery.fileupload.min.js"></script>
	<!-- bootbox script -->
	<script src="../js/bootbox/bootbox.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="../js/script.js"></script>
	<script src="../js/jquery-validate/jquery.validate.js"></script>
	<script src="../js/jquery-validate/additional-methods.js"></script>
	<script src="../js/error_message.js"></script>
	<script>
		jQuery(document).ready(function() {		
			App.setPage("forms");  //Set current page
			App.init(); //Initialise plugins and elements
			$("#photo").attr("src","getPhoto?safe_IdUser="+<% if(user != null){out.print(user.getSafe_IdUser());} %>);
			
      var planVal  = $("#e1").children(":selected").attr("id");
			var grid = new dhtmlXGridObject('grid_container');
			grid.setImagePath("../dhtmlx/dhtmlxGrid/codebase/imgs/");
      grid.setHeader("Clave,Nombre");
      grid.setInitWidths("*,*");
      grid.setColAlign("left,left");
      grid.setColSorting("center,str");
      grid.setColTypes("ch,txt");
      grid.setEditable(true);
      grid.init();
      grid.setSkin("clear");
			grid.objBox.style.overflowX = "hidden";
			
         if(planVal === "-1"){
                $("#e1").change(function(){
                    var id=$(this).children(":selected").attr("id");
                    $('#e2 option:gt(0)').remove(); 
                
                if(id>0){
                    grid.clearAll();
                    $("#e2").removeAttr("disabled");
                    dhtmlxAjax.post("ProgCoSubjects_auxiliary.jsp","prog_IdPlan="+id+"&opt=0",function (loader){
                        $("#e2").append(loader.xmlDoc.responseText);
                        grid.loadXML("ProgCoSubjects_view?idenv="+id);   
                    });
                } else {
                  $("#e2").attr("disabled","disabled");   
                }                
                });
               
               $("#e2").change(function(){
									 values.length = 0;
                   var plan = $("#e1").children(":selected").attr("id");
                   grid.clearAll();
                   var req  = $(this).children(":selected").attr("id");
                   
                   grid.load("ProgCoSubjects_view?idenv="+plan, function () {
                   
                   if( req > 0 ){
                       dhtmlxAjax.post("ProgCoSubjects_auxiliary.jsp","prog_IdPlan="+req+"&opt=1",function (loader){
                           
                           var response = loader.xmlDoc.responseText;
                           
                           var result = response.split("#");
                           
                           var size = result.filter(function (value){
                              return value 
                           }).length;
                           //console.log(size);
                           
                           var resultSize = (size - 1);
                           //console.log(resultSize);
                           
                           if(resultSize > 0){
                                for(var i = 0 ; i < resultSize ; i++){
                                    var idRow = parseInt(result[i]);
                             //           console.log(result[i]);
                                        grid.cellById(idRow,0).setValue("1");
                                        //console.log("Valor "+idRow+" Marcado");
																		values.push(idRow);
                                    var index = values.indexOf(idRow);
                                    //console.log("Indice del valor en el arreglo : " + index);
                                }
                           }
                       });
                   }
                   
                   grid.attachEvent("onCheck",function(id,e){
                         var idItem = id ;
                         var x = grid.cells(idItem,0).getValue();
                         var status = parseInt(x);
                              
                              if (status === 1) {
																		values.push(idItem);
                              } 

                              else if (status === 0) {
                                    //console.log("Valor " + idItem + " unchecked");
                                    remove(idItem);
                              }
                           showValues();
                              
                         e.stopPropagation();
                   });
                });
                });
                
    } else{
            $("#e1").attr("disabled","disabled");
               var req = $("#e2").children(":selected").attr("id");
                   grid.load("ProgCoSubjects_view?idenv="+planVal, function(){
									dhtmlxAjax.post("ProgCoSubjects_auxiliary.jsp","prog_IdPlan="+req+"&opt=1",function (loader){
                   var response = loader.xmlDoc.responseText;
                       //console.log("Respuesta del Servidor :"+response);
                           
                   var result = response.split("#");
                       //console.log("Respuesta del Split    :"+result);
                           
                   var size = result.filter(function (value){
                       return value 
                   }).length;
                       //console.log("Tama�o de valores del resultado :" + size);
                           
                    var resultSize = (size - 1);
                    //console.log("Elementos validos a recorrer : " +resultSize);
                           
                    if(resultSize > 0){
                       
                        for(var i = 0 ; i < resultSize ; i++){
                            var idRow = parseInt(result[i]);
                                grid.cellById(idRow,0).setValue("1");
                                //console.log("Valor "+idRow+" Marcado");
                                values.push(idRow);
                            var index = values.indexOf(idRow);
                                //console.log("Indice del valor en el arreglo : " + index);
                        }
                     }
                 });
                 
                grid.attachEvent("onCheck",function(id){
                           var idItem = id ;
                           var x = grid.cells(idItem,0).getValue();
                           var status = parseInt(x);
                                if (status === 1) {
                                    //console.log("Valor " + idItem + " checked");
																		values.push(idItem);
                                } 

                                else if (status === 0) {
                                    //console.log("Valor " + idItem + " unchecked");
                                    remove(idItem);
                                }
                 });
                 });
                     
                     
                     $("#e1").change(function(){
                        var id=$(this).children(":selected").attr("id");
                        // remove all options, but not the first
                        $('#e2 option:gt(0)').remove(); 

                        if(id > 0){
                            grid.clearAll();
                            dhtmlxAjax.post("ProgCoSubjects_auxiliary.jsp","prog_IdPlan="+id+"&opt=0",function (loader){
                            $("#e2").append(loader.xmlDoc.responseText);
                            grid.loadXML("ProgCoSubjects_view?idenv="+id);
                            $("#e2").removeAttr("disabled");
                        });
                        }else{
                          $("#e2").attr("disabled","disabled");   
                        }
                    
               });
               
               $("#e2").change(function(){
                  values.length = 0;

                   var plan = $("#e1").children(":selected").attr("id");
                   grid.clearAll();
                   var req  = $(this).children(":selected").attr("id");
                   grid.load("ProgCoSubjects_view?idenv="+plan , function (){ 
                   if( req > 0 ){
                       dhtmlxAjax.post("ProgCoSubjects_auxiliary.jsp","prog_IdPlan="+req+"&opt=1",function (loader){
                           var response = loader.xmlDoc.responseText;
                           var result = response.split("#");
                           size = result.filter(function (value){
                              return value 
                           }).length;

                           var resultSize = (size - 1);
                           
                           if(resultSize > 0){
                                
                                for(var i = 0 ; i < resultSize ; i++){
                                    var idRow = parseInt(result[i]);
                                    grid.cellById(idRow,0).setValue("1");
                                    values.push(idRow);
                                    var index = values.indexOf(idRow);
                                }
                           }
                       });
                   }
                   
                   grid.attachEvent("onCheck",function(id,e){
                         var idItem = id ;
                         var x = grid.cells(idItem,0).getValue();
                         var status = parseInt(x);
                              
                              if (status === 1) {
																		values.push(idItem);
                              } 

                              else if (status === 0) {
                                    //console.log("Valor " + idItem + " unchecked");
                                    remove(idItem);
                              }
                           showValues();
                              
                         e.stopPropagation();
                   });
                });
                });
    }

			
			$("#nuevo").click(function () {
				window.location = "ProgCoSubjects_catalog.jsp?progIdCS=0&addOrEdit=0";
			});
			$("#guardar").click(function () {

				var plan = $('#e1');
				var planName = $('#e2');
        plan.attr('disabled','disabled');
        planName.attr('disabled','disabled');
		    
				var idReq   = $('#e2').children(":selected").attr("id");
				var data = "values="+idReq+","+values;
				var url = "ProgCoSubjects_save";
				var request = url+"?"+data;
        dhtmlxAjax.get(request,function(loader){
           var resp = loader.xmlDoc.responseText;
           if (resp === "1"){
							bootbox.alert("Se guardo con exito!", function() {});
           }else{
							bootbox.alert("Fallo al guardar!", function() {});
					 }
        });
			});
		});
	</script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
			}else{
				response.sendRedirect("error600.jsp");
			}
		}else{
			response.sendRedirect("error600.jsp");
		}
%>