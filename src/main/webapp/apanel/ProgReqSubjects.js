//Funciones para Administrador


//Funcion Eliminar
function action_deleteReq(id,base,name,url,response){
    if(action_validateRow(id)){ 
    dhtmlx.confirm({
           title : "Eliminar Requisito",
           type  : "confirm-warning",
           text  : "Deseas eliminar la materia requisito "+name+" de "+base+"?",
           callback: function(res){
               if(res){
                var value= "prog_IdRS="+id;
                var request = url+"?"+value;
                    dhtmlxAjax.post(url,value,function(loader){
                        var resp = loader.xmlDoc.responseText;
                        if(resp === "1"){
                            dhtmlx.message({
                                type:"alert",
                                title:"Status",
                                text:"Se ha eliminado "+name+"."
                            });
                            grid.clearAndLoad(response);
                            tool.disableItem("_edit");
                            tool.disableItem("_delete");
                        }
                    });
                }
            }
     });     
     }
}

//Funciones para Catalogo

//Funcion de Nuevo
function action_catalog_newReq(){
    var plan = $('#plan');
    var planName = $('#planName');
        
        plan.removeAttr('disabled');
        planName.removeAttr('disabled');
        
        plan.val('-1');
        planName.val('-1');
        planName.attr('disabled','disabled');
            tool.disableItem('_edit');
            tool.enableItem('_save');
    grid.clearAll();
    grid.setEditable(true);
}

function action_catalog_editReq(){
    var plan = $('#plan');
    var planName = $('#planName');
    
    tool.enableItem('_new');
    tool.enableItem('_save');
    
    plan.removeAttr('disabled');
    planName.removeAttr('disabled');
    grid.setEditable(true);

}

function action_catalog_saveReq(){
     var plan = $('#plan');
     var planName = $('#planName');
        
        plan.attr('disabled','disabled');
        planName.attr('disabled','disabled');
        
        tool.disableItem('_save');
        tool.enableItem('_new');
     
    
    var idReq   = $('#planName').children(":selected").attr("id");
    var data = "values="+idReq+","+values;
    var url = "SaveRequirement";
        dhtmlxAjax.get(url+"?"+data,function(loader){
           var resp = loader.xmlDoc.responseText;
           if (resp === "1"){
                 dhtmlx.message({
                    type:"alert",
                    title:"Guardado",
                    text:"Exitosamente"
                 }); 
           }
        });
    grid.setEditable(false);
}

//Data Manipulation of the checkboxes
//var values = [];

function add(item){
    values.push(item);
}

function remove(item){
    var index = values.indexOf(item);

    values.splice(index,1);
    
    showValues();
    
}

function showValues(){
    var x = values.length;
    if (x > 0){
        for(var i = 0 ; i < x ; i++){
        }
    }
}

function emptyArray(){
    values.length = 0;
}

