//Funciones del administracion de calificaciones finales
function final_admin(){
    var response = "../getAdminView?table=GradeGroups_view&id=group_IdGroupSubject&parameters=gpo,subject,professor,calendar";
    grid = new dhtmlXGridObject('grid_container');
    grid.setImagePath("../dhtmlx/dhtmlxGrid/codebase/imgs/");
    grid.setHeader("Grupo,Materia,Profesor,Calendario");
		grid.attachHeader("#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter");
    grid.setColAlign("left,left,left,left");
    grid.setColSorting("str,str,str,str");
    grid.setEditable(false);
    grid.setSkin("clear");
    grid.init();
		grid.objBox.style.overflowX = "hidden";
    spInit();
    grid.load(response,function(){
        spStop();
    });
    var docente;
    var materia;
    var IDRow;
    grid.attachEvent("onRowSelect",function(id){
             IDRow = id;
             materia  = grid.cellById(id,1).getValue();
             docente  = grid.cellById(id,2).getValue();
						  $("#editar").removeAttr('disabled','disabled');
    });
    $("#editar").click(function(){
           window.location = ("GroupList_catalog.jsp?idGroupSubject="+IDRow+"&subject="+materia+"&docente="+docente); 
    });
}

//Funciones del catalogo de calificaciones finales
function final_catalog(){
     var response = "GroupList_aux?idGroupSubject="+idGroupSubject; 
     grid = new dhtmlXGridObject('grid_container');
     grid.setImagePath("../dhtmlx/dhtmlxGrid/codebase/imgs/");
     grid.setHeader("No,Matricula,Alumno,Final,Opcion");
		 grid.attachHeader("#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter");
     grid.setInitWidths("80,100,*,80,80");
     grid.setColAlign("left,center,left,center,center");
     grid.setColSorting("str,str,str,str,str");
     grid.setEditable(true);
     grid.init();
		 grid.objBox.style.overflowX = "hidden";
     grid.setSkin("clear");
     spInit();
     grid.load(response,function(){
         spStop();
     });
     $("#guardar").click(function (){
            action_saveFinal();
     });
}