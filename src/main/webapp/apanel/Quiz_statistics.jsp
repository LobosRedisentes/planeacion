
<%@page import="com.edcore.model.safe.User"%>
<%@page import="com.edcore.model.quiz.Quiz_report"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html>
<html lang="es">
<head>
	<%
		HttpSession sesion;
    sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if(user != null){
			ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
			if(privileges.contains("REP04")){
	%>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Encuestas SAT</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
	<link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
	
	<!-- FONTS -->
	<link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	<script src="../js/spin.js"></script>
</head>
<body>
	<!-- HEADER -->
	<%@include file="header.jsp" %>
	<!--/HEADER -->
	<!-- PAGE -->
	<section id="page">
				<!-- SIDEBAR -->
				<%@include file="sidebar.jsp" %>
				<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<!-- STYLER -->
									
									<!-- /STYLER -->
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="index.jsp">Inicio</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Estadisticas</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="Quiz_report.jsp">Encuestas SAT</a>
										</li>
										<li></li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										
									</div>
									<div class="description">
                                                                            <form id="form" role="form">
                                                                                <div class="form-group col-sm-3">
                                                                                      <label class="" for="exampleInputEmail2">Carrera</label>
                                                                                      <select style="width: 100%;" class="form-control caller" name="carrer" id="setCarrera">
                                                                                          <option value='0'>Seleccione...</option>
                                                                                          <%
                                                                                                ArrayList carresr = Quiz_report.Carrers();
                                                                                                for(int x=0; x<carresr.size(); x++){
                                                                                                    String s[] = (String [])carresr.get(x);
                                                                                                    out.println("<option value='"+s[0]+"'>"+s[1]+"</option>");
                                                                                                }
                                                                                          %>
                                                                                          
                                                                                      </select>
                                                                                </div>
                                                                                <div class="form-group col-sm-3">
                                                                                      <label class="" for="setSemestre">Semestre</label>
                                                                                      <select style="width: 100%;"  class="form-control caller" name="semester" id="setSemestre">
                                                                                          <option value='0'>Seleccione...</option>
                                                                                          <option value="1">1</option>
                                                                                          <option value="2">2</option>
                                                                                          <option value="3">3</option>
                                                                                          <option value="4">4</option>
                                                                                          <option value="5">5</option>
                                                                                          <option value="6">6</option>
                                                                                          <option value="7">7</option>
                                                                                          <option value="8">8</option>
                                                                                          <option value="9">9</option>
                                                                                          <option value="10">10</option>
                                                                                          <option value="11">11</option>
                                                                                          <option value="12">12</option>
                                                                                      </select>
                                                                                </div>
                                                                                <div class="form-group col-sm-3">
                                                                                       <label class="" for="exampleInputPassword2">Pregunta</label>
                                                                                       <select style="width: 100%;"  class="form-control caller" name="question" id="setQuestion">
                                                                                           <option value='0'>Seleccione...</option>
                                                                                           <%
                                                                                                ArrayList questions = Quiz_report.ListQuestionsByQuiz(Integer.parseInt(request.getParameter("quiz")));
                                                                                                for(int x=0; x<questions.size(); x++){
                                                                                                    String s[] = (String [])questions.get(x);
                                                                                                    out.println("<option value='"+s[0]+"'>"+s[1]+"</option>");
                                                                                                }
                                                                                          %>
                                                                                       </select>
                                                                                          <input  type="hidden" value="<% out.print(request.getParameter("quiz").toString()); %>" name="quiz" id="quiz"/>
                                                                                 </div>
                                                                        </form>
									</div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						<!-- ADVANCED -->
						<div class="row">
							<div class="col-md-12">
								<!-- BOX -->
								<div class="box border red">
									<div class="box-title">
										<h4><i class="fa fa-bars"></i>Estadisticas <% out.print(Quiz_report.QuizByName(Integer.parseInt(request.getParameter("quiz").toString()))); %></h4>
									</div>
									<div class="box-body">
										<!-- OVERVIEW -->
										<div id="res"></div>
										<!-- /OVERVIEW -->
									</div>
								</div>
								<!-- /BOX -->
							</div>
						</div>
						<!-- /ADVANCED -->
						<div class="footer-tools">
							<span class="go-top">
								<i class="fa fa-chevron-up"></i> Top
							</span>
						</div>
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	<script src="../js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="../bootstrap-dist/js/bootstrap.min.js"></script>
	<!-- AUTOSIZE -->
	<script type="text/javascript" src="../js/autosize/jquery.autosize.min.js"></script>
	<!-- The basic File Upload plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- BLOCK UI -->
	<script type="text/javascript" src="../js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
	<!-- FLOT CHARTS -->
	<script src="../js/flot/jquery.flot.min.js"></script>
	<script src="../js/flot/jquery.flot.time.min.js"></script>
  <script src="../js/flot/jquery.flot.selection.min.js"></script>
	<script src="../js/flot/jquery.flot.resize.min.js"></script>
  <script src="../js/flot/jquery.flot.pie.min.js"></script>
  <script src="../js/flot/jquery.flot.stack.min.js"></script>
  <script src="../js/flot/jquery.flot.crosshair.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="../js/script.js"></script>
	<script src="../js/error_message.js"></script>
	<script src="../js/charts.js"></script>
	<script>
		jQuery(document).ready(function() {		
			App.setPage("flot_charts");  //Set current page
			App.init(); //Initialise plugins and elements
			$("#photo").attr("src","getPhoto?safe_IdUser="+<% if(user != null){out.print(user.getSafe_IdUser());} %>);
			
              $(".caller").change(function(){
                 send();
                return false;
              }); //submit

            function send(){
								spInit();
                $.ajax({
                  type: "POST",
                  url: "QuizSheetReport",
                  async: true,
                  //Serializamos los datos del Form. Los parámetros son los NAME del formulario, no los id
                  data: $(document.getElementById("form")).serialize(),
                  success: function(data){
                      $("#res").html(data);
											spStop();
                      return true;
                  },
                  error: function(xml,msg){
                    $("#res").text(msg);
										spStop();
                  }
                }); //$.ajax
                
            }
						
						send();
		});
	</script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
			}else{
				response.sendRedirect("error600.jsp");
			}
		}else{
			response.sendRedirect("error600.jsp");
		}
%>
