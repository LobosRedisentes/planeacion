/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function reload_a(url){
    grid.clearAndLoad(url);
}
function generateLock_a(){
    var popUpWindow=layout.dhxWins.createWindow("popUp"+ new Date().getTime(),0,0,500,300);
    popUpWindow.setText("Fechas de Reinscripción");
    popUpWindow.button("close").hide();
    popUpWindow.button("minmax1").hide();
    popUpWindow.button("park").hide();
    var popUpLayout=popUpWindow.attachLayout("1C");
    popUpLayout.cells("a").hideHeader();
    var popUpTool=popUpLayout.attachToolbar();
    popUpTool.setIconsPath("../../icon/");
    popUpTool.setSkin("dhx_skyblue");
    var x = "_exit=enabled";
    popUpTool.loadXML("../../getToolBar?"+x);
    popUpTool.attachEvent("onClick",
        function (id){
            switch(id){
                case "_exit" : 
                    document.body.appendChild(document.getElementById('vp'));
                    popUp.close();
                break
            }
        }
    );
    var obj=document.getElementById("vp");
    popUpLayout.cells("a").attachObject(obj);
    popUp=popUpWindow;
}
function send_a(){
	
    var response= $.ajax({
        url:"locks_admin_generateLocks",
        data:"",
        method:"POST",
        beforeSend: function(){
					spInit();
        }
    });
    response.done(function(){
			spStop();
        bootbox.alert("la operacion fue exitosa");
    });
    response.error(function(){
			spStop();
        bootbox.alert("ocurrio un error en la operacion");
    })
}
function fill_calendar(){
    var response=$.ajax({
        url:"locks_admin_fillActiveCalendar",
        data:"",
        method:"POST"
    });
    response.done(function(msg){
        $("#calendar").val(msg);
    });
}
function exit(win_id){
    action_closeWindow(win_id);
}
function edit_c(){
    $("#mat").removeAttr("disabled");
    $("#date").removeAttr("disabled");
    $("#mount").removeAttr("disabled");
    $("#concept").removeAttr("disabled");
    $("#dept").removeAttr("disabled");
    $("#status").removeAttr("disabled");
}
function save_c(){
    $("#form").submit();
}
function send(){
    //event.preventDefault();
    var response;
    var data;
    var url;
    if(addOrEdit==0){
        url="locks_catalog_insert";
        data="idStud="+$("#idStud").val()+"&department="+$("#dept").children(":selected").attr("id")+"&concept="+$("#concept").children(":selected").attr("id")+"&startDate="+$("#date").val()+"&amount="+$("#mount").val()+"&startDate="+$("#date").val()+"&status="+$("#status").val();
        //int idStud,int department,String concept,String startDate,String status,float amount
    }else if(addOrEdit==1){
        url="locks_catalog_update";
        data="idLock="+$("#hide").val()+"&department="+$("#dept").children(":selected").attr("id")+"&concept="+$("#concept").children(":selected").attr("id")+"&startDate="+$("#date").val()+"&amount="+$("#mount").val()+"&status="+$("#status").val();
    }
    response=$.ajax({
        url:url,
        data: data,
        method:"POST"
        /*beforesend:function(){
            
        },*/
    });
    response.done(function(msg){
            var respo=msg.split("&");
            if(1==respo[1]){
                $("#hide").val(respo[0]);
                $("#mat").attr("disabled","disabled");
                $("#stu").attr("disabled","disabled");
                $("#date").attr("disabled","disabled");
                $("#mount").attr("disabled","disabled");
                $("#concept").attr("disabled","disabled");
                $("#dept").attr("disabled","disabled");
                $("#status").attr("disabled","disabled");
                $("#editar").removeAttr("disabled");
                addOrEdit=1;
                bootbox.alert("guardado exitoso");
            }else if(2==respo[1]){
                bootbox.alert("El registro ya existe");
            }else if(0==respo[1]){
                bootbox.alert("Ocurrio un error al guardar");
            }else{
                bootbox.alert("Error : "+msg);
            }
            
        });
}
//llena el catalogo al editar o el combo box cuando es nuevo
function fill_c(){
        if(addOrEdit==1){
            //llenar el catalogo
            var response;
            var id=$("#hide").val();
            response=$.ajax({
                url:"locks_catalog_filled",
                data:"id="+id,
                method:"POST"
            });
            response.done(function(msg){
                var data=msg.split("_");
                $("#dept").append(data[8]);
                var string=data[5];
                var dateTime=string.split(" ");
                $("#mat").val(data[0]);//matricula
                $("#stu").val(data[1]);//nombre
                $("#idStud").val(data[2]);//idUserAccount
                $("#dept").children("#"+data[3]).attr("selected","selected");//selecciona departamento
								if(data[4] == "ADEUDO DE INSCRIPCION"){
									$("#concept option:eq(1)").prop("selected","selected");//selecciona descripcion
								}
								if(data[4] == "ADEUDO DE REINSCRIPCION"){
									$("#concept option:eq(2)").prop("selected","selected");//selecciona descripcion
								}
								if(data[4] == "ADEUDO DE MATERIAL"){
									$("#concept option:eq(3)").prop("selected","selected");//selecciona descripcion
								}
								if(data[4] == "ADEUDO DE CONVALIDACION"){
									$("#concept option:eq(4)").prop("selected","selected");//selecciona descripcion
								}
								if(data[4] == "ADEUDO DE REVALIDACION"){
									$("#concept option:eq(5)").prop("selected","selected");//selecciona descripcion
								}
								if(data[4] == "ADEUDO EQUIPAMIENTO"){
									$("#concept option:eq(6)").prop("selected","selected");//selecciona descripcion
								}
                //$("#concept").children("#"+data[4]).attr("selected","selected");//selecciona descripcion
                $("#date").val(dateTime[0]);//fecha de inicio
                $("#status").children("#"+data[6]).attr("selected","selected");//status
                $("#mount").val(data[7]);//monto
                $("#mat").attr("disabled","disabled");
                $("#editar").removeAttr("disabled");
            });
            response.error(function(){
                
            });
        }else{
            var response;
            response=$.ajax({
                url:"locks_catalog_departments",
                method:"POST"
            });
            response.done(function(msg){
                $("#dept").append(msg);
                $("#editar").attr("disabled","disabled");
            });
            response.error(function(){
            });
        }
    }
//trae los datos del estudiante
function studentSearch(obj){
    var mat=$(obj).val();
    if(mat.length==8 && $.isNumeric(mat)){
        $.ajax({
            url:"locks_catalog_searchStudent",
            data:"code="+mat,
            method:"POST",
            success:function(msg){
                var data=msg.split("_");
                if (data[0]>0){
                    $("#idStud").val(data[0]);
                    $("#stu").val(data[1]);
                }else{
                    $("#idStud").val(0);
                    $("#stu").val("");
                    $("#mat").val("");
                    bootbox.alert("no hay datos que coincidan");
                }   

            }
            /*,
            error:function(){}*/
        });
    }else{
        bootbox.alert("La matricula no es correcta");
    }
}