<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.edcore.model.db.DBConnector"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.edcore.model.safe.User"%>
	<%
		HttpSession sesion;
    sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if(user != null){
			ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
			if(privileges.contains("CAT23")){
	%>
<%
	DBConnector db = new DBConnector();
    ResultSet rs;
		PreparedStatement ps;
	Connection con;
	con=db.open();    

						int addOrEdit  = Integer.parseInt(request.getParameter("addOrEdit"));
            int idAplicant = Integer.parseInt(request.getParameter("user_IdApplicant"));
            int IdUser     = 0;
            ///////////
            String name="";
            String fLast="";
            String sLast="";
            ///////////
            
            int Prog=0;
            int Cal=0;
            String Cod="";
            String Status="";
            
            if(idAplicant > 0){
                ps = con.prepareStatement("SELECT UserApplicant.safe_IdUser,UserApplicant.prog_IdProgram,"
                        + "UserApplicant.code,UserApplicant.status "
                        + "FROM UserApplicant "
                        + "WHERE UserApplicant.user_IdApplicant='"+idAplicant+"'");
                rs = ps.executeQuery();
                
                while(rs != null && rs.next()){
                    IdUser = rs.getInt("UserApplicant.safe_IdUser");
                    Prog = rs.getInt("UserApplicant.prog_IdProgram");
                    Cod = rs.getString("UserApplicant.code");
                    Status = rs.getString("UserApplicant.status");
                }
            }
%>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Aspirantes</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
	<link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
	
	<link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- DATE RANGE PICKER -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
	<!-- TYPEAHEAD -->
	<link rel="stylesheet" type="text/css" href="../js/typeahead/typeahead.css" />
	<!-- FILE UPLOAD -->
	<link rel="stylesheet" type="text/css" href="../js/bootstrap-fileupload/bootstrap-fileupload.min.css" />
	<!-- SELECT2 -->
	<link rel="stylesheet" type="text/css" href="../js/select2/select2.min.css" />
	<!-- UNIFORM -->
	<link rel="stylesheet" type="text/css" href="../js/uniform/css/uniform.default.min.css" />
	<!-- JQUERY UPLOAD -->
	<!-- blueimp Gallery styles -->
	<link rel="stylesheet" href="../js/blueimp/gallery/blueimp-gallery.min.css">
	<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload.css">
	<link rel="stylesheet" href="../js/jquery-upload/css/jquery.fileupload-ui.css">
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	<style>
	 .error-message, label.error {
	    color: #ff0000;
	    margin:0;
	    display: inline;
	    font-size: 1em !important;
	    font-weight:bold;
	 }
	 </style>
	<!-- LIBRERIA EDCORE -->
  <script src='UserApplicants.js'></script>
  <script src="../dhtmlx/libCompiler/dhtmlxcommon.js"></script>
	<script src="../js/action_dhx.js"></script>
</head>
<body>
	<!-- HEADER -->
	<%@include file="header.jsp" %>
	<!--/HEADER -->
	<!-- PAGE -->
	<section id="page">
				<!-- SIDEBAR -->
				<%@include file="sidebar.jsp" %>
				<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<!-- STYLER -->
									
									<!-- /STYLER -->
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="index.jsp">Inicio</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Inscripciones</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Aspirantes</a>
										</li>
										<li>Nuevo/Editar</li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										<h3 class="content-title pull-left">Cat�logo - Aspirantes</h3>
										<span class="date-range pull-right">
										<p class="btn-toolbar">
											<%if(privileges.contains("CAT39")){%>
											<button id="nuevo" class="btn btn-success" ><i class="fa fa-file-text-o"></i> Nuevo</button>
											<%}%>
											<%if(privileges.contains("CAT40")){%>
											<button id="editar" class="btn btn-success" disabled=""><i class="fa fa-pencil-square-o"></i> Editar</button>
											<%}%>
											<%if(privileges.contains("CAT41")){%>
											<button id="guardar" class="btn btn-success" ><i class="fa fa-save"></i> Guardar</button>
											<%}%>
										</p>
										</span>
									</div>
									<div class="description"></div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						<!-- ADVANCED -->
								  	<form role="form" class="form-horizontal" action="" method="post" name="frm_cuentas" id="frm_aspirante">
										<div class="box border red col-lg-12">
											<div class="box-title">
												<h4>Aspirantes</h4>
												<div class="tools hidden-xs">
												</div>
											</div>
											<div class="box-body">
												<div class="tabbable header-tabs">
												  <ul class="nav nav-tabs">
													 <li class="active"><a id="tab1" href="#box_tab3" data-toggle="tab"> <span class="hidden-inline-mobile"><i class="fa fa-male"></i></span></a></li>
												  </ul>
													  <div class="tab-content">
														 <div class="tab-pane fade in active" id="box_tab3">
															 <div class="row">
															 	<div class="col-md-2">
																	<center>
																		<img height="150px" width="150px" class="img-circle" name="photo2" id="photo2" src="" alt="" />
																	</center>
																</div>
																<div class="col-md-10">
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="e1">Clave:</label>
																		<div class="col-sm-9">
                                                                <%
                                                                if(Cod != null){
                                                                   out.print("<input type='text' class='form-control' id='Cod' name='Cod' value='"+Cod+"' />");
                                                                }else{
                                                                   out.print("<input type='text' class='form-control' id='Cod' name='Cod'/>");
                                                                }
                                                                %>

																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="Name">Usuario:</label>
																		<div class="col-sm-9">
																														<select  id='user' name="user" class='form-control'/> 
                                                            <%
                                                            ps = con.prepareStatement("SELECT SafeUsers.safe_IdUser,SafeUsers.name,SafeUsers.firstLast,SafeUsers.secondLast "
                                                                                    + "FROM SafeUsers ");
                                                            out.println("<option id='0'> </option>");
                                                            rs = ps.executeQuery();
                                                            while(rs.next()){
                                                                    if(Integer.parseInt(rs.getString("safe_IdUser"))== IdUser){
                                                                            out.println("<option id='"+rs.getString("safe_IdUser")+"' selected>"+
                                                                                    rs.getString("name")+" "+rs.getString("firstLast")+" "+rs.getString("secondLast")+"</option>");
                                                                    }else{
                                                                            out.println("<option id='"+rs.getString("safe_IdUser")+"'>"+
                                                                                    rs.getString("name")+" "+rs.getString("firstLast")+" "+rs.getString("secondLast")+"</option>");
                                                                    }
                                                            }
                                                            %>
                                                            </select>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="flName">Programa:</label>
																		<div class="col-sm-9">
																													<select name="program" id='program' class='form-control'/> 
                                                            <%
                                                                ps = con.prepareStatement("SELECT Programs.prog_IdProgram,Programs.name "
                                                                                        + "FROM Programs");
                                                                out.println("<option id='0'> </option>");
                                                                rs = ps.executeQuery();
                                                                while(rs.next()){
                                                                        if(Integer.parseInt(rs.getString("prog_IdProgram"))== Prog){
                                                                                out.println("<option id='"+rs.getString("prog_IdProgram")+"' selected>"+
                                                                                        rs.getString("name")+"</option>");
                                                                        }else{
                                                                                out.println("<option id='"+rs.getString("prog_IdProgram")+"'>"+
                                                                                        rs.getString("name")+"</option>");
                                                                        }
                                                                }
																																db.close();
																																
                                                            %>
                                                            </select>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label" for="slName">Estado:</label>
																		<div class="col-sm-9">
																				<select name="status" id='status' class='form-control'/>
                                                              
                                                                <% 
                                                                    String stCivArray[]={" ","NO INSCRITO","INSCRITO"};
                                                                    for(int i=0;i<3;i++){
                                                                            if(Status.equals(stCivArray[i])){
                                                                                    out.print("<option id='"+stCivArray[i]+"' selected>"+stCivArray[i]+"</option>");
                                                                            }else{
                                                                                    out.print("<option id='"+stCivArray[i]+"'>"+stCivArray[i]+"</option>");
                                                                            }
                                                                    } 
                                                                %>
                                                            </select>	
																		</div>
																	</div>
																		
																</div>
															 </div>
														 </div>
													  </div>
												   </div>
												</div>
											</div>
											<input type='hidden' id='addOrEdit' value='<%out.println(addOrEdit);%>'>
											<input type='hidden' id='idUser' value='<%out.println(IdUser);%>'>
											<input type='hidden' id='idAplicant' value='<%out.println(idAplicant);%>'>
						</form>

						<!-- /ADVANCED -->
						<div class="footer-tools">
							<span class="go-top">
								<i class="fa fa-chevron-up"></i> Top
							</span>
						</div>
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	<script src="../js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="../bootstrap-dist/js/bootstrap.min.js"></script>
	<!-- DATE RANGE PICKER -->
	<script src="../js/bootstrap-daterangepicker/moment.min.js"></script>
	<script src="../js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
	<!-- SLIMSCROLL -->
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="../js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
	<!-- BLOCK UI -->
	<script type="text/javascript" src="../js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
	<!-- TYPEHEAD -->
	<script type="text/javascript" src="../js/typeahead/typeahead.min.js"></script>
	<!-- AUTOSIZE -->
	<script type="text/javascript" src="../js/autosize/jquery.autosize.min.js"></script>
	<!-- COUNTABLE -->
	<script type="text/javascript" src="../js/countable/jquery.simplyCountable.min.js"></script>
	<!-- INPUT MASK -->
	<script type="text/javascript" src="../js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
	<!-- FILE UPLOAD -->
	<script type="text/javascript" src="../js/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
	<!-- SELECT2 -->
	<script type="text/javascript" src="../js/select2/select2.min.js"></script>
	<!-- UNIFORM -->
	<script type="text/javascript" src="../js/uniform/jquery.uniform.min.js"></script>
	<!-- JQUERY UPLOAD -->
	<!-- The Templates plugin is included to render the upload/download listings -->
	<script src="../js/blueimp/javascript-template/tmpl.min.js"></script>
	<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="../js/blueimp/javascript-loadimg/load-image.min.js"></script>
	<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script src="../js/blueimp/javascript-canvas-to-blob/canvas-to-blob.min.js"></script>
	<!-- blueimp Gallery script -->
	<script src="../js/blueimp/gallery/jquery.blueimp-gallery.min.js"></script>
	<!-- The basic File Upload plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload.min.js"></script>
	<!-- The File Upload processing plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-process.min.js"></script>
	<!-- The File Upload image preview & resize plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-image.min.js"></script>
	<!-- The File Upload audio preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-audio.min.js"></script>
	<!-- The File Upload video preview plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-video.min.js"></script>
	<!-- The File Upload validation plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-validate.min.js"></script>
	<!-- The File Upload user interface plugin -->
	<script src="../js/jquery-upload/js/jquery.fileupload-ui.min.js"></script>
	<!-- The main application script -->
	<script src="../js/jquery-upload/js/main.js"></script>
	<!-- bootbox script -->
	<script src="../js/bootbox/bootbox.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="../js/script.js"></script>
	<script src="../js/jquery-validate/jquery.validate.js"></script>
	<script src="../js/jquery-validate/additional-methods.js"></script>
	<script src="../js/error_message.js"></script>
	<script>
		jQuery(document).ready(function() {		
			App.setPage("forms");  //Set current page
			App.init(); //Initialise plugins and elements

		  $("#photo2").attr("src","getPhoto?safe_IdUser="+id);
			$("#photo").attr("src","getPhoto?safe_IdUser="+<% if(user != null){out.print(user.getSafe_IdUser());} %>);
			var addOrEdit=<%out.println(request.getParameter("addOrEdit")+";"); %>      
			$("#frm_aspirantes").validate({
				ignore: "",
				rules:{
					user:{
						required: true
					},
					program:{
						required: true
					},
					status:{
						required: true
					}
				},
				highlight: function(element){
					$(element).parent().parent().addClass("has-error");
				},
				unhighlight: function(element){
				$(element).parent().parent().removeClass("has-error");
			}});

		var val = parseInt($("#addOrEdit").val());
    var codigo  =  $("#Cod").val();
    var usuario =  $("#user").children(":selected").attr("id");
    var program =  $("#program").children(":selected").attr("id");
    var status  =  $('#status').children(":selected").attr("id");
    var idAplicant  =  $('#idAplicant').val();
    var idUser  =  $("#idUser").val();
    var values  =  idAplicant+"#"+idUser+"#"+program+"#"+usuario+""+status;
						

			
			$("#nuevo").click(function () {
					window.location = "UserNvoApplicants_catalog.jsp?user_IdApplicant=0&addOrEdit=0";
			});
			$("#editar").click(function () {
                        identification.removeAttr("disabled");
                        dept.removeAttr("disabled");
                        cert.removeAttr("disabled");
											  code.removeAttr("disabled");
                        addOrEdit=1;
				$("#editar").attr("disabled","disabled");
			});
			$("#guardar").click(function () {
				
                         var codeVal=code.val().trim();
                         var nameVal=name.val().trim();
                         var fistNameVal=fistName.val().trim();
                         var lastNameVal=lastName.val().trim();
                         var identificationVal=identification.val().trim();
                         var deptVal=parseInt(dept.children(":selected").attr('id'));
                         var nomiVal=code.val().trim();
                         var certVal=parseInt(cert.children(":selected").attr('id'));
												 
				if($("#frm_docentes").valid()){
															 													 
                             var values="values="+parseInt(hide.val())+"#"+parseInt(hideProf.val())+"#0#"+deptVal+"#"+ 
                             certVal+"#"+identificationVal+"#"+codeVal+"&addOrEdit="+addOrEdit;
													 
													 alert(values);
													 
					dhtmlxAjax.post("UserProfessors_save",values,function(loader){
						var resp = loader.xmlDoc.responseText;
						respo = resp.split("#");
						document.getElementById("hide").value=respo[0];
						var resp = parseInt(respo[1]);
						if(resp == 1){
							bootbox.alert("Se guardo con exito!", function() {});
						} else {
							bootbox.alert("Fallo al guardar!", function() {});
						}
					});

                            code.attr("disabled","disabled");
                            name.attr("disabled","disabled");
                            fistName.attr("disabled","disabled");
                            lastName.attr("disabled","disabled");
                            identification.attr("disabled","disabled");
                            dept.attr("disabled","disabled");
                            cert.attr("disabled","disabled");

													$("#editar").removeAttr("disabled");
				}else{
					bootbox.alert("Datos incorrectos!", function() {});
				}
			});
		});
	</script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
			}else{
				response.sendRedirect("error600.jsp");
			}
		}else{
			response.sendRedirect("error600.jsp");
		}
%>