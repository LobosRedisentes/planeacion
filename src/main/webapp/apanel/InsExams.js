/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var grid;
function edit_c(){
    $("#cve").removeAttr("disabled");
    $("#name").removeAttr("disabled");
		$("#editar").removeAttr("disabled");
		grid.setEditable(true);
    addOrEdit=1;
}

function save_c(){
    if(addOrEdit==1){
        var code=$("#cve").val().trim();
        var desc=$("#name").val().trim();
        var idPrograms="";
        grid.forEachRow(function(id){
            var selected=grid.cellById(id,0).getValue();
            if(selected==1){
                idPrograms=idPrograms+""+id+"_";
            }
        });
        if(idPrograms==""){
            bootbox.alert("debes selecccionar un plan para el examen");
        }else{
            var data="id="+$("#id").val()+"&code="+code+"&desc="+desc+"&idPrograms="+idPrograms;
            $.ajax({
                url:"types_catalog_update",
                data:data,
                method:"POST",
                beforeSend:function(){
                    $("#cve").attr("disabled","disabled")
                    $("#name").attr("disabled","disabled")
                    grid.setEditable(false);
                },
                success:function(){
                    bootbox.alert("registro actualizado correctamente");
                }
            });
						$("#editar").removeAttr("disabled");
        }
    }else{
        var code=$("#cve").val().trim();
        var desc=$("#name").val().trim();
        var idPrograms="";
        grid.forEachRow(function(id){
            var selected=grid.cellById(id,0).getValue();
            if(selected==1){
                idPrograms=idPrograms+""+id+"_";
            }
        });
        if(idPrograms==""){
            bootbox.alert("debes selecccionar un plan para el examen");
        }else{
            var data="code="+code+"&desc="+desc+"&idPrograms="+idPrograms;
            $.ajax({
                url:"types_catalog_insert",
                data:data,
                method:"POST",
                beforeSend:function(){
                    $("#cve").attr("disabled","disabled")
                    $("#name").attr("disabled","disabled")
                    grid.setEditable(false);
                },
                success:function(id){
                    $("#id").val(id);
                    addOrEdit=1;
                    bootbox.alert("registro insertado correctamente");
                }

            });
						$("#editar").removeAttr("disabled");
        }
    }
}


