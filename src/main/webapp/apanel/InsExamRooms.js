/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function edit_c(){    
    $("#room").removeAttr("disabled");
    $("#descrip").removeAttr("disabled");
    $("#limit").removeAttr("disabled");
    $("#type").removeAttr("disabled");
    $("#date").removeAttr("disabled");
		$("#editar").removeAttr("disabled");
    addOrEdit=1;
}
function bloquear_c(){
	$("#room").attr("disabled","disabled");
	$("#descrip").attr("disabled","disabled");
	$("#limit").attr("disabled","disabled");
	$("#type").attr("disabled","disabled");
	$("#date").attr("disabled","disabled");
}

function save_c(){
    var room=$("#room").children(":selected").attr("id");
    var descript=$("#descrip").val().trim();
    var limit=$("#limit").val().trim();
    var type=$("#type").children(":selected").attr("id");
    var date=$("#date").val().trim();
    var id=$("#hide").val();
    
    if(/*room > 0 &&*/ descript !="" && limit>0 && /*type &&*/ date != null){
        var data="room="+room
                +"&descrip="+descript
                +"&limit="+limit
                +"&type="+type
                +"&date="+date;
        var url;
        if(addOrEdit==0){
            url="rooms_catalog_insert";
            $.ajax({
            url:url,
            data:data,
            method:"POST",
            beforesend:function(){},
            success:function(msg){
                $("#hide").val(msg);
                addOrEdit=1;
								bloquear_c();
								$("#editar").removeAttr("disabled");
                bootbox.alert("se inserto correctamente");
            }
            });
        }else{
            url="rooms_catalog_update";
            data+="&id="+id;
            $.ajax({
            url:url,
            data:data,
            method:"POST",
            beforesend:function(){},
            success:function(msg){
								$("#editar").removeAttr("disabled");
								bloquear_c();
                addOrEdit=1;
                bootbox.alert("se actualizo correctamente");
            }
            });
        }
    
    }else{
			bootbox.alert("Los campos deben ser llenados en su totalidad");
    }
}

function fill (){
    var url;
    var data;
    if (addOrEdit==1){
         initialize_c();
        url="rooms_catalog_search";
        data="id="+$("#hide").val();
        $.ajax({
            url:url,
            data:data,
            method:"POST",
            beforesend:function(){
                
            },
            success:function(data){
                var info=data.split("_");
                $("#room").children("#"+info[1]).attr("selected","selected");
                $("#descrip").val(info[0]);
                $("#limit").val(info[3]);
                $("#type").children("#"+info[2]).attr("selected","selected");
                $("#date").val(info[4]);
            }
        });
    }else{
        initialize_c();
        
    }
    
}
function initialize_c(){
    
    var url="rooms_catalog_fill_rooms";
    var data="";
    $.ajax({
            url:url,
            data:data,
            method:"POST",
            beforesend:function(){
                
            },
            success:function(data){
                $("#room").append(data);
            }
     });
     
     url="rooms_catalog_fill_exams";
     data="";
     $.ajax({
            url:url,
            data:data,
            method:"POST",
            beforesend:function(){
                
            },
            success:function(data){
                $("#type").append(data);
            }
     });
}