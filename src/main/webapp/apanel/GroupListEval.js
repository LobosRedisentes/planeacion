var parcial = 1;
var grid;
var materia;
var docente;

function setParcial(idGroupSubject,evaluation){
    var response = "Semester_catalog_Aaux?idGroupSubject="+idGroupSubject+"&evaluation="+evaluation+"";
    spInit();
    grid.clearAndLoad(response,function(){
        spStop();
        columnAutoSize();
    }); 
    parcial = evaluation;
    grid.setEditable(true);
}

function semester_admin(){
    var response = "../getAdminView?table=GradeGroups_view&id=group_IdGroupSubject&parameters=gpo,subject,professor,plan,calendar";
        grid = new dhtmlXGridObject('grid_container');
        grid.setImagePath("../dhtmlx/dhtmlxGrid/codebase/imgs/");
        grid.setHeader("Grupo,Materia,Profesor,Plan,Calendario");
				grid.attachHeader("#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter");
        grid.setInitWidths("80,*,200,150,*");
        grid.setColAlign("left,left,left,left,left");
        grid.setColSorting("str,str,str,str,str");
        grid.setEditable(false);
        var IDRow;
        grid.attachEvent("onRowSelect",function(id){
             IDRow = id;
             materia  = grid.cellById(id,1).getValue();
             docente = grid.cellById(id,2).getValue();
             $("#editar").removeAttr('disabled','disabled');
        });
        grid.init();
        grid.setSkin("clear");
        grid.enableSmartRendering(true,50);
				grid.objBox.style.overflowX = "hidden";
        spInit();
        grid.load(response,function(){
            spStop();
            columnAutoSize();
        });
        $("#editar").click(function(){
           window.location = ("GroupListEval_catalog.jsp?idGroupSubject="+IDRow+"&addOrEdit=1&subject="+materia+"&docente="+docente); 
        });
        $("#update").click(function(){
        spInit();
        grid.clearAndLoad(response,function(){
            spStop();
        });
    });
}
function semester_catalog(idGroupSubject,evaluation){
    var response = "Semester_catalog_Aaux?idGroupSubject="+idGroupSubject+"&evaluation="+evaluation+"";
        grid = new dhtmlXGridObject('grid_container');
        grid.setImagePath("../dhtmlx/dhtmlxGrid/codebase/imgs/");
        grid.setHeader("No,Matricula,Alumno,Calificacion,Opcion");
        grid.setInitWidths("80,100,*,100,150");
        grid.setColAlign("left,left,left,left,left");
        grid.setColSorting("str,str,str,str,str");
        grid.setEditable(true);
        grid.init();
        grid.setSkin("clear");
				grid.objBox.style.overflowX = "hidden";
        spInit();
        grid.load(response,function(){
            spStop();
            columnAutoSize();
        });
}

function selectPartial(){
    $("#parcial").change(function(){
        var value = parseInt($(this).val());
        switch(value){
            case 1 : setParcial(idGroupSubject,1); break
            case 2 : setParcial(idGroupSubject,2); break
            case 3 : setParcial(idGroupSubject,3); break
        }
    });
}
