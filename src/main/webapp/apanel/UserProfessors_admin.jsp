<%@page import="java.util.ArrayList"%>
<%@page import="com.edcore.model.safe.User"%>
	<%
		HttpSession sesion;
    sesion = request.getSession();
		User user = (User) sesion.getAttribute("user");
		if(user != null){
			ArrayList privileges = (ArrayList) sesion.getAttribute("privileges");
			if(privileges.contains("CAT11")){
	%>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Docentes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
        <meta name="description" content="Sistema educativo">
        <meta name="author" content="Instituto Tecnologico Superior de Zapopan">	
        <link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
        <link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
        <link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
        <!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
        <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
        <!-- JQUERY ERROR MESSAGE-->
        <link href="../css/jquery_errormsn.css" rel="stylesheet">
        <!-- SELECT2 -->
        <link rel="stylesheet" type="text/css" href="../js/select2/select2.min.css" />
        <!-- LIBRERIA DHTMLX -->
        <link rel="stylesheet" type="text/css" href="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.css" >
        <script src="../dhtmlx/libCompiler/dhtmlxcommon.js"></script>
        <script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.js"></script>
        <script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_srnd.js"></script>
        <script src="../dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_filter.js"></script>
        <script src="../dhtmlx/dhtmlxGrid/codebase/dhtmlxgridcell.js"></script>
        <script src="../dhtmlx/dhtmlxDataProcessor/codebase/dhtmlxdataprocessor.js"></script>
        <script src="../dhtmlx/libCompiler/connector.js"></script>
</head>
<body>
    <!-- Modal -->
    <div class="modal fade" id="box_nueva_constancia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 550px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header" style="background-color: rgb(207, 0, 0); color: #FFFFFF; height: 50px">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Constancia FED</h4>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="form-horizontal" name="frm_nuevo"  id="frm_nuevo">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="cal_IdCalSchool">Calendario</label>
                                <div class="col-md-8">
                                    <input type="hidden" id="cal_IdCalSchool" class="col-md-12"  name="cal_IdCalSchool" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="cumplio">Evaluación</label>
                                <div class="col-md-8">
                                    <select id="cumplio" class='form-control' class="col-md-12"  name="cumplio">
                                        <option value="En tiempo y forma" selected>En tiempo y forma</option>
                                        <option value="No cumplio">No cumplio</option>
                                    </select>
                                </div>
                            </div>
                            <span class="date-range pull-right">
                                <button id="btn_print" class="btn btn-danger"><i class="fa fa-print-square-o"></i> Imprimir </button>
                            </span>
                            </form>
                        </div>
                     </div>
                </div>
            </div>
        </div>
    </div>
      <!-- Modal -->
    <div class="modal fade" id="box_ISO" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 550px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header" style="background-color: rgb(207, 0, 0); color: #FFFFFF; height: 50px">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Formato ISO</h4>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="form-horizontal" name="frm_nuevo"  id="frm_nuevo">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="cal_ISO">Calendario</label>
                                <div class="col-md-8">
                                    <input type="hidden" id="cal_ISO" class="col-md-12"  name="cal_ISO" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="cumplio">Evaluación</label>
                                <div class="col-md-8">
                                    <select id="evaluacion" class='form-control' class="col-md-12"  name="cumplio">
                                        <option value="1" selected>Primer parcial</option>
                                        <option value="2">Segundo parcial</option>
                                        <option value="3">Tercer parcial</option>
                                    </select>
                                </div>
                            </div>
                            <span class="date-range pull-right">
                                <button id="btn_printISO" class="btn btn-danger"><i class="fa fa-print-square-o"></i> Imprimir </button>
                            </span>
                            </form>
                        </div>
                     </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="box_nuevo_formato" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 550px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header" style="background-color: rgb(207, 0, 0); color: #FFFFFF; height: 50px">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Formato FED</h4>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="form-horizontal" name="frm_nuevo"  id="frm_nuevo">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="cal_IdCalSchool1">Calendario primero:</label>
                                <div class="col-md-8">
                                    <input type="hidden" id="cal_IdCalSchool1" class="col-md-12"  name="cal_IdCalSchool1" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="cal_IdCalSchool2">Calendario segundo:</label>
                                <div class="col-md-8">
                                    <input type="hidden" id="cal_IdCalSchool2" class="col-md-12"  name="cal_IdCalSchool2" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="cal_IdCalSchool3">Calendario actual:</label>
                                <div class="col-md-8">
                                    <input type="hidden" id="cal_IdCalSchool3" class="col-md-12"  name="cal_IdCalSchool3" />
                                </div>
                            </div>
                            <span class="date-range pull-right">
                                <button id="btn_print_formato" class="btn btn-danger"><i class="fa fa-print-square-o"></i> Imprimir </button>
                            </span>
                            </form>
                        </div>
                     </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="box_print" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 80%; height: 98%">
            <div class="modal-content" style="width: 100%; height: 90%">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Imprimir</h4>
                </div>
                <div class="modal-body" style="width: 100%; height: 90%">
                    <iframe src="" style="width: 100%; height: 100%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="box_print_card" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 30%; height: 70%">
            <div class="modal-content" style="width: 100%; height: 90%">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Imprimir credencial</h4>
                </div>
                <div class="modal-body" style="width: 100%; height: 90%">
                    <iframe id="frame2" name="frame2" src="" style="width: 100%; height: 100%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
	<!-- HEADER -->
	<%@include file="header.jsp" %>
	<!--/HEADER -->
	<!-- PAGE -->
	<section id="page">
		<!-- SIDEBAR -->
		<%@include file="sidebar.jsp" %>
		<!-- /SIDEBAR -->
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="index.jsp">Inicio</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Catalogos</a>
										</li>
										<li>
											<i class="fa"></i>
											<a href="#">Usuarios</a>
										</li>
										<li>Docentes</li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										<h3 class="content-title pull-left">Administración de Docentes</h3>
										<span class="date-range pull-right">
										<p class="btn-toolbar">
                                                                                        <button id="btn_ISO" class="btn btn-danger" disabled ><i class="fa fa-file-text-o"></i> ISO</button>
											<%if(privileges.contains("CAT39")){%>
                                                                                        <button id="btn_nueva_constancia" class="btn btn-danger" disabled ><i class="fa fa-file-text-o"></i> Constancia FED</button>
											<%}%>
											<%if(privileges.contains("CAT39")){%>
                                                                                        <button id="btn_nuevo_formato" class="btn btn-danger" disabled ><i class="fa fa-file-text-o"></i> Formato FED</button>
											<%}%>
											<%if(privileges.contains("CAT39")){%>
											<button id="nuevo" class="btn btn-danger" ><i class="fa fa-file-text-o"></i> Nuevo</button>
											<%}%>
											<%if(privileges.contains("CAT40")){%>
											<button id="editar" class="btn btn-danger" disabled=""><i class="fa fa-pencil-square-o"></i> Editar</button>
											<%}%>
                                                                                        <button id="print" class="btn btn-danger" disabled=""><i class="fa fa-print-square-o"></i>Credencial</button>
											<button id="actualizar" class="btn btn-danger" ><i class="fa fa-rotate-right"></i> Actualizar</button>
										</p>
										</span>
									</div>
									<div class="description"></div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						<!-- DASHBOARD CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<!-- BOX -->
								<div id="grid_container" class="box border" style="height:600px; width: 100%">
								</div>
							</div>
						</div>
					   <!-- /DASHBOARD CONTENT -->
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- JQUERY -->
	<script src="../js/jquery/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script src="../js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="../bootstrap-dist/js/bootstrap.min.js"></script>
        <!-- SELECT2 -->
        <script type="text/javascript" src="../js/select2/select2.min.js"></script>
        <!-- DATA MASK -->
        <script type="text/javascript" src="../js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
        <!-- bootbox script -->
        <script src="../js/bootbox/bootbox.min.js"></script>
  	<!-- COOKIE -->
	<script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="../js/script.js"></script>
	<script src="../js/jquery-validate/jquery.validate.js"></script>
	<script src="../js/jquery-validate/additional-methods.js"></script>
        <script src="../js/jquery_errormsn.js"></script>
        <script src="../js/spin.js" ></script>
	<script>
		jQuery(document).ready(function() {
			App.setPage("forms");  //Set current page
			App.init(); //Initialise plugins and elements
			$("#photo").attr("src","getPhoto?safe_IdUser="+<% if(user != null){out.print(user.getSafe_IdUser());} %>);
			var response = "../getAdminView?table=UserProfessors_view&id=safe_IdUser&parameters=code,nombre,paterno,materno,departamento,curp";
			var IDRow;
			var name;
			var values;
			
			grid = new dhtmlXGridObject('grid_container');
			grid.setImagePath("../dhtmlx/dhtmlxGrid/codebase/imgs/");
                        grid.setHeader("Clave,Nombre,Paterno,Materno,Departamento,CURP");
                        grid.attachHeader("#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter");
                        grid.setInitWidths("80,100,100,100,*,*");
                        grid.setColAlign("left,left,left,left");
                        grid.setColSorting("str,str,str,str");
			grid.setEditable(false);
			grid.init();
			grid.setSkin("clear");
			grid.enableSmartRendering(true,50);
			grid.load(response,function(){
				columnAutoSize(); 
			});
			grid.attachEvent("onRowSelect",function(id){
				//split para editar
				IDRow = id;
				$("#editar").removeAttr("disabled");
                                $("#print").removeAttr("disabled");
                                $("#btn_nueva_constancia").removeAttr("disabled");
                                $("#btn_nuevo_formato").removeAttr("disabled");
                                $("#btn_ISO").removeAttr("disabled");
                        });

			grid.attachEvent("onRowDblClicked",function(id){
				IDRow = id;
				window.location = "UserProfessors_catalog.jsp?idProf="+IDRow+"&addOrEdit=1";
                        });
                        
     $("#btn_ISO").click(function(){
        $('#cal_ISO').select2({
            multiple: false,
            maximumSelectionSize: 1,
            tags: [],
            ajax: {
                url: 'getCalendarlOptions',
                dataType: 'json',
                type: "POST",
                quietMillis: 150,
                data: function (term, page) {
                    return {
                        term: term
                    };
                },
                results: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        })
                    };
                },
                cache: true,
                placeholder: "Selecciona ..."
            }
        });
        $('#cal_ISO').val('');
        $('#box_ISO').modal({show:true});
    });
    
    $("#btn_nueva_constancia").click(function(){

        $('#cal_IdCalSchool').select2({
            multiple: false,
            maximumSelectionSize: 1,
            tags: [],
            ajax: {
                url: 'getCalendarlOptions',
                dataType: 'json',
                type: "POST",
                quietMillis: 150,
                data: function (term, page) {
                    return {
                        term: term
                    };
                },
                results: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        })
                    };
                },
                cache: true,
                placeholder: "Selecciona ..."
            }
        });
        $('#cal_IdCalSchool').val('');
        $('#box_nueva_constancia').modal({show:true});
    });
    
    $("#btn_nuevo_formato").click(function(){

        $('#cal_IdCalSchool1').select2({
            multiple: false,
            maximumSelectionSize: 1,
            tags: [],
            ajax: {
                url: 'getCalendarlOptions',
                dataType: 'json',
                type: "POST",
                quietMillis: 150,
                data: function (term, page) {
                    return {
                        term: term
                    };
                },
                results: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        })
                    };
                },
                cache: true,
                placeholder: "Selecciona ..."
            }
        });
        $('#cal_IdCalSchool2').select2({
            multiple: false,
            maximumSelectionSize: 1,
            tags: [],
            ajax: {
                url: 'getCalendarlOptions',
                dataType: 'json',
                type: "POST",
                quietMillis: 150,
                data: function (term, page) {
                    return {
                        term: term
                    };
                },
                results: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        })
                    };
                },
                cache: true,
                placeholder: "Selecciona ..."
            }
        });
        $('#cal_IdCalSchool3').select2({
            multiple: false,
            maximumSelectionSize: 1,
            tags: [],
            ajax: {
                url: 'getCalendarlOptions',
                dataType: 'json',
                type: "POST",
                quietMillis: 150,
                data: function (term, page) {
                    return {
                        term: term
                    };
                },
                results: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        })
                    };
                },
                cache: true,
                placeholder: "Selecciona ..."
            }
        });
        $('#cal_IdCalSchool1').val('');
        $('#cal_IdCalSchool2').val('');
        $('#cal_IdCalSchool3').val('');
        $('#box_nuevo_formato').modal({show:true});
    });
    
    $("#btn_print").click(function(event){
        var cal_IdCalSchool = $('#cal_IdCalSchool').val();
        if(cal_IdCalSchool !== ''){
            $('#box_nueva_constancia').modal('toggle');
            var cumplio = $("#cumplio").children(":selected").attr("value");
            $('iframe').attr("src","getConstanciaFED?safe_IdUser="+IDRow+"&cal_IdCalSchool="+cal_IdCalSchool+"&cumplio="+cumplio);
            $('#box_print').modal({show:true});
        }else{
            bootbox.alert("Favor de selecciona un calendario! ", function() {});
        }
        event.preventDefault();
    });
    
    $("#btn_print_formato").click(function(event){
        var cal_IdCalSchool1 = $('#cal_IdCalSchool1').val();
        var cal_IdCalSchool2 = $('#cal_IdCalSchool2').val();
        var cal_IdCalSchool3 = $('#cal_IdCalSchool3').val();
        if(cal_IdCalSchool1 !== '' && cal_IdCalSchool2 !== '' && cal_IdCalSchool3 !== ''){
            $('#box_nuevo_formato').modal('toggle');
            $('iframe').attr("src","getFormatoFED?safe_IdUser="+IDRow+"&cal_IdCalSchool1="+cal_IdCalSchool1+"&cal_IdCalSchool2="+cal_IdCalSchool2+"&cal_IdCalSchool3="+cal_IdCalSchool3);
            $('#box_print').modal({show:true});
        }else{
            bootbox.alert("Favor de selecciona los 3 calendario! ", function() {});
        }
        event.preventDefault();
    });
     $("#btn_printISO").click(function(event){
        var cal_IdCalSchool = $('#cal_ISO').val();
        var evaluacion = $("#evaluacion").children(":selected").attr("value");
        if(cal_IdCalSchool !== ''){
            $('#box_ISO').modal('toggle');
            $('iframe').attr("src","getReporteISO?safe_IdUser="+IDRow+"&cal_IdCalSchool="+cal_IdCalSchool+"&parcial="+evaluacion);
            $('#box_print').modal({show:true});
        }else{
            bootbox.alert("Favor de selecciona el calendario! ", function() {});
        }
        event.preventDefault();
    });
    $("#print").click(function(event){
        $('#frame2').attr("src","getProfessorCard?id="+IDRow);
        $('#box_print_card').modal({show:true});
    });
			$("#nuevo").click(function () {
				window.location = "UserProfessors_catalog.jsp?idProf=0&addOrEdit=0";
			});
			$("#editar").click(function () {
				window.location = "UserProfessors_catalog.jsp?idProf="+IDRow+"&addOrEdit=1";
			});
			/*$("#print").click(function () {
				print(IDRow);
			});*/
			$("#actualizar").click(function () {
				grid.clearAndLoad(response);
			});		
		
		});
	</script>
	<!-- /JAVASCRIPTS -->
</body>
</html>
<%
			}else{
				response.sendRedirect("error600.jsp");
			}
		}else{
			response.sendRedirect("error600.jsp");
		}
%>