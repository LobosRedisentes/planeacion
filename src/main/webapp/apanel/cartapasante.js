/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery(document).ready(function() {
    App.setPage("forms");  //Set current page
    App.init(); //Initialise plugins and elements
    $("#photo").attr("src","getPhoto?safe_IdUser="+safe_IdUser);
    var response = "../getAdminView?sql=select * FROM VIW023 &id=dm_IdIntern&parameters=code,studentcode,name,firstLast,secondLast,program,day,month,year";
    var IDRow;            
    grid = new dhtmlXGridObject('grid_container');
    grid.setImagePath("../dhtmlx/dhtmlxGrid/codebase/imgs/");
    grid.setHeader("FOLIO,Matricula,Nombre,Paterno,Materno,Programa,Dia,Mes,Año");
    grid.attachHeader("#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter,#connector_text_filter");
    grid.setInitWidths("*,*,*,*,*,*,*,*,*");
    grid.setColAlign("left,left,left,left,left,left,left,left,left");
    grid.setColSorting("str,str,str,str,str,str,str,str,str");
    grid.setEditable(false);
    grid.init();
    grid.setSkin("clear");
    grid.enableSmartRendering(true,50);
    grid.objBox.style.overflowX = "hidden";
    grid.load(response,function(){});
    
    
    $("#frm_nuevo").validate({
        rules:{
            user_IdStudent:{required: true},
            code:{required: true},
            day:{required: true, digits: true},
            month:{required: true},
            year:{required: true}
        },
        highlight: function(element){$(element).parent().parent().addClass("has-error");},
        unhighlight: function(element){$(element).parent().parent().removeClass("has-error");}
    });
    
    $('#month').select2();
    grid.attachEvent("onRowSelect",function(id){
        IDRow = id;
        $("#btn_print").removeAttr("disabled");
    });

    $("#btn_nuevo").click(function(){
        $('#box_nuevo').modal({show:true});
$('#user_IdStudent').select2({
    multiple: false,
    maximumSelectionSize: 1,
    tags: [],
    ajax: {
        url: "getStudentsOptions?status=EG",
        dataType: 'json',
        type: "POST",
        quietMillis: 150,
        data: function (term, page) {
            return {
                term: term
            };
        },
        results: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.name,
                        id: item.id
                    };
                })
            };
        },
        cache: true,
        placeholder: "Selecciona ..."
    }
});
    });
    
    $("#btn_print").click(function(){
        $('iframe').attr("src","GetPdfCartaPasante?dm_IdIntern="+IDRow);
        $('#print_box').modal({show:true});
    });
    $("#btn_guardar").click(function(){
        if($("#frm_nuevo").valid()){
            var user_IdStudent = $('#user_IdStudent').val();
            var code = $('#code').val().trim();
            var day = $('#day').val().trim();
            var month = $('#month').val().trim();
            var year = $('#year').val();
            var save = $.ajax({url:"CartaPasante_save",data:"user_IdStudent="+user_IdStudent+"&code="+code+"&day="+day+"&month="+month+"&year="+year,method:"POST"});
            save.done(function(msn){
                if(msn === '' || msn === '0'){
                    bootbox.alert("No se pudo guardar! ", function() {});
                }else{
                    grid.clearAndLoad(response);
                }
            });
        }
    });
});
