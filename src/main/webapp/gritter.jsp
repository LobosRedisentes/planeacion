<%-- 
    Document   : gritter
    Created on : 24-sep-2015, 14:52:05
    Author     : gabo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <script>
    $.extend($.gritter.options, {position: 'top-left'});
    $("#help01").click(function () {
        var unique_id = $.gritter.add({
            title: 'Municipio de nacimiento',
            text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eget tincidunt velit. Cum sociis natoque penatibus et <a href="#" style="color:#ccc">magnis dis parturient</a> montes, nascetur ridiculus mus.',
            sticky: true,
            time: '',
            class_name: 'my-sticky-class'
        });
        setTimeout(function () {
            $.gritter.remove(unique_id, {
                fade: true,
                speed: 'slow'
            });
        }, 3000)
        false;
    });
    </script>
    </body>
</html>
