function throwException(msn) {
    switch (msn) {
        case "CORE0001":
            bootbox.alert("ERROR [CORE0001]: No se pudo iniciar sesion!.", function () {});
            break;
        case "CORE0002":
            bootbox.alert("ERROR [CORE0002]: El password es incorrecto!.", function () {});
            break;
        case "CORE0003":
            bootbox.alert("ERROR [CORE0003]: La cuenta esta inactiva!.", function () {});
            break;
        case "CORE0004":
            bootbox.alert("ERROR [CORE0004]: La cuenta no pertenece a este CU!.", function () {});
            break;
        case "CORE0005":
            bootbox.alert("ERROR [CORE0005]: La cuenta no existe en el sistema!.", function() {});
            break;
        case "CORE0006":
            bootbox.alert("ERROR [CORE0006]: Verifique que este ingresando al panel correspondiente!.", function () {});
            break;
        case "CORE0007":
            bootbox.alert("ERROR [CORE0007]: Fall&oacute; la transacci&oacute;n!.", function () {});
            break;
        case "CORE0008":
            break;
        case "CORE0009":
            bootbox.alert("ERROR [CORE0009]: Transacci&oacute;n no permitida!.", function () {});
            break;
        default:
            bootbox.alert("ERROR DESCONOCIDO [" + msn + "]", function () {});
            break;
    };
    return;
}

