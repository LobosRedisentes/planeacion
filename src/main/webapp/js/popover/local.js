$("#popover_local").popover({
    trigger: 'focus',
    container: 'body',
    html: true,
    content: function () {
        var clone = $($(this).data('popover-content')).clone(true).removeClass('hide');
        return clone;
    }
}).click(function (e) {
    e.preventDefault();
});
var popover_local_content = $("#popover_local").data('bs.popover');
popover_local_content.options.content = "<div><p>En caso de no se despliegue la colonia, favor de enviar un correo electrónico a soporte técnico para proceder a registrarlo.</p><p><a href='mailto: soporte@edcore.com.mx' >soporte@edcore.com.mx</a></p></div>";
$("#popover_local").attr("data-original-title", "COLONIA");



