$("#popover_rfc").popover({
    trigger: 'focus',
    container: 'body',
    html: true,
    content: function () {
        var clone = $($(this).data('popover-content')).clone(true).removeClass('hide');
        return clone;
    }
}).click(function (e) {
    e.preventDefault();
});
var popover_rfc_content = $("#popover_rfc").data('bs.popover');
popover_rfc_content.options.content = "<div><p>En caso de no conocer tu RFC, ingresa en la siguiente pagina:</p><p><a href='https://www.recaudanet.gob.mx/recaudanet/rfc.jsp' target='_blank'>https://www.recaudanet.gob.mx</a></p></div>";
$("#popover_rfc").attr("data-original-title", "RFC");



