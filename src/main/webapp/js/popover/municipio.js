$("#popover_municipio").popover({
    trigger: 'focus',
    container: 'body',
    html: true,
    content: function () {
        var clone = $($(this).data('popover-content')).clone(true).removeClass('hide');
        return clone;
    }
}).click(function (e) {
    e.preventDefault();
});
var popover_municipio_content = $("#popover_municipio").data('bs.popover');
popover_municipio_content.options.content = "<div><p>En caso de no se despliegue el municipio, favor de enviar un correo electrónico a soporte técnico para proceder a registrarlo.</p><p><a href='mailto: soporte@edcore.com.mx' >soporte@edcore.com.mx</a></p></div>";
$("#popover_municipio").attr("data-original-title", "MUNICIPIO");



