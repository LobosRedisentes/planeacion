$("#popover_curp").popover({
    trigger: 'focus',
    container: 'body',
    html: true,
    content: function () {
        var clone = $($(this).data('popover-content')).clone(true).removeClass('hide');
        return clone;
    }
}).click(function (e) {
    e.preventDefault();
});
var popover_curp_content = $("#popover_curp").data('bs.popover');
popover_curp_content.options.content = "<div><p>En caso de no conocer tu CURP, ingresa en la siguiente pagina:</p><p><a href='https://consultas.curp.gob.mx/' target='_blank'>https://consultas.curp.gob.mx/</a></p></div>";
$("#popover_curp").attr("data-original-title", "CURP");



