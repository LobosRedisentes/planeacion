$("#popover_factor").popover({
    trigger: 'focus',
    container: 'body',
    html: true,
    content: function () {
        var clone = $($(this).data('popover-content')).clone(true).removeClass('hide');
        return clone;
    }
}).click(function (e) {
    e.preventDefault();
});
var popover_factor_content = $("#popover_factor").data('bs.popover');
popover_factor_content.options.content = "<div><p>Para cualquier siniestro es importante conocer tu tipo de sangre.</p><p>Favor de ingresar tu tipo según tu certificado médico.</p></div>";
$("#popover_factor").attr("data-original-title", "Factor sanguíneo");



