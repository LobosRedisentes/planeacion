$("#frm").validate({
    highlight: function (element) {
        $(element).parent().parent().addClass("has-error");
    },
    unhighlight: function (element) {
        $(element).parent().parent().removeClass("has-error");
    }
});

