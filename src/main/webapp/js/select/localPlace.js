$('#select2_localState').select2({
    multiple: false,
    maximumSelectionSize: 1,
    tags: [],
    ajax: {
        url: select2_localState_url,
        dataType: 'json',
        type: "POST",
        quietMillis: 150,
        data: function (term, page) {
            return {
                term: term
            };
        },
        results: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.name,
                        id: item.id
                    };
                })
            };
        },
        cache: true,
        placeholder: "Selecciona ..."
    }
});

$("#select2_localState").change(function () {
    var id = $(this).val();
    $('#select2_localTown').select2({
        multiple: false,
        maximumSelectionSize: 1,
        tags: [],
        ajax: {
            url: select2_localTown_url + id,
            dataType: 'json',
            type: "POST",
            quietMillis: 150,
            data: function (term, page) {
                return {
                    term: term
                };
            },
            results: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        };
                    })
                };
            },
            cache: true,
            placeholder: "Selecciona ..."
        }
    });
    if (id === "") {
        $("#select2_localTown").select2('destroy');
        $("#select2_local").select2('destroy');
    }
});

$("#select2_localTown").change(function () {
    var id = $(this).val();
    $('#select2_local').select2({
        multiple: false,
        maximumSelectionSize: 1,
        tags: [],
        ajax: {
            url: select2_local_url + id,
            dataType: 'json',
            type: "POST",
            quietMillis: 150,
            data: function (term, page) {
                return {
                    term: term
                };
            },
            results: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        };
                    })
                };
            },
            cache: true,
            placeholder: "Selecciona ..."
        }
    });
    if (id === "") {
        $("#select2_local_url").select2('destroy');
    }
});