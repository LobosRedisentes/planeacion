$('#select2_birthState').select2({
    multiple: false,
    maximumSelectionSize: 1,
    tags: [],
    ajax: {
        url: select2_birthState_url,
        dataType: 'json',
        type: "POST",
        quietMillis: 150,
        data: function (term, page) {
            return {
                term: term
            };
        },
        results: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.name,
                        id: item.id
                    };
                })
            };
        },
        cache: true,
        placeholder: "Selecciona ..."
    }
});

$("#select2_birthState").change(function () {
    var id = $(this).val();
    $('#select2_birthTown').select2({
        multiple: false,
        maximumSelectionSize: 1,
        tags: [],
        ajax: {
            url: select2_birthTown_url + id,
            dataType: 'json',
            type: "POST",
            quietMillis: 150,
            data: function (term, page) {
                return {
                    term: term
                };
            },
            results: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        };
                    })
                };
            },
            cache: true,
            placeholder: "Selecciona ..."
        }
    });
    if (id === "") {
        $("#select2_birthTown").select2('destroy');
    }
});

