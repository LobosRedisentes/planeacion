jQuery(document).ready(function () {
    App.setPage("form");  //Set current page
    App.init(); //Initialise plugins and element
    
    $("#frm").validate({
        rules: {
            pre1: {required: true},
            pre6: {required: true},
            pre8: {required: true},
            pre9: {required: true},
            pre10: {required: true},
            pre12: {required: true},
            pre14: {required: true},
            pre16: {required: true},
            pre18: {required: true},
            pre20: {required: true},
            pre22: {required: true},
            pre24: {required: true},
            pre27: {required: true}
        },
        highlight : function(label) {
            $(label).closest('.form-group').addClass('has-error');
            $(".tab-content").find("fieldset.tab-pane:has(has-error)").each(function(index, tab) {
                if ($(".tab-content").find("field.tab-pane.active:has(has-error)").length === 0) {
                    var id = $(tab).attr("id");
                    $('a[href="#' + id + '"]').tab('show');
                }

            });
        },
        invalidHandler : function(event, validator) {
            // 'this' refers to the form
            $('#myTab').on('shown', function(e) {
                e.target;// activated tab
                $($(e.target).attr('href')).find("fieldset.has-error :input:first").focus();
            });
            var errors = validator.numberOfInvalids();
            if (errors) {
                var message = errors === 1 ? 'Te falto un campo' : 'Te faltaron ' + errors + ' campos.';
                bootbox.alert(message, function() {});
                $("div.has-error").show();
            } else {
                $("div.has-error").hide();
            }
        },
        ignore : []
    });
    $("#btn_save").click(function (event) {
        $("#btn_save").attr( "disabled", "disabled" );
        if ($("#frm").valid()) {
            var values = "values=" + parseInt($("#user").val().trim()) + "#" + $("#pre1").children(":selected").attr("value") + "#" + $("#pre2").val().trim() + "#" + $("#pre3").children(":selected").attr("value") + "#"
                    + $("#pre4").children(":selected").attr("value") + "#" + $("#pre5").val().trim() + "#" + $("#pre6").children(":selected").attr("value") + "#"
                    + $("#pre7").val().trim() + "#" + $("#pre8").children(":selected").attr("value") + "#" + $("#pre9").children(":selected").attr("value") + "#"
                    + $("#pre10").children(":selected").attr("value") + "#" + $("#pre11").val().trim() + "#" + $("#pre12").children(":selected").attr("value") + "#"
                    + $("#pre13").val().trim() + "#" + $("#pre14").children(":selected").attr("value") + "#" + $("#pre15").val().trim() + "#"
                    + $("#pre16").children(":selected").attr("value") + "#" + $("#pre17").val().trim() + "#" + $("#pre18").children(":selected").attr("value") + "#"
                    + $("#pre19").val().trim() + "#" + $("#pre20").children(":selected").attr("value") + "#" + $("#pre21").val().trim() + "#"
                    + $("#pre22").children(":selected").attr("value") + "#" + $("#pre23").val().trim() + "#" + $("#pre24").children(":selected").attr("value") + "#"
                    + $("#pre25").children(":selected").attr("value") + "#" + $("#pre26").val().trim() + "#" + $("#pre27").children(":selected").attr("value")+ "&id=0";
            var save = $.ajax({url: "saveSurveyHealth",data: values,method: "POST"});
            save.done(function (msn) {
                if (msn === "0") {
                    bootbox.alert("Fallo al guardar!", function () {});
                } else {
                    $("#pre1").attr( "disabled", "disabled" );
                    $("#pre2").attr( "disabled", "disabled" );
                    $("#pre3").attr( "disabled", "disabled" );
                    $("#pre4").attr( "disabled", "disabled" );
                    $("#pre5").attr( "disabled", "disabled" );
                    $("#pre6").attr( "disabled", "disabled" );
                    $("#pre7").attr( "disabled", "disabled" );
                    $("#pre8").attr( "disabled", "disabled" );
                    $("#pre9").attr( "disabled", "disabled" );
                    $("#pre10").attr( "disabled", "disabled" );
                    $("#pre11").attr( "disabled", "disabled" );
                    $("#pre12").attr( "disabled", "disabled" );
                    $("#pre13").attr( "disabled", "disabled" );
                    $("#pre14").attr( "disabled", "disabled" );
                    $("#pre15").attr( "disabled", "disabled" );
                    $("#pre16").attr( "disabled", "disabled" );
                    $("#pre17").attr( "disabled", "disabled" );
                    $("#pre18").attr( "disabled", "disabled" );
                    $("#pre19").attr( "disabled", "disabled" );
                    $("#pre20").attr( "disabled", "disabled" );
                    $("#pre21").attr( "disabled", "disabled" );
                    $("#pre22").attr( "disabled", "disabled" );
                    $("#pre23").attr( "disabled", "disabled" );
                    $("#pre24").attr( "disabled", "disabled" );
                    $("#pre25").attr( "disabled", "disabled" );
                    $("#pre26").attr( "disabled", "disabled" );
                    $("#pre27").attr( "disabled", "disabled" );
                    $("#BTN_save").attr( "disabled", "disabled" );
                    bootbox.alert("Se guardo con exito!", function () {});
                }
            });
            save.error(function(msn){
                $("#btn_save").removeAttr("disabled", "disabled");
                bootbox.alert("Al parecer se presento un problema de conexi&oacute;n, intente mas tarde! ", function() {});
            });
            event.preventDefault();
        }else{
            $("#btn_save").removeAttr("disabled", "disabled");
        }
    });
});