<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sistema edcore - Bienvenido!</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
        <meta name="description" content="Sistema Escolar - edcore">
        <meta name="author" content="Instituto Tecnol�gico Superior de Zapopan">	
        <link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
        <link rel="stylsheet" type="text/css" href="../css/themes/default.css" id="skin-switcher" >
        <link rel="stylesheet" type="text/css" href="../css/responsive.css" >esheet" type="text/css" href="../css/themes/default.css" id="skin-switcher" >
        <link rel="stylesheet" type="text/css" href="../css/responsive.css" >
        <link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
    </head>
    <body>
        <div class="col-md-12" style="display: block; margin-left: 5.2%; width: 91%; height: 130px; background-image: url('../img/header-logo.png'); background-size:contain; background-repeat: no-repeat; background-repeat: round;" ></div>
        <div class="col-md-12" style="display: block; margin-left: 3%; width: 95%;" >
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="login-box-plain">
                            <p style="text-align: center"><img src="../img/favicon-32x32.png" /><strong> &nbsp;&nbsp;SISTEMA ESCOLAR EDCORE</strong></p><br>
                            <h4>Bienvenido!</h4><br>
                            <p>Tu cuenta de aspirante fue creada de forma satisfactoria. Debes de conservar los datos de acceso al panel de edcore ya
                                que estos ser�n necesarios durante todo tu proceso de inscripci�n.</p>
                            <div class="form-actions">
                                <center>Usuario: <%= request.getParameter("mail")%>
                                <br>Password: 
                                <%
                                    int size = request.getParameter("password").length();
                                    for (int i = 0; i < size; i++) {
                                        out.print("*");
                                    }
                                %></center>
                            </div>
                            <center><button  id="btn_continuar" class="btn btn-default" style="width: 50%">Ingresar al panel edcore</button></center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="display: block; margin-left: 1.5%; width: 95%" >
            <p style="text-align: center">Copyright &copy; edcore 2015 Instituto T�cnologico Superior de Zapopan<br>Todos los derechos reservados.</p>
        </div>
        <!-- JAVASCRIPTS -->
        <script src="../js/jquery/jquery-2.0.3.min.js"></script>
        <script src="../bootstrap-dist/js/bootstrap.min.js"></script>
        <script src="../js/script.js"></script>
        <script>
            jQuery(document).ready(function() {
                $("#btn_continuar").click(function () {
                    window.location = "login.jsp";
                });
            });
        </script>
    </body>
</html>
