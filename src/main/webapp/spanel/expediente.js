jQuery(document).ready(function () {
    App.setPage("form");  //Set current page
    App.init(); //Initialise plugins and element
    
    
    $("#btn_lista").click(function(){
        $('#document').select2({
            multiple: false,
            maximumSelectionSize: 1,
            tags: [],
            ajax: {
                url: 'getDocOptions',
                dataType: 'json',
                type: "POST",
                quietMillis: 150,
                data: function (term, page) {
                    return {
                        term: term
                    };
                },
                results: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            };
                        })
                    };
                },
                cache: true,
                placeholder: "Selecciona ..."
            }
        });
        $('#box_lista').modal({show:true});
    });
    
    $("#btn_nuevo").click(function(event){
        var document = $('#document').val();
        if(document !== ''){
            $('#box_lista').modal('toggle');
            if(document === "1"){
                $('#box_documentos_width').css("width","80%");
                $('#frame_documentos').css("height","600px");
                $('#box_documentos_title').html('Encuesta de Salud');
                $('#frame_documentos').attr("src","doc001.jsp?id=0");
            }
            $('#box_documentos').modal({show:true});
        }else{
            bootbox.alert("Selecciona un tipo de documento! ", function() {});
        }
        event.preventDefault();
    });
    
    /*$("#btn_nuevoDocumento").click(function(){
        $('#box_documentos_title').html("Nuevo");
        $('#frame_documentos').attr("src","doc001.jsp?");
        $('#box_documentos').modal({show:true});
    });*/
});