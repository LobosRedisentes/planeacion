<%@page import="mx.com.edcore.vo.AccountVO"%>
<%@page import="mx.com.edcore.vo.SessionVO"%>
<%
    HttpSession websession;
    websession = request.getSession();
    SessionVO sesion =  (SessionVO) websession.getAttribute("session");
    if(sesion != null){
        AccountVO account =  (AccountVO) websession.getAttribute("account");
%>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Encuesta de Salud</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="Sistema Escolar - Edcore">
	<meta name="author" content="Instituto Tecnologico Superior de Zapopan">	
	<link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
	<link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
	<!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>	
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        <link rel="stylesheet" type="text/css" href="../css/error_msn.css" >
</head>
<body style="background-color: white">

    <section id="page" style="background-color: white">

            <div class="container" style="background-color: white">

                    <div class="row" style="background-color: white">
                        <form role="form" class="form-horizontal" action="" method="post" name="frm" id="frm">
                            <input type="hidden" name="focus" id="focus" value="0"/>
                            <input type="hidden" name="id" id="id" value="0" />
                            <input type="hidden" name="user" id="user" value="<%= account.getUser() %>" />
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="tabbable">   
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#tab_1_1" data-toggle="tab"><i class="fa fa-share"></i> Secci�n 1</a></li>
                                            <li><a href="#tab_1_2" data-toggle="tab"><i class="fa fa-share"></i> Seccion 2</a></li>
                                            <li><a href="#tab_1_3" data-toggle="tab"><i class="fa fa-share"></i> Seccion 3</a></li>
                                            <li><a href="#tab_1_4" data-toggle="tab"><i class="fa fa-share"></i> Seccion 4</a></li>
                                        </ul>
                                        <div class="tab-content">

                                            <div class="tab-pane fade in active" id="tab_1_1"><br>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="pre1">Realizaste tr�mites en otra universidad?</label>
                                                    <div class="col-sm-6">
                                                        <select id="pre1" name="pre1" class="form-control">
                                                            <option value="">Selecciona ..</option>
                                                            <option value="NO">NO</option>
                                                            <option value="SI">SI</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="pre2">En caso que realizaste tr�mite en otra universidad, menciona cual?</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" id="pre2" name="pre2" class="form-control" autocomplete='off' placeholder="Nombre completo de la universidad" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="pre3">Fuiste aceptado?</label>
                                                    <div class="col-sm-6">
                                                        <select id="pre3" name="pre3" class="form-control">
                                                            <option value="">Selecciona ..</option>
                                                            <option value="NO">NO</option>
                                                            <option value="SI">SI</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="pre4">Porque fuiste aceptado/rechazado ?</label>
                                                    <div class="col-sm-6">
                                                        <select id="pre4" name="pre4" class="form-control">
                                                            <option value="">Selecciona ..</option>
                                                            <option value="POR PUNTUAJE">POR PUNTUAJE</option>
                                                            <option value="POR DOCUMENTACION">POR DOCUMENTACI�N</option>
                                                            <option value="OTRO">OTRO</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="pre5">En caso de que sea otro motivo de aceptaci�n o rechazo, mencione:</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" id="pre5" name="pre5" class="form-control" autocomplete='off' placeholder="Menciona el motivo" />
                                                    </div>
                                                </div>
                                                <div class="separator"></div>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="pre6">Tienes alguna discapacidad?</label>
                                                    <div class="col-sm-6">
                                                        <select id="pre6" name="pre6" class="form-control">
                                                            <option value="">Selecciona ..</option>
                                                            <option value="NO">NO</option>
                                                            <option value="SI">SI</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="pre7">En caso de tener una discapacidad, mencione:</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" id="pre7" name="pre7" class="form-control" autocomplete='off' placeholder="Menciona la discapacidad" />
                                                    </div>
                                                </div>
                                                <div class="separator"></div>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="pre8">Utiliza anteojos?</label>
                                                    <div class="col-sm-6">
                                                        <select id="pre8" name="pre8" class="form-control">
                                                            <option value="">Selecciona ..</option>
                                                            <option value="NO">NO</option>
                                                            <option value="SI">SI</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab_1_2"><br>



                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="pre9">Utiliza aparato auditivo?</label>
                                                    <div class="col-sm-6">
                                                        <select id="pre9" name="pre9" class="form-control">
                                                            <option value="">Selecciona ..</option>
                                                            <option value="NO">NO</option>
                                                            <option value="SI">SI</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="separator"></div>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="pre10">Falga alguna extremidad de tu cuerpo (piernas,brazo,dedos)?</label>
                                                    <div class="col-sm-6">
                                                        <select id="pre10" name="pre10" class="form-control">
                                                            <option value="">Selecciona ..</option>
                                                            <option value="NO">NO</option>
                                                            <option value="SI">SI</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="pre11">En caso de no contar conn una extremidad, mencione:</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" id="pre11" name="pre11" class="form-control" autocomplete='off' placeholder="Menciona la discapacidad" />
                                                    </div>
                                                </div>
                                                <div class="separator"></div>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="pre12">Utilizas silla de ruedas para alguna discapacidad?</label>
                                                    <div class="col-sm-6">
                                                        <select id="pre12" name="pre12" class="form-control">
                                                            <option value="">Selecciona ..</option>
                                                            <option value="NO">NO</option>
                                                            <option value="SI">SI</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="pre13">En caso de usar silla de ruedas para alguna discapacidad, mencione:</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" id="pre13" name="pre13" class="form-control" autocomplete='off' placeholder="Menciona la discapacidad" />
                                                    </div>
                                                </div>
                                                <div class="separator"></div>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="pre14">Utilizas pr�tesis ortop�dica?</label>
                                                    <div class="col-sm-6">
                                                        <select id="pre14" name="pre14" class="form-control">
                                                            <option value="">Selecciona ..</option>
                                                            <option value="NO">NO</option>
                                                            <option value="SI">SI</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="pre15">En caso de usar pr�tesis ortop�dica, mencione:</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" id="pre15" name="pre15" class="form-control" autocomplete='off' placeholder="Que tipo de pr�tesis ortop�dica" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab_1_3"><br>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="pre16">Tienes alguna discapacidad o problema de lenguaje?</label>
                                                    <div class="col-sm-6">
                                                        <select id="pre16" name="pre16" class="form-control">
                                                            <option value="">Selecciona ..</option>
                                                            <option value="NO">NO</option>
                                                            <option value="SI">SI</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="pre17">En caso de tener un problema de lenguaje, mencione:</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" id="pre17" name="pre17" class="form-control" autocomplete='off' placeholder="Menciona el problema de discapacidad" />
                                                    </div>
                                                </div>
                                                <div class="separator"></div>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="pre18">Tienes alguna discapacidad o problema de lectura?</label>
                                                    <div class="col-sm-6">
                                                        <select id="pre18" name="pre18" class="form-control">
                                                            <option value="">Selecciona ..</option>
                                                            <option value="NO">NO</option>
                                                            <option value="SI">SI</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="pre19">En caso de tener un problema de lectura, mencione:</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" id="pre19" name="pre19" class="form-control" autocomplete='off' placeholder="Menciona el problema de lectura" />
                                                    </div>
                                                </div>
                                                <div class="separator"></div>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="pre20">Tienes alguna discapacidad o problema de escritura?</label>
                                                    <div class="col-sm-6">
                                                        <select id="pre20" name="pre20" class="form-control">
                                                            <option value="">Selecciona ..</option>
                                                            <option value="NO">NO</option>
                                                            <option value="SI">SI</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="pre21">En caso de tener un problema de escritura, mencione:</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" id="pre21" name="pre21" class="form-control" autocomplete='off' placeholder="Menciona el problema de escritura" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab_1_4"><br>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="pre22">Proviene de alguna etnia?</label>
                                                    <div class="col-sm-6">
                                                        <select id="pre22" name="pre22" class="form-control">
                                                            <option value="">Selecciona ..</option>
                                                            <option value="NO">NO</option>
                                                            <option value="SI">SI</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="pre23">En caso de venir de una etnia, mencione:</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" id="pre23" name="pre23" class="form-control" autocomplete='off' placeholder="Menciona de que etnia" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="pre24">Profesa alguna religi�n?</label>
                                                    <div class="col-sm-6">
                                                        <select id="pre24" name="pre24" class="form-control">
                                                            <option value="">Selecciona ..</option>
                                                            <option value="NO">NO</option>
                                                            <option value="SI">SI</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="pre25">En caso de profesar una religi�n, mencione:</label>
                                                    <div class="col-sm-6">
                                                        <select id="pre25" name="pre25" class="form-control">
                                                            <option value="">Selecciona ..</option>
                                                            <option value="CAT�LICA" >CAT�LICA</option>
                                                            <option value="EVANG�LICA CRISTIANA">EVANG�LICA CRISTIANA</option>
                                                            <option value="BAUTISTA">BAUTISTA</option>
                                                            <option value="MORMONES">MORMONES</option>
                                                            <option value="TESTIGOS DE JEHOV�">TESTIGOS DE JEHOV�</option>
                                                            <option value="SIN RELIGI�N">SIN RELIGI�N</option>
                                                            <option value="OTRA">OTRA</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="pre26">En caso de profesar otra regili�n, mencione:</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" id="pre26" name="pre26" class="form-control" autocomplete='off' placeholder="Menciona la religi�n" />
                                                    </div>
                                                </div>
                                                <div class="separator"></div>
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="pre27">Tus ingresos familiares son mayor o igual a $5,524?</label>
                                                    <div class="col-sm-6">
                                                        <select id="pre27" name="pre27" class="form-control">
                                                            <option value="">Selecciona ..</option>
                                                            <option value="NO" >NO</option>
                                                            <option value="SI">SI</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                    </div>
                                </div>
                            </div>
                            <span class="date-range pull-right">
                                <button id="btn_save" class="btn btn-default"><i class="fa fa-print-square-o"></i> Guardar encuesta </button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </span>
                        </form>
                    </div>
                    </div>
            </div>
    </section>
    <!--/PAGE -->
    <!-- JAVASCRIPTS -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- JQUERY -->
    <script src="../js/jquery/jquery-2.0.3.min.js"></script>
    <!-- BOOTSTRAP -->
    <script src="../bootstrap-dist/js/bootstrap.min.js"></script>
    <script src="../js/bootbox/bootbox.min.js"></script>
    <!-- COOKIE -->
    <script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
    <script src="../js/jquery-validate/jquery.validate.js"></script>
    <script src="../js/jquery-validate/additional-methods.js"></script>
    <script src="../js/error_message.js"></script>
    <!-- CUSTOM SCRIPT -->
    <script src="../js/script.js"></script>
    <script src="doc001.js"></script>
    <!-- /JAVASCRIPTS -->
</body>
</html>
<%
    }else{
        response.sendRedirect("error600.jsp");
    }
%>