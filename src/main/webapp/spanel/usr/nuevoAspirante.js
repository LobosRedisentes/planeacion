jQuery(document).ready(function () {

    window.select2_birthState_url = "../getStateOptions";
    window.select2_birthTown_url = "../getTownOptions?state=";
    window.select2_localState_url = "../getStateOptions";
    window.select2_localTown_url = "../getTownOptions?state=";
    window.select2_local_url = "../getLocationOptions?town=";
    $.getScript("../js/edcore/popover_curp.js", function (data, textStatus, jqxhr) {});
    $.getScript("../js/edcore/popover_municipio.js", function (data, textStatus, jqxhr) {});
    $.getScript("../js/edcore/popover_rfc.js", function (data, textStatus, jqxhr) {});
    $.getScript("../js/edcore/popover_factor.js", function (data, textStatus, jqxhr) {});
    $.getScript("../js/edcore/popover_local.js", function (data, textStatus, jqxhr) {});
    $.getScript("../js/edcore/select2_birthPlace.js", function (data, textStatus, jqxhr) {});
    $.getScript("../js/edcore/select2_localPlace.js", function (data, textStatus, jqxhr) {});
    $.getScript("../js/edcore/validate_tabForm.js", function (data, textStatus, jqxhr) {
        $("#name").rules("add", {required: true, maxlength: 100});
        $("#firstLast").rules("add", {required: true, maxlength: 25});
        $("#secondLast").rules("add", {required: false, maxlength: 25});
        $("#birthDate").rules("add", {required: true});
        $("#select2_birthState").rules("add", {required: true});
        $("#select2_birthTown").rules("add", {required: true});
        $("#blobFactor").rules("add", {required: true});
        $("#rfc").rules("add", {required: true, maxlength: 13, minlength: 10});
        $("#curp").rules("add", {required: true, maxlength: 18, minlength: 18});
        $("#occupation").rules("add", {required: true});
        $("#status").rules("add", {required: true});
        $("#select2_localState").rules("add", {required: true});
        $("#select2_localTown").rules("add", {required: true});
        $("#select2_local").rules("add", {required: true});
        $("#street").rules("add", {required: true, maxlength: 100, minlength: 1});
        $("#numExternal").rules("add", {required: true, maxlength: 10});
        $("#numInternal").rules("add", {required: false, maxlength: 10});
        $("#phone").rules("add", {required: true});
        $("#cellPhone").rules("add", {required: true});
        $("#email").rules("add", {required: true, email: true});
        $("#password").rules("add", {required: true, maxlength: 15, minlength: 1});
        $("#repPassword").rules("add", {required: true, maxlength: 15, minlength: 1, equalTo: "#password"});
    });
    $("#sex").select2();
    $("#blobFactor").select2();
    $("#birthCountry").select2();
    $("#country").select2();
    $("#occupation").select2();
    $("#status").select2();

    $("#btn_guardar").click(function () {
        if ($("#frm_tab").valid()) {
            var name = $("#name").val();
            var firstLast = $("#firstLast").val();
            var secondLast = $("#secondLast").val();
            var birthDate = $("#birthDate").val();
            var birthTown = $("#select2_birthTown").val();
            var blobFactor = $("#blobFactor").val();
            var sex = $("input[name='sex']:checked", "#frm_tab").val();
            var rfc = $("#rfc").val();
            var curp = $("#curp").val();
            var occupation = $("#occupation").val();
            var status = $("#status").val();
            var local = $("#select2_local").val();
            var street = $("#street").val();
            var numExternal = $("#numExternal").val();
            var numInternal = $("#numInternal").val();
            var phone = $("#phone").val();
            var cellPhone = $("#cellPhone").val();
            var email = $("#email").val();
            var password = $("#password").val();
            spInit();
            var save = $.ajax({url: "newApplicant",
                data: "id=0&local=" + local + "&street=" + street + "&numExternal=" + numExternal + "&numInternal=" + numInternal + "&phohe=" + phone +
                        "&cellPhone=" + cellPhone + "&name=" + name + "&firstLast=" + firstLast + "&secondLast=" + secondLast + "&birthDate=" + birthDate +
                        "&birthTown=" + birthTown + "&blobFactor=" + blobFactor + "&sex=" + sex + "&rfc=" + rfc + "&curp=" + curp + "&occupation=" + occupation +
                        "&status=" + status + "&email=" + email + "&password=" + password,
                method: "POST"});
            save.done(function (msn) {
                spStop();
                switch(msn){
                    case "CORE0004": bootbox.alert("ERROR [CORE0004]: El usuario ya existe!. Favor de recuperar tus datos de acceso a traves de la opci&oacute;n proporcionada en el login de aspirantes", function() {}); break;
                    case "CORE0000": $.ajax({url: "../sendMail",data: "code=0001&password=" + password+"&mail="+email,method: "POST"});
                                     window.location = "bienvenida.jsp?mail=" + email + "&password=" + password;
                                     break;
                    default:         bootbox.alert("ERROR DESCONOCIDO!", function() {}); break;
                }
            });
            save.error(function () {
                spStop();
                bootbox.alert("Al parecer se presento un problema de conexi&oacute;n, intente mas tarde! ", function () {});
            });
            event.preventDefault();
        }
    });
});