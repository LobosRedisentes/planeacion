<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Sistema edcore - Nueva cuenta de usuario</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
    <meta name="description" content="Sistema Escolar - edcore">
    <meta name="author" content="Instituto Tecnol�gico Superior de Zapopan">	
    <link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
    <link rel="stylesheet" type="text/css" href="../css/themes/default.css" id="skin-switcher" >
    <link rel="stylesheet" type="text/css" href="../css/responsive.css" >
    <link rel="stylesheet" type="text/css" href="../js/select2/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.min.css" >
    <link rel="stylesheet" type="text/css" href="../css/edcore/main.css" >
    <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
</head>
<body>
    <div class="col-md-12" style="display: block; margin-left: 5.2%; width: 91%; height: 130px; margin-bottom: 20px; background-image: url('../img/header-logo.png'); background-size:contain; background-repeat: no-repeat; background-repeat: round;" >
    </div>
    
    <div class="col-md-12" style="display: block; margin-left: 3%; margin-bottom: -25px; width: 95%;" >
    <div class="col-md-12">
        <div class="box border inverse">
            <div class="box-title">
                <h4><i class="fa fa-bars"></i>Nueva cuenta de usuario</h4>
            </div>
            <div class="box-body">
                <form class="form-horizontal" id="frm_tab" action="#">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="tabbable">   
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab_1_1" data-toggle="tab"><i class="fa fa-home"></i> Generales</a></li>
                                    <li><a href="#tab_1_2" data-toggle="tab"><i class="fa fa-envelope"></i> Contacto/Cuenta</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="tab_1_1"><br>
                                        <div class="form-group ">
                                            <label class="col-md-4">Nombre</label>
                                            <label class="col-md-4">Apellido paterno</label>
                                            <label class="col-md-4">Apellido materno</label>
                                            <div class="col-md-4">
                                                <input type="text" name="name" id="name" placeholder="nombre(s)" class="form-control" style="text-transform:uppercase">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" name="firstLast" placeholder="apellido paterno" id="firstLast" class="form-control" style="text-transform:uppercase">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" name="secondLast" placeholder="apellido materno" id="secondLast" class="form-control" style="text-transform:uppercase">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label class="col-md-4">Estado civil</label>
                                            <label class="col-md-4">Ocupaci�n</label>
                                            <label class="col-md-4">Genero</label>
                                            <div class="col-md-4">
                                                <select name ="status" id ="status" class="col-md-12">
                                                    <option value=""> Selecciona ... </option>
                                                    <option value="SOLTERO">Soltero</option>
                                                    <option value="CASADO">Casado</option>
                                                    <option value="DIVORCIADO">Divorciado</option>
                                                    <option value="VIUDO">Viudo</option>
                                                    <option value="UNION LIBRE">Uni�n libre</option>
                                                    <option value="FINADO">Finado</option>
                                                    <option value="INDEFINIDO">Indefinido</option>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <select name ="occupation" id ="occupation" class="col-md-12">
                                                    <option value=""> Selecciona ... </option>
                                                    <option value="ESTUDIANTE"> Estudiante</option>
                                                    <option value="TRABAJADOR">Trabajador</option>
                                                    <option value="AMA DE CASA">Ama de casa</option>
                                                    <option value="OTRO">Otro</option>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <center>
                                                    <label class="radio-inline"> <input type="radio" class="uniform" name="sex" value="M" checked> Masculino </label>
                                                    &nbsp;&nbsp;&nbsp;<label class="radio-inline"> <input type="radio" class="uniform" name="sex" value="F"> Femenino </label>
                                                </center>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group ">
                                            <label class="col-md-4">Pais de nacimiento</label>
                                            <label class="col-md-4">Estado de nacimiento</label>
                                            <label class="col-md-4">Ciudad/Municipio de nacimiento</label>
                                            <div class="col-md-4">
                                                <select name ="birthCountry" id ="birthCountry" class="col-md-12" disabled>
                                                    <option value="1" selected> MEXICO </option>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="hidden" id="select2_birthState" class="col-md-12"  name="select2_birthState"  />
                                            </div>
                                            <div class="col-md-3">
                                                <input type="hidden" id="select2_birthTown" class="col-md-10"  name="select2_birthTown" />&nbsp;
                                            </div>
                                            <div class="col-md-1">
                                                <button id="popover_municipio" class="btn btn-xs btn-primary" data-toggle="popover" data-placement="left"><i class="fa fa-info"></i></button>
                                            </div>  
                                        </div>
                                        <div class="form-group ">
                                            <label class="col-md-4">Fecha de nacimiento</label>
                                            <label class="col-md-4">CURP</label>
                                            <label class="col-md-4">RFC</label>
                                            <div class="col-md-4">
                                                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <input type="text" name="birthDate" id="birthDate" placeholder="dd/mm/yyyy" class="form-control" data-mask="99/99/9999">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="curp" placeholder="curp" id="curp" class="form-control" style="text-transform:uppercase">
                                            </div>
                                            <div class="col-md-1">
                                                <button id="popover_curp" class="btn btn-xs btn-primary"  data-toggle="popover" ><i class="fa fa-info"></i></button>
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="rfc" placeholder="rfc" id="rfc" class="form-control" style="text-transform:uppercase">
                                            </div>
                                            <div class="col-md-1">
                                                <button id="popover_rfc" class="btn btn-xs btn-primary"  data-toggle="popover" data-placement="left"><i class="fa fa-info"></i></button>
                                            </div>                                        
                                        </div>
                                        <div class="form-group ">
                                            <label class="col-md-4">Factor sanguineo</label>
                                            <label class="col-md-4">&nbsp;</label>
                                            <label class="col-md-4">&nbsp;</label>
                                            <div class="col-md-3">
                                                <select name ="blobFactor" id ="blobFactor" class="col-md-12">
                                                    <option value="INDEFINIDO"> Selecciona ... </option>
                                                    <option value="A_P"> A+ </option>
                                                    <option value="A_N"> A- </option>
                                                    <option value="B_P"> B+ </option>
                                                    <option value="B_N"> B- </option>
                                                    <option value="O_P"> O+ </option>
                                                    <option value="AB_P"> AB+ </option>
                                                    <option value="AB_N"> AB- </option>
                                                </select>                                                
                                            </div>
                                            <div class="col-md-1">
                                                <button id="popover_factor" class="btn btn-xs btn-primary"  data-toggle="popover" ><i class="fa fa-info"></i></button>
                                            </div>
                                            <div class="col-md-4"></div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab_1_2"><br>
                                       <div class="form-group ">
                                            <label class="col-md-4">Pais</label>
                                            <label class="col-md-4">Estado</label>
                                            <label class="col-md-4">Ciudad/Municpio</label>
                                            <div class="col-md-4">
                                                <select name ="country" id ="country" class="col-md-12" disabled>
                                                    <option value="1" selected> MEXICO </option>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="hidden" id="select2_localState" class="col-md-12"  name="select2_localState" />
                                            </div>
                                            <div class="col-md-4">
                                                <input type="hidden" id="select2_localTown" class="col-md-12"  name="select2_localTown" />
                                            </div>
                                        </div>
                                       <div class="form-group ">
                                            <label class="col-md-4">Colonia</label>
                                            <label class="col-md-4">Domicilio</label>
                                            <label class="col-md-2">Numero exterior</label>
                                            <label class="col-md-2">Numero interior</label>
                                            <div class="col-md-3">
                                                <input type="hidden" id="select2_local" class="col-md-12"  name="select2_local" />
                                            </div>
                                            <div class="col-md-1">
                                                <button id="popover_local" class="btn btn-xs btn-primary" data-toggle="popover" data-placement="left"><i class="fa fa-info"></i></button>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" name="street"  id="street" placeholder="calle" class="form-control" style="text-transform:uppercase">
                                            </div>
                                            <div class="col-md-2">
                                                <input type="text" name="numExternal" id="numExternal" placeholder="0" class="form-control">
                                            </div>
                                            <div class="col-md-2">
                                                <input type="text" name="numInternal" id="numInternal" placeholder="0" class="form-control">
                                            </div>
                                        </div>
                                       <div class="form-group ">
                                            <label class="col-md-4">Telefono</label>
                                            <label class="col-md-4">Celular</label>
                                            <label class="col-md-4">Correo</label>
                                            <div class="col-md-4">
                                                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                                    <input type="text" name="phone" id="phone" placeholder="##-##-##-##" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                                <input type="text" name="cellPhone" placeholder="##-##-##-##" id="cellPhone" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="input-group"> <span class="input-group-addon">@</span>
                                                    <input type="text" name="email" id="email" placeholder="email@dominio.com" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label class="col-md-4">Asigna una contrase�a para tu nueva cuenta</label>
                                            <label class="col-md-4">Confirma tu nueva contrase�a</label>
                                            <label class="col-md-4">&nbsp;</label>
                                            <div class="col-md-4">
                                                <input type="password" name="password" placeholder="M�x 15 car�cteres" id="password" class="form-control">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="password" name="repPassword" placeholder="M�x 15 car�cteres" id="repPassword" class="form-control">
                                            </div>
                                            <div class="col-md-4">&nbsp;</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    <div class="col-md-12" style="display: block; margin-left: 3%; width: 94%; height: 60px;" >
        <div class="clearfix">
            <span class="date-range pull-right">
                <p class="btn-toolbar">
                    <button id="btn_guardar" class="btn btn-default"><i class="fa fa-save"></i> Guardar</button>
                </p>
            </span>
        </div>    
    </div>
    <div class="col-md-12" style="display: block; margin-left: 1.5%; margin-top: 10px; width: 95%" >
        <p style="text-align: center">Copyright &copy; edcore 2015 Instituto T�cnologico Superior de Zapopan<br>Todos los derechos reservados.</p>
    </div>
    <!-- JAVASCRIPTS -->
    <script src="../js/jquery/jquery-2.0.3.min.js"></script>
    <script src="../bootstrap-dist/js/bootstrap.min.js"></script>
    <script src="../js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
    <script src="../js/bootbox/bootbox.min.js"></script>
    <script src="../js/spin.js"></script>
    <script src="../js/jquery-validate/jquery.validate.js"></script>
    <script src="../js/jquery-validate/additional-methods.js"></script>
    <script src="../js/edcore/main.js"></script>
    <script src="../js/select2/select2.min.js"></script>
    <script src="../js/script.js"></script>
    <script src="nuevoAspirante.js"></script>
</body>
</html>
