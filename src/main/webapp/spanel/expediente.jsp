<%@page import="mx.com.edcore.vo.AccountVO"%>
<%@page import="mx.com.edcore.vo.SessionVO"%>
<%
    HttpSession websession;
    websession = request.getSession();
    SessionVO sesion =  (SessionVO) websession.getAttribute("session");
    if(sesion != null){
        AccountVO account =  (AccountVO) websession.getAttribute("account");
        
%>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Sistema escolar - Edcore</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="Sistema Escolar - Edcore">
	<meta name="author" content="Instituto Tecnologico Superior de Zapopan">	
	<link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
	<link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
	<!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>	
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        <link rel="stylesheet" type="text/css" href="../js/select2/select2.min.css" />
        <link rel="stylesheet" type="text/css" href="../css/error_msn.css" >
</head>
<body>
    <!-- Modal -->
    <div class="modal fade" id="box_lista" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 30%">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header" style="background-color: rgb(207, 0, 0); color: #FFFFFF; height: 50px">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Nuevo documento</h4>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <div class="row">
                        <form role="form" class="form-horizontal" action="" method="post" name="frm_lista" id="frm_lista">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="document">Tipo:</label>
                                <div class="col-sm-9">
                                    <input type="hidden" id="document" class="col-md-12"  name="document" />
                                </div>
                            </div>
                            <span class="date-range pull-right">
                                <button id="btn_nuevo" class="btn btn-default"><i class="fa fa-print-square-o"></i> Crear  </button>&nbsp;
                                <button id="btn_cancelarDocumento" class="btn btn-default"><i class="fa fa-print-square-o"></i> Cancelar  </button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </span>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="box_test_vocacional" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 80%">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header" style="background-color: rgb(207, 0, 0); color: #FFFFFF; height: 50px">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Test Vocacional</h4>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <div class="row">
                        <iframe id="if_test_vocacional" src="testVocacional.jsp" style="width: 100%; height: 600px;" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="box_documentos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" id="box_documentos_width">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header" style="background-color: rgb(207, 0, 0); color: #FFFFFF; height: 50px">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title"  id="box_documentos_title"></h4>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <div class="row">
                        <iframe id="frame_documentos" name="frame_doc"  src="" style="width: 100%;" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- HEADER -->
    <%@include file="header.jsp" %>
    <!--/HEADER -->
    <!-- PAGE -->
    <section id="page">
        <!-- SIDEBAR -->
        <%@include file="sidebar.jsp" %>
        <!-- /SIDEBAR -->
        <div id="main-content">
            <div class="container">
                <div class="row">
                    <div id="content" class="col-lg-12">
                        <!-- PAGE HEADER-->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-header">
                                    <!-- BREADCRUMBS -->
                                    <ul class="breadcrumb">
                                        <li><i class="fa fa-home"></i><a href="index.jsp">Inicio</a></li>
                                        <li><i class="fa"></i><a href="#">Inscripciones</a></li>
                                        <li>Expedientes</li>
                                    </ul>
                                    <!-- /BREADCRUMBS -->
                                    <div class="clearfix">
                                        <h3 class="content-title pull-left">Expediente de inscripciones</h3>
                                            <span class="date-range pull-right">
                                                <p class="btn-toolbar">
                                                    <button id="btn_lista" class="btn btn-default"><i class="fa fa-print-square-o"></i> Nuevo </button>
                                                    <button id="btn_print" class="btn btn-default" disabled=""><i class="fa fa-print-square-o"></i> Editar</button>
                                                    <button id="btn_print" class="btn btn-default" disabled=""><i class="fa fa-print-square-o"></i> Imprimir</button>
                                                </p>
                                            </span>
                                    </div>
                                    <div class="description"></div>
                                </div>
                            </div>
                        </div>
                        <!-- /PAGE HEADER -->
                        <!-- DASHBOARD CONTENT -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box border red">
                                    <div class="box-title">
                                        <h4>Documentos de nuevo ingreso</h4>
                                        <div class="tools hidden-xs"></div>
                                    </div>
                                    <div class="box-body">
                                        <div class="item">
                                            <div class="item-header" style="padding: 5px; height: 300px;">
                                                <div class="row">
                                                    <div class="col-xs-2 col-md-2">
                                                        <a id="btn_test_vocacional" name="btn_test_vocacional" class="thumbnail" style="width: 100%; border: none;">
                                                            <img src="../img/nuevo.png" alt="50px" width="40px">
                                                            <center style="font-family: Arial; font-weight: normal;">Test Vocacional</center>
                                                        </a>
                                                    </div>
                                                    <div class="col-xs-2 col-md-2">
                                                        <a id="DOC001_001_BTN_openBox" name="DOC001_001_BTN_openBox" class="thumbnail" style="width: 100%; border: none;">
                                                            <img src="../img/nuevo.png" alt="50px" width="40px">
                                                            <center style="font-family: Arial; font-weight: normal;">Encuesta de Salud</center>
                                                        </a>
                                                    </div>
                                                    <div class="col-xs-2 col-md-2">
                                                        <a href="#" class="thumbnail" style="width: 100%; border: none;">
                                                            <img src="../img/pdf.png" alt="50px" width="40px">
                                                            <center style="font-family: Arial; font-weight: normal;">Solicitud Licenciatura<br>Agosto 2015 </center>
                                                        </a>
                                                    </div>
                                                    <div class="col-xs-2 col-md-2">
                                                        <a href="#" class="thumbnail" style="width: 100%; border: none;">
                                                            <img src="../img/pdf.png" alt="50px" width="40px">
                                                            <center style="font-family: Arial; font-weight: normal;">Ficha de Examen<br>Agosto 2015 </center>
                                                        </a>
                                                    </div>
                                                    <div class="col-xs-2 col-md-2">
                                                        <a href="#" class="thumbnail" style="width: 100%; border: none;">
                                                            <img src="../img/pdf.png" alt="50px" width="40px">
                                                            <center style="font-family: Arial; font-weight: normal;">Convalidación<br>Agosto 2015 </center>
                                                        </a>
                                                    </div>
                                                    <div class="col-xs-2 col-md-2">
                                                        <a href="#" class="thumbnail" style="width: 100%; border: none;">
                                                            <img src="../img/pdf.png" alt="50px" width="40px">
                                                            <center style="font-family: Arial; font-weight: normal;">Ficha de pago<br> Agosto 2015 </center>
                                                        </a>
                                                    </div>
                                                    <div class="col-xs-2 col-md-2">
                                                        <a href="#" class="thumbnail" style="width: 100%; border: none;">
                                                            <img src="../img/pdf.png" alt="50px" width="40px">
                                                            <center style="font-family: Arial; font-weight: normal;">Carga Horaria<br> Agosto 2015 </center>
                                                        </a>
                                                    </div>
                                                </div>          
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" style="display: block; margin-left: 1.5%; width: 95%" >
                                <p style="text-align: center">Copyright &copy; edcore 2015 Instituto Técnologico Superior de Zapopan<br>Todos los derechos reservados.</p>
                            </div>
                        </div>
                        <!-- /DASHBOARD CONTENT -->
                    </div><!-- /CONTENT-->
                </div>
            </div>
        </div>
    </section>
    <!--/PAGE -->
    <!-- JAVASCRIPTS -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- JQUERY -->
    <script src="../js/jquery/jquery-2.0.3.min.js"></script>
    <!-- BOOTSTRAP -->
    <script src="../bootstrap-dist/js/bootstrap.min.js"></script>
    <script src="../js/bootbox/bootbox.min.js"></script>
    <!-- COOKIE -->
    <script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
    <script src="../js/jquery-validate/jquery.validate.js"></script>
    <script src="../js/jquery-validate/additional-methods.js"></script>
    <script src="../js/error_message.js"></script>
    <!-- SELECT2 -->
    <script type="text/javascript" src="../js/select2/select2.min.js"></script>
    <!-- CUSTOM SCRIPT -->
    <script src="../js/script.js"></script>
    <script src="expediente.js"></script>
    <!-- /JAVASCRIPTS -->
</body>
</html>
<%
    }else{
        response.sendRedirect("error600.jsp");
    }
%>