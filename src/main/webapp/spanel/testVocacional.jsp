<%@page import="mx.com.edcore.dao.UserDAO"%>
<%@page import="mx.com.edcore.vo.UserVO"%>
<%@page import="mx.com.edcore.vo.AccountVO"%>
<%@page import="mx.com.edcore.vo.SessionVO"%>
<%
    HttpSession websession;
    websession = request.getSession();
    AccountVO account =  (AccountVO) websession.getAttribute("account");
    UserVO user = UserDAO.getUser(account.getUser());
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Test Vocacionales</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="Sistema Escolar - Edcore">
	<meta name="author" content="Instituto Tecnologico Superior de Zapopan">	
	<link rel="stylesheet" type="text/css" href="../css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="../css/themes/default.css" id="skin-switcher" >
	<link rel="stylesheet" type="text/css"  href="../css/responsive.css" >
	<!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>	
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        
        <link rel="stylesheet" type="text/css" href="../css/error_msn.css" >
    </head>
    <body>
        <form method="POST" action="http://200.78.248.93/sistema/orientacion/php/test_reac.php">
            <input type="hidden" name="folio" value="<%= user.getId() %>" />
            <input type="hidden" name="nombre" value="<%= user.getName() %>" />
            <input type="hidden" name="apellido" value="<%= user.getFirstName()+" "+user.getSecondName() %>" />
            <button id="test" class="btn btn-default" ><i class="fa fa-pencil"></i> Empezar a realizar Test</button>
        </form>
            <button id="close" class="btn btn-default" ><i class="fa fa-pencil"></i> Close</button>
    <!--/PAGE -->
    <!-- JAVASCRIPTS -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- JQUERY -->
    <script src="../js/jquery/jquery-2.0.3.min.js"></script>
    <!-- BOOTSTRAP -->
    <script src="../bootstrap-dist/js/bootstrap.min.js"></script>
    <script src="../js/bootbox/bootbox.min.js"></script>
    <!-- COOKIE -->
    <script type="text/javascript" src="../js/jQuery-Cookie/jquery.cookie.min.js"></script>
    <script src="../js/jquery-validate/jquery.validate.js"></script>
    <script src="../js/jquery-validate/additional-methods.js"></script>
    <script src="../js/error_message.js"></script>
    <!-- CUSTOM SCRIPT -->
    <script src="../js/script.js"></script>
            
        <script>
        jQuery(document).ready(function () {
            App.setPage("form");  //Set current page
            App.init(); //Initialise plugins and element
            
            $("#close").click(function(){
                window.parent.jQuery('#box_test_vocacional').modal('toggle');
            });
    
        });
        </script>
    </body>
</html>
