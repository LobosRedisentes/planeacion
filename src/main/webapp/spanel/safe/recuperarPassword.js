jQuery(document).ready(function() {

    $.getScript("../../js/validate/form.js", function (data, textStatus, jqxhr) {
        $("#email").rules("add", {required: true, maxlength: 100, minlength: 5});
    });
    $("#btn_passwd").click(function (event) {
        if($("#frm").valid()){
            var email = $('#email').val();
            var save = $.ajax({url:"../../safe/recoverPassword",data:"email="+email,method:"POST"});
            spInit();
            save.done(function (msn) {
                spStop();
                switch (msn) {
                    case "CORE0000":
                        bootbox.alert("Los datos de tu cuenta fueron enviados a tu correo", function() {window.location = "login.jsp";});
                        break;
                    case "CORE0001":
                        bootbox.alert("ERROR [CORE0001]: El correo no esta registrado!.", function () {});
                        break;
                    case "CORE0003":
                        bootbox.alert("ERROR [CORE0003]: La cuenta esta inactiva!.", function () {});
                        break;
                    default:
                        bootbox.alert("ERROR DESCONOCIDO [" + msn + "]", function () {});
                        break;
                };
            });
            save.error(function () {
                spStop();
                bootbox.alert("Al parecer se presento un problema de conexi&oacute;n, intente refrescar la pagina. ", function () {
                });
            });
            event.preventDefault();
        }
    });
});