<%@page import="java.util.List"%>
<%@page import="mx.com.edcore.vo.AccountVO"%>
<%@page import="mx.com.edcore.vo.SessionVO"%>
<%
    HttpSession websession;
    websession = request.getSession();
    SessionVO sesion = (SessionVO) websession.getAttribute("session");
    if (sesion != null) {
        List<String> privileges = (List<String>) websession.getAttribute("privileges");
        if (privileges.contains("SAFE03")) {
%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <title>edcore - Escolar</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
        <meta name="description" content="edcore - Escolar">
        <link rel="icon" type="image/png" sizes="32x32" href="../../img/favicon-32x32.png">
        <meta name="author" content="Instituto Tecnologico Superior de Zapopan">	
        <link rel="stylesheet" type="text/css" href="../../lib/cloudAdmin/css/cloud-admin.css" >
        <link rel="stylesheet" type="text/css" href="../../lib/cloudAdmin/css/default.css" id="skin-switcher" >
        <link rel="stylesheet" type="text/css" href="../../lib/cloudAdmin/css/responsive.css" >
        <link rel="stylesheet" type="text/css" href="../../lib/select2/select2.min.css" />
        <link rel="stylesheet" type="text/css" href="../../css/edcore.css" >
        <link rel="stylesheet" type="text/css" href="../../font/awesome/css/font-awesome.min.css">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
        <script>var id = 0;</script>
    </head>
    <body>
    <div class="modal fade" id="box_new" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 780px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header" style="background-color: rgb(207, 0, 0); color: #FFFFFF; height: 50px">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Cuenta de usuario</h4>
                </div>
                    <div class="modal-body" style="width: 100%;">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="form-horizontal" name="frm"  id="frm">
                                    <div class="col-md-3"> &nbsp;&nbsp;&nbsp;&nbsp;
                                        <img height="150px" width="150px" class="img-circle" name="photo2" id="photo2" src="" alt="" />
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="code">Matricula:</label>
                                            <div class="col-md-6">
                                                <input type="text" id="code" name="code"  class="form-control" autocomplete="off" disabled="true"  />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="name">Nombre:</label>
                                            <div class="col-md-6">
                                                <input type="text" id="name" name="name"  class="form-control" autocomplete="off" disabled="true"  />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="email">Email:</label>
                                            <div class="col-md-6">
                                                <input type="email" id="email" name="email"  class="form-control" autocomplete="off"  disabled="true" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="passwd">Password:</label>
                                            <div class="col-md-6">
                                                <input type="password" id="passwd" name="passwd"  class="form-control" autocomplete="off" disabled="true"  />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="rol">Rol:</label>
                                            <div class="col-md-6">
                                                <input type="text" id="rol" name="rol"  class="form-control" autocomplete="off" disabled="true"  />
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- HEADER -->
        <%@include file="../header.jsp" %>
        <!--/HEADER -->
        <!-- PAGE -->
        <section id="page">
            <!-- SIDEBAR -->
            <%@include file="../sidebar.jsp" %>
            <!-- /SIDEBAR -->
            <div id="main-content">
                <div class="container">
                    <div class="row">
                        <div id="content" class="col-lg-12">
                            <!-- PAGE HEADER-->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="page-header">
                                        <!-- BREADCRUMBS -->
                                        <ul class="breadcrumb">
                                            <li><i class="fa fa-home"></i><a href="index.jsp">Inicio</a></li>
                                            <li><i class="fa"></i><a href="#">Seguridad</a></li>
                                            <li>Cuenta</li>
                                        </ul>
                                        <!-- /BREADCRUMBS -->
                                        <div class="clearfix">
                                            <h3 class="content-title pull-left">Administración de cuentas</h3>
                                            <span class="date-range pull-right">
                                                <p class="btn-toolbar">
                                                    <%if (privileges.contains("SAFE09")) {%>
                                                    <button id="btn_new" class="btn btn-default" disabled="true" ><i class="fa fa-print-square-o"></i> Nuevo</button>
                                                    <%}%>
                                                    <%if (privileges.contains("SAFE09")) {%>
                                                    <button id="btn_edit" class="btn btn-default" disabled="true" ><i class="fa fa-print-square-o"></i> Editar</button>
                                                    <%}%>
                                                </p>
                                            </span>
                                        </div>
                                        <div class="description"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- /PAGE HEADER -->
                            <!-- DASHBOARD CONTENT -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box border red">
                                        <div class="box-title">
                                            <h4>Cuentas de usuario</h4>
                                            <div class="tools hidden-xs"></div>
                                        </div>
                                        <div class="box-body">
                                            <table id="datatable" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-hover table-responsive">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <th>id</th>
                                                        <th>code</th>
                                                        <th>name</th>
                                                        <th>rol</th>
                                                        <th>type</th>
                                                        <th>office</th>
                                                        <th>status</th>
                                                    </tr>
                                                </thead>

                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" style="display: block; margin-left: 1.5%; width: 95%" >
                                    <p style="text-align: center">Copyright &copy; edcore 2015 Instituto Técnologico Superior de Zapopan<br>Todos los derechos reservados.</p>
                                </div>
                            </div>
                            <!-- /DASHBOARD CONTENT -->
                        </div><!-- /CONTENT-->
                    </div>
                </div>
            </div>
        </section>
        <!--/PAGE -->
        <!-- JAVASCRIPTS -->
        <script src="../../lib/jquery/jquery-2.0.3.min.js"></script>
        <script src="../../lib/bootstrap-dist/js/bootstrap.min.js"></script>
        <script src="../../lib/cloudAdmin/js/script.js"></script>
        <script src="../../lib/bootbox/bootbox.min.js"></script>
        <script src="../../lib/jquery-validate/jquery.validate.js"></script>
        <script src="../../lib/jquery-validate/additional-methods.js"></script>
        <script src="../../js/edcore.js"></script>
        <script src="../../js/spin.js"></script>
        <script src="../../js/exceptions.js"></script>
        <script src="../../lib/datatables/media/js/jquery.dataTables.min.js"></script>
        <script src="../../lib/datatables/media/assets/js/datatables.min.js"></script>
        <script src="../../lib/datatables/extras/jquery.dataTables.columnFilter.js"></script>
        <script src="../../lib/jQuery-Cookie/jquery.cookie.min.js"></script>
        <script src="cuentas.js"></script>
        <!-- /JAVASCRIPTS -->
    </body>
</html>
<%
        } else {
            response.sendRedirect("error600.jsp");
        }
    } else {
        response.sendRedirect("error600.jsp");
    }
%>