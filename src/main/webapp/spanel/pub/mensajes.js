jQuery(document).ready(function () {
    App.setPage("form");
    App.init(); 
    $.getScript("../js/messages.js");
    $("#photo").attr("src", "../users/getPhoto?mode=usr");

    var table = $('#datatable').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sort": "position",
        "sAjaxSource": "getMessageGrid?mode=usr",
        "aoColumns": [
            { "mData": "id", "bSortable": false },
            { "mData": "fromUserName" },
            { "mData": "subject" },
            { "mData": "date" },
            { "mData": "status" }
        ],
        aoColumnDefs  : [
            {
                aTargets: [0],    // Column number which needs to be modified
                fnRender: function (o, v) {   // o, v contains the object and value for the column
                    return '<input type="checkbox" id="id" name="someCheckbox" />';
                },
                sClass: 'tableCell'    // Optional - class to be applied to this table cell
            },
            {
                aTargets: [4],    // Column number which needs to be modified
                fnRender: function (o, v) {   // o, v contains the object and value for the column
                    if(v === "NO LEIDO"){
                    return '<input type="checkbox" id="id" name="someCheckbox" />';
                    }else{
                     return '<label>leido</label>';
                    }
                },
                sClass: 'tableCell'    // Optional - class to be applied to this table cell
            }
        ]
    });
    
});