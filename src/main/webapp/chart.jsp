<%-- 
    Document   : chart
    Created on : 28-feb-2016, 16:57:33
    Author     : gabriel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="lib/xcharts/xcharts.css">
    </head>
    <body>
        <figure style="width: 800px; height: 300px;" id="chart1"></figure>
        <figure style="width: 800px; height: 300px;" id="chart2"></figure>
        
        <script src="lib/jquery/jquery-2.0.3.min.js"></script>
	<script src="lib/bootstrap-dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="lib/d3/d3.v3.min.js"></script>
	<script type="text/javascript" src="lib/xcharts/xcharts.min.js"></script>
	<script>
            jQuery(document).ready(function() {
 
                var chart; 
                var set_m = [];
                var set_f = [];
                var data = [];
                
                

                
                var tt = document.createElement('div');
                var leftOffset = -(~~$('html').css('padding-left').replace('px', '') + ~~$('body').css('margin-left').replace('px', ''));
                var topOffset = -32;
                tt.className = 'ex-tooltip';
                document.body.appendChild(tt);
                
                var opts = {
                "paddingLeft" : 30,
                "paddingTop" : 10,
                "paddingRight" : 10,
                  "axisPaddingTop": 25,
                   axisPaddingLeft : 25,
                  "mouseover": function (d, i) {
                    var pos = $(this).offset();
                    $(tt).text('Alumnos : ' + d.y).css({top: topOffset + pos.top, left: pos.left + leftOffset}).show();
                  },
                  "mouseout": function (x) {
                    $(tt).hide();
                  }
                };
                
                $.getJSON('getVal001Chart', function (data) {
                   $.each(data, function (key, value) {
                       if(value.sex === "M"){
                           set_m.push({
                               x: value.program,
                               y: value.students
                           });
                       }
                       if(value.sex === "F"){
                           set_f.push({
                               x: value.program,
                               y: value.students
                           });
                       }   
                   });

                   data = {"xScale":"ordinal",
                            "comp":[],
                            "main":[{"className":".main.l1","legend": "M","data":set_m},
                                    {"className":".main.l2","legend": "F","data":set_f}],
                            "type":"bar",
                            "yScale":"linear"};
                    chart = new xChart('line-dotted', data, '#chart1', opts);
               });
           });
	</script>
        <script>
            jQuery(document).ready(function() {
 
                var chart; 
                var set_m = [];
                var set_f = [];
                var data = [];
                
                

                
                var tt = document.createElement('div');
                var leftOffset = -(~~$('html').css('padding-left').replace('px', '') + ~~$('body').css('margin-left').replace('px', ''));
                var topOffset = -32;
                tt.className = 'ex-tooltip';
                document.body.appendChild(tt);
                
                var opts = {
                "paddingLeft" : 30,
                "paddingTop" : 10,
                "paddingRight" : 10,
                  "axisPaddingTop": 25,
                   axisPaddingLeft : 25,
                  "mouseover": function (d, i) {
                    var pos = $(this).offset();
                    $(tt).text('Alumnos : ' + d.y).css({top: topOffset + pos.top, left: pos.left + leftOffset}).show();
                  },
                  "mouseout": function (x) {
                    $(tt).hide();
                  }
                };
                
                $.getJSON('getVal002Chart', function (data) {
                   $.each(data, function (key, value) {
                       if(value.sex === "M"){
                           set_m.push({
                               x: value.program,
                               y: value.students
                           });
                       }
                       if(value.sex === "F"){
                           set_f.push({
                               x: value.program,
                               y: value.students
                           });
                       }   
                   });

                   data = {"xScale":"ordinal",
                            "comp":[],
                            "main":[{"className":".main.l1","legend": "M","data":set_m},
                                    {"className":".main.l2","legend": "F","data":set_f}],
                            "type":"bar",
                            "yScale":"linear"};
                    chart = new xChart('line-dotted', data, '#chart2', opts);
               });
           });
	</script>
    </body>
</html>
