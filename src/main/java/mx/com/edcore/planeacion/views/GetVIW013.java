/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.planeacion.views;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mx.com.edcore.util.HibernateUtil;
import mx.com.edcore.util.JsonObject;
import mx.com.edcore.view.VIW013;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
/**
 *
 * @author wero
 */
public class GetVIW013 extends HttpServlet {
    
/**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        
        int displaystart;
        int displaylength;
        StringBuilder sql;
        SessionFactory sf;
        Session sesion = null;
        Query query;
        List<VIW013> list = null;
        JsonObject viewObject;
        Gson gson;
        
        try {
            displaystart = Integer.parseInt(request.getParameter("iDisplayStart"));
            displaylength = Integer.parseInt(request.getParameter("iDisplayLength"));
            sql = new StringBuilder("FROM VIW013 ");
            
            if(request.getParameter("sSearch") != null){
                String search = request.getParameter("sSearch");
                sql.append("WHERE LOWER(transmitter) LIKE '%");
                sql.append(search.toLowerCase());
                sql.append("%' OR LOWER(receiver) LIKE '%");
                sql.append(search.toLowerCase());
                sql.append("%' OR LOWER(issue) LIKE '%");
                sql.append(search.toLowerCase());
                sql.append("%' OR LOWER(content) LIKE '%");
                sql.append(search.toLowerCase());
                sql.append("%' OR LOWER(date) LIKE '%");
                sql.append(search.toLowerCase());
                sql.append("%' OR LOWER(status) LIKE '%");
                sql.append(search.toLowerCase());
                sql.append("%') ");
            }
            if(request.getParameter("iSortCol_0") != null){
                int sortcol = Integer.parseInt(request.getParameter("iSortCol_0"));
                switch(sortcol){
                    case 1: sql.append("ORDER BY transmitter "); 
                            sql.append(request.getParameter("sSortDir_0")); 
                            break;
                    case 2: sql.append("ORDER BY receiver ");
                            sql.append(request.getParameter("sSortDir_0"));
                            break;
                    case 3: sql.append("ORDER BY issue "); 
                            sql.append(request.getParameter("sSortDir_0"));
                            break;
                    case 4: sql.append("ORDER BY content "); 
                            sql.append(request.getParameter("sSortDir_0"));
                            break;
                    case 5: sql.append("ORDER BY date ");
                            sql.append(request.getParameter("sSortDir_0"));
                            break;
                    case 6: sql.append("ORDER BY status "); 
                            sql.append(request.getParameter("sSortDir_0"));
                            break;
                }
            }
            sf = HibernateUtil.getSessionFactory();
            sesion = sf.openSession();
            query = sesion.createQuery(sql.toString());
            list = query.list();
            int max_length = list.size();
            if(list.size() < displaylength){
                displaylength = list.size();
            }
            list = list.subList(displaystart,displaylength);
            sesion.close();
            
            viewObject = new JsonObject();
            viewObject.setsEcho(Integer.parseInt(request.getParameter("sEcho")));
            viewObject.setiTotalDisplayRecords(list.size());
            viewObject.setiTotalRecords(max_length);
            viewObject.setAaData(list);
            
            gson = new GsonBuilder().setPrettyPrinting().create();
            String json = gson.toJson(viewObject);
            out.print(json);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
