/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.planeacion.pub;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.com.edcore.dao.PublicationDAO;
import mx.com.edcore.view.VIW028;
import mx.com.edcore.view.VIW046;


@WebServlet(name = "getMessageList", urlPatterns = {"/pub/getMessageList"})
public class GetMessageList extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();

        HttpSession websession = request.getSession();
        VIW028 waccount = (VIW028) websession.getAttribute("account");
        Long id = 0L;
        List<VIW046> list;
        String json = "";
        try {
            String mode = request.getParameter("mode").toUpperCase();
            String status = request.getParameter("status").toUpperCase();
            if(mode.equals("USR")){
                id = waccount.getId();
            }else if(mode.contentEquals("ADM")){
                id = Long.valueOf(request.getParameter("id"));
            }
            if(id != 0){
                list = PublicationDAO.getMessages("SELECT * FROM VIW046 WHERE toAccount = "+id+" AND status = '"+status+"'",10,0);
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                json = gson.toJson(list);
            }
            out.print(json);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
