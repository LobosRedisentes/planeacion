/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.planeacion.pub;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.com.edcore.util.HibernateUtil;
import mx.com.edcore.util.JsonObject;
import mx.com.edcore.view.VIW028;
import mx.com.edcore.view.VIW046;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

@WebServlet(name = "getMessageList", urlPatterns = {"/pub/getMessageGrid"})
public class GetMessageGrid extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        
        HttpSession websession = request.getSession();
        VIW028 waccount = (VIW028) websession.getAttribute("account");
        Long id = 0L;
        int displaystart;
        int displaylength;
        StringBuilder sql;
        List<VIW046> list;        
        JsonObject viewObject;
        Gson gson;
        
        try {
             String mode = request.getParameter("mode").toUpperCase();
            if(mode.equals("USR")){
                id = waccount.getId();
            }else if(mode.contentEquals("ADM")){
                id = Long.valueOf(request.getParameter("id"));
            }
            displaystart = Integer.parseInt(request.getParameter("iDisplayStart"));
            displaylength = Integer.parseInt(request.getParameter("iDisplayLength"));
            sql = new StringBuilder("SELECT id,fromUser,concat(fromUserName,' ',fromUserFirstLast) as fromUserName,fromUserFirstLast,fromUserSecondLast,subject,body,date,toUser,toUserName,toUserFirstLast,toUserSecondLast,status FROM VIW046 WHERE toUser = "+id+" AND status <> 'BORRADO' AND (");
            
            if(request.getParameter("sSearch") != null){
                String search = request.getParameter("sSearch");
                sql.append(" LOWER(concat(fromUserName,' ',fromUserFirstLast)) LIKE '%");
                sql.append(search.toLowerCase());
                sql.append("%' OR LOWER(subject) LIKE '%");
                sql.append(search.toLowerCase());
                sql.append("%' OR LOWER(date) LIKE '%");
                sql.append(search.toLowerCase());
                sql.append("%' OR LOWER(status) LIKE '%");
                sql.append(search.toLowerCase());
                sql.append("%') ");
            }
            if(request.getParameter("iSortCol_0") != null){
                int sortcol = Integer.parseInt(request.getParameter("iSortCol_0"));
                switch(sortcol){
                    case 1: sql.append("ORDER BY concat(concat(fromUserName,' ',fromUserFirstLast)) ");
                            sql.append(request.getParameter("sSortDir_0"));
                            break;
                    case 2: sql.append("ORDER BY subject "); 
                            sql.append(request.getParameter("sSortDir_0"));
                            break;
                    case 3: sql.append("ORDER BY date "); 
                            sql.append(request.getParameter("sSortDir_0"));
                            break;
                    case 4: sql.append("ORDER BY status "); 
                            sql.append(request.getParameter("sSortDir_0")); 
                            break;
                }
            }
            
            SessionFactory sf = HibernateUtil.getSessionFactory();
            Session sesion = sf.getCurrentSession();
            sesion.getTransaction().begin();
            Query query = sesion.createSQLQuery(sql.toString()).addEntity(VIW046.class);
            query.setMaxResults(displaylength);
            query.setFirstResult(displaystart);
            list = query.list();
            sesion.getTransaction().commit();

            viewObject = new JsonObject();
            viewObject.setsEcho(Integer.parseInt(request.getParameter("sEcho")));
            viewObject.setiTotalDisplayRecords(list.size());
            viewObject.setiTotalRecords(displaylength);
            viewObject.setAaData(list);
            
            gson = new GsonBuilder().setPrettyPrinting().create();
            String json = gson.toJson(viewObject);
            out.print(json);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
