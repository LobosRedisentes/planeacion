/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.planeacion.pub;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.com.edcore.util.Configure;
import mx.com.edcore.util.Mailer;
import mx.com.edcore.vo.AccountVO;

@WebServlet(name = "sendMail", urlPatterns = {"/pub/sendMail"})
public class SendMail extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String band = "CORE0008";
        HttpSession websession = request.getSession();
        AccountVO waccount = (AccountVO) websession.getAttribute("account");
        String office_name = Configure.OFFICE_NAME;
        String office_url = Configure.OFFICE_URL;
        String path = Configure.PATH;
        String support_email = Configure.SUPPORT_EMAIL;
        String fromEmail = Configure.NOT_REPLAY_EMAIL;
        String fromPassword = Configure.NOT_REPLAY_PASSWORD;
        
        try {
            String mode = request.getParameter("mode").toUpperCase();
            String title = request.getParameter("title");
            String body = request.getParameter("body");
            String toEmail = request.getParameter("email");            
            if(mode.equals("USR")){
                fromEmail = waccount.getEmail();
                fromPassword = waccount.getPassword();
            }
            
            String msn = new Scanner(new File(request.getServletContext().getRealPath("") + "/mail.htm")).useDelimiter("\\Z").next();
            msn = msn.replaceAll("#image_header", path + "/img/email_header.png");
            msn = msn.replaceAll("#office_name", office_name);
            msn = msn.replaceAll("#office_url", office_url);
            msn = msn.replaceAll("#support_email", support_email);
            msn = msn.replaceAll("#title", title);
            msn = msn.replaceAll("#body", body);
            Mailer mail = new Mailer(fromEmail, fromPassword, toEmail, title, msn);
            band = mail.send();
            out.print(band);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
