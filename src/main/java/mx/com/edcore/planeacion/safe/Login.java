package mx.com.edcore.planeacion.safe;

import mx.com.edcore.dao.SafeDAO;
import mx.com.edcore.vo.SessionVO;
import mx.com.edcore.util.CoreException;
import mx.com.edcore.util.Date;
import mx.com.edcore.util.MD5;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.com.edcore.util.Configure;
import mx.com.edcore.util.IP;
import mx.com.edcore.view.VIW028;
import mx.com.edcore.view.VIW048;

@WebServlet(name = "login", urlPatterns = {"/safe/login"})
public class Login extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out;
        out = response.getWriter();
        try {
            List<VIW028> accounts;
            VIW028 account;
            String band = "CORE0001";
            String email = request.getParameter("email");
            String password = MD5.encriptar(request.getParameter("passwd"));
            String panel = request.getParameter("panel");

            accounts = SafeDAO.getAccounts("SELECT * FROM VIW028 WHERE email = '" + email + "'",1,0);
            if (accounts != null && accounts.size() > 0) {
                account = accounts.get(0);
                if (account.getPassword().equals(password)) {
                    if (account.getStatus().equals("ACTIVO")) {
                        if (account.getOffice().compareTo(Configure.OFFICE_ID) == 0) {
                            if(panel.equals("spanel") && !(account.getType().equals("ASPIRANTE") || account.getType().equals("ALUMNO"))){
                                   throw new CoreException("CORE0006");
                            }
                            if(panel.equals("dpanel") && !(account.getType().equals("DOCENTE"))){
                                   throw new CoreException("CORE0006");
                            }
                            if(panel.equals("apanel") && !(account.getType().equals("ADMINISTRATIVO"))){
                                   throw new CoreException("CORE0006");
                            }
                            SessionVO session = new SessionVO();
                            session.setAccount(account.getId());
                            session.setPlatform("WEB");
                            session.setPanel(panel.toUpperCase());
                            session.setApp("ESCOLAR");
                            session.setIp(getIpFromRequest(request));
                            session.setStatus("ACTIVO");
                            session.setApp("ESCOLAR");
                            session.setOpened(Date.getSqlDate());
                            SafeDAO.addSession(session);
                            List<VIW048> view = SafeDAO.getPrivileges("SELECT * FROM VIW048 WHERE rol = " + account.getRol(),1000,0);
                            List<String> privileges = new ArrayList<String>();
                            for (VIW048 privilege : view) {
                                privileges.add(privilege.getCode());
                            }
                            HttpSession websession = request.getSession();
                            websession.setAttribute("session", session);
                            websession.setAttribute("privileges", privileges);
                            websession.setAttribute("account", account);
                            band = "CORE0000";
                        } else {
                            throw new CoreException("CORE0004");
                        }
                    } else {
                        throw new CoreException("CORE0003");
                    }
                } else {
                    throw new CoreException("CORE0002");
                }
            } else {
                throw new CoreException("CORE0001");
            }
            out.print(band);
        } catch (CoreException ex) {
            out.print(ex.getMessage());
        }
    }

    public String getIpFromRequest(HttpServletRequest request) {
        String ip;
        boolean found = false;
        if ((ip = request.getHeader("x-forwarded-for")) != null) {
            StringTokenizer tokenizer = new StringTokenizer(ip, ",");
            while (tokenizer.hasMoreTokens()) {
                ip = tokenizer.nextToken().trim();
                if (IP.isIPv4Valid(ip) && !IP.isIPv4Private(ip)) {
                    found = true;
                    break;
                }
            }
        }

        if (!found) {
            ip = request.getRemoteAddr();
        }

        return ip;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
