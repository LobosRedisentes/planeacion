/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.planeacion.safe;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.com.edcore.dao.SafeDAO;
import mx.com.edcore.util.Date;
import mx.com.edcore.vo.SessionVO;

@WebServlet(name = "logout", urlPatterns = {"/safe/logout"})
public class Logout extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            HttpSession websession = request.getSession();
            SessionVO sesion = (SessionVO) websession.getAttribute("session");
            sesion.setClosed(Date.getSqlDate());
            sesion.setStatus("INACTIVO");
            String panel = sesion.getPanel().toUpperCase();
            SafeDAO.updateSession(sesion);
            websession.invalidate();
            String url = "www.edcore.com.mx";
            if (panel.equals("SPANEL")) {
                url = "/escolar/spanel/safe/login.jsp";
            }
            if (panel.equals("APANEL")) {
                url = "/escolar/apanel/safe/login.jsp";
            }
            if (panel.equals("DPANEL")) {
                url = "/escolar/dpanel/safe/login.jsp";
            }
            response.sendRedirect(url);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
