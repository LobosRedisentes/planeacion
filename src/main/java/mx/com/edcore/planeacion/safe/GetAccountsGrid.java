/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.planeacion.safe;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.com.edcore.dao.SafeDAO;
import mx.com.edcore.util.Configure;
import mx.com.edcore.util.JsonObject;
import mx.com.edcore.view.VIW028;

@WebServlet(name = "getAccountsGrid", urlPatterns = {"/safe/getAccountsGrid"})
public class GetAccountsGrid extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();

        int displaystart = 0;
        int displaylength = 0;
        StringBuilder sql;
        List<VIW028> list;
        JsonObject viewObject;
        Gson gson;

        try {
            if (request.getParameter("iDisplayStart") != null) {
                displaystart = Integer.parseInt(request.getParameter("iDisplayStart"));
            }
            if (request.getParameter("iDisplayLength") != null) {
                displaylength = Integer.parseInt(request.getParameter("iDisplayLength"));
            }
            sql = new StringBuilder("SELECT safe_IdAccount,org_IdOffice, office, safe_IdUser, code,  email,CONCAT_WS(' ',firstLast,secondLast,name ) as name, "
                    + "firstLast,secondLast,curp,safe_IdRol, rol, type, status FROM VIW028 WHERE org_IdOffice = " + Configure.OFFICE_ID);

            if (request.getParameter("sSearch") != null && !request.getParameter("sSearch").equals("")) {
                String search = request.getParameter("sSearch");
                sql.append(" AND ( LOWER(code) LIKE '%");
                sql.append(search.toLowerCase());
                sql.append("%' OR LOWER(CONCAT_WS(' ',firstLast,secondLast,name)) LIKE '%");
                sql.append(search.toLowerCase());
                sql.append("%' OR LOWER(rol) LIKE '%");
                sql.append(search.toLowerCase());
                sql.append("%' OR LOWER(type) LIKE '%");
                sql.append(search.toLowerCase());
                sql.append("%' OR LOWER(office) LIKE '%");
                sql.append(search.toLowerCase());
                sql.append("%' OR LOWER(status) LIKE '%");
                sql.append(search.toLowerCase());
                sql.append("%') ");
            }
            if (request.getParameter("sSearch_1") != null && !request.getParameter("sSearch_1").equals("")) {
                String search = request.getParameter("sSearch_1");
                sql.append(" AND ( LOWER(code) LIKE '%");
                sql.append(search.toLowerCase());
                sql.append("%') ");
            }
            if (request.getParameter("sSearch_2") != null && !request.getParameter("sSearch_2").equals("")) {
                String search = request.getParameter("sSearch_2");
                sql.append(" AND ( LOWER(CONCAT_WS(' ',firstLast,secondLast,name)) LIKE '%");
                sql.append(search.toLowerCase());
                sql.append("%') ");
            }
            if (request.getParameter("sSearch_3") != null && !request.getParameter("sSearch_3").equals("")) {
                String search = request.getParameter("sSearch_3");
                sql.append(" AND ( LOWER(rol) LIKE '%");
                sql.append(search.toLowerCase());
                sql.append("%') ");
            }
            if (request.getParameter("sSearch_4") != null && !request.getParameter("sSearch_4").equals("")) {
                String search = request.getParameter("sSearch_4");
                sql.append(" AND ( LOWER(type) LIKE '%");
                sql.append(search.toLowerCase());
                sql.append("%') ");
            }
            if (request.getParameter("sSearch_5") != null && !request.getParameter("sSearch_5").equals("")) {
                String search = request.getParameter("sSearch_5");
                sql.append(" AND ( LOWER(office) LIKE '%");
                sql.append(search.toLowerCase());
                sql.append("%') ");
            }
            if (request.getParameter("sSearch_6") != null && !request.getParameter("sSearch_6").equals("")) {
                String search = request.getParameter("sSearch_6");
                sql.append(" AND ( LOWER(status) LIKE '%");
                sql.append(search.toLowerCase());
                sql.append("%') ");
            }
            if (request.getParameter("iSortCol_0") != null) {
                int sortcol = Integer.parseInt(request.getParameter("iSortCol_0"));
                switch (sortcol) {
                    case 1:
                        sql.append("ORDER BY safe_IdAccount ");
                        sql.append(request.getParameter("sSortDir_0"));
                        break;
                    case 2:
                        sql.append("ORDER BY code ");
                        sql.append(request.getParameter("sSortDir_0"));
                        break;
                    case 3:
                        sql.append("ORDER BY CONCAT_WS(' ',firstLast,secondLast,name) ");
                        sql.append(request.getParameter("sSortDir_0"));
                        break;
                    case 4:
                        sql.append("ORDER BY rol ");
                        sql.append(request.getParameter("sSortDir_0"));
                        break;
                    case 5:
                        sql.append("ORDER BY type ");
                        sql.append(request.getParameter("sSortDir_0"));
                        break;
                    case 6:
                        sql.append("ORDER BY office ");
                        sql.append(request.getParameter("sSortDir_0"));
                        break;
                    case 7:
                        sql.append("ORDER BY status ");
                        sql.append(request.getParameter("sSortDir_0"));
                        break;
                }
            }

            list = SafeDAO.getAccounts(sql.toString(), displaylength, displaystart);
            viewObject = new JsonObject();
            viewObject.setsEcho(Integer.parseInt(request.getParameter("sEcho")));
            viewObject.setiTotalDisplayRecords(list.size());
            viewObject.setiTotalRecords(displaylength);
            viewObject.setAaData(list);

            gson = new GsonBuilder().setPrettyPrinting().create();
            String json = gson.toJson(viewObject);
            out.print(json);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
