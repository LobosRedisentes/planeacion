/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.planeacion.safe;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.com.edcore.dao.SafeDAO;
import mx.com.edcore.util.MD5;
import mx.com.edcore.view.VIW028;
import mx.com.edcore.view.VIW048;
import mx.com.edcore.vo.AccountVO;
import mx.com.edcore.vo.LogVO;
import mx.com.edcore.vo.SessionVO;

@WebServlet(name = "setAccount", urlPatterns = {"/safe/setAccount"})
public class SetAccount extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String band = "CORE0007";
        LogVO log = new LogVO();
        HttpSession websession = request.getSession();
        SessionVO sesion = (SessionVO) websession.getAttribute("session");
        AccountVO waccount = (AccountVO) websession.getAttribute("account");
        String opc = request.getParameter("opc").toLowerCase();
        String operation = "";
        if (opc.equals("add")) {
            operation = "SAFE12";
        } else if (opc.equals("update")) {
            operation = "SAFE13";
        }
        try {
            VIW048 privilege = SafeDAO.getPrivileges("SELECT * FROM VIW048 WHERE code = '" + operation + "' AND safe_IdRol = " + waccount.getRol(),1,1).get(1);
            if (privilege != null) {

                AccountVO account = new AccountVO();
                VIW028 obj;
                String mode = request.getParameter("mode").toUpperCase();
                String mail_title = "Transacción Fallida!";
                String mail_body = "La operación que se intento realizar no se pudo llevar acabo";
                String sendEmail = request.getParameter("sendEmail").toLowerCase();

                if (opc.equals("add")) {
                    account.setEmail(request.getParameter("email"));
                    account.setUser(Long.valueOf(request.getParameter("user")));
                    account.setCode(request.getParameter("code"));
                    account.setRol(Long.valueOf(request.getParameter("rol")));
                    account.setStatus(request.getParameter("status"));
                    if(!request.getParameter("passwd").equals("000")){
                        account.setPassword(MD5.encriptar(request.getParameter("passwd")));
                        account.setRecoverKey("passwd");
                    }
                    SafeDAO.addAccount(account);
                    band = "CORE0000";
                    mail_title = "Nueva cuenta de usuario";
                    mail_body = "Tu cuenta de usuario se creo de forma satisfactoria:"
                                + "<br/><br/><h4><center>USUARIO: " + account.getEmail() + "</center></h4>"
                                + "<h4><center>PASSWORD: " + account.getRecoverKey() + "</center></h4>";
                } else if (opc.equals("update")) {
                    if(mode.equals("ADM")){
                        obj = SafeDAO.getAccounts("SELECT * FROM VIW028 WHERE id = "+Long.valueOf(request.getParameter("id")),1,1).get(0);
                        account.setId(obj.getId());
                        account.setCode(request.getParameter("code"));
                        account.setUser(Long.valueOf(request.getParameter("user")));
                        account.setRol(Long.valueOf(request.getParameter("rol")));
                        account.setStatus(request.getParameter("status"));
                    }else if(mode.equals("USR")){
                        obj = SafeDAO.getAccounts("SELECT * FROM VIW028 WHERE id = "+waccount.getId(),1,1).get(0);
                        account.setId(obj.getId());
                        account.setCode(waccount.getCode());
                        account.setUser(waccount.getUser());
                        account.setRol(waccount.getRol());
                        account.setStatus(waccount.getStatus());
                    }
                    account.setEmail(request.getParameter("email"));
                    if(!request.getParameter("passwd").equals("000")){
                        account.setPassword(MD5.encriptar(request.getParameter("passwd")));
                        account.setRecoverKey(request.getParameter("passwd"));
                    }
                    SafeDAO.updateAccount(account);
                    band = "CORE0000";
                    mail_title = "Actualización completa";
                    mail_body = "Tu cuenta de usuario se actualizo de forma satisfactoria:"
                                + "<br/><br/><h4><center>USUARIO: " + account.getEmail() + "</center></h4>"
                                + "<h4><center>PASSWORD: " + account.getRecoverKey() + "</center></h4>";
                }
                if(band.equals("CORE0000")){
                    log.setSession(sesion.getId());
                    log.setOperation(privilege.getOperation());
                    log.setDescription("[" + account.getId() + "] SE COMPLETO LA TRANSACCIÓN SOBRE LA CUENTA ");
                    log.setType("EXITO");
                    SafeDAO.addLog(log);
                    if (sendEmail.equals("true")) {
                        RequestDispatcher rd = request.getRequestDispatcher("/pub/sendMail?mode=ADM&title="+mail_title+"&body=" + mail_body + "&mail=" + account.getEmail());
                        rd.forward(request, response);
                    }
                }
            } else {
                log.setSession(sesion.getId());
                log.setOperation(1L);
                log.setDescription("[" + waccount.getId() + "] SE INTENTO ACTUALIZAR UNA CUENTA SIN PERMISO");
                log.setType("FALLO");
                SafeDAO.addLog(log);
                band = "CORE0009";
            }
            out.print(band);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
