/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.planeacion.safe;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.com.edcore.dao.SafeDAO;
import mx.com.edcore.util.CoreException;
import mx.com.edcore.view.VIW028;

@WebServlet(name = "recoverPassword", urlPatterns = {"/safe/recoverPassword"})
public class RecoverPassword extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            List<VIW028> accounts;
            VIW028 account;
            String band = "CORE0000";
            String email = request.getParameter("email");
            accounts = SafeDAO.getAccounts("SELECT * FROM VIW028 WHERE email = '" + email + "'",1,0);
            if (accounts != null && accounts.size() > 0) {
                account =  accounts.get(0);
                if (account.getStatus().equals("ACTIVO")) {
                    String body = "Conserva tus datos de acceso ya que son importantes para salvaguardar tu información:"
                            + "<br/><br/><h4><center>USUARIO: " + account.getEmail() + "</center></h4>"
                            + "<h4><center>PASSWORD: " + account.getRecoverkey() + "</center></h4>";
                    RequestDispatcher rd = request.getRequestDispatcher("/pub/sendMail?mode=ADM&title=Datos de la cuenta!&body=" + body + "&mail=" + account.getEmail());
                    rd.forward(request, response);
                } else {
                    throw new CoreException("CORE0003");
                }
            } else {
                throw new CoreException("CORE0001");
            }
            out.print(band);
        } catch (CoreException ex) {
            out.print(ex.getMessage());
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
