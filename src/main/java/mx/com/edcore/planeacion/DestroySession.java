/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.planeacion;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import mx.com.edcore.dao.SafeDAO;
import mx.com.edcore.util.Date;
import mx.com.edcore.vo.SessionVO;

/**
 * Web application lifecycle listener.
 *
 * @author Administrador
 */
@WebListener()
public class DestroySession implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        HttpSession sesion = se.getSession();
        SessionVO session = (SessionVO) sesion.getAttribute("session");
        session.setStatus("INACTIVO");
        session.setClosed(Date.getSqlDate());
        SafeDAO.addSession(session);
        sesion.removeAttribute("session");
        sesion.removeAttribute("account");
        sesion.removeAttribute("privileges");
    }
}
