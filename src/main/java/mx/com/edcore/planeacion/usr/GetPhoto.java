package mx.com.edcore.planeacion.usr;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import mx.com.edcore.dao.UserDAO;
import mx.com.edcore.vo.UserPhotoVO;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.com.edcore.util.CoreException;
import mx.com.edcore.view.VIW028;

@WebServlet(name = "getPhoto", urlPatterns = {"/users/getPhoto"})
public class GetPhoto extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("image");
        ServletOutputStream out = response.getOutputStream();
        HttpSession websession = request.getSession();
        VIW028 account = (VIW028) websession.getAttribute("account");
        Long id = 0L;
        int lengt;
        byte[] buffer = new byte[1024];

        try {
            String mode = request.getParameter("mode").toUpperCase();
            if (mode.equals("USR")) {
                id = account.getUser();
            } else if (mode.contentEquals("ADM")) {
                id = Long.valueOf(request.getParameter("id"));
            }
            if (id != 0L) {
                UserPhotoVO user = UserDAO.getPhoto(id);
                if (user != null && user.getPhoto() != null) {
                    InputStream in = user.getPhoto().getBinaryStream();
                    user.getPhoto().free();
                    while ((lengt = in.read(buffer)) != -1) {
                        out.write(buffer, 0, lengt);
                    }
                    out.flush();
                } else {
                    throw new CoreException("EL USUARIO NO CUENTA CON FOTO");
                }
            } else {
                throw new CoreException("NO EXISTE MODO");
            }
        } catch (IllegalStateException e) {
        } catch (SQLException ex) {
        } catch (CoreException ex) {
            FileInputStream fis = new FileInputStream(new File(request.getServletContext().getRealPath("") + "/img/user.jpg"));
            while ((lengt = fis.read(buffer)) != -1) {
                out.write(buffer, 0, lengt);
            }
            out.flush();
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
