/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.planeacion.usr;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.com.edcore.dao.UserDAO;
import mx.com.edcore.util.CoreException;
import mx.com.edcore.util.Date;
import mx.com.edcore.util.MD5;
import mx.com.edcore.view.VIW058;
import mx.com.edcore.vo.AccountVO;
import mx.com.edcore.vo.AddressBookVO;
import mx.com.edcore.vo.UserVO;

/**
 *
 * @author Gabriel Cisneros Landeros
 */
public class NewApplicant extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            
            VIW058 obj;
            UserVO user;
            AddressBookVO addrBook;
            AccountVO account;
            
            Long safe_IdUser = new Long(request.getParameter("id"));
            String band = "CORE0007"; 
            if (safe_IdUser == 0) {
                obj = UserDAO.getUsers("FROM UserVO WHERE curp = '" + request.getParameter("curp") + "'",1,1).get(0);
                if (obj == null) {
                    addrBook = new AddressBookVO();
                    addrBook.setLocation(new Long(request.getParameter("local")));
                    addrBook.setStreet(request.getParameter("street").toUpperCase());
                    addrBook.setNumExternal(request.getParameter("numExternal"));
                    addrBook.setNumInternal(request.getParameter("numInternal"));
                    addrBook.setPhone(request.getParameter("phone"));
                    addrBook.setCellPhone(request.getParameter("cellPhone"));

                    String [] factor = request.getParameter("blobFactor").split("_");
                    String blod = factor[0];
                    if(factor[1].equals("P")){
                        blod +="+";
                    }else{
                        blod += "-"; 
                    }
                    user = new UserVO();
                    user.setName(request.getParameter("name").toUpperCase());
                    user.setFirstName(request.getParameter("firstLast").toUpperCase());
                    user.setSecondName(request.getParameter("secondLast").toUpperCase());
                    user.setBirthDate(Date.getSqlDate(request.getParameter("birthDate")));
                    user.setBirthTown(new Long(request.getParameter("birthTown")));
                    user.setBlobFactor(blod);
                    user.setSex(request.getParameter("sex"));
                    user.setRfc(request.getParameter("rfc").toUpperCase());
                    user.setCurp(request.getParameter("curp").toUpperCase());
                    user.setOccupation(request.getParameter("occupation"));
                    user.setStatus(request.getParameter("status"));
                    account = new AccountVO();
                    account.setCode("000");
                    account.setEmail(request.getParameter("email"));
                    account.setPassword(MD5.encriptar(request.getParameter("password")));
                    account.setRecoverKey(request.getParameter("password"));
                    account.setStatus("ACTIVO");
                    account.setRol(new Long(7));
                    if(UserDAO.addUser(user, addrBook, account) > 0){
                        band = "CORE0000";
                    }
                } else {
                    throw new CoreException("CORE0004");
                }
            }
            out.print(band);
        } catch (CoreException ex) {
            out.print(ex.getMessage());
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
