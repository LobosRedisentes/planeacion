/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.planeacion.quiz;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.com.edcore.dao.SurveyDAO;
import mx.com.edcore.vo.SurveyHealthVO;


public class SaveSurveyHealth extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String parametro = request.getParameter("values").toUpperCase();
            String [] codigo = parametro.split("#");
            SurveyHealthVO survey;
            Long id = new Long(request.getParameter("id"));
            if(id == 0){
                survey = new SurveyHealthVO();
                survey.setId(id);
                survey.setUser(new Long(codigo[0]));
                survey.setResp1(codigo[1]);
                survey.setResp2(codigo[2]);
                survey.setResp3(codigo[3]);
                survey.setResp4(codigo[4]);
                survey.setResp5(codigo[5]);
                survey.setResp6(codigo[6]);
                survey.setResp7(codigo[7]);
                survey.setResp8(codigo[8]);
                survey.setResp9(codigo[9]);
                survey.setResp10(codigo[10]);
                survey.setResp11(codigo[11]);
                survey.setResp12(codigo[12]);
                survey.setResp13(codigo[13]);
                survey.setResp14(codigo[14]);
                survey.setResp15(codigo[15]);
                survey.setResp16(codigo[16]);
                survey.setResp17(codigo[17]);
                survey.setResp18(codigo[18]);
                survey.setResp19(codigo[19]);
                survey.setResp20(codigo[20]);
                survey.setResp21(codigo[21]);
                survey.setResp22(codigo[22]);
                survey.setResp23(codigo[23]);
                survey.setResp24(codigo[24]);
                survey.setResp25(codigo[25]);
                survey.setResp26(codigo[26]);
                survey.setResp27(codigo[27]);
                SurveyDAO.addHealth(survey);
                id = survey.getId();
            }
            out.print(id);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
