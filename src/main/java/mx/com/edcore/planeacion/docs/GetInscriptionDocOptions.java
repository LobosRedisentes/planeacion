/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.planeacion.docs;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.com.edcore.dao.SurveyDAO;
import mx.com.edcore.util.DocumentList;
import mx.com.edcore.vo.AccountVO;
import mx.com.edcore.vo.SurveyHealthVO;

/**
 * Devuelve la lista de los municipios en formato GSON
 * @author Gabriel Cisneros Landeros
 * @version 1.0.0
 */
public class GetInscriptionDocOptions extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            HttpSession websession;
            websession = request.getSession();
            AccountVO account =  (AccountVO) websession.getAttribute("account");
            List<DocumentList> documents = new ArrayList<DocumentList>();
            String txt_gson;
            SurveyHealthVO survey = SurveyDAO.getHealths("SELECT * FROM SurveyHealthVO WHERE safe_IdUser = "+account.getUser(),1,1).get(1);
            if(survey == null){
                DocumentList document = new DocumentList();
                document.setId(1);
                document.setCode("DOC001");
                document.setName("Encuesta de Salud");
                documents.add(document);
            }
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            txt_gson = gson.toJson(documents);
            out.print(txt_gson);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
