package mx.com.edcore.planeacion;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import mx.com.edcore.dao.CalendarDAO;
import mx.com.edcore.dao.OrganizationDAO;
import mx.com.edcore.util.Configure;
import mx.com.edcore.view.VIW055;
import mx.com.edcore.view.VIW059;

@WebListener()
public class Init implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        ServletContext sc = sce.getServletContext();
        Configure.OFFICE_ID = Long.valueOf(sc.getInitParameter("OFFICE_ID"));
        VIW059 office =  OrganizationDAO.getOffices("SELECT * FROM VIW059 WHERE id = "+Configure.OFFICE_ID,1,0).get(0);
        Configure.OFFICE_NAME = office.getName();
        Configure.OFFICE_LOCAL = office.getTown();
        Configure.OFFICE_PHONE = office.getPhone();
        Configure.OFFICE_ADDRESS = office.getStreet()+" "+office.getNumExternal()+" "+office.getNumInternal();
        VIW055 calendar = CalendarDAO.getCalendars("SELECT * FROM VIW055 WHERE status = 'ACTIVO' AND office= "+Configure.OFFICE_ID,1,0).get(0);
        Configure.CALENDAR_ID = calendar.getId();
        Configure.CALENDAR_NAME = calendar.getName();
        Configure.OFFICE_URL = office.getUrl();
        Configure.PATH = sc.getInitParameter("PATH");
        Configure.SUPPORT_EMAIL = sc.getInitParameter("SUPPORT_EMAIL");
        Configure.SUPPORT_PASSWORD = sc.getInitParameter("SUPPORT_PASSWORD");
        Configure.NOT_REPLAY_EMAIL = sc.getInitParameter("NOT_REPLAY_EMAIL");
        Configure.NOT_REPLAY_PASSWORD = sc.getInitParameter("NOT_REPLAY_PASSWORD");
    }
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ;//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
